//
//  ShareViewController.swift
//  AppShareExtension
//
//  Created by Dreamlabs on 24/03/2021.
//  Copyright © 2021 Ashish Parmar. All rights reserved.
//

import UIKit
import Social
import CoreServices
import Alamofire
import AVKit

class ShareViewController: UIViewController {
    let avPlayerController = AVPlayerViewController()
    
    @IBOutlet weak var categoryCollection: UICollectionView!
    
    @IBOutlet weak var txtTitle: UITextField!
    
    @IBOutlet weak var videoImage: UIImageView!
    @IBOutlet weak var btnPost: UIButton!
    
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var clsSelectExpert: UICollectionView!
    
    @IBOutlet weak var txtDescription: UITextField!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var arrCategoryList = [InterestedCategoryListModel]()
    var arrExpertList = [ExpertListModel]()
    
     
     var expert_id = ""
     
     var category_id = ""
    var category_name = ""
     
     var postId = ""
     var videoUrl: String = ""
     
    var dictCatgeory : InterestedCategoryListModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.hidesWhenStopped = true
        btnPost.isUserInteractionEnabled = false
        btnPost.alpha = 0.5
        
        categoryCollection.dataSource = self
        categoryCollection.delegate = self

        categoryCollection.register(UINib.init(nibName: "categorySelectCLVCell", bundle: nil), forCellWithReuseIdentifier: "categorySelectCLVCell")
        
//        mentorsCollection.register(UINib.init(nibName: "selectExpertCollectionCell", bundle: nil), forCellWithReuseIdentifier: "selectExpertCollectionCell")
      
        debugPrint("Got here 0")
        webCall_InterestedCategory()
        
        self.clsSelectExpert.refreshControl = UIRefreshControl()
        self.clsSelectExpert.refreshControl?.tintColor = .AppSkyBlue
        
        clsSelectExpert.dataSource = self
        clsSelectExpert.delegate = self
        self.clsSelectExpert.refreshControl?.addTarget(self, action: #selector(refreshCalled), for: UIControl.Event.valueChanged)
        self.getExpertList()
        
        loadItem()
    }
    
    
    @IBAction func cancelClick(_ sender: Any) {
        let error = NSError(domain: "com.Proclapp.Proclapp", code: 0, userInfo: [NSLocalizedDescriptionKey: "Share dialog closed"])
        extensionContext?.cancelRequest(withError: error)
        
    }
    
    @IBAction func postContent(_ sender: Any) {
        btnPost.isEnabled = false
        activityIndicator.startAnimating()
        if (videoUrl.contains("http")){
            createOrEditArcticleService()
        }
        else {
            callUploadVideoRecorderWS(url: videoUrl)
        }
    }
    
    private func loadItem(){
        // Get the all encompasing object that holds whatever was shared. If not, dismiss view.
        guard let extensionItem = extensionContext?.inputItems.first as? NSExtensionItem,
            let itemProvider = extensionItem.attachments?.first else {
                self.extensionContext?.completeRequest(returningItems: nil, completionHandler: nil)
                return
        }
        
        
        // Check if object is of type text
        if itemProvider.hasItemConformingToTypeIdentifier(String(kUTTypeMovie)) {
            txtTitle.borderStyle = .none
            txtTitle.removeFromSuperview()
            didSelectPost(itemExtension: extensionItem)
        // Check if object is of type URL
        } else if itemProvider.hasItemConformingToTypeIdentifier(String(kUTTypeURL)) {
            handleIncomingURL(itemProvider: itemProvider)
        } else {
            print("Error: No url or text found")
            self.extensionContext?.completeRequest(returningItems: nil, completionHandler: nil)
        }
    }
    
    private func handleIncomingURL(itemProvider: NSItemProvider) {
        itemProvider.loadItem(forTypeIdentifier: String(kUTTypeURL), options: nil) { (item, error) in
            if let error = error {
                print("URL-Error: \(error.localizedDescription)")
            }

            if let url = item as? NSURL, let urlString = url.absoluteString {
                self.videoUrl = urlString
            }
        }
    }
    
    func didSelectPost(itemExtension: NSExtensionItem) {
        // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.
        let contentType = kUTTypeMovie as String

        if let contents = itemExtension.attachments {
            for attachment in contents {
                if attachment.hasItemConformingToTypeIdentifier(contentType){
                    attachment.loadItem(forTypeIdentifier: contentType, options: nil){ data, error in
                        if let error = error {
                            print("Text-Error: \(error.localizedDescription)")
                        }

                        if let url = data as? NSURL {
                            if let videoData = NSData(contentsOf: url.absoluteURL!) {
                                self.videoUrl = url.absoluteURL!.absoluteString
                            }
                        }

                    }
                }
            }
        }
        
    }
    
   
    func callUploadVideoRecorderWS(url : String){
        guard let getURL = URL(string: url) else {
            self.btnPost.isEnabled = true
            self.activityIndicator.stopAnimating()
            return
        }
        
        getThumbnailImageFromVideoUrl(url: getURL, completion: { (thum) in

            WebService.shared.webRequestForUplaodVideo(DictionaryVideo:  ["file": url],DictionaryImages: ["vimg":thum] , urlString: ServerURL.AddVVPost, Perameters: ["user_id":globalUserId,"vv_type":"2","post_title": self.category_name, "post_category":self.category_id,"expert":self.expert_id, "post_description": ToString(self.txtDescription.text)], completion: { (dicRes, success) in
                debugPrint(dicRes)
                
                if success == true
                {
////                    appDelegate.window?.rootViewController?.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
////
////                        NotificationCenter.default.post(name: .VideoUpload, object:nil)
////
////                    })
                }
                else{
//                    appDelegate.window?.rootViewController?.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
                }
                
                self.btnPost.isEnabled = true
                self.activityIndicator.stopAnimating()

                self.extensionContext?.completeRequest(returningItems: [], completionHandler: nil)
            }) { (err) in
                debugPrint(err)
                self.showOkAlert(msg: toString(err))
                self.btnPost.isEnabled = true
                self.activityIndicator.stopAnimating()
            }
        })

    }
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbImage) //9
                }
            } catch {
                self.showOkAlert(msg: toString(error.localizedDescription))
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
}

extension ShareViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       if collectionView.tag == 1{
            return arrCategoryList.count
        }
        else{
            return arrExpertList.count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView.tag == 1){
            let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "categorySelectCLVCell", for: indexPath) as! categorySelectCLVCell

            let obj = arrCategoryList[indexPath.row]
            cell.lblTitle.text = obj.category_name

            cell.btnImage.addTarget(self, action: #selector(tapSelectCategory(_:)), for: .touchUpInside)

            cell.btnImage.isSelected = obj.is_selected

            if let url = URL.init(string: obj.category_img){
                //            cell.btnImage.af_setImage(for: .normal, url: url)
                cell.btnImage.af_setImage(for: .normal, url: url, placeholderImage: UIImage.init(named: "place_logo"))
            }
            if let url = URL.init(string: obj.category_selected_img){
                //            cell.btnImage.af_setImage(for: .selected, url: url)
                cell.btnImage.af_setImage(for: .selected, url: url, placeholderImage: UIImage.init(named: "place_logo"))

            }

            return cell
        }
        else {
            let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "selectExpertCollectionCell", for: indexPath) as! selectExpertCollectionCell
            
            let obj = arrExpertList[indexPath.row]
            
            cell.lblName.text = obj.FullName
            cell.btnActive.isSelected = obj.is_active
            cell.btnSelect.isSelected = obj.is_selected
            cell.img.setImageWithURL(obj.profile_image, "experts_placeholder")
            
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 1{
            return CGSize.init(width: collectionView.frame.width/3.5, height: 106 * screenscale)
        }
        else{
            return CGSize.init(width: collectionView.frame.width/3.5, height: 114 * screenscale)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag != 1{
        _ = self.arrExpertList.map({ (obj) -> ExpertListModel in
                obj.is_selected = false
                return obj
            })
            arrExpertList[indexPath.row].is_selected = true

            expert_id = arrExpertList[indexPath.row].user_id

           if(category_id != ""){
            btnPost.alpha = 1.0
            btnPost.isUserInteractionEnabled = true
           }
            self.clsSelectExpert.reloadData()
        }
    }
    
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func tapSelectCategory(_ sender: UIButton) {

//        btnNext.alpha = 1.0
//        btnNext.isUserInteractionEnabled = true

        if let getIndexPath = self.categoryCollection.indexPathForView_Collection(sender)
        {
            
            _ = self.arrCategoryList.map({ (obj) -> InterestedCategoryListModel in
                obj.is_selected = false
                return obj
            })
            arrCategoryList[getIndexPath.row].is_selected = true
            category_id = arrCategoryList[getIndexPath.row].category_id
            category_name = arrCategoryList[getIndexPath.row].category_name
            if(expert_id != ""){
             btnPost.alpha = 1.0
             btnPost.isUserInteractionEnabled = true
            }
            self.categoryCollection.reloadData()
        }
    }
    
    
    @objc func refreshCalled() {
        self.getExpertList()
    }
    
    
    func webCall_InterestedCategory()
    {
        
        WebService.shared.RequesURL(ServerURL.Interested_Category, Perameters: ["user_id" : globalUserId],showProgress: true,completion: { (dicRes, success) in
            
            debugPrint("Got here 2")

            debugPrint("\(ServerURL.Interested_Category):-->",dicRes)

            if success == true
            {
                if let arrPrice = dicRes["interested_category"] as? [[String:Any]] {
                    self.arrCategoryList = arrPrice.map({InterestedCategoryListModel.init($0)})
                }
                _ = self.arrCategoryList.map({ (obj) -> InterestedCategoryListModel in
                    obj.is_selected = false
                    return obj
                })
                self.categoryCollection.reloadData()
            }

        }) { (err) in
            debugPrint("Got here 3")
            debugPrint("\(ServerURL.Interested_Category):-->",err)
        }
    }
    
    func webCallRequestToExpert(){
    //        let expert_id = self.arrExpertList.filter({$0.is_selected}).map({$0.user_id})
            if expert_id.isEmpty{return}
            
            
            WebService.shared.RequesURL(ServerURL.request_to_expert, Perameters: ["user_id" : globalUserId,"post_id":postId,"expert_id":expert_id],showProgress: true,completion: { (dicRes, success) in
                if success == true{
                    debugPrint(dicRes)
                    self.clickBack(self)
                }
                else{
                    self.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
                }
            }) { (err) in
            }
        }
    
    @objc func getExpertList(_ serviceCount : Int = 0) {
        WebService.shared.getUserData(Constant.userDeafult_LoginDic)
       
            //if self.clsSelectExpert.accessibilityHint == "service_calling" { return }
            
            self.clsSelectExpert.accessibilityHint = "service_calling"
            var delayTime = DispatchTime.now()
            
            if self.clsSelectExpert.refreshControl?.isRefreshing == false{
                delayTime = DispatchTime.now() + 0.5
                self.clsSelectExpert.refreshControl?.beginRefreshingManually()
            }
            
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                WebService.shared.RequesURL(ServerURL.ExpertList, Perameters: ["user_id" : globalUserId],showProgress: false,completion: { (dicRes, success) in
                    debugPrint(dicRes)
                    if success == true
                    {
                        if let arrData = dicRes["expert_list"] as? [[String:Any]] {
    //                        self.arrExpertList.removeAll()
                            self.arrExpertList = arrData.map({ExpertListModel.init($0)})
                        }
                    }
                    DispatchQueue.main.async {
                        UIView.performWithoutAnimation {
                            self.clsSelectExpert.reloadData()
                        }
                        self.clsSelectExpert.accessibilityHint = nil
                        self.clsSelectExpert.refreshControl?.endRefreshing()
                    }
                }) { (err) in
                    debugPrint(err)
                    if serviceCount < 2 {
                        self.clsSelectExpert.accessibilityHint = nil
                        self.getExpertList(serviceCount + 1)
                    } else {
                        debugPrint("\(ServerURL.Interested_Category):-->",err)
                        self.clsSelectExpert.accessibilityHint = nil
                        self.clsSelectExpert.refreshControl?.endRefreshing()
                    }
                }
            }
        }
    
    func createOrEditArcticleService()
    {
        var param = Dictionary<String, Any>()
        
        param = ["user_id":globalUserId, "post_title": ToString(txtTitle.text), "post_description": ToString(txtDescription.text), "expert":expert_id, "post_category": category_id, "post_link": self.videoUrl
        ]
        
        WebService.shared.webRequestForMultipleImages(DictionaryImages: nil, urlString: ServerURL.add_article_post, Perameters: param, completion: { (dicRes, success) in
            debugPrint(dicRes)
            
            if success == true
            {
               
//                self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
//                    NotificationCenter.default.post(name: .NewDataLoadHome, object:nil)
                    self.navigationController?.popViewController(animated: true)
//
//                })
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
                self.btnPost.isEnabled = true
                self.activityIndicator.stopAnimating()
            }
            
            self.extensionContext?.completeRequest(returningItems: [], completionHandler: nil)
            
        }) { (err) in
            self.showOkAlert(msg: toString(err))
            debugPrint(err)
            self.btnPost.isEnabled = true
            self.activityIndicator.stopAnimating()
        }
    }
}

class selectExpertCollectionCell: UICollectionViewCell {
    
    @IBOutlet var img: UIImageView!
    @IBOutlet var viewRound: UIView!
    @IBOutlet var btnActive: UIButton!
    @IBOutlet var btnSelect: UIButton!
    
    @IBOutlet var lblName: UILabel!
    
    override func awakeFromNib() {
        
        DispatchQueue.main.async {
            self.img.layer.cornerRadius = self.img.frame.height / 2
            self.img.layer.masksToBounds = true
            self.viewRound.layer.cornerRadius = self.viewRound.frame.height/2
        }
    }
    
    
}







class InterestedCategoryListModel {

    var category_id =  ""
    var category_img =  ""
    var category_img_thumb =  ""
    var category_name =  ""
    var category_selected_img =  ""
    var category_selected_img_thumb =  ""
    var date =  ""
    var is_selected =  false

    init(_ dict : [String:Any]) {
        self.category_id = toString(dict["category_id"])
        self.category_img = toString(dict["category_img"])
        self.category_img_thumb = toString(dict["category_img_thumb"])
        self.category_name = toString(dict["category_name"])
        self.category_selected_img = toString(dict["category_selected_img"])
        self.category_selected_img_thumb = toString(dict["category_selected_img_thumb"])
        self.date = toString(dict["date"])
        self.is_selected = toString(dict["is_selected"]) == "1"
    }
}

class ExpertListModel {
    
    var user_id =  ""
    var firstname =  ""
    var lastname =  ""
    var FullName =  ""
    var profile_image =  ""
    var profile_image_thumb =  ""
    var is_selected =  false
    var email =  ""
    var is_active =  false
    var profession = ""


    init(_ dict : [String:Any]) {
        
        self.user_id =  toString(dict["user_id"])
        self.firstname = toString(dict["firstname"])
        self.lastname = toString(dict["lastname"])
        self.profession = toString(dict["profession"])
        self.FullName =  (self.firstname + " " + self.lastname).capitalized
        self.profile_image = toString(dict["profile_image"])
        self.profile_image_thumb = toString(dict["profile_image_thumb"])
        self.is_active = ConvertToBool(toString((dict["is_active"])))
    }
}

class ProfileModel {
    
    var about_me =  ""
    var about_my_profession =  ""
    var address =  ""
    // var apply_reset_pwd =  ""
    var category_status =  ""
    var cover_image =  ""
    var date =  ""
    var email =  ""
    var firstname =  ""
    var lastname =  ""
    var fullName =  ""
    var followers =  ""
    var gender =  ""
    var media_id =  ""
    var media_type =  ""
    var member_since =  ""
    var phone =  ""
    var phone_code =  ""
    var profile_image =  ""
    var profile_image_thumb =  ""
    var profile_updated =  ""
    var token =  ""
    var total_following =  ""
    var total_friends =  ""
    var user_id =  ""
    var user_post =  ""
    var screen_code =  ""
    var profession =  ""


    var is_expert =  false
    var status =  false
    var notification_status =  false
    
    var arrInt = [objCategoryInterested]()
    var arrMyFriends = [objMYFriends]()
    var arrProfessionalQua = [objProfessionalQuali]()
    
    var arrEducationQuali = [objEducationQuali]()
    
    var aboutEducation = ""

    
    init(_ dict : [String:Any])
    {
        //For Interested Category
        if let arr = dict["interest_categories"] as? [[String:Any]] {
            self.arrInt = arr.map({objCategoryInterested.init($0)})
        }
        
        //For My Friend list
        if let arr = dict["my_friends"] as? [[String:Any]] {
            self.arrMyFriends = arr.map({objMYFriends.init($0)})
        }
        
        //For Professional
        if let arr = dict["professional_qualification"] as? [[String:Any]] {
            self.arrProfessionalQua = arr.map({objProfessionalQuali.init($0)})
        }
        
        //For Education
        if let arr = dict["educational_qualification"] as? [[String:Any]] {
            self.arrEducationQuali = arr.map({objEducationQuali.init($0)})
        }
        
        //For Education
        if let arr = dict["educational_qualification"] as? [[String:Any]] {
            if let dic = arr.first
            {
                self.aboutEducation = toString(dic["qualification"])
            }
        }
        
        self.user_id = toString(dict["user_id"])
        self.token = toString(dict["token"])
        self.screen_code = toString(dict["screen_code"])
        self.profession = toString(dict["profession"])
        self.email = toString(dict["email"])
        self.firstname = toString(dict["firstname"])
        self.lastname = toString(dict["lastname"])
       self.fullName = self.firstname + " " + self.lastname
        self.about_my_profession = toString(dict["about_my_profession"])
        self.about_me = toString(dict["about_me"])
        self.address = toString(dict["address"])
        self.date = toString(dict["date"])
        self.total_following = toString(dict["total_following"])
        self.followers = toString(dict["followers"])
        self.cover_image = toString(dict["cover_image"])
        self.profile_image = toString(dict["profile_image"])
        self.profile_image_thumb = toString(dict["profile_image_thumb"])
        self.category_status = toString(dict["category_status"])
        self.gender =  toString(dict["gender"])
        
        self.member_since = toString(dict["member_since"]).dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_MMMddyyyy)
        self.phone = toString(dict["phone"])
        self.phone_code = toString(dict["phone_code"])
        
        
        //BOOL
        self.is_expert = toString(dict["is_expert"]) == "1"
        self.status = toString(dict["status"]) == "1"
        self.notification_status = toString(dict["notification_status"]) == "1"
        
    }
    
    init(_ other : ProfileModel) {
        
        self.token = other.token
        self.arrInt = other.arrInt
        self.arrMyFriends = other.arrMyFriends
        self.arrProfessionalQua = other.arrProfessionalQua
        self.arrEducationQuali = other.arrEducationQuali
        self.user_id = other.user_id
        self.email = other.email
        self.firstname = other.firstname
        self.lastname = other.lastname
        self.about_my_profession = other.about_my_profession
        self.about_me = other.about_me
        self.address = other.address
        self.date = other.date
        self.total_following = other.total_following
        self.followers = other.followers
        self.cover_image = other.cover_image
        self.profile_image = other.profile_image
        self.profile_image_thumb = other.profile_image_thumb
        self.category_status = other.category_status
        self.member_since = other.member_since
        self.phone = other.phone
        self.phone_code = other.phone_code
        self.is_expert = other.is_expert
        self.status = other.status
        self.notification_status = other.notification_status
        self.gender =  other.gender
    }
}

//Selected category
class objCategoryInterested
{
    var category_id = ""
    var category_name = ""
    
    init(_ dict : [String:Any]) {
        self.category_id = toString(dict["category_id"])
        self.category_name = toString(dict["category_name"])
    }
}

//My Friends
class objMYFriends
{
    //    var cover_image = ""
    //    var email = ""
    //    var firstname = ""
    //    var is_expert = ""
    //    var lastname = ""
    var name = ""
    //    var notification_status = ""
    //    var profession = ""
    var profile_image = ""
    var profile_image_thumb = ""
    
    
    init(_ dict : [String:Any]) {
        self.name = toString(dict["name"])
        self.profile_image_thumb = toString(dict["profile_image_thumb"])
        self.profile_image = toString(dict["profile_image"])
    }
}

//
class objProfessionalQuali
{
    var date = ""
    var experience = ""
    var is_current_job = ""
    var job_title = ""
    var user_id = ""
    var user_profession_id = ""
    
    
    init(_ dict : [String:Any]) {
        self.date = toString(dict["date"])
        self.experience = toString(dict["experience"])
        self.is_current_job = toString(dict["is_current_job"])
        self.job_title = toString(dict["job_title"])
        self.user_id = toString(dict["user_id"])
        self.user_profession_id = toString(dict["user_profession_id"])
    }
    
    init(_ temp: String = "blank_init") {
        self.date = ""
        self.experience = ""
        self.is_current_job = ""
        self.job_title = ""
        self.user_id = ""
        self.user_profession_id = ""
    }
    
    var toDict_Professional : [String : Any] {
        return ["date":self.date, "experience" : self.experience, "is_current_job": self.is_current_job, "job_title": self.job_title, "user_id": self.user_id, "user_profession_id": self.user_profession_id]
    }
}

class objEducationQuali
{
    var qualification = ""
    var year = ""
    
    
    init(_ dict : [String:Any]) {
        self.qualification = toString(dict["qualification"])
        self.year = toString(dict["year"])
    }
    
    init(_ qua: String, year: String) {
        self.qualification = qua
        self.year = year
    }
    
    var toDict_Education : [String : Any] {
        return ["edu_qua":self.qualification,"year" : self.year]
    }
}



//
//  CreateArticleVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/20/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

//import UIKit
//import AlamofireImage
//import SafariServices
//import SwiftLinkPreview
//import CropViewController
//
//class CreateArticleVC: UIViewController {
//
//
////    private var image: UIImage?
////    private var croppingStyle = CropViewCroppingStyle.default
//
////    private var croppedRect = CGRect.zero
////    private var croppedAngle = 0
//
//
//    @IBOutlet weak var txtView: UITextView!
//    @IBOutlet weak var txtCategory:UITextField!
//    @IBOutlet weak var txtTitle:UITextField!
//    @IBOutlet weak var colview:UICollectionView!
//    @IBOutlet weak var colviewConst:NSLayoutConstraint!
//    @IBOutlet weak var scrollView:UIScrollView!
//
//    let imagePicker = UIImagePickerController()
//    var imageArray = NSMutableArray()
//
//    var pickerView = UIPickerView()
//    var categoryaSelectedId = String()
//
//    var deleteImageIds = NSMutableArray()
//    var arrCategoryList = [InterestedCategoryListModel]()
//    var editDict : allPostModel!
//
//    let defultTextDes = "Write your article here"
//
//    var textField: UITextField?
//
//    var strPostLink = ""
//
//
////    private var result = Response()
//    private let slp = SwiftLinkPreview(cache: InMemoryCache())
//
//    @IBOutlet private weak var previewTitle: UILabel?
//    @IBOutlet private weak var previewCanonicalUrl: UILabel?
//    @IBOutlet private weak var previewDescription: UILabel?
//    @IBOutlet private weak var detailedView: UIView?
//    @IBOutlet private weak var favicon: UIImageView?
//    @IBOutlet private weak var indiCator: UIActivityIndicatorView?
//
//
////    static let vcInstace = StoryBoard.AddPost.instantiateViewController(withIdentifier: "CreateArticleVC") as! CreateArticleVC
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        detailedView?.isHidden = true
//        indiCator?.stopAnimating()
//        detailedView?.applyDropShadow()
//
//
//
//
//        colviewConst.constant = 100
//        if editDict != nil
//        {
//            categoryaSelectedId = editDict.categories_id
//            txtTitle.text = editDict.post_title
//            txtView.text = editDict.post_description
//            txtCategory.text = editDict.category_name
//            imageArray = NSMutableArray.init(array: editDict.arrImages)
//            colview.reloadData()
//
//            if let previewUrl = editDict?.dicPostLink?.link, checkValidString(previewUrl)
//            {
//                self.showPreview(previewUrl)
//            }
//        }
//        else
//        {
//            categoryaSelectedId = ""
//            txtView.text = defultTextDes
//            txtView.textColor = UIColor.lightGray
//        }
//
//        txtView.layer.borderColor = UIColor.lightGray.cgColor
//        txtView.layer.borderWidth = 1
//
//        pickerView.dataSource = self
//        pickerView.delegate = self
//        txtCategory.inputView = pickerView
//        txtCategory.delegate = self
//
//        imagePicker.delegate = self
//
//        webCall_InterestedCategory()
//
//        self.view.layoutIfNeeded()
//
//        // Do any additional setup after loading the view.
//    }
//
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if scrollView.contentOffset.x != 0 {
//            scrollView.contentOffset.x = 0
//        }
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        self.navigationController?.isNavigationBarHidden = true
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        //        self.navigationController?.isNavigationBarHidden = false
//    }
//
//    @IBAction func clickBack(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
//    }
//
//    @IBAction func postBtnClicked(_ sender: Any) {
//        if validationAskQuestion == true
//        {
//            if editDict != nil
//            {
//                createOrEditArcticleService(url: ServerURL.edit_post)
//            }
//            else
//            {
//                createOrEditArcticleService(url: ServerURL.add_article_post)
//            }
//        }
//    }
//
//    @IBAction func photoBtnclicked(_ sender: Any) {
//        showActionSheet()
//    }
//
//    @IBAction func closeBtnClicked(_ sender: UIButton) {
//
//        let obj = imageArray[sender.tag] as Any
//
//        if obj is imagePostModel
//        {
//            deleteImageIds.add((obj as! imagePostModel).post_img_id)
//        }
//
//        imageArray.removeObject(at: sender.tag)
//        self.colview.reloadData()
//    }
//
//}
////MARK: Services
//extension CreateArticleVC
//{
//    func webCall_InterestedCategory()
//    {        WebService.shared.RequesURL(ServerURL.Interested_Category, Perameters: ["user_id" : globalUserId],showProgress: true,completion: { (dicRes, success) in
//
//        debugPrint("\(ServerURL.Interested_Category):-->",dicRes)
//
//        if success == true
//        {
//            if let arrPrice = dicRes["interested_category"] as? [[String:Any]] {
//                self.arrCategoryList = arrPrice.map({InterestedCategoryListModel.init($0)})
//            }
//            self.pickerView.reloadAllComponents()
//        }
//
//    }) { (err) in
//        debugPrint("\(ServerURL.Interested_Category):-->",err)
//        }
//    }
//
//    var validationAskQuestion: Bool{
//
//        self.view.endEditing(true)
//
//        var mess = ""
//
//        if categoryaSelectedId.isBlank {
//            mess = "Please select category"
//        }
//        else if ToString(txtTitle.text).isBlank{
//            mess = "Please enter title"
//        }
//        else if ToString(txtView.text).isBlank || ToString(txtView.text) == defultTextDes{
//            mess = "Please enter your article"
//        }
//        if mess != ""{
//            self.showOkAlert(msg: mess)
//            return false
//        }
//        return true
//    }
//
//    func createOrEditArcticleService(url : String)
//    {
//        var param = Dictionary<String, Any>()
//        if(editDict != nil)
//        {
//            param = ["user_id":globalUserId, "post_title": ToString(txtTitle.text), "post_description": ToString(txtView.text), "post_category": categoryaSelectedId, "post_id":editDict.post_id, "delete_img": deleteImageIds.count == 0 ? "" : deleteImageIds.componentsJoined(by: ","),"post_link":strPostLink]
//        }
//        else
//        {
//            param = ["user_id":globalUserId, "post_title": ToString(txtTitle.text), "post_description": ToString(txtView.text), "post_category": categoryaSelectedId,"post_link":strPostLink]
//        }
//
//        var imges = [UIImage]()
//        imageArray.forEach { (obj) in
//            if obj is UIImage
//            {
//                imges.append(obj as! UIImage)
//            }
//        }
//        WebService.shared.webRequestForMultipleImages(DictionaryImages: imageArray.count > 0 ? ["post_img": imges] : nil, urlString: url, Perameters: param, completion: { (dicRes, success) in
//            debugPrint(dicRes)
//
//            if success == true
//            {
//                self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
//                    NotificationCenter.default.post(name: .NewDataLoadHome, object:nil)
//                    self.navigationController?.popViewController(animated: true)
//
//                })
//            }
//            else{
//                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
//            }
//
//        }) { (err) in
//            debugPrint(err)
//        }
//    }
//}
//extension  CreateArticleVC : UITextViewDelegate,UITextFieldDelegate
//{
//    //MARK: - UITextview Delegate Methods
//    func textViewDidBeginEditing(_ textView: UITextView)
//    {
//        if textView.text == defultTextDes {
//            txtView.text = nil
//        }
//        txtView.textColor = UIColor.Appcolor51
//
//    }
//
//    func textViewDidEndEditing(_ textView: UITextView)
//    {
//        if textView.text.isEmpty {
//            txtView.textColor = UIColor.lightGray
//            txtView.text = defultTextDes
//        }
//    }
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if textField == txtCategory{
//            pickerView.selectRow(0, inComponent: 0, animated: false)
//            pickerView(pickerView, didSelectRow: 0, inComponent: 0)
//        }
//        return true
//    }
//
//
//}
////MARK: picker View methods
//
//extension CreateArticleVC: UIPickerViewDelegate, UIPickerViewDataSource
//{
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 1
//    }
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        return arrCategoryList.count
//    }
//
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return  arrCategoryList[row].category_name
//    }
//
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
//    {
//        if arrCategoryList.count > 0
//        {
//            txtCategory.text = arrCategoryList[row].category_name
//            categoryaSelectedId = arrCategoryList[row].category_id
//        }
//    }
//}
//
////MARK: Collection view methods
//extension CreateArticleVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
//{
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return imageArray.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! imageCell
//
//        let obj = imageArray[indexPath.row] as Any
//
//        if obj is imagePostModel
//        {
//            cell.imageView.setImageWithURL((obj as! imagePostModel).image_name, nil)
//        }
//        else if obj is UIImage
//        {
//            cell.imageView.image = (imageArray[indexPath.row] as! UIImage)
//        }
//
//        cell.closeBtn.tag = indexPath.row
//        let height = colview.collectionViewLayout.collectionViewContentSize.height
//        colviewConst.constant = height
//        self.view.layoutIfNeeded()
//
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: (collectionView.frame.width - 30)/3, height: (collectionView.frame.width - 30)/3)
//    }
//
//}
//
////MARK: Image picker
//
//extension CreateArticleVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate
//{
//    func camera()
//    {
//        imagePicker.sourceType = .camera
//        imagePicker.allowsEditing = false
//        self.present(imagePicker, animated: true, completion: nil)
//    }
//
//    func photoLibrary()
//    {
//        imagePicker.sourceType = .photoLibrary
//        imagePicker.allowsEditing = false
//        self.present(imagePicker, animated: true, completion: nil)
//    }
//
//    func showActionSheet() {
//        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//
//        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert:UIAlertAction!) -> Void in
//            self.camera()
//        }))
//
//        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (alert:UIAlertAction!) -> Void in
//            self.photoLibrary()
//        }))
//
//        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//
//        self.present(actionSheet, animated: true, completion: nil)
//
//    }
//
//    /*func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        imageArray.add(info[UIImagePickerController.InfoKey.editedImage] as! UIImage)
//        self.dismiss(animated: true, completion: nil)
//        self.colview.reloadData()
//    }*/
//
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        guard let image = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) else { return }
//
//        let cropController = CropViewController(croppingStyle: .default, image: image)
//        //cropController.modalPresentationStyle = .fullScreen
//        cropController.delegate = self
////        self.image = image
//        picker.dismiss(animated: true, completion: {
//            self.present(cropController, animated: true, completion: nil)
//            //self.navigationController!.pushViewController(cropController, animated: true)
//        })
//    }
//
//
//    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
//        //        self.croppedRect = cropRect
//        //        self.croppedAngle = angle
//        updateImageViewWithImage(image, fromCropViewController: cropViewController)
//    }
//
//    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
//        //        self.croppedRect = cropRect
//        //        self.croppedAngle = angle
//        updateImageViewWithImage(image, fromCropViewController: cropViewController)
//    }
//
//
//    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
//
//        cropViewController.dismiss(animated: true, completion:{
//            self.imageArray.add(image)
//            self.colview.reloadData()
//        })
//    }
//
//
//}
//
//class imageCell : UICollectionViewCell
//{
//    @IBOutlet var imageView : UIImageView!
//    @IBOutlet var closeBtn : UIButton!
//
//    override func awakeFromNib() {
//        imageView.layer.cornerRadius = 8
//    }
//}
//
//extension String {
//    func contains(find: String) -> Bool{
//        return self.range(of: find) != nil
//    }
//    func containsIgnoringCase(find: String) -> Bool{
//        return self.range(of: find, options: .caseInsensitive) != nil
//    }
//}
//
//
//extension CreateArticleVC
//{
//    @IBAction func clickLink(_ sender: UIButton) {
//
//        openAlertView()
//    }
//    func configurationTextField(textField: UITextField!) {
//        if (textField) != nil {
//            self.textField = textField!
//            //Save reference to the UITextField
//            self.textField?.placeholder = "Enter a link"
//            self.textField?.keyboardType = .URL
//        }
//    }
//
//    func openAlertView() {
//        let alert = UIAlertController(title: Constant.appName, message: nil, preferredStyle: .alert)
//        alert.addTextField(configurationHandler: configurationTextField)
//        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
//        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler:{ (UIAlertAction) in
//            self.showPreview(toString(self.textField?.text))
//        }))
//        self.present(alert, animated: true, completion: nil)
//    }
//
//    func showPreview(_ linkURL:String) {
//
//        if !checkValidString(linkURL)
//        {
//            return
//        }
//
//        self.detailedView?.isHidden = true
//        indiCator?.startAnimating()
//
//        func setDataPreview(_ result: Response) {
//            print("url: ", result.url ?? "no url")
//            print("finalUrl: ", result.finalUrl ?? "no finalUrl")
//            print("canonicalUrl: ", result.canonicalUrl ?? "no canonicalUrl")
//            print("title: ", result.title ?? "no title")
//            print("images: ", result.images ?? "no images")
//            print("image: ", result.image ?? "no image")
//            print("video: ", result.video ?? "no video")
//            print("icon: ", result.icon ?? "no icon")
//            print("description: ", result.description ?? "no description")
//
//            self.previewTitle?.text = toString(result.title)
//            self.previewCanonicalUrl?.text = toString(result.finalUrl)
//            self.previewDescription?.text = toString(result.description)
//
//            if let icon = result.image
//            {
//                self.favicon?.setImageWithURL(toString(icon), "logo_small")
//            }
//            else
//            {
//                self.favicon?.setImageWithURL(toString(result.icon), "logo_small")
//            }
//
//            self.detailedView?.isHidden = false
//            self.indiCator?.stopAnimating()
//            self.strPostLink =  self.previewCanonicalUrl?.text ?? ""
//        }
//
//        self.slp.preview(
//            linkURL,
//            onSuccess: { result in
//                setDataPreview(result)
//                //                self.result = result
//        },
//            onError: { error in
//                print(error)
//                self.showOkAlert(msg: "Invalid URL!")
//                self.indiCator?.stopAnimating()
//        }
//        )
//    }
//    @IBAction func openWithAction(_ sender: UIButton) {
//        if strPostLink.isEmpty == false{
//            openURL(URL.init(string: strPostLink)!)
//        }
//    }
//    func openURL(_ url: URL) {
//        if ["http", "https"].contains(url.scheme?.lowercased() ?? "") {
//            self.present(SFSafariViewController(url: url), animated: true, completion: nil)
//        } else {
//            linkurlOpen_MS(linkurl: url.absoluteString)
//        }
//    }
//    @IBAction func closePriview(_ sender: UIButton) {
//       strPostLink = ""
//        detailedView?.isHidden = true
//    }
//
//}
//
