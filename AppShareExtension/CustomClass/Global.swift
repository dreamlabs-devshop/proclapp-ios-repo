//
//  Global.swift
//  Proclapp
//
//  Created by Mac-4 on 05/06/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import Foundation
import UIKit
//import SwiftAudio
import SafariServices

var globalChatUserId = ""

extension UIView {
    
    func setborder(_ width: CGFloat, _color: UIColor) {
        self.layer.borderWidth = width
        self.layer.borderColor = _color.cgColor
    }
    
}



func ToString(_ StingText : Any?) -> String
{
    if let strText = StingText
    {
        if let TempString = strText as? String
        {
            return TempString
        }
        else if let TempFloat = StingText as? Float
        {
            return String(TempFloat)
        }
        else if let TempDouble = StingText as? Double
        {
            return String(TempDouble)
        }
        else if let TempNumber = strText as? NSNumber
        {
            return TempNumber.stringValue
        }
        else if let TempNumber = strText as? Int
        {
            return String(TempNumber)
        }
        else if let TempNumber = strText as? NSURL, let urlToString = TempNumber.absoluteString
        {
            return urlToString
        } else if let cvag = strText as? CVarArg {
            return String(format: "%@", cvag)
        }
        
        return "\(strText)"
    }
    else
    {
        return ""
    }
}
func ToInt(_ StrText : Any?) -> Int
{
    if let TempNumber = StrText as? NSNumber
    {
        return TempNumber.intValue
    }
    else if let TempNumber = StrText as? Int
    {
        return TempNumber
    }
    else if let myInteger = Int(ToString(StrText))
    {
        return myInteger
    }
    return 0
}





//Global
func setUserDefault(_ params:AnyObject, Key: String)
{
    let data = NSKeyedArchiver.archivedData(withRootObject: params)
    UserDefaults.setUserDefault(value: data, key: Key)
}

func getUserDefault(Key:String) -> AnyObject
{
    if let data = UserDefaults.getUserDefault(key: Key) as? Data {
        if let storedData = NSKeyedUnarchiver.unarchiveObject(with: data){
            return storedData as AnyObject
        }
    }
    return UserDefaults.getUserDefault(key: Key) as AnyObject
    //    return NSNull()
}


