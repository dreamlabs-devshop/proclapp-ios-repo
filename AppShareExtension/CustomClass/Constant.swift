//
//  Constant.swift
//  Proclapp
//
//  Created by Ashish Parmar on 29/07/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

let dFMS = DateFormatter()


//MARK: *************** Constant ***************
struct Constant {
    static let appName                = "proclapp"
    
    static let responsemsg                = "response_msg"

    static let kAlertFnameMsg = "Please enter your first name."
    static let kAlertMobileMsg = "Please enter your mobile number."
    static let kAlertLnameMsg = "Please enter your last name."
    static let kAlertEmailMsg = "Please enter your email address."
    static let kAlertDeclinePost = "Would you like to decline post?"
    static let kAlertLogout = "Are you sure you want to log out?"
    static let kAlertDeletePost = "Would you like to delete post?"
    static let kAlertdiscuss = "Would you like to delete discuss?"
    static let kAlertanswer = "Would you like to delete answer?"
    static let kAlertComment = "Would you like to delete comment?"
    static let kAlertDiscUpdate = "Discussion updated."
    static let kAlertDiscAdded = "Discussion added."
    static let kAlertCaryover = "Already carryover"

    static let kAlertGuest = "Please Login Or Signup"

    
    static let kAlertDeclineDelete = "Are you sure you want to delete decline question & article?"
    static let kAlertDeclineDeleteALL = "Are you sure you want to delete all decline question & article?"
    
    static let kAlertDeclineUndo = "Are you sure you want to undo decline question & article?"
    static let kAlertDeclineUndoALL = "Are you sure you want to undo all decline question & article?"
    
    static let kAlertSubscibe = "You are not a subscribed user. Please, subscribe to access the research paper now."
    
    static let kAlertExpertAnswerSee = "You are not a subscribed user. Please, subscribe to access the see expert answer."

    
    static let kAlertPaymentFaild = "Payment Fail!!"
    static let kAlertPaymentSuccess = "Payment Success!!"


    //UserDeafult
    static let userDeafult_ProfileDic                = "user_data"
    static let userDeafult_LoginDic                = "LoginDic"
    static let dataSharingGroup                = "group.Proclapp.swift.share.extension"
    static let userDeafult_isFirstTimeInstall                = "FirstTimeInstall"

    //Date
    static let date_ServerFormate                = "yyyy-MM-dd HH:mm:ss"
    static let date_ddMMM              = "dd MMM"
    static let date_MMMddyyyy             = "MMM dd, yyyy"
    static let date_HHMMMddyy             = "h:mm a, MMM dd yy"
    static let date_HHMMMdd           = "h:mm a, MMM dd"

    static let karrFilter = ["Updates","User","Views"]
    
    //webview Payment
    
    static let kPayment_Fail                = "payment_failed"
    static let kPayment_Success               = "payment_success"

}
