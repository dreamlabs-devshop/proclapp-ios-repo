//
//  StringExtension.swift
//  YeahYag
//
//  Created by Pris Mac on 3/7/19.
//  Copyright © 2019 Ajay. All rights reserved.
//

import Foundation
import UIKit

func toString(_ StingText : Any?) -> String
{
    if let strText = StingText
    {
        if let TempString = strText as? String
        {
            return TempString
        }
        else if let TempNumber = strText as? NSNumber
        {
            return TempNumber.stringValue
        }
        else if let TempNumber = strText as? Int
        {
            return String(TempNumber)
        }
        else if let TempFloat = StingText as? Float
        {
            return String(TempFloat)
        }
        else if let TempDouble = StingText as? Double
        {
            return String(TempDouble)
        }
        
        return "\(strText)"
    }
    else
    {
        return ""
    }
}
func ConvertToBool(_ StrText : Any?) -> Bool
{
    switch ToString(StrText)
    {
    case "True", "true", "yes", "1":
        return true
    case "False", "false", "no", "0":
        return false
    default:
        return false
    }
}
func checkValidString(_ CheckString : String?) -> Bool
{
    if let strTemp = CheckString
    {
        return  !(strTemp.isEmpty || strTemp == "" || strTemp == "(null)" || strTemp == "<null>" || strTemp == " " ||  strTemp.trimmingCharacters(in: .whitespacesAndNewlines).count == 0)
    }
    else
    {
        return false
    }
}

/*func dateConvertUTC(date:String,serverFormate:String,appFormate:String) -> String {
    //    "yyyy-MM-dd HH:mm:ss", GetFormate: "hh:mm a,dd MMMM yyyy"
    dFMS.timeZone = TimeZone.init(identifier: "UTC")
    dFMS.dateFormat = serverFormate // Formate Get from Response
    if let yourDate: Date = dFMS.date(from: date)
    {
        dFMS.dateFormat = appFormate // WhitchFormate u  have convert
        dFMS.timeZone = NSTimeZone.system
        return dFMS.string(from: yourDate)
    }
    return ""
}*/

extension String {
    var nsString : NSString { return self as NSString }
    var integerValue : Int { return nsString.integerValue }
    var intValue : Int32 { return nsString.intValue }
    var doubleValue : Double { return nsString.doubleValue }
    var floatValue : Float { return nsString.floatValue }
    var cgFloat : CGFloat { return CGFloat(floatValue) }
    var longLongValue : Int64 { return nsString.longLongValue }
    var boolValue : Bool { return nsString.boolValue }
    var localize : String { return NSLocalizedString(self, comment: "") }
    
    var isValid : Bool {
        return  !(self.isEmpty || self == "" || self == "(null)" ||  self == "<null>" || self == " " ||  self.trimmingCharacters(in: .whitespacesAndNewlines).count == 0)
    }
   
    
//    var isValidEmail : Bool {
//        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
//        return  NSPredicate(format:"SELF MATCHES %@", emailRegEx).evaluate(with: self)
//    }
    
//    func getSubString(_ i: Int) -> String {
//        return String(self[i] as Character)
//    }
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, count) ..< count]
    }
    
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(count, r.lowerBound)),
                                            upper: min(count, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
    

    var isAlphaBets: Bool {
        return !isEmpty && range(of: "[^a-zA-Z]", options: .regularExpression) == nil
    }
    func toBase64() -> String? {
        guard let data = self.data(using: String.Encoding.utf8) else {
            return nil
        }
        
        return data.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        return self.size(withAttributes: [NSAttributedString.Key.font: font]).width
    }
    
    func heightOfString(usingFont font: UIFont) -> CGFloat {
        return self.size(withAttributes: [NSAttributedString.Key.font: font]).height
    }
    
    /*func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }*/
    
    var UTF8CString : UnsafeMutablePointer<Int8>? {
        return UnsafeMutablePointer<Int8>(mutating: (self as NSString).utf8String)
        //        return UnsafeMutablePointer((self as NSString).UTF8String)
    }
    
    var containsEmoji: Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x1F600...0x1F64F, // Emoticons
            0x1F300...0x1F5FF, // Misc Symbols and Pictographs
            0x1F680...0x1F6FF, // Transport and Map
            0x2600...0x26FF,   // Misc symbols
            0x2700...0x27BF,   // Dingbats
            0xFE00...0xFE0F,   // Variation Selectors
            0x1F900...0x1F9FF, // Supplemental Symbols and Pictographs
            0x1F1E6...0x1F1FF: // Flags
                return true
            default:
                continue
            }
        }
        return false
    }
    
    var digits: String {
        return components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
    }

    func dateConvertUTC(serverFormate:String,appFormate:String) -> String {
        //    "yyyy-MM-dd HH:mm:ss", GetFormate: "hh:mm a,dd MMMM yyyy"
        dFMS.timeZone = TimeZone.init(identifier: "UTC")
        dFMS.dateFormat = serverFormate // Formate Get from Response
        if let yourDate: Date = dFMS.date(from:self)
        {
            dFMS.dateFormat = appFormate // WhitchFormate u  have convert
            dFMS.timeZone = NSTimeZone.system
            return dFMS.string(from: yourDate)
        }
        return ""
    }
    
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    
}
enum CardType: String {
    case Unknown, Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay
    
    static let allCards = [Amex, Visa, MasterCard, Diners, Discover, JCB, Elo, Hipercard, UnionPay]
    
    var regex : String {
        switch self {
        case .Amex:
            return "^3[47][0-9]{5,}$"
        case .Visa:
            return "^4[0-9]{6,}([0-9]{3})?$"
        case .MasterCard:
            return "^(5[1-5][0-9]{4}|677189)[0-9]{5,}$"
        case .Diners:
            return "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$"
        case .Discover:
            return "^6(?:011|5[0-9]{2})[0-9]{3,}$"
        case .JCB:
            return "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"
        case .UnionPay:
            return "^(62|88)[0-9]{5,}$"
        case .Hipercard:
            return "^(606282|3841)[0-9]{5,}$"
        case .Elo:
            return "^((((636368)|(438935)|(504175)|(451416)|(636297))[0-9]{0,10})|((5067)|(4576)|(4011))[0-9]{0,12})$"
        default:
            return ""
        }
    }
}


extension Double
{
    var toString : String {
        return NSNumber(value: self).stringValue
    }
}
extension CGFloat
{
    var toString : String {
        return String(format: "%.f", self)
    }
}
extension Bool {
    var toString : String {
        return self == true ? "1" : "0"
    }
}

extension BidirectionalCollection {
    subscript(safe offset: Int) -> Element? {
        guard !isEmpty, let i = index(startIndex, offsetBy: offset, limitedBy: index(before: endIndex)) else { return nil }
        return self[i]
    }
}
func getCurrentDate(formate: String) -> String {
    
    let dateFormatter = DateFormatter()
    
    dateFormatter.dateFormat = formate
    
    return dateFormatter.string(from: Date())
    
}



extension String {
    func htmlAttributed(family: String?, size: CGFloat, color: UIColor) -> NSAttributedString? {
        do {
            let htmlCSSString = "<style>" +
                "html *" +
                "{" +
                "font-size: \(size)pt !important;" +
                "color: #\(color.hexString!) !important;" +
                "font-family: \(family ?? "Helvetica"), Helvetica !important;" +
            "}</style> \(self)"

            guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
                return nil
            }

            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
}
extension UIColor {
    var hexString:String? {
        if let components = self.cgColor.components {
            let r = components[0]
            let g = components[1]
            let b = components[2]
            return  String(format: "%02X%02X%02X", (Int)(r * 255), (Int)(g * 255), (Int)(b * 255))
        }
        return nil
    }
}
