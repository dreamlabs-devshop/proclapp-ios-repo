//
//  StoryBoardExtension.swift
//  YeahYag
//
//  Created by Pris Mac on 3/2/19.
//  Copyright © 2019 Ajay. All rights reserved.
//

import Foundation
import UIKit

struct StoryBoard
{
    static let Main = UIStoryboard(name: "Main", bundle: nil)
    static let Home = UIStoryboard(name: "Home", bundle: nil)
    static let Flash = UIStoryboard(name: "Flash", bundle: nil)
    static let Profile = UIStoryboard(name: "Profile", bundle: nil)
    static let Voice = UIStoryboard(name: "Voice", bundle: nil)
    static let Notification = UIStoryboard(name: "Notification", bundle: nil)
    static let Message = UIStoryboard(name: "Message", bundle: nil)
    static let Other = UIStoryboard(name: "Other", bundle: nil)
    static let AddPost = UIStoryboard(name: "AddPost", bundle: nil)
    static let More = UIStoryboard(name: "More", bundle: nil)
    static let Advert = UIStoryboard(name: "Advert", bundle: nil)
    static let Wallet = UIStoryboard(name: "Wallet", bundle: nil)

}

struct BottomName
{
    static let Edit = "Edit"
    static let Share = "Share"
    static let Delete = "Delete"
    static let RequestExpert = "Request Expert"
    static let SuggestEdit = "Suggest Edit"
    static let CompareAnswer = "Compare Answers"
    static let BroadcastAll = "Broadcast to All"
    static let Mute = "Mute Conversation"
    static let ReportAbuse = "Report Abuse"
    static let UNMute = "Unmute Conversation"

}


