//
//  UrlHelper.swift
//  YeahYag
//
//  Created by Pris Mac on 5/15/19.
//  Copyright © 2019 Ajay. All rights reserved.
//

import Foundation


struct ServerURL {
    
    static let socketURL = "https://www.proclapp.com:1333?"
    static let DomainURL = "https://www.proclapp.com/ws/v1/api/"
    
    static let api_version = "v1"
    
    static let SignUp = DomainURL + "signup"
    static let SignIn = DomainURL + "signin"
    static let LogOut = DomainURL + "logout"
    static let ResendVerificationMail = DomainURL + "resend_verification_mail"
    static let ForgotPswd = DomainURL + "forgot_password"
    static let ChangePswd = DomainURL + "change_password"
    static let UpdateDeviceToken = DomainURL + "update_device_token"
    static let AdvertTimeSlot = DomainURL + "advert_time_slot"
    
    static let get_msg_not_fri_counts = DomainURL + "get_msg_not_fri_counts"

    
    //Social Sign in
    static let signin_with_social = DomainURL + "signin_with_social"
    static let check_social_id = DomainURL + "check_social_id"
    
    //PROFILE

    static let GetOtherProfile = DomainURL + "profile"
    static let GetProfile = DomainURL + "get_profile"
    static let UpdateProfile = DomainURL + "update_profile"
    static let AddFriendUnFriend = DomainURL + "friend_unfriend"
    static let FollowUnFollow = DomainURL + "follow_status"
    static let GetNotificationList = DomainURL + "get_notification_list"

    static let Follower_list = DomainURL + "follower_list"
    static let Following_list = DomainURL + "following_list"

    static let check_email_exists = DomainURL + "check_email_exists"



    
    static let Interested_Category = DomainURL + "interested_category_list"
    static let Interested_Category_Save = DomainURL + "update_category_to_user_profile"

    
    static let aboutUS = DomainURL + "about_us"
    static let privacyPolicy = DomainURL + "privacy_policy"
    static let termsCondition = DomainURL + "term_and_conditions"
    
    
    //Menu
    static let Trending = DomainURL + "trending_topics"

    
    
    //HOME
    static let ExpertList = DomainURL + "expert_list"
    static let GetallPost = DomainURL + "get_posts"
    static let GetallPost_typeWise = DomainURL + "rp_list"//Type of post (1 = Article, 2 = Question, 3 = Voice or video content, 4 = Research Papers).
    static let VVList = DomainURL + "vv_list"
    static let vv_general_list = DomainURL + "vv_general_list"
    static let AdvertList = DomainURL + "advert_list"
    static let AdvertSummary = DomainURL + "advert_summary"
    static let GetSearchPost = DomainURL + "get_posts"



    static let BookmarkUnbookmark = DomainURL + "bookmark_unbookmark"
    static let LikeUnlikePost = DomainURL + "like_unlike"
    static let DeclineQestion = DomainURL + "decline_question"
    static let GetDeclinePostList = DomainURL + "declined_post_list"
    static let MuteUnmuteConversation = DomainURL + "mute_conversation"

    static let UndoDeclinePost = DomainURL + "undo_decline_post"
    static let DeleteDeclinePost = DomainURL + "delete_decline_post"
    static let DeletePost = DomainURL + "delete_post"
    static let BroadCastPost = DomainURL + "broadcast_post"
    static let CaryOverPost = DomainURL + "carryover_post"



    static let PostRead = DomainURL + "post_read"
    static let AnswerList = DomainURL + "answer_list"
    static let GetAnswerDetails = DomainURL + "get_answer_details"
    static let AddPostAnswerComment = DomainURL + "add_post_answer_comment"
    static let EditPostAnswerComment = DomainURL + "edit_post_answer_comment"
    static let LikeUnlikeAnswer = DomainURL + "like_unlike_post_answer"


    static let BookmarkUnbookmarkPostAnswer = DomainURL + "bookmark_unbookmark_post_answer"
    static let PostUpvoteDownvote = DomainURL + "post_upvote_downvote"
    
    //NOTIFICATION
    static let AcceptRejectFriend = DomainURL + "accept_reject_friend"
    static let GetNotificationStatus = DomainURL + "get_notification_status"
    static let UpdateNotificationStatus = DomainURL + "update_notification_status"
    static let update_post_from_suggestion = DomainURL + "update_post_from_suggestion"

    //FRIENDS LIST
    static let FriendSuggestionList = DomainURL + "friend_suggestion_list"
    static let GetFriends = DomainURL + "get_friends"
    
    //Add post
    static let add_QA_post = DomainURL + "add_QA_post"
    static let add_article_post = DomainURL + "add_article_post"
    static let add_RP_post = DomainURL + "add_RP_post"
    
    static let get_post_by_id = DomainURL + "get_post_by_id"

    
    
    //Edit post
    static let edit_post = DomainURL + "edit_post"
    
    //MORE
    static let BookmarkedPostList = DomainURL + "bookmarked_post_list"
    
    
    //MENU QUESTIONS ANSWERES
    static let GetAnsweredQaPosts = DomainURL + "get_answered_qa_posts"
    static let GetUnansweredQaPosts = DomainURL + "get_unanswered_qa_posts"
    
    
    //MENU Discuss
    static let Getdiscussed_articles = DomainURL + "discussed_articles"
    static let Getnondiscussed_articles = DomainURL + "nondiscussed_articles"
    
    //Upload image for post content
    static let post_content_image = DomainURL + "post_content_image"
    //Add Answer
    static let add_answer = DomainURL + "add_answer"
    static let edit_answer = DomainURL + "edit_answer"
    static let delete_answer = DomainURL + "delete_answer"
    static let suggest_edit_post = DomainURL + "suggest_edit_post"

    
    static let delete_post_answer_comment = DomainURL + "delete_post_answer_comment"


    
    //asha03/09
    // Video/Audio
    static let AddVVPost = DomainURL + "add_VV_post"
    //asha03/09
    
    static let post_report_abuse = DomainURL + "post_report_abuse"
    static let request_to_expert = DomainURL + "request_to_expert"
    static let FlashUserList = DomainURL + "user_list"
    static let FlashPost = DomainURL + "flash_post"
    static let RecommendPost = DomainURL + "recommended_post"
    static let GetUserPosts = DomainURL + "get_user_posts"

    
    
    //Advert
    static let AddAdvert = DomainURL + "add_advert"
    static let UpdateAdvert = DomainURL + "update_advert"


//chat
    
    static let GetConversationList = DomainURL + "get_conversation_list"
    static let GetMsgList = DomainURL + "get_conversation_msg"
    
    //wallet
    
    
    static let getExpertwallet = DomainURL + "wallet"
    static let getRedeemwallet = DomainURL + "redeem_wallet_amount"

    
    //
    static let ExpertReply = DomainURL + "expert_reply"
    static let vv_answer_list = DomainURL + "vv_answer_list"

    

}
