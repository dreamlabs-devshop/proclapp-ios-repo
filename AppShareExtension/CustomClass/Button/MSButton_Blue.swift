//
//  MSButton_Blue.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/1/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//



import Foundation
import UIKit

@IBDesignable
class MSButton_Blue:UIButton {
    
    override func awakeFromNib() {
        
        titleLabel?.textAlignment = .center
        backgroundColor = UIColor.AppSkyBlue
        DispatchQueue.main.async {
            self.layer.cornerRadius = self.frame.height/2
        }
    }
    
    @IBInspectable
    var borderWidth:CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    var borderColor:UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    //    @IBInspectable
    //    var cornerRadius: CGFloat = 0.0 {
    //        didSet {
    //            layer.cornerRadius = cornerRadius * screenscale
    //            layer.masksToBounds = cornerRadius * screenscale > 0
    //        }
    //    }
    
    @IBInspectable
    var fontSize: CGFloat = 0.0 {
        didSet {
            self.titleLabel?.font = UIFont(name: "Lato-Regular", size: (fontSize * screenscale))!
        }
    }
    
}
