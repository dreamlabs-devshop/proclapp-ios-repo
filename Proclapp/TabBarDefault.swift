//
//  TabBarDefault.swift
//  Proclapp
//
//  Created by Prismetric Tech on 8/29/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import GoogleMobileAds

class TabBarDefault: UITabBarController,GADBannerViewDelegate {
    
    @IBOutlet weak var viewShowBanner: UIView!
    
    var bannerView: GADBannerView!
    
    fileprivate lazy var defaultTabBarHeight = { tabBar.frame.size.height }()
    
    static let shared = TabBarDefault()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard self.viewShowBanner != nil else {
            return
        }
        self.viewShowBanner.isHidden = true
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.viewShowBanner.frame = CGRect(x: 0, y: (self.view.bounds.height - (49 + self.defaultTabBarHeight)) , width: self.view.bounds.size.width, height: 49)
            self.viewShowBanner.backgroundColor = UIColor.red
            
            self.bannerView = GADBannerView(adSize: kGADAdSizeBanner)
            self.bannerView.frame = CGRect(x: 0, y: 0 , width: self.view.bounds.size.width, height: 49)
            self.viewShowBanner.addSubview(self.bannerView)
            self.bannerView.delegate = self
//            GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers =
//            ["5ba0adda95c961b86347845b0f550dc6"]
            self.bannerView.adUnitID = "ca-app-pub-2131800490612394/4862210561"
            self.bannerView.rootViewController = self
            self.bannerView.load(GADRequest())
            self.bannerView.backgroundColor = UIColor.white
            
            self.viewShowBanner.isHidden = false
            self.view.addSubview(self.viewShowBanner)

        }
       /* DispatchQueue.main.async {
            
            self.viewShowBanner.frame = CGRect(x: 0, y: (self.view.bounds.height - (49 + self.defaultTabBarHeight)) , width: self.view.bounds.size.width, height: 49)
            self.viewShowBanner.backgroundColor = UIColor.red
            
            self.bannerView = GADBannerView(adSize: kGADAdSizeBanner)
            self.bannerView.frame = CGRect(x: 0, y: 0 , width: self.view.bounds.size.width, height: 49)
            self.viewShowBanner.addSubview(self.bannerView)
            
            self.bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
            self.bannerView.rootViewController = self
            self.bannerView.load(GADRequest())
            self.bannerView.backgroundColor = UIColor.white
        }*/
        
        // Do any additional setup after loading the view.
    }
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
      print("adViewDidReceiveAd")
    }

    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
        didFailToReceiveAdWithError error: GADRequestError) {
      print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }

    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
      print("adViewWillPresentScreen")
    }

    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
      print("adViewWillDismissScreen")
    }

    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
      print("adViewDidDismissScreen")
    }

    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
      print("adViewWillLeaveApplication")
    }
    
   
}
