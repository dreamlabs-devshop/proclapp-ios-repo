//
//  ViewShadow.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/11/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import Foundation
import UIKit

class ViewShadow: UIView {
    
    override func awakeFromNib() {
        
        DispatchQueue.main.async {
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
            self.layer.shadowOpacity = 0.5
            self.layer.shadowRadius = 2//3
            self.layer.cornerRadius = self.frame.height/2
        }
    }
}

class addShadow: UIView {
    override func awakeFromNib() {
        DispatchQueue.main.async {
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOpacity = 0.5
            self.layer.shadowRadius = 1
            self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        }
    }
}


