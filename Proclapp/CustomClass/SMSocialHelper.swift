//
//  SMSocialHelper.swift
//  AbayaBazar
//
//  Created by iMac-4 on 7/26/17.
//  Copyright © 2017 iMac-4. All rights reserved.
//

import Foundation
import UIKit
import FBSDKLoginKit
import FBSDKCoreKit


class SMSocialHelper: NSObject
{
    var completionSM: ((signUpData,Error?) -> Void)?
    class var Shared : SMSocialHelper {
        struct Static {
            static let instance : SMSocialHelper = SMSocialHelper()
        }
        return Static.instance
    }
    
    //MARK: FACEBOOK AUTHENTICATION
    
    func FacebookSignin(delegate:UIViewController,handler:@escaping (signUpData,Error?) -> Void) {
        
        let fbLoginManager : LoginManager = LoginManager()
      
        LoginManager().logOut()


        fbLoginManager.logIn(permissions: ["email"], from: delegate) { (result, error) in
            
            let socialObj = signUpData()
            
            print("error")
            debugPrint(error ?? "")
            
            if (error == nil)
            {
                let fbloginresult : LoginManagerLoginResult = result!
                
                if(fbloginresult.isCancelled)
                {
//                    ShowAlert(title: "", message: "Request Denied", buttonTitle: "Ok")
                }
                else if fbloginresult.grantedPermissions.contains("email")
                {
                    debugPrint("Facebook Profile:\(String(describing: result))")
                    
                    let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"email, name"])
                    graphRequest.start(completionHandler: { (connection, result, error) -> Void in
                        if ((error) != nil)
                        {
                            print("Error  \(String(describing: error))")
                        }
                        else
                        {
                            let resultDic = result as! NSDictionary
                            
//                            debugPrint(result)
                            let createdEmail = ToString(resultDic.value(forKey:"name")!).replacingOccurrences(of: " ", with: ".") + "@fb.com"
                            
                            socialObj.userName = ToString(resultDic.value(forKey:"name")!)
                            socialObj.emailAddress = ToString(resultDic.value(forKey:"email") ?? createdEmail)
                            socialObj.media_id = ToString(resultDic.value(forKey:"id")!)
                            socialObj.media_type = "fb"
                        }
                        
                        handler(socialObj,nil)
                    })
                }
                else {
                    debugPrint("not going")
                }
            }
            else {
                print("there was an error")
            }
        }
    }
}


