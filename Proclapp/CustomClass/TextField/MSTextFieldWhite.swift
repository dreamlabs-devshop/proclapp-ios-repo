//
//  MSTextFieldWhite.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/1/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//


import Foundation
import UIKit

private var kAssociationKeyMaxLength: Int = 0

class MSTextFieldWhite: UITextField {
    
    // Provides left padding for images
   /* override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        
        var textRect = super.rightViewRect(forBounds: bounds)
        textRect.origin.x += rightPadding
        return textRect
    }*/
    // placeholder position
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: leftPadding , dy: 0)
    }
    
    // text position
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: leftPadding , dy: 0)
    }
    
    
    
    

    
   
    
    @IBInspectable var leftPadding: CGFloat = 10
    
    @IBInspectable var rightPadding: CGFloat = 10
    
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
        }
    }
    
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
//    override func editingRect(forBounds bounds: CGRect) -> CGRect {
//        return textRect(forBounds: bounds)
//    }
   
   
    @IBInspectable var placeHolderColor: UIColor {
        get { return self.placeHolderColor }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: placeHolderColor ?? UIColor.gray])
        }
    }
    
    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }
    
    @objc func checkMaxLength(textField: UITextField) {
        guard let prospectiveText = self.text,
            prospectiveText.count > maxLength
            else {
                return
        }
        
        let selection = selectedTextRange
        
        let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        let substring = prospectiveText[..<indexEndOfText]
        text = String(substring)
        
        selectedTextRange = selection
    }
    
    
    
    // MARK: - Class Life Cycle
    
    override internal func awakeFromNib()
    {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            self.cornerRadius = self.frame.height/2
        }
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            
            let customFont:UIFont = UIFont.init(name: (self.font?.fontName)!, size: 15.0)!
            font = customFont
            self.font = customFont
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.font :customFont, NSAttributedString.Key.foregroundColor:UIColor.Appcolor153])
            self.tintColor = UIColor.Appcolor153
        }
            
        else {
            
            let customFont:UIFont = UIFont.init(name: (self.font?.fontName)!, size: 20.0)!
            font = customFont
            self.font = customFont
            
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.font :customFont, NSAttributedString.Key.foregroundColor:UIColor.white.withAlphaComponent(0.62)])
            self.tintColor = UIColor.white.withAlphaComponent(0.62)
            
        }
    }
    
}


class MSTextFieldWhite_Dropdown: UITextField {
    
    
    // placeholder position
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: leftPadding , dy: 0)
    }
    
    // text position
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: leftPadding , dy: 0)
    }
    
    
    
    @IBInspectable var leftPadding: CGFloat = 20
    
    @IBInspectable var rightPadding: CGFloat = 20
    
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
        }
    }
    
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }

    @IBInspectable var placeHolderColor: UIColor {
        get { return self.placeHolderColor }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue ?? UIColor.gray])
        }
    }
    
    
    
    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }
    
    @objc func checkMaxLength(textField: UITextField) {
        guard let prospectiveText = self.text,
            prospectiveText.count > maxLength
            else {
                return
        }
        
        let selection = selectedTextRange
        
        let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        let substring = prospectiveText[..<indexEndOfText]
        text = String(substring)
        
        selectedTextRange = selection
    }
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
    
    
    // MARK: - Class Life Cycle
    
    override internal func awakeFromNib()
    {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            self.cornerRadius = self.frame.height/2
        }
        
        let customFont:UIFont = UIFont.init(name: (self.font?.fontName)!, size: 15.0)!
        font = customFont
        self.font = customFont
        self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.font :customFont, NSAttributedString.Key.foregroundColor:UIColor.Appcolor153])
        self.tintColor = UIColor.clear
    }
    
}
