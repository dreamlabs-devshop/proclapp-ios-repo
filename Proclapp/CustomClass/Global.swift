//
//  Global.swift
//  Proclapp
//
//  Created by Mac-4 on 05/06/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import Foundation
import UIKit
//import SwiftAudio
import SafariServices

var objAdvert : Advert?
//let player = AudioPlayer()

//var isFromChat = false

var globalChatUserId = ""

extension UIView {
    
    func setborder(_ width: CGFloat, _color: UIColor) {
        self.layer.borderWidth = width
        self.layer.borderColor = _color.cgColor
    }
    
}



func ToString(_ StingText : Any?) -> String
{
    if let strText = StingText
    {
        if let TempString = strText as? String
        {
            return TempString
        }
        else if let TempFloat = StingText as? Float
        {
            return String(TempFloat)
        }
        else if let TempDouble = StingText as? Double
        {
            return String(TempDouble)
        }
        else if let TempNumber = strText as? NSNumber
        {
            return TempNumber.stringValue
        }
        else if let TempNumber = strText as? Int
        {
            return String(TempNumber)
        }
        else if let TempNumber = strText as? NSURL, let urlToString = TempNumber.absoluteString
        {
            return urlToString
        } else if let cvag = strText as? CVarArg {
            return String(format: "%@", cvag)
        }
        
        return "\(strText)"
    }
    else
    {
        return ""
    }
}
func ToInt(_ StrText : Any?) -> Int
{
    if let TempNumber = StrText as? NSNumber
    {
        return TempNumber.intValue
    }
    else if let TempNumber = StrText as? Int
    {
        return TempNumber
    }
    else if let myInteger = Int(ToString(StrText))
    {
        return myInteger
    }
    return 0
}
func setTabBarHideGBL(navCtrl:UINavigationController,isHide:Bool) {
    
    if let objHomeTab = navCtrl.parent as? TabBarDefault {
        objHomeTab.viewShowBanner.isHidden = isHide
    }
}

func setLoginData(_ tempDict : [String : Any]? = nil) {
    
    let dictData = tempDict ?? (getUserDefault(Key: Constant.userDeafult_LoginDic) as? [String : Any]) ?? [String : Any]()
    loginData = ProfileModel.init(dictData)
    
    if loginData?.user_id.isValid == true {
        globalUserId = loginData?.user_id ?? ""
        WebService.shared.setAuthorizationToken(loginData?.token ?? "")
        
    }
}
func setLoginDataFromProfile(_ tempDict : [String : Any]? = nil) {
    
    let dictData = tempDict ?? (getUserDefault(Key: Constant.userDeafult_ProfileDic) as? [String : Any]) ?? [String : Any]()
    loginData = ProfileModel.init(dictData)
    
    if loginData?.user_id.isValid == true {
        globalUserId = loginData?.user_id ?? ""
        WebService.shared.setAuthorizationToken(loginData?.token ?? "")
        
    }
}

func resetData() {
    
    UIApplication.shared.applicationIconBadgeNumber = 0
    let center = UNUserNotificationCenter.current()
    center.removeAllDeliveredNotifications() // To remove all delivered notifications
    center.removeAllPendingNotificationRequests() // T
    
    global_count_Msg = "0"
    global_count_Noti = "0"
    global_count_Friend = "0"
    globalUserId = ""
    WebService.shared.setAuthorizationToken("")
    loginData = nil
    UserDefaults.RemovefromUserDefault(key: Constant.userDeafult_LoginDic)
}
//Global
func setUserDefault(_ params:AnyObject, Key: String)
{
    let data = NSKeyedArchiver.archivedData(withRootObject: params)
    UserDefaults.setUserDefault(value: data, key: Key)
    
    let sharedDefault = UserDefaults(suiteName: Constant.dataSharingGroup)!
    sharedDefault.set(data, forKey: Key)

}

func getUserDefault(Key:String) -> AnyObject
{
    if let data = UserDefaults.getUserDefault(key: Key) as? Data {
        if let storedData = NSKeyedUnarchiver.unarchiveObject(with: data){
            
//            let sharedDefault = UserDefaults(suiteName: Constant.dataSharingGroup)!
//            sharedDefault.set(data, forKey: Key)
//
            return storedData as AnyObject
        }
    }
    
    
//    let sharedDefault = UserDefaults(suiteName: Constant.dataSharingGroup)!
//    sharedDefault.set(UserDefaults.getUserDefault(key: Key) as AnyObject, forKey: Key)
    
    return UserDefaults.getUserDefault(key: Key) as AnyObject
    //    return NSNull()
}
func dialNumber_MS(number : String) {
    
    if let url = URL(string: "tel://\(number)"),
        UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler:nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    } else {
        // add error message here
    }
}

func safariOpen(viewController:UIViewController,url:URL)
{
    if ["http", "https"].contains(url.scheme?.lowercased() ?? "")
    {
        viewController.present(SFSafariViewController(url: url), animated: true, completion: nil)
    }
}

func linkurlOpen_MS(linkurl : String) {
    
    if let url = URL(string: "\(linkurl)"),
        UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler:nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    } else {
        debugPrint("locha")
        // add error message here
    }
}
func linkurlOpen_Email(email : String) {
    if let url = URL(string: "mailto://\(email)"),
        UIApplication.shared.canOpenURL(url) {
        if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler:nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    } else {
        // add error message here
    }
}

func calculateTimeDifference(_ dateTime: String) -> String {

    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

    let dateAsString = dateTime
    let date1 = dateFormatter.date(from: dateAsString)!
    
    let date = Date()
    let df = DateFormatter()
    df.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let dateAsString2 = df.string(from: date)
    let date2 = dateFormatter.date(from: dateAsString2)!

    let components : NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfMonth, .month, .year]
    let difference = (Calendar.current as NSCalendar).components(components, from: date1, to: date2, options: [])

    var dateTimeDifferenceString = "hello"

//    if difference.day != 0 {
//        dateTimeDifferenceString = "\(difference.day!)d \(difference.hour!)h \(difference.minute!)m"
//    } else if  difference.day == 0 {
//        dateTimeDifferenceString = "\(difference.hour!)h \(difference.minute!)m"
//    }
    
    if difference.year != 0 {
        dateTimeDifferenceString = "\(difference.year!) year" + pluralize(difference.year!)
    } else if  difference.month != 0 {
        dateTimeDifferenceString = "\(difference.month!) month" + pluralize(difference.month!)
    }
    else if  difference.weekOfMonth != 0 {
        dateTimeDifferenceString = "\(difference.weekOfMonth!) week" + pluralize(difference.weekOfMonth!)
    }
    else if  difference.day != 0 {
        dateTimeDifferenceString = "\(difference.day!) day" + pluralize(difference.day!)
    }
    else if  difference.hour != 0 {
        dateTimeDifferenceString = "\(difference.hour!) hour" + pluralize(difference.hour!)
    }
    else if  difference.minute != 0 {
        dateTimeDifferenceString = "\(difference.minute!) minute" + pluralize(difference.minute!)
    }
    else {
        dateTimeDifferenceString = "30 seconds"
    }

    return dateTimeDifferenceString
}

func pluralize(_ value: Int) -> String {
    if(value > 1){
        return "s ago";
    }
    
    return " ago";
}
