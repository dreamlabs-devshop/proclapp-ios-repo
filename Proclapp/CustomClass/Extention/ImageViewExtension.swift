//
//  ImageViewExtension.swift
//  YeahYag
//
//  Created by Pris Mac on 5/18/19.
//  Copyright © 2019 Ajay. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage
//import Alamofire

extension UIImageView
{
    func setImageWithURLWithCompletionBlock(_ imageUrl: String? , Placeholderimage: String?,_ completionBlock: ((UIImage?,Error?) -> ())?)
    {
        if imageUrl != nil
        {
            if let URLImage = URL(string: imageUrl ?? "")
            {
                af_setImage(withURL: URLImage, placeholderImage: checkValidString(Placeholderimage) ?  UIImage(named: Placeholderimage ?? "") : nil, completion : { response in
                    completionBlock?(response.result.value , response.result.error)
                })
            }
            else
            {
                image = checkValidString(Placeholderimage) ?  UIImage(named: Placeholderimage ?? "") : nil
            }
        }
        else
        {
            image = checkValidString(Placeholderimage) ?  UIImage(named: Placeholderimage ?? "") : nil
        }
    }
    
    func setImageWithURL_Colletion(_ TempUrl: String? , _ placeHolderImg: UIImage?)
    {
        if let imageUrl = TempUrl, imageUrl.isValid , let URLImage = URL(string: imageUrl)
        {
            af_setImage(withURL: URLImage, placeholderImage: placeHolderImg)
        }
        else
        {
            af_cancelImageRequest()
            image = placeHolderImg
        }
    }
  
    
    func setImageWithURL(_ TempUrl: String? , _ Placeholderimage: String?)
    {
        if let imageUrl = TempUrl , let URLImage = URL(string: imageUrl)
        {
            af_setImage(withURL: URLImage, placeholderImage: checkValidString(Placeholderimage) ? UIImage(named: Placeholderimage ?? "") : nil)
        }
        else
        {
            image = checkValidString(Placeholderimage) ? UIImage(named: Placeholderimage ?? "") : nil
        }
    }
    
   
    
    func GetOriginaImageFromURL(_ imageUrl: String? , Placeholderimage: UIImage?)
    {
        if checkValidString(imageUrl)
        {
            if let URLImage = URL(string: imageUrl ?? "")
            {
                af_setImage(withURL: URLImage, placeholderImage: Placeholderimage)
            }
            else
            {
                image = Placeholderimage
            }
        }
        else
        {
            image = Placeholderimage
        }
    }
}
