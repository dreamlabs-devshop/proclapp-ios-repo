//
//  MSExtension.swift
//  Proclapp
//
//  Created by Ashish Parmar on 3/26/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import Foundation
import UIKit

let screenscale = UIScreen.main.bounds.width / 320.0
let screenwidth = UIScreen.main.bounds.width
let screenheight = UIScreen.main.bounds.height

let screenScaleWidth = UIScreen.main.bounds.width / 320.0
let screenScaleHeight = UIScreen.main.bounds.width / 568.0

let appDelegate = UIApplication.shared.delegate as! AppDelegate


//MARK:- Extension_UIColor
extension UIColor {
 
    public class var AppBlack142: UIColor
    {
        return UIColor(red: 142.0/255.0, green: 142.0/255.0, blue:142.0/255.0, alpha: 1.0)
    }
    public class var AppBlack230: UIColor
    {
        return UIColor(red: 51.0/255.0, green: 51.0/255.0, blue:51.0/255.0, alpha: 0.5)//alpha
    }
 
    public class var AppBlack: UIColor
    {
        return UIColor(red: 51.0/255.0, green: 51.0/255.0, blue:51.0/255.0, alpha: 0.7)//alpha 
    }
    
    public class var Appcolor51: UIColor{
        return UIColor.init(named: "51") ?? UIColor.black
    }
    
    public class var Appcolor96: UIColor {
        return UIColor(red: 96.0/255.0, green: 96.0/255.0, blue: 96.0/255.0, alpha: 1.0)
    }
    public class var AppShadowBlack: UIColor {
        return UIColor(red: 96.0/255.0, green: 96.0/255.0, blue: 96.0/255.0, alpha: 0.44)
    }
    
    public class var AppSkyBlue: UIColor {
        return UIColor(red: 54.0/255.0, green: 207.0/255.0, blue: 217.0/255.0, alpha: 1.0)
    }
    
    public class var AppBlue_Dark: UIColor {
        return UIColor(red: 12.0/255.0, green: 105.0/255.0, blue: 132.0/255.0, alpha: 1.0)
    }
    
    public class var App166: UIColor {
        return UIColor(red: 166.0/255.0, green: 166.0/255.0, blue: 166.0/255.0, alpha: 1.0)
    }
    public class var Appcolor204: UIColor {
        return UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0)
    }
    public class var Appcolor102: UIColor? {
        return UIColor.init(named: "102")
    }
    public class var Appcolor153: UIColor {
        return UIColor(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0)
    }
    public class var AppcolorRed: UIColor {
        return UIColor(red: 217.0/255.0, green: 7.0/255.0, blue: 7.0/255.0, alpha: 1.0)
    }
    
    
    
    var r: CGFloat{
        return self.cgColor.components![0]
    }
    
    var g: CGFloat{
        return self.cgColor.components![1]
    }
    
    var b: CGFloat{
        return self.cgColor.components![2]
    }
    
    var alpha: CGFloat{
        return self.cgColor.components![3]
    }
    
    class func getRGBColor(_ r:CGFloat,g:CGFloat,b:CGFloat)-> UIColor {
        return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1.0)
    }
}

/*public extension UILabel
{
    @IBInspectable var MinFontSize : CGFloat {
        set {
            if MinFontSize > 0
            {
                font = UIFont(name: font.fontName, size: MinFontSize * screenscale)
            }
        }
        get{
            return font.pointSize
        }
    }
}*/

public extension UILabel
{
    @IBInspectable var MinFontSize : CGFloat {
        get {
            return font.pointSize
        }
        set {
            font = UIFont(name: font.fontName, size: newValue * screenscale)
        }
    }
}

extension UILabel {
    var maxNumberOfLines: Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(MAXFLOAT))
        let text = (self.text ?? "") as NSString
        let textHeight = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil).height
        let lineHeight = font.lineHeight
        return Int(ceil(textHeight / lineHeight))
    }
    var numberOfVisibleLines: Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(MAXFLOAT))
        let textHeight = sizeThatFits(maxSize).height
        let lineHeight = font.lineHeight
        return Int(ceil(textHeight / lineHeight))
    }
}
extension UITextView {
    var maxNumberOfLinesTextview: Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(MAXFLOAT))
        let text = (self.text ?? "") as NSString
        let textHeight = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil).height
        guard let lineHeight = font?.lineHeight else { return 0 }
        return Int(ceil(textHeight / lineHeight))
    }
   
}
class UIViewWithDashedLineBorder: UIView {
    
    override func draw(_ rect: CGRect) {
        
        let path = UIBezierPath(roundedRect: rect, cornerRadius: self.frame.height/2)
        
        UIColor.clear.setFill()
        path.fill()
        
        UIColor.App166.setStroke()
        path.lineWidth = 1
        
        let dashPattern : [CGFloat] = [5, 2]
        path.setLineDash(dashPattern, count: 2, phase: 0)
        path.stroke()
    }
}
class UIViewWithDashedLineBorder_new: UIView {
    
    override func draw(_ rect: CGRect) {
        
        let path = UIBezierPath(roundedRect: rect, cornerRadius: 5)
        
        UIColor.clear.setFill()
        path.fill()
        
        UIColor.App166.setStroke()
        path.lineWidth = 1
        
        let dashPattern : [CGFloat] = [5, 2]
        path.setLineDash(dashPattern, count: 2, phase: 0)
        path.stroke()
    }
}
extension UIView {
    var parentViewController : UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    func addShadow(radius: CGFloat,corner:CGFloat){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = radius
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.layer.cornerRadius = corner
        self.layer.masksToBounds = true
    }
    
    func setCornerRaduisToRound() {
        self.clipsToBounds = true
        self.layer.cornerRadius = self.frame.size.width / 2
        self.layer.masksToBounds = true
    }
    func setBorder() {
        self.layer.borderWidth = 1
        self.layer.masksToBounds = true
        self.clipsToBounds = true
        self.layer.cornerRadius = 5
        self.layer.borderColor = UIColor.lightGray.cgColor


    }
    
    func addDashedBorder() {
        
        DispatchQueue.main.async {
            //Create a CAShapeLayer
            let shapeLayer = CAShapeLayer()
            shapeLayer.strokeColor = UIColor.App166.cgColor
            shapeLayer.lineWidth = 2
            // passing an array with the values [2,3] sets a dash pattern that alternates between a 2-user-space-unit-long painted segment and a 3-user-space-unit-long unpainted segment
            shapeLayer.lineDashPattern = [2,3]
            let path = CGMutablePath()
            path.addLines(between: [CGPoint(x: 0, y: 0),
                                    CGPoint(x: self.frame.width, y: 0)])
            shapeLayer.path = path
            self.layer.addSublayer(shapeLayer)
        }
    }
    
    func addDashedBorderNew() {
        
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = UIColor.AppBlack.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [6,3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
}
class UIViewWithBorder: UIView {
    override func draw(_ rect: CGRect) {
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.Appcolor153.cgColor
        DispatchQueue.main.async {
            self.layer.cornerRadius = self.frame.height/2
        }
    }
   
    
}

extension UITabBarController {
    @IBInspectable var selected_index: Int {
        get {
            return selectedIndex
        }
        set(index) {
            selectedIndex = index
        }
    }
}
extension UIViewController
{
    func pushTo(_ contr : UIViewController , animated : Bool = true)
    {
        self.navigationController?.pushViewController(contr, animated: animated)
    }
    static func getViewControllerFrom(_ story : UIStoryboard) -> UIViewController
    {
        return story.instantiateViewController(withIdentifier:"")
    }
        func hideKeyboardWhenTappedAround() {
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
            tap.cancelsTouchesInView = false
            view.addGestureRecognizer(tap)
            /*      how to use
             override func viewDidLoad() {
             super.viewDidLoad()
             self.hideKeyboardWhenTappedAround()
             }*/
        }
        
        @objc func dismissKeyboard() {
            view.endEditing(true)
        }
        
        func showAlertWithActions(msg: String,titles:[String], handler:@escaping (_ clickedIndex: Int) -> Void)
        {
            let alert = UIAlertController(title: Constant.appName, message: msg, preferredStyle: .alert)
            
            for title in titles
            {
                
                let action  = UIAlertAction(title: title, style: .default, handler: { (alertAction) in
                    //Call back fall when user clicked
                    let index = titles.firstIndex(of: alertAction.title!)
                    if index != nil {
                        handler(index!+1)
                    }
                    else {
                        handler(0)
                    }
                    
                })
                alert.addAction(action)
            }
            present(alert, animated: true, completion: nil)
        }
        
        func showOkCancelAlertWithAction(msg: String, handler:@escaping (_ isOkAction: Bool) -> Void)
        {
            let alert = UIAlertController(title: Constant.appName , message: msg, preferredStyle: .alert)
            let okAction =  UIAlertAction(title: "OK", style: .default) { (action) -> Void in
                return handler(true)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
                return handler(false)
            }
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
        }
        
        func showOkAlert(msg: String)
        {
            let alert = UIAlertController(title: Constant.appName , message: msg, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
        }
        
        func showOkAlertWithHandler(msg: String,handler: @escaping ()->Void)
        {
            let alert = UIAlertController(title: Constant.appName , message: msg, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { (type) -> Void in
                handler()
            }
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
        }
    
    func shareMaulik(shareText:String?,shareImage:UIImage?,shreUrl:String?){
        
        DispatchQueue.main.async {
            var objectsToShare = [AnyObject]()
            
            if let shareTextObj = shareText{
                objectsToShare.append(shareTextObj as AnyObject)
            }
            
            if let shareImageObj = shareImage{
                objectsToShare.append(shareImageObj)
            }
            if let shareUrlObj = shreUrl{
                objectsToShare.append(shareUrlObj as AnyObject)
            }
            
            if shareText != nil || shareImage != nil || shreUrl != nil {
                let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view
                activityViewController.setValue(Constant.appName, forKey: "Subject")
                
                self.present(activityViewController, animated: true, completion: nil)
            }else
            {
                print("There is nothing to share")
            }
        }
    }
        
}
extension UIFont
{
    static func FontLatoRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "Lato-Regular", size: size)!
    }
    static func FontLatoHeavy(size: CGFloat) -> UIFont {
        return UIFont(name: "Lato-Heavy", size: size)!
    }
    static func FontLatoMedium(size: CGFloat) -> UIFont {
        return UIFont(name: "Lato-Medium", size: size)!
    }
    static func FontLatoBold(size: CGFloat) -> UIFont {
        return UIFont(name: "Lato-Bold", size: size)!
    }
    
    //["Lato-Regular", "Lato-Semibold", "Lato-Thin", "Lato-Medium", "Lato-ThinItalic", "Lato-LightItalic", "Lato-Italic", "Lato-Bold", "Lato-SemiboldItalic", "Lato-BoldItalic", "Lato-MediumItalic", "Lato-Black", "Lato-HeavyItalic", "Lato-Light", "Lato-BlackItalic", "Lato-Heavy"]
    
}

extension UITableView
{
    func ScrollToBottom(animated : Bool = true ,ScrollPosition : UITableView.ScrollPosition = .bottom)
    {
        DispatchQueue.main.async{
            if self.numberOfSections > 0
            {
                let numberofRowInLastSection = self.numberOfRows(inSection: self.numberOfSections - 1)
                if numberofRowInLastSection > 0
                {
                    let NewIndexpath = IndexPath(row: numberofRowInLastSection - 1, section: self.numberOfSections - 1)
                    self.scrollToRow(at: NewIndexpath, at: ScrollPosition, animated: false)
                    self.selectRow(at: NewIndexpath, animated: animated, scrollPosition: ScrollPosition)
                }
            }
        }
    }
    func indexPathForView(_ view: UIView) -> IndexPath?
    {
        return self.indexPathForRow(at: convert(view.center, from: view.superview))
    }
}

extension String {
    var isBlank: Bool {
        return self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    var trim: String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
   
    
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}"
        let emailTest  = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    
    

}

extension Array
{
    var jsonString : String
    {
        if let jsonData = try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted), let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
            return JSONString
        }
        return ""
    }
   
}

extension Optional where Wrapped == String {
    var isBlank: Bool {
        if let unwrapped = self {
            return unwrapped.isBlank
        } else {
            return true
        }
    }
}
extension Bundle {
    
    var appName: String {
        return infoDictionary?["CFBundleName"] as! String
    }
    
    var bundleId: String {
        return bundleIdentifier!
    }
    
    var versionNumber: String {
        return infoDictionary?["CFBundleShortVersionString"] as! String
    }
    
    var buildNumber: String {
        return infoDictionary?["CFBundleVersion"] as! String
    }
    
}
extension UICollectionView
{
    func indexPathForView_Collection(_ view: UIView) -> IndexPath?
    {
        return self.indexPathForItem(at: convert(view.center, from: view.superview))
    }
}

extension UIRefreshControl {
    func beginRefreshingManually() {
        beginRefreshing()
        
        if let scrollView = superview as? UIScrollView {
            scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y - frame.height), animated: true)
        }
}
}
extension UITableViewCell {
    
    //Gets the owner tableView of the cell
    var tableView: UITableView? {
        var view = self.superview
        while (view != nil && view!.isKind(of: UITableView.self) == false) {
            view = view!.superview
        }
        return view as? UITableView
    }
}

extension NSNotification.Name
{
    static let NewDataLoadHome = NSNotification.Name.init("NewDataLoadHome")
    static let VideoUpload = NSNotification.Name.init("videoUpload")
    
    static let AnswerPost = NSNotification.Name.init("AnswerPost")
    static let DiscussPost = NSNotification.Name.init("DiscussPost")
//    static let DeclineQuestion  = NSNotification.Name.init("DeclineQuestion")
//    static let DeclineArticle = NSNotification.Name.init("DeclineArticle")
    
    static let Delete_Post = NSNotification.Name.init("Delete_Post")



    static let NewDataLoadAdvert = NSNotification.Name.init("NewDataLoadAdvert")
    static let socketStateChanged = NSNotification.Name.init("socketStateChanged")


}
extension UserDefaults
{
    static func setUserDefault(value : Any , key : String){
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    static func getUserDefault(key : String) -> Any{
        return UserDefaults.standard.object(forKey:key) as Any
    }
    
    static func RemovefromUserDefault(key : String){
        UserDefaults.standard.removeObject(forKey:key)
        UserDefaults.standard.synchronize()
    }
}
struct Image {
        
    static let experts_placeholder = UIImage.init(named: "experts_placeholder")

}
