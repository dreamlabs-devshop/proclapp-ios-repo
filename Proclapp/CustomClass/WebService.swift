//
//  WebService.swift
//  YeahYag
//
//  Created by Pris Mac on 5/15/19.
//  Copyright © 2019 Ajay. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SVProgressHUD
import MobileCoreServices

#if DEBUG
let Env = "d"
#else
let Env = "p"
#endif

var GlobalDeviceToken = "123"
var globalUserId = ""
var isOPenNavDefault = false

var global_count_Msg = "0"
var global_count_Noti = "0"
var global_count_Friend = "0"

var loginData : ProfileModel?

class WebService {
    
    static let shared = WebService()
    var AuthorizationToken = ""
    
    var getHeaders : HTTPHeaders
    {
        return ["Authorization":toString(loginData?.token),"Accept":"application/json", "Env" : Env , "Plateform" : "ios" , "Device-name" : UIDevice.current.name , "Os-version" : UIDevice.current.systemVersion , "Application-version" : Bundle.main.buildNumber , "Api-version" : ServerURL.api_version] ///"Content-Type":"application/json"
    }
    
    func setAuthorizationToken(_ Token: String) {
        AuthorizationToken = Token
    }
    
    func removeAuthorizationToken(){
        AuthorizationToken = ""
    }
    
    func RequesURL(_ urlString : URLConvertible , Perameters : [String: Any]? = nil ,showProgress:Bool,completion: @escaping ((NSDictionary , Bool) -> Void), failure:@escaping ((Error) -> Void))
    {
        if showProgress{
            SVProgressHUD.show()
            SVProgressHUD.setDefaultAnimationType(.native)
            SVProgressHUD.setDefaultMaskType(.black)
        }
        
        Alamofire.request(urlString,method: .post, parameters: Perameters, encoding: URLEncoding.httpBody, headers: getHeaders)
            .responseJSON { response in
                
                switch(response.result) {
                    
                    
                    
                case .success(let Value):
                    if showProgress{
                        SVProgressHUD.dismiss()
                    }
                    if let json = JSON(Value)["response"][0].dictionaryObject as NSDictionary?
                    {
                        if self.LogoutForcefully(json) == false
                        {
                            completion(json , toString(json["status"]).boolValue)
                        }
                    } else {
                        failure(MYError.init(description: "Something went wrong", domain: ""))
                    }
                    
                case .failure(let encodingError):
                    if showProgress{
                        SVProgressHUD.dismiss()
                    }
                    print("Error:\(String(describing: response.result.error))")
                    failure(encodingError)
                    
                    if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                        print("Data: \(utf8Text)")
                    }
                }
                
                /*switch(response.result) {
                    
                case .success(let Value):
                    if let json = JSON(Value).dictionaryObject as NSDictionary?
                    {
                        if self.LogoutForcefully(json) == false
                        {
                            completion(json , toString(json["status"]).boolValue)
                        }
                    } else {
                        failure(MYError.init(description: "Something went wrong", domain: ""))
                    }
                    
                case .failure(let encodingError):
                    print("Error:\(String(describing: response.result.error))")
                    failure(encodingError)
                }*/
        }
    }
    
    func WebServiceRequestURLByPassingData(DictionaryImages : NSDictionary? = nil, urlString : URLConvertible , Perameters : [String: Any]  ,completion: @escaping ((NSDictionary ,String, Bool) -> Void), failure:@escaping ((Error) -> Void))
    {
        debugPrint("URL:-",urlString)
        debugPrint("Param:-",Perameters)
        
        SVProgressHUD.show()
        SVProgressHUD.setDefaultAnimationType(.native)
        SVProgressHUD.setDefaultMaskType(.black)
        
        Alamofire.upload(multipartFormData:{ multipartFormData in
            
            // POST PARAMETER
            for (key, value) in Perameters {
                //            print("parameter: \(key):\(value)")
                let par:String = value as! String
                multipartFormData.append(par.data(using: .utf8)!, withName: key)
            }
            
            // UPLOAD IMAGE
            if let imagedata = DictionaryImages
            {
                for (key,value) in imagedata
                {
                    if(value is(UIImage))
                    {
                        let image:UIImage = value as! UIImage
                        if let data = image.pngData()
                        {
                            let ImageName = "Image\(NSUUID().uuidString).png"
                            multipartFormData.append(data, withName: key as! String, fileName: ImageName, mimeType: "image/*")
                        }
                    }
                }
            }
            
        },usingThreshold:UInt64.init(), to:urlString, method:.post, headers:getHeaders, encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                print("UP [ \(upload.uploadProgress.fractionCompleted)]")
                upload.responseJSON { response in
                    print("responseJSON:---\(response)")
                    
                    switch(response.result) {
                        
                    case .success(let Value):
                        if response.result.value != nil , let json = JSON(Value).dictionaryObject as NSDictionary?
                        {
                            
                            completion(json ,String(format: "%@", json["next_offset"] as? CVarArg ?? "0"), ConvertToBool(json["status"]))
                            
                        }
                        break
                        
                    case .failure(let encodingError):
                        print("Error:\(String(describing: response.result.error))")
                        failure(encodingError)
                        break
                    }
                    SVProgressHUD.dismiss()
                    }
                    .responseString { (responseString) in
                        
                        SVProgressHUD.dismiss()
                        //                    debugPrint("responseString Upload:\(responseString)")
                }
                
            case .failure(let encodingError):
                failure(encodingError)
                
                SVProgressHUD.dismiss()
            }
        })
        
    }
    
    func uploadData(_ uploadDocs : [UploadImage]?, urlString : URLConvertible , Perameters : [String: Any]? = nil , progressCompletionBlock : ((Progress) -> Void)? = nil,completion: @escaping ((NSDictionary , Bool) -> Void), failure:@escaping ((Error) -> Void)) {
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            if let arrUpload = uploadDocs {
                arrUpload.forEach({ (objUploadImage) in
                    if let uploadData = objUploadImage.imgData {
                        multipartFormData.append(uploadData, withName: objUploadImage.name, fileName: objUploadImage.fileName, mimeType: objUploadImage.mimeType)
                    }
                })
            }
            
            if let para = Perameters {
                for (key, value) in para {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }
        }, usingThreshold:UInt64.init(), to:urlString, method:.post, headers:getHeaders, encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                
                                upload.uploadProgress(closure: { (progress) in
                                    progressCompletionBlock?(progress)
                                    print("uploding: \(progress.fractionCompleted)")
                                })
                                    .responseJSON { response in
                                        switch response.result {
                                        case .success(let value):
                                            if let json = JSON(value)["response"][0].dictionaryObject as NSDictionary?
                                            {
                                                if self.LogoutForcefully(json) == false
                                                {
                                                    completion(json , toString(json["status"]).boolValue)
                                                }
                                            } else {
                                                failure(MYError.init(description: "Something went wrong", domain: ""))
                                            }
                                        case .failure(let responseError):
                                            failure(responseError)
                                        }
                                }
                            case .failure(let encodingError):
                                failure(encodingError)
                            }
        })
    }
    
    
    //asha03/09
    func webRequestForUplaodVoice(DictionaryImages : NSDictionary? = nil, urlString : URLConvertible , Perameters : [String: Any] ,completion: @escaping ((NSDictionary , Bool) -> Void), failure:@escaping ((Error) -> Void))
    {
        Alamofire.upload(multipartFormData:{ multipartFormData in
            
            // POST PARAMETER
            for (key, value) in Perameters {
                print("parameter: \(key):\(value)")
                let par:String = value as! String
                multipartFormData.append(par.data(using: .utf8)!, withName: key)
            }
            
            //UPLOAD Audio
            if let audioURL = DictionaryImages
            {
                for (key,value) in audioURL
                {
                    if let audURL = URL.init(string: toString(value)), let audioData = try? Data(contentsOf: audURL){
                        let audioName = "Audio\(Int(Date().timeIntervalSince1970)).wav"
                        multipartFormData.append(audioData, withName: "\(key)", fileName: audioName, mimeType: "audio/wav")
                    }
                    
                    
                }
            }
            
            
        },usingThreshold:UInt64.init(), to:urlString, method:.post, headers:getHeaders, encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                print("UP [ \(upload.uploadProgress.fractionCompleted)]")
                upload.responseJSON { response in
                    print("responseJSON:---\(response)")
                    
                    switch(response.result) {
                        
                    case .success(let Value):
                        if response.result.value != nil , let json = JSON(Value)["response"][0].dictionaryObject as NSDictionary?
                        {
                            print("===> \(urlString) \n ===>\(json)")
                            if self.LogoutForcefully(json) == false
                            {
                                completion(json , ConvertToBool(json["status"]))
                            }
                        }
                        
                        break
                        
                    case .failure(let encodingError):
                        print("Error:\(String(describing: response.result.error))")
                        failure(encodingError)
                        break
                    }
                    }
                    .responseString { (responseString) in
                        debugPrint("responseString Upload:\(responseString)")
                }
                
            case .failure(let encodingError):
                failure(encodingError)
            }
        })
        
    }
    //asha03/09
    
    //asha03/09
    func webRequestForUplaodVideo(DictionaryVideo : NSDictionary? = nil, DictionaryImages :  [String:Any?]? = nil,  urlString : URLConvertible , Perameters : [String: Any] ,completion: @escaping ((NSDictionary , Bool) -> Void), failure:@escaping ((Error) -> Void))
    {
        Alamofire.upload(multipartFormData:{ multipartFormData in
            
            // POST PARAMETER
            for (key, value) in Perameters {
                print("parameter: \(key):\(value)")
//                let par:String = value as! String
//                multipartFormData.append(par.data(using: .utf8)!, withName: key)
                
//                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)

                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)

            }
            
            
            
            //UPLOAD VIDEO
            if let videoURL = DictionaryVideo
            {
                for (key,value) in videoURL
                {
                    if let vidURL = URL.init(string: toString(value)), let videoData = try? Data(contentsOf: vidURL){
                        
                        let mim = self.mimeTypeForPath(path: vidURL.absoluteString)

                        let videoName = "Video\(Int(Date().timeIntervalSince1970)).mp4"
                        multipartFormData.append(videoData, withName: "\(key)", fileName: videoName, mimeType: mim)
                    }
                    
                    
                }
            }
            // UPLOAD IMAGE
            if let imagedata = DictionaryImages
            {
                for (key,value) in imagedata
                {
                    if(value is(UIImage))
                    {
                        let image:UIImage = value as! UIImage
                        if let data = image.pngData()
                        {
                            let ImageName = "vimg\(NSUUID().uuidString).png"
                            multipartFormData.append(data, withName: key , fileName: ImageName, mimeType: "image/*")
                        }
                    }
                }
            }
            
            
            
        },usingThreshold:UInt64.init(), to:urlString, method:.post, headers:getHeaders, encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                print("UP [ \(upload.uploadProgress.fractionCompleted)]")
                upload.responseJSON { response in
                    print("responseJSON:---\(response)")
                    
                    switch(response.result) {
                        
                    case .success(let Value):
                        if response.result.value != nil , let json = JSON(Value)["response"][0].dictionaryObject as NSDictionary?
                        {
                            print("===> \(urlString) \n ===>\(json)")
                            if self.LogoutForcefully(json) == false
                            {
                                completion(json , ConvertToBool(json["status"]))
                            }
                        }
                        
                        break
                        
                    case .failure(let encodingError):
                        print("Error:\(String(describing: response.result.error))")
                        failure(encodingError)
                        break
                    }
                    }
                    .responseString { (responseString) in
                        debugPrint("responseString Upload:\(responseString)")
                }
                
            case .failure(let encodingError):
                failure(encodingError)
            }
        })
        
    }
    
    func mimeTypeForPath(path: String) -> String
    {
        let url = NSURL(fileURLWithPath: path)
        let pathExtension = url.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }
    //asha03/09
    
    func uploadPDFFile(_ uploadDocsData : Data, fileName : String, urlString : URLConvertible , Perameters : [String: Any]? = nil ,completion: @escaping ((NSDictionary , Bool) -> Void), failure:@escaping ((Error) -> Void)) {
        
        SVProgressHUD.show()
        SVProgressHUD.setDefaultAnimationType(.native)
        SVProgressHUD.setDefaultMaskType(.black)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            multipartFormData.append(uploadDocsData, withName: "file", fileName: fileName, mimeType:"pdf")
            
            if let para = Perameters {
                for (key, value) in para {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }
        }, usingThreshold:UInt64.init(), to:urlString, method:.post, headers:getHeaders, encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("uploding: \(progress.fractionCompleted)")
                })
                    .responseJSON { response in
                        switch response.result {
                        case .success(let value):
                            if let json = JSON(value)["response"][0].dictionaryObject as NSDictionary?
                            {
                                if self.LogoutForcefully(json) == false
                                {
                                    completion(json , toString(json["status"]).boolValue)
                                }
                            } else {
                                failure(MYError.init(description: "Something went wrong", domain: ""))
                            }
                            
                            SVProgressHUD.dismiss()
                        case .failure(let responseError):
                            failure(responseError)
                            SVProgressHUD.dismiss()
                        }
                }
            case .failure(let encodingError):
                failure(encodingError)
                SVProgressHUD.dismiss()
            }
        })
    }
    func webRequestForMultipleImages(DictionaryImages : NSDictionary? = nil, urlString : URLConvertible , Perameters : [String: Any] ,completion: @escaping ((NSDictionary , Bool) -> Void), failure:@escaping ((Error) -> Void))
    {
        SVProgressHUD.show()
        SVProgressHUD.setDefaultAnimationType(.native)
        SVProgressHUD.setDefaultMaskType(.black)
        
        Alamofire.upload(multipartFormData:{ multipartFormData in
            
            // POST PARAMETER
            for (key, value) in Perameters {
                print("parameter: \(key):\(value)")
                let par:String = value as! String
                multipartFormData.append(par.data(using: .utf8)!, withName: key)
            }
            
            // UPLOAD IMAGE
            if let imagedata = DictionaryImages
            {
                for (key,value) in imagedata
                {
                    print(key)
                    print(value)
                    
                    if(value is ([UIImage])){
                        for (image) in value as! [UIImage] {
                            
                            if let imageData = image.pngData() {
                                let ImageName = "Image\(NSUUID().uuidString).png"
                                multipartFormData.append(imageData, withName: "\(key)[]", fileName: ImageName, mimeType: "image/*")
                            }
                        }
                    }
                }
            }
            
            
        },usingThreshold:UInt64.init(), to:urlString, method:.post, headers:getHeaders, encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                print("UP [ \(upload.uploadProgress.fractionCompleted)]")
                upload.responseJSON { response in
                    print("responseJSON:---\(response)")
                    
                    switch(response.result) {
                        
                    case .success(let Value):
                        if response.result.value != nil , let json = JSON(Value)["response"][0].dictionaryObject as NSDictionary?
                        {
                            print("===> \(urlString) \n ===>\(json)")
                            if self.LogoutForcefully(json) == false
                            {
                                completion(json , ConvertToBool(json["status"]))
                            }
                        }
                        
                        break
                        
                    case .failure(let encodingError):
                        print("Error:\(String(describing: response.result.error))")
                        failure(encodingError)
                        break
                    }
                    
                    SVProgressHUD.dismiss()
                    }
                    .responseString { (responseString) in
                        debugPrint("responseString Upload:\(responseString)")
                }
                
            case .failure(let encodingError):
                failure(encodingError)
                SVProgressHUD.dismiss()
            }
        })
        
    }
    func CancelAllRequests()
    {
        Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
            tasks.forEach({$0.cancel()})
        }
    }
    
    func CancelRequestByPassingURL(_ strUrl : String)
    {
        Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
            tasks.forEach({task in
                if task.currentRequest?.url?.absoluteString == strUrl
                {
                    task.cancel()
                }
            })
        }
    }
    
    func isServiceCalledInBackground(_ strUrl : String , Completion : ((Bool) -> ())?)
    {
        Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
            tasks.forEach({task in
                Completion?(task.currentRequest?.url?.lastPathComponent == strUrl)
            })
        }
    }
    

    
    func LogoutForcefully(_ resDict : NSDictionary) -> Bool{
       
        if toString(resDict["screen_code"]) == "1001"
        {
            resetData()
            appDelegate.LoadLoginView()
           /* DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                ShowAlert(title: Language.authenticationFail , message: toString(resDict["response_msg"]), buttonTitle: Language.ok, handlerCB: nil)
            }*/
            return true
        }
        return false
    }
    
}

class UploadImage {
    var imgData : Data?
    var name = ""
    var fileName = ""
    var mimeType = ""
    
    init(_ ImgData : Data? , Name : String , FileName : String , Mime : String = "image/*") {
        self.imgData = ImgData
        self.name = Name
        self.fileName = FileName
        self.mimeType = Mime
    }
    
    class var getFileName : String {
        let dateForm = DateFormatter()
        dateForm.dateStyle = .medium
        dateForm.timeStyle = .none
        dateForm.dateFormat = "yyyyMMdd_HHmmss"
        return globalUserId + "_" + dateForm.string(from: Date())
    }
}
struct MYError : Error {
    let description : String
    let domain : String
    
    var localizedDescription: String {
        return NSLocalizedString(description, comment: "")
    }
}

/*class userModel
{
    var email =  ""
    var firstname =  ""
    var lastname =  ""
    var response_msg =  ""
    var screen_code =  ""
    var token =  ""
    var user_id =  ""
    var profile_image = ""
    var profile_image_thumb = ""
    var status =  false
    
    init(_ dict : [String:Any]) {
        self.email = toString(dict["email"])
        self.firstname = toString(dict["firstname"])
        self.lastname = toString(dict["lastname"])
        self.response_msg = toString(dict["response_msg"])
        self.screen_code = toString(dict["screen_code"])
        self.profile_image = toString(dict["profile_image"])
        self.profile_image_thumb = toString(dict["profile_image_thumb"])
        self.token = toString(dict["token"])
        self.user_id = toString(dict["user_id"])
        self.status = dict["status"] as? Bool ?? false
    }
    
    
}
*/
