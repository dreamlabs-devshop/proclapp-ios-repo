//
//  MSButton_Custom.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/20/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//
import Foundation
import UIKit

@IBDesignable
class MSButton_Custom:UIButton {
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
/*      titleLabel?.textAlignment = .center
        DispatchQueue.main.async {
            self.layer.cornerRadius = self.frame.height/2
        }*/
    }
    
    @IBInspectable var borderWidth: Double {
        get {
            return Double(self.layer.borderWidth)
        }
        set {
            self.layer.borderWidth = CGFloat(newValue)
        }
    }
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
        set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    @IBInspectable var cornerRadius: Double {
        get {
            return Double(self.layer.cornerRadius)
        }set {
            self.layer.cornerRadius = CGFloat(newValue)
        }
    }
    
    @IBInspectable var fontSize: CGFloat {
        get
        {
            return self.titleLabel?.font.pointSize ?? 0.0
        }
        set {
            self.titleLabel?.font = UIFont(name: "Lato-Semibold", size: (fontSize * screenscale))!
        }
    }
    
    
//    @IBInspectable
//    var borderWidth:CGFloat = 0.0 {
//        didSet {
//            layer.borderWidth = borderWidth
//        }
//    }
   /* @IBInspectable
    var borderColor:UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }*/
       /* @IBInspectable
        var cornerRadius: CGFloat = 0.0 {
            didSet {
                layer.cornerRadius = cornerRadius * screenscale
                layer.masksToBounds = cornerRadius * screenscale > 0
            }
        }*/
    
   /* @IBInspectable
    var fontSize: CGFloat = 0.0 {
        didSet {
            self.titleLabel?.font = UIFont(name: "Lato-Semibold", size: (fontSize * screenscale))!
        }
    }*/
    
  /*  override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }*/
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//
//    }
    
}


