//
//  AppDelegate.swift
//  Proclapp
//
//  Created by Ashish Parmar on 3/26/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//
//#ifndef Proclapp_bridging_header_h
//#define Proclapp_bridging_header_h
//
import UIKit
import IQKeyboardManagerSwift
import Firebase
import UserNotifications
import GoogleMobileAds
import FBSDKLoginKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
//        isFromChat = false
        
        IQKeyboardManager.shared.enable = true
     // Use Firebase library to configure APIs.
        FirebaseApp.configure()

        // Initialize the Google Mobile Ads SDK.
        GADMobileAds.sharedInstance().start(completionHandler: nil)

        UNUserNotificationCenter.current().delegate = self
        // request permission from user to send notification
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound], completionHandler: { authorized, error in
            if authorized {
                DispatchQueue.main.async(execute: {
                    application.registerForRemoteNotifications()
                })
            }
        })
        
        if let dicGetLogin = getUserDefault(Key: Constant.userDeafult_LoginDic) as? [String : Any]{
            setLoginData(dicGetLogin)
            appDelegate.LoadHomeView()
        }
        return true
    }
    
    static let shared = UIApplication.shared.delegate as! AppDelegate
    func LoadHomeView()
    {
        if let navigation = StoryBoard.Home.instantiateInitialViewController() as? UINavigationController {
            
            let isFisrt = getUserDefault(Key: Constant.userDeafult_isFirstTimeInstall)
            
            var arrNav = navigation.viewControllers
            
           
            if loginData?.screen_code == "111"{//change password
                let vcInstace = StoryBoard.More.instantiateViewController(withIdentifier: "ChangePassVC") as! ChangePassVC
                vcInstace.isComeFromHome = true
                arrNav.append(vcInstace)
            }
            else if loginData?.screen_code == "222"{//edit profile
                let editProfile = StoryBoard.Profile.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
                editProfile.isProfileFromLogin = true
                arrNav.append(editProfile)
            }
            else if toString(isFisrt) != "no"
            {
                      let vcInstace = StoryBoard.Main.instantiateViewController(withIdentifier: "LetsStartedVC") as! LetsStartedVC
                arrNav.append(vcInstace)
            }
            navigation.setViewControllers(arrNav, animated: false)
            self.window?.rootViewController = navigation
        } else {
            self.window?.rootViewController = StoryBoard.Home.instantiateInitialViewController()
        }
        self.window?.makeKeyAndVisible()
    }
    func LoadLoginView()
    {
        self.window?.rootViewController = StoryBoard.Main.instantiateInitialViewController()
        self.window?.makeKeyAndVisible()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    

    
   
    

}
extension AppDelegate: UNUserNotificationCenterDelegate{
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        if let userInfo = notification.request.content.userInfo as? NSDictionary
        {
            let notification_type = toString(userInfo["notification_type"])
            if notification_type == "11"
            {
                if let dicProfile = userInfo["sender"] as? NSDictionary
                {
                    if globalChatUserId != toString(dicProfile["user_id"])
                    {
                        completionHandler([.alert, .sound])
                    }
                }
            }
            else
            {
                completionHandler([.alert, .sound])
            }
        }
        else
        {
            completionHandler([.alert, .sound])
        }
    }
    // show the notification alert (banner), and with sound
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if let userInfo = response.notification.request.content.userInfo as? NSDictionary
        {
            debugPrint("userInfo:->>> \(userInfo)")
            
            let notification_type = toString(userInfo["notification_type"])
            let post_id = toString(userInfo["post_id"])
            let getType = toString(userInfo["post_type"])
            let post_type = PostTypeEnum.init(rawValue: getType.integerValue)
            let user_id =  toString(userInfo["user_id"])
            
            
            
            if notification_type == "4" || notification_type == "5" || notification_type == "9" ||
                notification_type == "13" || notification_type == "21" || notification_type == "6" || notification_type == "22" || notification_type == "20" ||  notification_type == "18" ||   notification_type == "19" ||  notification_type == "23" || notification_type == "3"
            {
                postDetails(post_id) { (model) in
                    
                    if post_type == .Article
                    {
                        if let nav = self.window?.rootViewController as? UINavigationController
                        {
                            var arrNav = nav.viewControllers
                            arrNav.removeAll(where: {$0 is ArticleDetailVC ||  $0 is AnswerListVC || $0 is VoiceVideoDetailVC})
                            let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "ArticleDetailVC") as! ArticleDetailVC
                            arrNav.append(vcInstace)
                            vcInstace.dictPost = model
                            nav.setViewControllers(arrNav, animated: true)
                        }
                    }
                    else if post_type == .Question ||  post_type == .ResearchPapers
                    {
                        if let nav = self.window?.rootViewController as? UINavigationController
                        {
                            var arrNav = nav.viewControllers
                            arrNav.removeAll(where: {$0 is ArticleDetailVC ||  $0 is AnswerListVC || $0 is VoiceVideoDetailVC})
                            let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerListVC") as! AnswerListVC
                            arrNav.append(vcInstace)
                            vcInstace.dictPost = model
                            nav.setViewControllers(arrNav, animated: true)
                        }
                        
                    }
                    else if post_type == .VoiceVideo
                    {
                        if let nav = self.window?.rootViewController as? UINavigationController
                        {
                            var arrNav = nav.viewControllers
                            arrNav.removeAll(where: {$0 is ArticleDetailVC ||  $0 is AnswerListVC || $0 is VoiceVideoDetailVC})
                            let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "VoiceVideoDetailVC") as! VoiceVideoDetailVC
                            arrNav.append(vcInstace)
                            vcInstace.objPost = model
                            nav.setViewControllers(arrNav, animated: true)
                        }
                    }
                    else if post_type == .Advert
                    {
                        if let nav = self.window?.rootViewController as? UINavigationController
                        {
                            var arrNav = nav.viewControllers
                            arrNav.removeAll(where: {$0 is AdvertDetailsVC})
                            let vcInstace = StoryBoard.Advert.instantiateViewController(withIdentifier: "AdvertDetailsVC") as! AdvertDetailsVC
                            arrNav.append(vcInstace)
                            vcInstace.dictPost = model
                            nav.setViewControllers(arrNav, animated: true)
                        }
                    }
                }
            }
                
            else if notification_type == "1"
            {
                if let nav = self.window?.rootViewController as? UINavigationController
                {
                    var arrNav = nav.viewControllers
                    arrNav.removeAll(where: {$0 is FriendsVC})
                    let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "FriendsVC") as! FriendsVC
                    arrNav.append(vcInstace)
                    vcInstace.isFromNoti = true
                    nav.setViewControllers(arrNav, animated: true)
                }
            }
            else if notification_type == "2"//accept friend
            {
                if let nav = self.window?.rootViewController as? UINavigationController
                {
                    var arrNav = nav.viewControllers
                    arrNav.removeAll(where: {$0 is OtherProfileVC})
                    let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "OtherProfileVC") as! OtherProfileVC
                    arrNav.append(vcInstace)
                    vcInstace.profileId = user_id
                    nav.setViewControllers(arrNav, animated: true)
                }
            }
            else if notification_type == "11"//chat
            {
                if let nav = self.window?.rootViewController as? UINavigationController
                {
                    if let dicProfile = userInfo["sender"] as? NSDictionary
                    {
                        
                        let user_id =  toString(dicProfile["user_id"])
                        let name =  toString(dicProfile["firstname"]) + " " +  toString(dicProfile["lastname"])
                        let profile_image =  toString(dicProfile["profile_image"])
                        
                        globalChatUserId = user_id
                        
                        var arrNav = nav.viewControllers
                        arrNav.removeAll(where: {$0 is ChatVC})
                        let vcInstace = StoryBoard.Message.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                        arrNav.append(vcInstace)
                        vcInstace.touserId = user_id
                        vcInstace.profileURL = profile_image
                        vcInstace.name = name
                        nav.setViewControllers(arrNav, animated: true)
                        
                    }
                    
                    
                }
            }
            
            //                   {
            //                       let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "OtherProfileVC") as! OtherProfileVC
            //                       vcInstace.profileId = obj.user_id
            //                       self.navigationController?.pushViewController(vcInstace, animated: true)
            //                   }
            
            //        let application = UIApplication.shared
            
            //        if(application.applicationState == .active)
            //        {
            //            print("user tapped the notification bar when the app is in foreground")
            //
            //        }
            //
            //        if(application.applicationState == .inactive)
            //        {
            //            print("user tapped the notification bar when the app is in background")
            
            //        }
            completionHandler()
        }
    }
    func postDetails(_ post_id: String, completion:((allPostModel) -> Void)?){
        
        var dict = [String: Any]()
        dict["user_id"] = globalUserId
        dict["post_id"] = post_id
        
        WebService.shared.RequesURL(ServerURL.get_post_by_id, Perameters: dict, showProgress: true, completion: { (dictResponse, status) in
            if status{
                //                debugPrint(dictResponse)
                if let dic = dictResponse["detail"] as? [String:Any] {
                    completion?(allPostModel.init(dic))
                }
            }
            else
            {
                
            }
        }) { (error) in
            //            self.showOkAlert(msg: error.localizedDescription)
        }
    }
    
    func application(
        _ application: UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
    ) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        GlobalDeviceToken = tokenParts.joined()
//        self.funUpdateToken()
        print("Device Token: \(GlobalDeviceToken)")
        
    }
    func funUpdateToken(_ serviceCount : Int = 0) {
        
        if !globalUserId.isValid{ return }
        
        let dic = ["user_id" : globalUserId,"device_type":"ios","device_name":ToString(UIDevice.current.name),"device_id":ToString(UIDevice.current.systemVersion)  ,"device_token":GlobalDeviceToken]
        debugPrint(dic)
        WebService.shared.RequesURL(ServerURL.UpdateDeviceToken, Perameters: dic,showProgress: false,completion: { (dicRes, success) in
            debugPrint(dicRes)
        }) { (err) in
            if serviceCount < 1 {
                self.funUpdateToken(serviceCount + 1)
            } else {
                debugPrint(err)
            }
        }
    }
    
    
    
    func application(
        _ application: UIApplication,
        didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func funGetnoticount(completion:(() -> Void)?){
        if !globalUserId.isValid{ return }
        WebService.shared.RequesURL(ServerURL.get_msg_not_fri_counts, Perameters: ["user_id" : globalUserId],showProgress: false,completion: { (dicRes, success) in
            if success{
                if let dic = dicRes["list"] as? [String:Any]
                {
                    global_count_Friend = toString(dic["friend_requests"])
                    global_count_Noti = toString(dic["unread_notifications"])
                    global_count_Msg = toString(dic["unread_messages"])
                    completion?()
                }
            }
            //            debugPrint(dicRes)
        }) { (err) in
        }
    }
    // [START openurl]
    func application(_ application: UIApplication,
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    // [END openurl]
    // [START openurl_new]
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        return  ApplicationDelegate.shared.application(app, open: url, options: options)
    }
   /* func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print ("received :%@",userInfo)
    }*/
    func showAlertGuest(){
         self.window?.rootViewController?.showOkCancelAlertWithAction(msg: Constant.kAlertGuest) { (bool) in
            if bool{
                self.LoadLoginView()
            }
        }
    }
   
}

