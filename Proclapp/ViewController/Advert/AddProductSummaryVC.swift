//
//  AddProductSummaryVC.swift
//  Proclapp
//
//  Created by Prismetric Tech on 9/17/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class AddProductSummaryVC: UIViewController {

//    @IBOutlet weak var viewBottomHeightConst: NSLayoutConstraint!
    @IBOutlet weak var viewBottom: UIView!

    var dictPost : allPostModel?

    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnUpdate: UIButton!

    
//    var getAdvert_id = ""
    var isEdit = false
    
//    var arrImages = [imagePostModel]()

    
    @IBOutlet weak var tblProducSummary: UITableView!
    
    @IBOutlet weak var lblTotolPaybleAmount: UILabel!
    
    var arrCellRow = [String]()
    
    var SummaryCLVImages : SummaryCLVImages!
 
    override func viewDidLoad() {
        super.viewDidLoad()

        arrCellRow.append("Category")
        arrCellRow.append("TitleDesc")
        arrCellRow.append("Images")
        arrCellRow.append("Duration")
        
        tblProducSummary.register(UINib.init(nibName: "SummaryTblTitleCell", bundle: nil), forCellReuseIdentifier: "SummaryTblTitleCell")

        tblProducSummary.register(UINib.init(nibName: "SummaryTitleDesc", bundle: nil), forCellReuseIdentifier: "SummaryTitleDesc")
        
        tblProducSummary.register(UINib.init(nibName: "SummaryCLVImages", bundle: nil), forCellReuseIdentifier: "SummaryCLVImages")
        
        SummaryCLVImages = tblProducSummary.dequeueReusableCell(withIdentifier: "SummaryCLVImages") as? SummaryCLVImages
        
        if isEdit
        {
            
            viewBottom.isHidden = true
//            viewBottomHeightConst.constant = 0
//            self.view.layoutIfNeeded()
            
            
            if let obj = dictPost
            {
                objAdvert?.strTitle = obj.post_title
                objAdvert?.strDesc = obj.post_description
                objAdvert?.strURLlink = obj.advert_url

                objAdvert?.strCaterory = obj.category_label
                objAdvert?.strDuration = obj.time_in_min
                objAdvert?.strDurationId = obj.advert_time_slot_id
                objAdvert?.strAmount = obj.currency + obj.amount
                
               objAdvert?.imageArray = NSMutableArray.init(array: obj.arrImages)

                
//                self.arrImages = obj.arrImages
                self.tblProducSummary.reloadData()
                self.SummaryCLVImages.clvViewImages.reloadData()
            }
            
            /*tblProducSummary.isHidden = true
            self.tblProducSummary.refreshControl = UIRefreshControl()
            self.tblProducSummary.refreshControl?.tintColor = .AppSkyBlue
            self.tblProducSummary.refreshControl?.addTarget(self, action: #selector(refreshCalled), for: UIControl.Event.valueChanged)
            self.getAdvertSummary()
            viewBottom.isHidden = true*/
        }
        

        btnUpdate.isHidden = !isEdit
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnBackPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickUpdate(_ sender: Any)
    {
//        let imageArray = objAdvert?.arrUploadImg ?? [ImageSel]()
        
        var param = Dictionary<String, Any>()

        
        let category = toString(objAdvert?.strCaterory.uppercased() == "Company".uppercased() ? "1" : "2")
        
        let de = toString(objAdvert?.deleteImageIds.componentsJoined(by: ","))
        
         param = ["user_id":globalUserId, "post_title": toString(objAdvert?.strTitle), "post_description": ToString(objAdvert?.strDesc), "advert_time_slot_id": toString(objAdvert?.strDurationId), "category":category,"advert_id":toString(dictPost?.post_id),"delete_image":objAdvert?.deleteImageIds.count == 0 ? "" : de,"url":ToString(objAdvert?.strURLlink)]
        
        let countGet = objAdvert?.imageArray.count ?? 0
        
        var imges = [UIImage]()
        objAdvert?.imageArray.forEach { (obj) in
            if obj is UIImage
            {
                imges.append(obj as! UIImage)
            }
        }
        
        WebService.shared.webRequestForMultipleImages(DictionaryImages: countGet > 0 ? ["post_images": imges] : nil, urlString: ServerURL.UpdateAdvert, Perameters: param, completion: { (dicRes, success) in
            debugPrint(dicRes)
            
            if success == true
            {
                self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                    NotificationCenter.default.post(name: .NewDataLoadAdvert, object:nil)
                    self.btnBackPressed(self)
                    
                })
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
            
        }) { (err) in
            debugPrint(err)
        }
    }
    
    @IBAction func clickPayNow(_ sender: Any)
    {
        self.createOrEditArcticleService()
    }
    
    func createOrEditArcticleService()
    {
        let category = objAdvert?.strCaterory.uppercased() == "Company".uppercased() ? "1" : "2"
        let param = ["user_id":globalUserId, "post_title": ToString(objAdvert?.strTitle), "post_description": ToString(objAdvert?.strDesc), "advert_time_slot_id": toString(objAdvert?.strDurationId), "category":category,"url":ToString(objAdvert?.strURLlink)]
        let countGet = objAdvert?.imageArray.count ?? 0
        var imges = [UIImage]()
        objAdvert?.imageArray.forEach { (obj) in
            if obj is UIImage
            {
                imges.append(obj as! UIImage)
            }
        }
        WebService.shared.webRequestForMultipleImages(DictionaryImages: countGet > 0 ? ["post_images": imges] : nil, urlString: ServerURL.AddAdvert, Perameters: param, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true
            {
                self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                    
                    let vcInstace = StoryBoard.Wallet.instantiateViewController(withIdentifier: "WebviewPaymentVC") as! WebviewPaymentVC
                    vcInstace.strPaymentType = "4"
                    vcInstace.postId = toString(dicRes["advert_id"])
                    vcInstace.callbackPayment = { status in
                        if status == Constant.kPayment_Success
                        {
                            NotificationCenter.default.post(name: .NewDataLoadAdvert, object:nil)
                            
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: AdvertVC.self) {
                                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                        }
                    }
                    self.pushTo(vcInstace)
                    
                })
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
            
        }) { (err) in
            debugPrint(err)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tblProducSummary.reloadData()
        SummaryCLVImages.clvViewImages.reloadData()
        lblTotolPaybleAmount.text = objAdvert?.strAmount
        self.navigationController?.isNavigationBarHidden = true
    }
}


extension AddProductSummaryVC:UITableViewDelegate,UITableViewDataSource {
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrCellRow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.arrCellRow[indexPath.row] == "Category" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SummaryTblTitleCell") as! SummaryTblTitleCell
            cell.lblTitle.text = "Category"
            cell.lblValue.text = objAdvert?.strCaterory
            cell.btnEdit.addTarget(self, action: #selector(btnEdtitCategory), for: .touchUpInside)
            
//            cell.btnEdit.isHidden = isEdit
            
            return cell
        }
        else if self.arrCellRow[indexPath.row] == "TitleDesc" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SummaryTitleDesc") as! SummaryTitleDesc
            
            cell.lblTitleValue.text = objAdvert?.strTitle
            cell.lblDescValue.text =  objAdvert?.strDesc
            cell.btnEdit.addTarget(self, action: #selector(btnEdtitTitle), for: .touchUpInside)
            
//            cell.btnEdit.isHidden = isEdit
            
            return cell
        }
        else if self.arrCellRow[indexPath.row] == "Images" {


           /* if isEdit
            {
                SummaryCLVImages.isFromShow = true
                SummaryCLVImages.arrImagesShow = dictPost?.arrImages ?? [imagePostModel]()
            }
            else
            {
                SummaryCLVImages.isFromShow = false
                SummaryCLVImages.arrImage = objAdvert?.arrUploadImg ?? [ImageSel]()
            }*/
            

            SummaryCLVImages.btnEdit.addTarget(self, action: #selector(btnEdtitImages), for: .touchUpInside)
            
//            SummaryCLVImages.btnEdit.isHidden = isEdit
            
            return SummaryCLVImages
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SummaryTblTitleCell") as! SummaryTblTitleCell
            cell.lblTitle.text = "Duration"
            cell.lblValue.text = toString(objAdvert?.strDuration) + " Min"
            cell.btnEdit.addTarget(self, action: #selector(btnEdtitDuration), for: .touchUpInside)
//            cell.btnEdit.isHidden = isEdit

            return cell
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func btnEdtitCategory() {
        let vc = StoryBoard.Advert.instantiateViewController(withIdentifier: "SelectTwoCategoryVC") as! SelectTwoCategoryVC
        vc.isEditingProd = true
        self.pushTo(vc)
    }
 
    @objc func btnEdtitTitle() {
        let vc = StoryBoard.Advert.instantiateViewController(withIdentifier: "AddProductDesc") as! AddProductDesc
        vc.isEditingProd = true
        self.pushTo(vc)
    }
    
    @objc func btnEdtitImages()
    {
        let vc = StoryBoard.Advert.instantiateViewController(withIdentifier: "AddProductImagesUpload") as! AddProductImagesUpload
//        vc.arrImageSelected  = objAdvert?.arrUploadImg ?? [ImageSel]()
        vc.isEditingProd = true
        self.pushTo(vc)
    }
    
    @objc func btnEdtitDuration() {
        let vc = StoryBoard.Advert.instantiateViewController(withIdentifier: "AddProductDurationVC") as! AddProductDurationVC
        vc.isEditingProd = true
        self.pushTo(vc)
    }
    
    
    /*@objc func refreshCalled() {
        self.getAdvertSummary()
    }
    
    @objc func getAdvertSummary() {
        
        var delayTime = DispatchTime.now()
        
        if self.tblProducSummary.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblProducSummary.refreshControl?.beginRefreshingManually()
        }
        
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.AdvertSummary, Perameters: ["user_id" : globalUserId,"advert_id":self.getAdvert_id],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                    debugPrint(dicRes)
                    
                    if let dicSummary = dicRes["summary"] as? NSDictionary {
                        
                         objAdvert?.strTitle = toString(dicSummary["post_title"])
                        objAdvert?.strDesc = toString(dicSummary["post_description"])
                        
                        objAdvert?.strCaterory = toString(dicSummary["category_label"])

                        objAdvert?.strDuration = toString(dicSummary["time_in_min"])
                        
                        if let arr = dicSummary["images"] as? [[String:Any]] {
                            self.arrImages = arr.map({imagePostModel.init($0)})
                        }
                    }
                    
                }
                else{
                    self.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
                }
                DispatchQueue.main.async {
                    UIView.performWithoutAnimation {
                        self.tblProducSummary.reloadData()
                        self.tblProducSummary.isHidden = false
                        self.SummaryCLVImages.clvViewImages.reloadData()
                    }
                    self.tblProducSummary.refreshControl?.endRefreshing()
                }
            }) { (err) in
                self.tblProducSummary.refreshControl?.endRefreshing()
            }
        }
    }*/
    
}
