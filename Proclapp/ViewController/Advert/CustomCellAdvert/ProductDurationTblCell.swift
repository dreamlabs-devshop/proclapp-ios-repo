//
//  ProductDurationTblCell.swift
//  Proclapp
//
//  Created by Prismetric Tech on 9/16/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class ProductDurationTblCell: UITableViewCell {
    
    
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var lblMinute: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
