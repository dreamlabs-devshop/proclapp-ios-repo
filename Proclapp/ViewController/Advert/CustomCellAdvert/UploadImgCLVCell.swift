//
//  UploadImgCLVCell.swift
//  Proclapp
//
//  Created by Prismetric Tech on 9/16/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class UploadImgCLVCell: UICollectionViewCell {

    @IBOutlet weak var imageSelected: UIImageView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnAdd: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
