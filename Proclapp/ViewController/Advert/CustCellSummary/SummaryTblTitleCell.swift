//
//  SummaryTblTitleCell.swift
//  Proclapp
//
//  Created by Prismetric Tech on 9/17/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class SummaryTblTitleCell: UITableViewCell {

    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
