//
//  SummaryCLVImages.swift
//  Proclapp
//
//  Created by Prismetric Tech on 9/17/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class SummaryCLVImages: UITableViewCell {

    @IBOutlet weak var clvViewImages: UICollectionView!
    @IBOutlet weak var btnEdit: UIButton!
    
    var isFromShow = false
    
//    var arrImage = [ImageSel]()
    
//    var arrImagesShow = [imagePostModel]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clvViewImages.register(UINib.init(nibName: "UploadImgCLVCell", bundle: nil), forCellWithReuseIdentifier: "UploadImgCLVCell")
        
        clvViewImages.delegate = self
        clvViewImages.dataSource = self
        clvViewImages.reloadData()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


extension SummaryCLVImages : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
    
        return objAdvert?.imageArray.count ?? 0
        
        //return isFromShow == false ? self.arrImage.count : self.arrImagesShow.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "UploadImgCLVCell", for: indexPath) as! UploadImgCLVCell
        
        cell.btnClose.isHidden = true
        cell.btnAdd.isHidden = true
        
        let obj = objAdvert?.imageArray[indexPath.row] as Any
        
        if obj is imagePostModel
        {
            cell.imageSelected.setImageWithURL((obj as! imagePostModel).image_name, "place_logo")
        }
        else if obj is UIImage
        {
            cell.imageSelected.image = (objAdvert?.imageArray[indexPath.row] as! UIImage)
        }
        
       /* if isFromShow
        {
            cell.imageSelected.setImageWithURL(arrImagesShow[indexPath.row].image_name, "place_logo")
            
        }
        else
        {
            cell.imageSelected.image = self.arrImage[indexPath.row].imageSeleced

        }*/
        
         cell.imageSelected.setborder(0.5, _color: UIColor.AppBlack230)

        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.height, height: collectionView.frame.height)
    }
    
    
    
}
