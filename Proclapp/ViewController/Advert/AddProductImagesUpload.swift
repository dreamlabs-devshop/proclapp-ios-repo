//
//  AddProductImagesUpload.swift
//  Proclapp
//
//  Created by Prismetric Tech on 9/16/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import CropViewController

class AddProductImagesUpload: UIViewController {

    var imagePicker : UIImagePickerController!

    
    @IBOutlet weak var clvUploadImages: UICollectionView!
    
    var isEditingProd: Bool = false
    
    @IBOutlet weak var btnNext: MSButton_Blue!
    
//    var arrImageSelected = [ImageSel]()
    
    var arrEdit = [imagePostModel]()

    var pickerView: UIPickerView!
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        DispatchQueue.main.async {
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.allowsEditing = false
        }

//        let objAddBtn = ImageSel()
//        objAddBtn.imageID = "0"
//        self.arrImageSelected.append(objAddBtn)
        
        
        if isEditingProd == true
        {
//            self.arrImageSelected  = objAdvert?.arrUploadImg ?? [ImageSel]()
            btnNext.setTitle("Done", for: .normal)
            self.clvUploadImages.reloadData()
        }
        
        // Do any additional setup after loading the view.
        clvUploadImages.register(UINib.init(nibName: "UploadImgCLVCell", bundle: nil), forCellWithReuseIdentifier: "UploadImgCLVCell")

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.clvUploadImages.reloadData()
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
//         objAdvert.arrUploadImg = [ImageSel]()
        if isEditingProd
        {
            if validationLogin
            {
                self.navigationController?.popViewController(animated: true)
            }
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    var validationLogin: Bool{
        
        self.view.endEditing(true)
        
        var mess = ""
        
        if objAdvert?.imageArray.count == 0
        {
            mess = "Please upload a image"
        }
        
        
        if mess != ""{
            self.showOkAlert(msg: mess)
            return false
        }
        return true
    }
    
    @IBAction func btnNextPressed(_ sender: Any) {
        
//        objAdvert?.arrUploadImg.removeAll()
        
//        objAdvert?.arrUploadImg = self.arrImageSelected
        
        if validationLogin
        {
            if isEditingProd {
                self.navigationController?.popViewController(animated: true)
            }
            else {
                let vc = StoryBoard.Advert.instantiateViewController(withIdentifier: "AddProductDurationVC") as! AddProductDurationVC
                self.pushTo(vc)
            }
        }
 
    }
    
    
    @objc func PickImage(){
        DispatchQueue.main.async {
            let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let camera = UIAlertAction.init(title: "Camera", style: .default) { _ in
                self.openImagePicker(type: .camera)
            }
            
            let photos = UIAlertAction.init(title: "Photos", style: .default) { _ in
                self.openImagePicker(type: .photoLibrary)
            }
            
            let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(camera)
            alert.addAction(photos)
            alert.addAction(cancel)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}


extension AddProductImagesUpload: UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate{
    
    func openImagePicker(type: UIImagePickerController.SourceType){
        DispatchQueue.main.async {
            self.imagePicker.sourceType = type
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    /*func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            
            objAdvert?.imageArray.add(image)

//            let addImage = ImageSel()
//            addImage.imageSeleced = image
//            self.arrImageSelected.insert(addImage, at:0)
            self.clvUploadImages.reloadData()
        }
        self.dismiss(animated: true, completion: nil)
    }*/

        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            guard let image = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) else { return }
            
            let cropController = CropViewController(croppingStyle: .default, image: image)
            //cropController.modalPresentationStyle = .fullScreen
            cropController.delegate = self
    //        self.image = image
            picker.dismiss(animated: true, completion: {
                self.present(cropController, animated: true, completion: nil)
                //self.navigationController!.pushViewController(cropController, animated: true)
            })
        }
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        //        self.croppedRect = cropRect
        //        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        //        self.croppedRect = cropRect
        //        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        
        cropViewController.dismiss(animated: true, completion:{
            objAdvert?.imageArray.add(image)
            self.clvUploadImages.reloadData()
        })
    }

    
}

extension AddProductImagesUpload : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if  let countGet =  objAdvert?.imageArray.count
        {
            return countGet + 1
        }
        return  1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "UploadImgCLVCell", for: indexPath) as! UploadImgCLVCell
        
        
        
        if  let countGet =  objAdvert?.imageArray.count
        {
            if (countGet - 1) >= indexPath.row
            {

                let obj = objAdvert?.imageArray[indexPath.row] as Any
                
                if obj is imagePostModel
                {
                    cell.imageSelected.setImageWithURL((obj as! imagePostModel).image_name, "place_logo")
                }
                else if obj is UIImage
                {
                    cell.imageSelected.image = (objAdvert?.imageArray[indexPath.row] as! UIImage)
                }
                
                cell.btnAdd.isHidden = true
                cell.btnClose.isHidden = false
                
                cell.btnClose.addTarget(self, action: #selector(btnRemoveImage(_:)), for: .touchUpInside)
                
            }
            else {
                
                cell.btnAdd.isHidden = false
                cell.btnClose.isHidden = true
                
                cell.imageSelected.image = nil
                
            }
            
        }
        
 
        
        
        
        cell.btnAdd.addTarget(self, action: #selector(PickImage), for: .touchUpInside)
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.height, height: collectionView.frame.height)
    }
 
    //MARK:- btnRemoveImage
    @objc func btnRemoveImage(_ sender:UIButton) {
        
        
         if let indexPath = self.clvUploadImages?.indexPathForView_Collection(sender){
        
            DispatchQueue.main.async {
                let alert = UIAlertController.init(title: nil, message: "Are you sure want to delete.", preferredStyle: .actionSheet)
                
                let camera = UIAlertAction.init(title: "Yes", style: .default) { _ in
                    
                    let obj = objAdvert?.imageArray[indexPath.row] as Any
                    
                    if obj is imagePostModel
                    {
                        objAdvert?.deleteImageIds.add((obj as! imagePostModel).post_img_id)
                    }
                    
                    objAdvert?.imageArray.removeObject(at: indexPath.row)
//                    self.colview.reloadData()
                    
//                    self.arrImageSelected.remove(at: indexPath.row)
                    
                    DispatchQueue.main.async {
                        self.clvUploadImages.reloadData()
                    }
                }
                
                let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
                
                alert.addAction(camera)
                alert.addAction(cancel)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        
        
    }
}


class ImageSel {
    var imageSeleced:UIImage? = nil
    var imageID:String = "0"
}
