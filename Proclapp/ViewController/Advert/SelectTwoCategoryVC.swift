
//
//  SelectTwoCategoryVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/26/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class SelectTwoCategoryVC: UIViewController {
    
    @IBOutlet var btnCompany: UIButton!
    @IBOutlet var btnProduct: UIButton!
    var isEditingProd: Bool = false
    @IBOutlet weak var btnNext: MSButton_Blue!
     
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupData()
        
//        btnCompany.isSelected = true
//        objAdvert.strCaterory = "company"

        if isEditingProd == true
        {
            btnNext.setTitle("Done", for: .normal)
        }
        else
        {
            objAdvert = Advert()
        }
        // Do any additional setup after loading the view.
    }
    
    func setupData()
    {
        btnCompany.isSelected = false
        btnProduct.isSelected = false

        btnProduct.isSelected = objAdvert?.strCaterory == "Product"
         btnCompany.isSelected = !btnProduct.isSelected
    }
    
    @IBAction func TapSelectCategory(_ sender: UIButton) {
        btnCompany.isSelected = false
        btnProduct.isSelected = false
        sender.isSelected = true
    }
    @IBAction func clickNext(_ sender: Any)
    {
        objAdvert?.strCaterory = btnCompany.isSelected == true ? "Company" : "Product"
        
        if isEditingProd {
            self.navigationController?.popViewController(animated: true)
        }
        else {
            
            let vc = StoryBoard.Advert.instantiateViewController(withIdentifier: "AddProductDesc") as! AddProductDesc
           
            self.pushTo(vc)
        }
        
//        btnCompany.isSelected == true ? pushTo(VoiceRecordListVC.vcInstace) : pushTo(VideoRecordListVC.vcInstace)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

class Advert {
    
    var strCaterory:String = ""
    var strTitle:String = ""
    var strDesc:String = ""
    var strURLlink:String = ""

//    var arrUploadImg = [ImageSel]()
    var strDuration:String = ""
    var strDurationId:String = "0"
    var strAmount:String = "$0"
    
    var deleteImageIds = NSMutableArray()
    var imageArray = NSMutableArray()


}
