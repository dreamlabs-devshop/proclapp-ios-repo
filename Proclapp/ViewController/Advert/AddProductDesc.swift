//
//  AddProductDesc.swift
//  Proclapp
//
//  Created by Prismetric Tech on 9/16/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import SafariServices
import SwiftLinkPreview
import CropViewController

class AddProductDesc: UIViewController {
    
    var imagePicker : UIImagePickerController!

        
    @IBOutlet weak var clvUploadImages: UICollectionView!
    
    var arrEdit = [imagePostModel]()

    var pickerView: UIPickerView!

    
    @IBOutlet weak var txtFieldTitle: MSTextField_DropDown!
    
    @IBOutlet weak var viewRounded: UIView!
    
    @IBOutlet weak var txtViewDesc: UITextView!
    
    @IBOutlet weak var btnNext: MSButton_Blue!
    
    var isEditingProd: Bool = false
    let defultTextDes = "Add description..."
     

    var textField: UITextField?
    var strPostLink = ""
    private let slp = SwiftLinkPreview(cache: InMemoryCache())
    
    @IBOutlet private weak var previewTitle: UILabel?
    @IBOutlet private weak var previewCanonicalUrl: UILabel?
    @IBOutlet private weak var previewDescription: UILabel?
    @IBOutlet private weak var detailedView: UIView?
    @IBOutlet private weak var favicon: UIImageView?
    @IBOutlet private weak var indiCator: UIActivityIndicatorView?

    override func viewDidLoad() {
        super.viewDidLoad()

        detailedView?.isHidden = true
        indiCator?.stopAnimating()
        detailedView?.applyDropShadow()
        
        DispatchQueue.main.async {
            self.viewRounded.setborder(1.0
                , _color: UIColor.AppBlack230)
            self.viewRounded.addShadow(radius: 3.0,corner: 8.0)
//            stackView.spacing = 30 * screenscale
        }
        
         DispatchQueue.main.async {
            self.imagePicker = UIImagePickerController()
            self.imagePicker.delegate = self
            self.imagePicker.allowsEditing = false
        }
        clvUploadImages.dataSource = self;
        clvUploadImages.delegate = self;

       
        // Do any additional setup after loading the view.
        clvUploadImages.register(UINib.init(nibName: "UploadImgCLVCell", bundle: nil), forCellWithReuseIdentifier: "UploadImgCLVCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        txtFieldTitle.text =  objAdvert?.strTitle
        txtViewDesc.text = objAdvert?.strDesc
        
        if ToString(txtViewDesc.text).isBlank || ToString(txtViewDesc.text) == defultTextDes
        {
            txtViewDesc.text = defultTextDes
            txtViewDesc.textColor = UIColor.lightGray
        }
        else
        {
            txtViewDesc.textColor = UIColor.Appcolor51
        }
        
        if isEditingProd == true
        {
            btnNext.setTitle("Done", for: .normal)
        }
        
        if objAdvert?.strURLlink.isEmpty == false
        {
            self.showPreview(toString(objAdvert?.strURLlink))
        }

        self.clvUploadImages.reloadData()
        
    }
    
  
    
    
    
 
    @IBAction func btnBackPressed(_ sender: Any) {
//        objAdvert.strTitle = ""
//        objAdvert.strDesc = ""
        self.navigationController?.popViewController(animated: true)
    }
    
    
    var validationLogin: Bool{
        
        self.view.endEditing(true)
        
        var mess = ""
        
        if ToString(txtFieldTitle.text).isBlank {
            mess = "Please enter a title"
        }
        else if ToString(txtViewDesc.text).isBlank || ToString(txtViewDesc.text) == defultTextDes {
            mess = "Please enter a description"
        }
        
        else if objAdvert?.imageArray.count == 0
        {
            mess = "Please upload a image"
        }
        
        if mess != ""{
            self.showOkAlert(msg: mess)
            return false
        }
        return true
    }
    
    
    //MARK:- btnNextPressed
    @IBAction func btnNextPressed(_ sender: Any) {
        
        objAdvert?.strTitle = self.txtFieldTitle.text ?? ""
        objAdvert?.strDesc = self.txtViewDesc.text ?? ""
        
        if validationLogin {
            
            if isEditingProd {
                self.navigationController?.popViewController(animated: true)
            }
            else {
//                let vc = StoryBoard.Advert.instantiateViewController(withIdentifier: "AddProductImagesUpload") as! AddProductImagesUpload
//
//                self.pushTo(vc)
                let vc = StoryBoard.Advert.instantiateViewController(withIdentifier: "AddProductDurationVC") as! AddProductDurationVC
                self.pushTo(vc)
            }
        }
        
    }
    
    @objc func PickImage(){
        
        objAdvert?.strTitle = self.txtFieldTitle.text ?? ""
        objAdvert?.strDesc = self.txtViewDesc.text ?? ""
        
        DispatchQueue.main.async {
            let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let camera = UIAlertAction.init(title: "Camera", style: .default) { _ in
                self.openImagePicker(type: .camera)
            }
            
            let photos = UIAlertAction.init(title: "Photos", style: .default) { _ in
                self.openImagePicker(type: .photoLibrary)
            }
            
            let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(camera)
            alert.addAction(photos)
            alert.addAction(cancel)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
}



extension  AddProductDesc : UITextViewDelegate
{
    //MARK: - UITextview Delegate Methods
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == defultTextDes {
            txtViewDesc.text = nil
        }
        txtViewDesc.textColor = UIColor.Appcolor51
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text.isEmpty {
            txtViewDesc.textColor = UIColor.lightGray
            txtViewDesc.text = defultTextDes
        }
    }
}
extension AddProductDesc
{
    @IBAction func clickLink(_ sender: UIButton) {
        
        openAlertView()
    }
    func configurationTextField(textField: UITextField!) {
        if (textField) != nil {
            self.textField = textField!
            //Save reference to the UITextField
            self.textField?.placeholder = "Enter a link"
            self.textField?.keyboardType = .URL
        }
    }
    
    func openAlertView() {
        let alert = UIAlertController(title: Constant.appName, message: nil, preferredStyle: .alert)
        alert.addTextField(configurationHandler: configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler:{ (UIAlertAction) in
            self.showPreview(toString(self.textField?.text))
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showPreview(_ linkURL:String) {
        
        if !checkValidString(linkURL)
        {
            return
        }
        
        self.detailedView?.isHidden = true
        indiCator?.startAnimating()
        
        func setDataPreview(_ result: Response) {
            print("url: ", result.url ?? "no url")
            print("finalUrl: ", result.finalUrl ?? "no finalUrl")
            print("canonicalUrl: ", result.canonicalUrl ?? "no canonicalUrl")
            print("title: ", result.title ?? "no title")
            print("images: ", result.images ?? "no images")
            print("image: ", result.image ?? "no image")
            print("video: ", result.video ?? "no video")
            print("icon: ", result.icon ?? "no icon")
            print("description: ", result.description ?? "no description")
            
            self.previewTitle?.text = toString(result.title)
            self.previewCanonicalUrl?.text = toString(result.finalUrl)
            self.previewDescription?.text = toString(result.description)
            
            if let icon = result.image
            {
                self.favicon?.setImageWithURL(toString(icon), "logo_small")
            }
            else
            {
                self.favicon?.setImageWithURL(toString(result.icon), "logo_small")
            }
            
            self.detailedView?.isHidden = false
            self.indiCator?.stopAnimating()
            self.strPostLink =  self.previewCanonicalUrl?.text ?? ""
            
            objAdvert?.strURLlink =  self.strPostLink
            
        }
        
        self.slp.preview(
            linkURL,
            onSuccess: { result in
                setDataPreview(result)
                //                self.result = result
        },
            onError: { error in
                print(error)
                self.showOkAlert(msg: "Invalid URL!")
                self.indiCator?.stopAnimating()
        }
        )
    }
    @IBAction func openWithAction(_ sender: UIButton) {
        if strPostLink.isEmpty == false{
            openURL(URL.init(string: strPostLink)!)
        }
    }
    func openURL(_ url: URL) {
        if ["http", "https"].contains(url.scheme?.lowercased() ?? "") {
            self.present(SFSafariViewController(url: url), animated: true, completion: nil)
        } else {
            linkurlOpen_MS(linkurl: url.absoluteString)
        }
    }
    @IBAction func closePriview(_ sender: UIButton) {
        strPostLink = ""
        detailedView?.isHidden = true
        objAdvert?.strURLlink =  self.strPostLink
    }
    
}

extension AddProductDesc: UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate{
    
    func openImagePicker(type: UIImagePickerController.SourceType){
        DispatchQueue.main.async {
            self.imagePicker.sourceType = type
            self.present(self.imagePicker, animated: true, completion: nil)
        }
    }
    
    /*func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            
            objAdvert?.imageArray.add(image)

//            let addImage = ImageSel()
//            addImage.imageSeleced = image
//            self.arrImageSelected.insert(addImage, at:0)
            self.clvUploadImages.reloadData()
        }
        self.dismiss(animated: true, completion: nil)
    }*/

        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            guard let image = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) else { return }
            
            let cropController = CropViewController(croppingStyle: .default, image: image)
            //cropController.modalPresentationStyle = .fullScreen
            cropController.delegate = self
    //        self.image = image
            picker.dismiss(animated: true, completion: {
                self.present(cropController, animated: true, completion: nil)
                //self.navigationController!.pushViewController(cropController, animated: true)
            })
        }
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        //        self.croppedRect = cropRect
        //        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        //        self.croppedRect = cropRect
        //        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        
        cropViewController.dismiss(animated: true, completion:{
            objAdvert?.imageArray.add(image)
            self.clvUploadImages.reloadData()
        })
    }

    
}

extension AddProductDesc : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if  let countGet =  objAdvert?.imageArray.count
        {
            return countGet + 1
        }
        return  1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "UploadImgCLVCell", for: indexPath) as! UploadImgCLVCell
        
        
        
        if  let countGet =  objAdvert?.imageArray.count
        {
            if (countGet - 1) >= indexPath.row
            {

                let obj = objAdvert?.imageArray[indexPath.row] as Any
                
                if obj is imagePostModel
                {
                    cell.imageSelected.setImageWithURL((obj as! imagePostModel).image_name, "place_logo")
                }
                else if obj is UIImage
                {
                    cell.imageSelected.image = (objAdvert?.imageArray[indexPath.row] as! UIImage)
                }
                
                cell.btnAdd.isHidden = true
                cell.btnClose.isHidden = false
                
                cell.btnClose.addTarget(self, action: #selector(btnRemoveImage(_:)), for: .touchUpInside)
                
            }
            else {
                
                cell.btnAdd.isHidden = false
                cell.btnClose.isHidden = true
                
                cell.imageSelected.image = nil
                
            }
            
        }
        
 
        
        
        
        cell.btnAdd.addTarget(self, action: #selector(PickImage), for: .touchUpInside)
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.height, height: collectionView.frame.height)
    }
 
    //MARK:- btnRemoveImage
    @objc func btnRemoveImage(_ sender:UIButton) {
        
        
         if let indexPath = self.clvUploadImages?.indexPathForView_Collection(sender){
        
            DispatchQueue.main.async {
                let alert = UIAlertController.init(title: nil, message: "Are you sure want to delete.", preferredStyle: .actionSheet)
                
                let camera = UIAlertAction.init(title: "Yes", style: .default) { _ in
                    
                    let obj = objAdvert?.imageArray[indexPath.row] as Any
                    
                    if obj is imagePostModel
                    {
                        objAdvert?.deleteImageIds.add((obj as! imagePostModel).post_img_id)
                    }
                    
                    objAdvert?.imageArray.removeObject(at: indexPath.row)
//                    self.colview.reloadData()
                    
//                    self.arrImageSelected.remove(at: indexPath.row)
                    
                    DispatchQueue.main.async {
                        self.clvUploadImages.reloadData()
                    }
                }
                
                let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
                
                alert.addAction(camera)
                alert.addAction(cancel)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        
        
    }
}


//class ImageSel {
//    var imageSeleced:UIImage? = nil
//    var imageID:String = "0"
//}

