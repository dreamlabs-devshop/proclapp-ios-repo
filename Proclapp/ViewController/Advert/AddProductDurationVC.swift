//
//  AddProductDurationVC.swift
//  Proclapp
//
//  Created by Prismetric Tech on 9/16/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import SVProgressHUD

class AddProductDurationVC: UIViewController {

    @IBOutlet weak var tblDuration: UITableView!
    @IBOutlet weak var lblPayableAmount: UILabel!
    
    @IBOutlet weak var btnNext: MSButton_Blue!
    var isEditingProd: Bool = false
    var strOffSet:String = "0"
    var arrTimeSlot = [TimeSlot]()
 
    var isMore_Home = false
    @IBOutlet weak var indicator_Home: UIActivityIndicatorView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isEditingProd == true
        {
            btnNext.setTitle("Done", for: .normal)
            
        }
        else
        {
//               lblPayableAmount.text = "$0"
        }
        lblPayableAmount.text = objAdvert?.strAmount

        
        self.tblDuration.estimatedRowHeight = 50
        self.tblDuration.rowHeight = UITableView.automaticDimension
        
        self.tblDuration.sectionHeaderHeight =  UITableView.automaticDimension
        self.tblDuration.estimatedSectionHeaderHeight = 50;
        
        tblDuration.register(UINib.init(nibName: "ProductDurationTblCell", bundle: nil), forCellReuseIdentifier: "ProductDurationTblCell")
        
        tblDuration.register(UINib.init(nibName: "ProductDurationSecCell", bundle: nil), forCellReuseIdentifier: "ProductDurationSecCell")
        
      
        
        self.indicator_Home.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tblDuration.bounds.width, height: CGFloat(30))
        self.tblDuration.tableFooterView = self.indicator_Home
        
        self.tblDuration.refreshControl = UIRefreshControl()
        self.tblDuration.refreshControl?.tintColor = .AppSkyBlue
        self.tblDuration.refreshControl?.addTarget(self, action: #selector(refreshCalled_AllPost), for: UIControl.Event.valueChanged)
        self.getAllPost()
    }
    
 
    @IBAction func btnBackPressed(_ sender: Any) {
//        objAdvert.strDurationId = ""
//        objAdvert.strDuration = ""
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnNextPressed(_ sender: Any) {
        
        if lblPayableAmount.text  == "$0" {
            return
        }

        
        if isEditingProd {
            self.navigationController?.popViewController(animated: true)
        }
        else {
            
            let vc = StoryBoard.Advert.instantiateViewController(withIdentifier: "AddProductSummaryVC") as! AddProductSummaryVC
            
            self.pushTo(vc)
        }
    }
    
    
    //MARK:- JasonToCallTimeSlot
    @objc func refreshCalled_AllPost() {
        self.getAllPost()
    }
    
    @objc func getAllPost(_ offset : Int = 0) {
        
        var delayTime = DispatchTime.now()
        
        if self.tblDuration.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblDuration.refreshControl?.beginRefreshingManually()
        }
        
        debugPrint("Offset= \(offset)")
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.AdvertTimeSlot, Perameters: ["user_id" : globalUserId,"offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                    debugPrint(dicRes)
                    
                    
                    if let arrData = dicRes["slot_list"] as? [[String:Any]] {
                        
                        if offset == 0{
                            self.arrTimeSlot = arrData.map({TimeSlot.init($0)})
                        }
                        else
                        {
                            self.arrTimeSlot.append(contentsOf: arrData.map({TimeSlot.init($0)}))
                        }
                    }
              
                    
                }
                DispatchQueue.main.async {
                    self.isMore_Home = self.arrTimeSlot.count >= ToInt(dicRes["offset"])
                    
                    debugPrint("isAvailableMoreData= \(self.isMore_Home)")
                    
                    debugPrint("arr= \(self.arrTimeSlot.count)")
                    
                    debugPrint("toint= \(ToInt(dicRes["offset"]))")
                    
                    
                    UIView.performWithoutAnimation {
                        self.tblDuration.reloadData()
                    }
                    self.tblDuration.accessibilityHint = nil
                    self.tblDuration.refreshControl?.endRefreshing()
                    self.indicator_Home.stopAnimating()
                }
            }) { (err) in
                self.tblDuration.accessibilityHint = nil
                self.tblDuration.refreshControl?.endRefreshing()
                self.indicator_Home.stopAnimating()
            }
        }
    }
    
}


extension AddProductDurationVC:UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductDurationSecCell") as! ProductDurationSecCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
         return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTimeSlot.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductDurationTblCell") as! ProductDurationTblCell
        
        let dicTime = arrTimeSlot[indexPath.row]
        
        cell.lblDays.text = dicTime.no_of_days
        cell.lblMinute.text = dicTime.time_in_min
        cell.lblAmount.text = "$" + dicTime.amount
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(btnSelectTimeSlot(_:)), for: .touchUpInside)
        
        objAdvert?.strDurationId == dicTime.advert_time_slot_id ? (cell.btnSelect.isSelected = true):(cell.btnSelect.isSelected = false)
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
   /* func scrollViewDidEndDragging(_ aScrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        let offset: CGPoint = aScrollView.contentOffset
        let bounds: CGRect = aScrollView.bounds
        let size: CGSize = aScrollView.contentSize
        let inset: UIEdgeInsets = aScrollView.contentInset
        let y = Float(offset.y + bounds.size.height - inset.bottom)
        let h = Float(size.height)
        let reload_distance: Float = 0
        //            print("load more data!!!!!")
        if y > h + reload_distance{
            
            if aScrollView == self.tblDuration
            {
                if self.isMore_Home == true , self.tblDuration.accessibilityHint == nil{
                    
                    self.indicator_Home.startAnimating()
                    self.tblDuration.accessibilityHint = "service_calling"
                    self.getAllPost(self.arrTimeSlot.count)
                }
            }
            
        }
    }*/
    
    //MARK:- btnSelectTimeSlot
    @objc func btnSelectTimeSlot(_ sender:UIButton){
        
        objAdvert?.strDurationId = toString(arrTimeSlot[sender.tag].advert_time_slot_id)
        objAdvert?.strDuration = toString(arrTimeSlot[sender.tag].time_in_min)
        
        objAdvert?.strAmount = "$" + toString(arrTimeSlot[sender.tag].amount)
        lblPayableAmount.text = objAdvert?.strAmount

        
        self.tblDuration.reloadData()
    }
    
}


class TimeSlot {
    var advert_time_slot_id = "0"
    var amount = "0"
    var no_of_days = "0"
    var time_in_min = "0"
    
    init(_ dict : [String:Any]) {
        
        self.advert_time_slot_id =  toString(dict["advert_time_slot_id"])
        self.amount = toString(dict["amount"])
        self.time_in_min = toString(dict["time_in_min"])
        self.no_of_days = toString(dict["no_of_days"])
        
     }
    
}


