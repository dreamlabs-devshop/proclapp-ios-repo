

//
//  AdvertVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/4/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class AdvertDetailsVC: UIViewController {
    
    @IBOutlet weak var tblAdvert: UITableView!
    
    var dictPost : allPostModel?

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var isMore = false
    
    var arrAnswer = [AnswerListModel]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
        tblAdvert.register(UINib.init(nibName: "AdvertTableCell", bundle: nil), forCellReuseIdentifier: "AdvertTableCell")
        tblAdvert.estimatedRowHeight = 100
        tblAdvert.rowHeight = UITableView.automaticDimension
        
        getAnswerListWS()

        NotificationCenter.default.addObserver(self, selector: #selector(self.AnswerPost), name: .AnswerPost, object: nil)
    }
    
    @objc func AnswerPost(notification: NSNotification){
        DispatchQueue.main.async {
            self.getAnswerListWS()
            debugPrint("!!!!!!!! AnswerPost !!!!!!!!!!")
        }
    }
    
    @IBAction func clickAnswer(_ sender: Any) {
        let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerVC") as! AnswerVC
        vcInstace.dictPost = dictPost
        self.present(vcInstace, animated: true, completion: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true

    }
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func clickFilter(_ sender: Any) {
        
        
        DispatchQueue.main.async {
            guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
            popupVC.arrTitle = Constant.karrFilter
            popupVC.callback =  { str in
                debugPrint(str)
                self.getAnswerListWS(str)
            }
            self.present(popupVC, animated: true, completion: nil)
        }
        
    }
    //MARK: - Web service
    
    func getAnswerListWS(_ strGet : String = "") {
        var passdata = "3"
        if strGet.lowercased() == "User".lowercased(){
            passdata = "4"
        }
        else if strGet.lowercased() == "Views".lowercased() {
            passdata = "2"
        }
        
        if self.tblAdvert.accessibilityHint == "service_calling" { return }
        
        self.tblAdvert.accessibilityHint = "service_calling"
        var delayTime = DispatchTime.now()
        
        if self.tblAdvert.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblAdvert.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.AnswerList, Perameters: ["user_id" : globalUserId,"post_id":toString(self.dictPost?.post_id),"sort_by":passdata],showProgress: false,completion: { (dicRes, success) in
                debugPrint(dicRes)
                if success == true{
                    if let arrTemp = dicRes["answer_list"] as? [[String:Any]] {
                        self.arrAnswer = arrTemp.map({AnswerListModel.init($0)})
                    }
                    
                    self.dictPost?.total_answers = ToInt(self.arrAnswer.count)
                }
                DispatchQueue.main.async {
                    UIView.performWithoutAnimation {
                        self.tblAdvert.reloadData()
                    }
                    self.tblAdvert.accessibilityHint = nil
                    self.tblAdvert.refreshControl?.endRefreshing()
                }
                
                
            }) { (err) in
                debugPrint(err)
                self.tblAdvert.accessibilityHint = nil
                self.tblAdvert.refreshControl?.endRefreshing()
            }
        }
        
    }
    
}

extension AdvertDetailsVC:UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return section == 0 ? 1 : self.arrAnswer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdvertTableCell") as! AdvertTableCell
            
            cell.model = dictPost
            cell.lblDesc.numberOfLines = 0
            cell.lblDesc.lineBreakMode = .byWordWrapping
            
            cell.clvAdvertPhoto.accessibilityHint = ToString(indexPath.row)
            
            cell.setUpCollection()
            
            return cell
        }
            
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSeeAnswer") as! cellSeeAnswer
            
//            cell.model = arrAnswer[indexPath.row]
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return indexPath.section == 0 ? UITableView.automaticDimension : 68 * screenscale
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellAnswerHeader") as! cellAnswerHeader
        cell.lblTitle.text = self.arrAnswer.count > 1 ? "\(self.arrAnswer.count) ANSWERS" : "\(self.arrAnswer.count) ANSWER"
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ?  CGFloat.leastNormalMagnitude : 32 * screenscale
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       /* objAdvert = nil
        objAdvert = Advert()
        
        let vc = StoryBoard.Advert.instantiateViewController(withIdentifier: "AddProductSummaryVC") as! AddProductSummaryVC
        vc.getAdvertModel = arrAdvert[indexPath.row]
        vc.getAdvert_id = arrAdvert[indexPath.row].post_id
        vc.isDetail = true
        self.pushTo(vc)*/
    }
    func webCall_deleteDiscuss(_ delete_answers_id:String ,callback: (()->())?){
        WebService.shared.RequesURL(ServerURL.delete_answer, Perameters: ["user_id":globalUserId,"post_answers_id":delete_answers_id],showProgress: true, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true
            {
                callback?()
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
    
}

