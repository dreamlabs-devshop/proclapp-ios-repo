

//
//  OtherProfileVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/16/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import OnlyPictures
import ImageViewer_swift

class OtherProfileVC: UIViewController {
    
    @IBOutlet weak var tblview: UITableView!
    
    @IBOutlet weak var coverImg: UIImageView!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!

    var isAvailavbleMoreDat = false
    
    var profileId = ""
    
    var arrALLHomePost = [allPostModel]()

    var cellTag : TagListTableViewCell?
    
    var objOtherProfile : OtherUserProfileModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.Delete_Post), name: .Delete_Post, object: nil)

        
//        NotificationCenter.default.addObserver(self, selector: #selector(self.DeclineQuestion), name: .DeclineQuestion, object: nil)

//              NotificationCenter.default.addObserver(self, selector: #selector(self.DeclineArticle), name: .DeclineArticle, object: nil)
        
//        tblview.register(UINib.init(nibName: "AnswerTableCell", bundle: nil), forCellReuseIdentifier: "AnswerTableCell")
        
        tblview.register(UINib.init(nibName: "downloadPDFTableCell", bundle: nil), forCellReuseIdentifier: "downloadPDFTableCell")
        
        tblview.register(UINib.init(nibName: "QuestionAnswerTableViewCell", bundle: nil), forCellReuseIdentifier: "QuestionAnswerTableViewCell")
        
        tblview.register(UINib.init(nibName: "ArticleDeclineTableCell", bundle: nil), forCellReuseIdentifier: "ArticleDeclineTableCell")
        
        tblview.register(UINib.init(nibName: "AudioTableViewCell", bundle: nil), forCellReuseIdentifier: "AudioTableViewCell")

        tblview.register(UINib.init(nibName: "VideoTableViewCell", bundle: nil), forCellReuseIdentifier: "VideoTableViewCell")

        
        
        cellTag = tblview.dequeueReusableCell(withIdentifier: "TagListTableViewCell") as? TagListTableViewCell
        
        let RegularFont = UIFont.FontLatoRegular(size: 13 * screenscale )
        cellTag?.sTagListView.textFont = RegularFont
        cellTag?.sTagListView.alignment = .left
        cellTag?.sTagListView.removeAllTags()
        
//        webCall_getOtheruserProfile()
        
        self.tblview.refreshControl = UIRefreshControl()
        self.tblview.refreshControl?.tintColor = .AppSkyBlue
        self.tblview.refreshControl?.addTarget(self, action: #selector(refreshCalled), for: UIControl.Event.valueChanged)
        
        self.refreshCalled()
        
        DispatchQueue.main.async {
            self.indicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tblview.bounds.width, height: CGFloat(30))
            self.tblview.tableFooterView = self.indicator
        }

        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        self.navigationController?.isNavigationBarHidden = false
//    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
        if let gesture = navigationController?.interactivePopGestureRecognizer
        {
            view.addGestureRecognizer(gesture)
        }
    }
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
    }
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func clickEdit(_ sender: Any) {

//        var vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC

        
    }
    @IBAction func clickChat(_ sender: Any) {
        
        if let obj = self.objOtherProfile{
            
            let vcInstace = StoryBoard.Message.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            vcInstace.touserId = obj.user_id
            vcInstace.profileURL = obj.profile_image
            vcInstace.name = obj.firstname + " " + obj.lastname
            self.pushTo(vcInstace)
            
        }
        
        
        
    }
   /* @objc func DeclineQuestion(notification: NSNotification){
         
         DispatchQueue.main.async {
             if let find_index = (self.arrALLHomePost).firstIndex(where: {$0.post_id == toString(notification.object)}) {
                 self.arrALLHomePost.remove(at: find_index)
             }
             self.tblview.reloadData()
         }
     }
     @objc func DeclineArticle(notification: NSNotification){
         
         DispatchQueue.main.async {
             if let find_index = (self.arrALLHomePost).firstIndex(where: {$0.post_id == toString(notification.object)}) {
                 self.arrALLHomePost.remove(at: find_index)
             }
             self.tblview.reloadData()
         }
     }*/
    @objc func Delete_Post(notification: NSNotification){
            
            if  let dic = notification.object as? [String : Any]{
                
                let removePostId = (toString(dic["id"]))
    //            let getType = PostTypeEnum.init(rawValue: ToInt(dic["type"]))
    //            debugPrint(toString(dic["type"]))
                if let find_index = (self.arrALLHomePost).firstIndex(where: {$0.post_id == removePostId}) {
                    self.arrALLHomePost.remove(at: find_index)
                }
                self.tblview.reloadData()
            }
        }
     
    
}
//MARK: - Table view data source
extension OtherProfileVC: UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  section == 0 ? 2 : arrALLHomePost.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            if indexPath.row == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell") as! ProfileTableViewCell
                if let obj = self.objOtherProfile{
                    
                    //For Category
                    
                cell.imgProfile.setImageWithURL(obj.profile_image,"dp_profile")

                    
                    cell.lblUserName.text = (obj.firstname + " " + obj.lastname).capitalized
                    cell.lblFollowingCount.text = obj.total_following
                    cell.lblFollowerCount.text = obj.followers
                    cell.lblAbout.text = obj.about_me
                    cell.lblAboutProfession.text = obj.about_my_profession
                    cell.lblJoinDate.text = obj.member_since
                    
                    cell.viewExpertDotborder.isHidden = !obj.is_expert
                    cell.lblPost.text = obj.professional_qualification
                    cell.lblAboutEducation.text = obj.aboutEducation

                    if obj.gender.isValid
                    {
                        cell.lblGender.text = obj.gender == "1" ? "Male" : "Female"
                    }
                    else{
                        cell.lblGender.text = ""
                    }

                    cell.lblAddress.text = (obj.address).capitalized

                      cell.onlyPictures.dataSource = self
                    
                    
                     cell.btnAddFriend.setTitle(obj.is_friend == true ? "UNFRIEND" : "ADD FRIEND",for: .normal)
                    
                    
                    if obj.request_status == false,obj.sender_id == globalUserId
                    {
                        obj.is_friend = true
                        cell.btnAddFriend.setTitle("REQUESTED",for: .normal)
                    }
//                    else  if obj.request_status == false// friend request not acept
//                    {
//                        obj.is_friend = true
//                        cell.btnAddFriend.setTitle("ACCEPT",for: .normal)
//
//                    }
//                    else if obj.request_status , obj.is_friend
//                    {
//                        cell.btnAddFriend.setTitle("UNFRIEND",for: .normal)
//                    }
//                    else
//                    {
//                        cell.btnAddFriend.setTitle("ADD FRIEND",for: .normal)
//                    }
                   
                    
                    cell.btnFollow.setTitle(obj.is_following == true ? "UNFOLLOW" : "FOLLOW",for: .normal)
                    
                    cell.btnAddFriend.addTarget(self, action: #selector(clickAddFriendUnFriend), for: .touchUpInside)
                    cell.btnFollow.addTarget(self, action: #selector(clickFollowUnFollow), for: .touchUpInside)
                }
                return cell
            }
            else
            {
                /* let cell = tableView.dequeueReusableCell(withIdentifier: "TagListTableViewCell") as! TagListTableViewCell
                 let RegularFont = UIFont.FontLatoRegular(size: 13 * screenscale )
                 cell.sTagListView.textFont = RegularFont*/
                return cellTag!
            }
        }
        else
        {
            switch self.arrALLHomePost[indexPath.row].postType {
                
            case .Article:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleDeclineTableCell") as! ArticleDeclineTableCell
                
                cell.model = arrALLHomePost[indexPath.row]
                
                /* cell.btnFlash.addTarget(self, action: #selector(tapFlash(_:)), for: .touchUpInside)
                 cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
                 cell.btnDot.accessibilityHint = "tableHome"*/
                
                
                return cell
                
            case .Question:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionAnswerTableViewCell") as! QuestionAnswerTableViewCell
                
                
                cell.model = arrALLHomePost[indexPath.row]
                
                
                /*cell.btnFlash.addTarget(self, action: #selector(tapFlash(_:)), for: .touchUpInside)
                 cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
                 cell.btnDot.accessibilityHint = "tableHome"*/
                return cell
                
            case .VoiceVideo:
                
                switch self.arrALLHomePost[indexPath.row].audioVideoType{
                    
                case .audio:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "AudioTableViewCell") as! AudioTableViewCell
                    
                    cell.model = arrALLHomePost[indexPath.row]
                    
                    /*cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
                     cell.btnDot.accessibilityHint = "tableHome"*/
                    
                    return cell
                    
                case .video:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell") as! VideoTableViewCell
                    
                    cell.model = arrALLHomePost[indexPath.row]
                    
                    /* cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
                     cell.btnDot.accessibilityHint = "tableviewVV"*/
                    
                    return cell
                    
                }
                
                
            case .ResearchPapers:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "downloadPDFTableCell") as! downloadPDFTableCell
                
                cell.model = arrALLHomePost[indexPath.row]
                
                
                /*cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
                 cell.btnDot.accessibilityHint = "tableHome"*/
                
                return cell
                
                
            case .Advert:
                return UITableViewCell()
            }
            
            
            //comment answer mate
            
            /* let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerTableCell") as! AnswerTableCell
             
             cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
             cell.btnDot.accessibilityHint = "tableHome"
             
             
             cell.selectionStyle = .none
             return cell*/
            
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return UITableView.automaticDimension
        
        /*  if indexPath.row == 0
         {
         return UITableView.automaticDimension
         }
         
         let cell = tableView.dequeueReusableCell(withIdentifier: "TagListTableViewCell") as! TagListTableViewCell
         let sH = cell.sTagListView.frame.height * screenscale
         let height = CGFloat(cell.sTagListView.rows) * (sH + cell.sTagListView.marginY)
         return height*/
        
    }
    func scrollViewDidEndDragging(_ aScrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if aScrollView == self.tblview{
            let offset: CGPoint = aScrollView.contentOffset
            let bounds: CGRect = aScrollView.bounds
            let size: CGSize = aScrollView.contentSize
            let inset: UIEdgeInsets = aScrollView.contentInset
            let y = Float(offset.y + bounds.size.height - inset.bottom)
            let h = Float(size.height)
            let reload_distance: Float = 0
            //            print("load more data!!!!!")
            if y > h + reload_distance{
                if self.isAvailavbleMoreDat == true , self.tblview.accessibilityHint == nil{
                    self.indicator.startAnimating()
                    self.tblview.accessibilityHint = "service_calling"
                    webCall_UserPost(self.arrALLHomePost.count)
                }
            }
        }
    }
    @IBAction func clickAddFriendUnFriend(_ sender: UIButton) {
        
        if let obj = self.objOtherProfile
        {
            if obj.request_status == false,obj.sender_id == globalUserId
            {
                webCall_AddRemoveFriend("0")
            }
//            else if obj.request_status == false
//            {
//                self.webCallAcceptRejectFriendRequest("1", senderID: obj.sender_id, completion: {
//                    obj.is_friend = true
//                    
//                    obj.request_status = true
//                    
//                    self.tblview.reloadData()
//                })
//            }
            else
            {
                webCall_AddRemoveFriend(obj.is_friend == true ? "0":"1")

            }
        }
    }
    
    @IBAction func clickFollowUnFollow(_ sender: Any) {
        if let obj = self.objOtherProfile{
            webCall_FollowUnFollow(obj.is_following == true ? "0":"1")
        }
    }
    
    
}



//MARK: - API call
extension OtherProfileVC
{
    func webCall_AddRemoveFriend( _ type:String){
        WebService.shared.RequesURL(ServerURL.AddFriendUnFriend, Perameters: ["user_id":globalUserId,"friend_id":profileId,"type":type],showProgress: true, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true{
                if let obj = self.objOtherProfile
                {
                    obj.is_friend = !obj.is_friend
                    
                    obj.request_status = !obj.is_friend
                    
                    obj.sender_id = obj.request_status ? "" : globalUserId
                    
                    debugPrint(obj.request_status)
                    
                    debugPrint(obj.sender_id)
                    
                    
                    /*if obj.request_status == false,obj.sender_id == globalUserId
                     {
                     cell.btnAddFriend.setTitle("REQUESTED",for: .normal)
                     }*/
                    
                    
                    self.tblview.reloadData()
                }
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
    func webCall_FollowUnFollow( _ type:String){
        WebService.shared.RequesURL(ServerURL.FollowUnFollow, Perameters: ["user_id":globalUserId,"follower_id":profileId,"type":type],showProgress: true, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true{
                if let obj = self.objOtherProfile{
                    obj.is_following = !obj.is_following
                    self.tblview.reloadData()
                }
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
    
    //MARK: - ACCEPT REQUEST

        func webCallAcceptRejectFriendRequest(_ status: String, senderID: String, completion:(()->())?){
            
            var dict = [String: Any]()
            dict["user_id"] = globalUserId
            dict["sender_id"] = senderID
            dict["status"] = status
            
            WebService.shared.RequesURL(ServerURL.AcceptRejectFriend, Perameters: dict, showProgress: true, completion: { (dictResponse, status) in
                if status{
    //                debugPrint(dictResponse)
                    completion?()
                }else{
                    
                }
            }) { (error) in
                self.showOkAlert(msg: error.localizedDescription)
            }
        }
    
    @objc func refreshCalled()
    {
        self.webCall_getOtheruserProfile()
    }
    func webCall_getOtheruserProfile(_ offset : Int = 0)
    {
        var delayTime = DispatchTime.now()
        
        if self.tblview.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblview.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            
            WebService.shared.RequesURL(ServerURL.GetOtherProfile, Perameters: ["user_id":globalUserId,"follower_id":self.profileId,"offset":offset],showProgress: false, completion: { (dicRes, success) in
                
                debugPrint(dicRes)
                
                if success == true
                {
                    if let profile = dicRes["profile"] as? [String : Any] {
                        self.objOtherProfile = OtherUserProfileModel.init(profile)
                    }
                    self.setupData()
                    self.webCall_UserPost()
                    
                    DispatchQueue.main.async {
//                                          self.isAvailavbleMoreDat = self.arrALLHomePost.count >= ToInt(dicRes["offset"])
                                          UIView.performWithoutAnimation {
                                              self.tblview.reloadData()
                                          }
//                                          self.tblview.accessibilityHint = nil
                                          self.tblview.refreshControl?.endRefreshing()
//                                          self.indicator.stopAnimating()
                                      }
                    
                }
                else{
                    //                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
//                    self.tblview.accessibilityHint = nil
                    self.tblview.refreshControl?.endRefreshing()
//                    self.indicator.stopAnimating()
                    
                }
            }) { (err) in
                debugPrint(err)
            }
        }
    }

    func setupData(){
        if let obj = self.objOtherProfile{
            self.coverImg.setImageWithURL(obj.cover_image,"video_placeholder")
        }
        self.coverImg.setupImageViewer(options: [.theme(.dark)])

        //For Category
        self.cellTag?.sTagListView.removeAllTags()
        if let arrCat = self.objOtherProfile?.arrInt {
            self.cellTag?.sTagListView.addTags(arrCat.map({$0.category_name}))
        }
        //        self.tblview.reloadData()
        self.tblview.isHidden = false
    }
    
    func webCall_UserPost(_ offset : Int = 0){
        
        var delayTime = DispatchTime.now()
        
        if self.tblview.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblview.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            
            WebService.shared.RequesURL(ServerURL.GetUserPosts, Perameters: ["user_id":globalUserId,"other_person_id":self.profileId,"offset":offset],showProgress: false, completion: { (dicRes, success) in
//                debugPrint(dicRes)
                if success == true{
                    if let arr = dicRes["detail"] as? [[String:Any]] {
                        if offset == 0{
                            self.arrALLHomePost = arr.map({allPostModel.init($0)})
                        }
                        else{
                            self.arrALLHomePost.append(contentsOf: arr.map({allPostModel.init($0)}))
                        }
                    }
                    DispatchQueue.main.async {
                        self.isAvailavbleMoreDat = self.arrALLHomePost.count >= ToInt(dicRes["offset"])
                        UIView.performWithoutAnimation {
                            self.tblview.reloadData()
                        }
                        self.tblview.accessibilityHint = nil
                        self.tblview.refreshControl?.endRefreshing()
                        self.indicator.stopAnimating()
                    }
                    
                }
                else{
                    //                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
                    self.tblview.accessibilityHint = nil
                    self.tblview.refreshControl?.endRefreshing()
                    self.indicator.stopAnimating()
                    
                }
            }) { (err) in
                debugPrint(err)
            }
        }
    }
}



extension OtherProfileVC: OnlyPicturesDataSource {
    
    // ---------------------------------------------------
    // returns the total no of pictures
    
    func numberOfPictures() -> Int {
        return objOtherProfile?.arrMyFriends.count ?? 0
    }
    
    // ---------------------------------------------------
    // returns the no of pictures should be visible in screen.
    // In above preview, Left & Right formats are example of visible pictures, if you want pictures to be shown without count, remove this function, it's optional.
    
    func visiblePictures() -> Int {
        return 3
    }
    
    
    // ---------------------------------------------------
    // return the images you want to show. If you have URL's for images, use next function instead of this.
    // use .defaultPicture property to set placeholder image. This only work with local images. for URL's images we provided imageView instance, it's your responsibility to assign placeholder image in it. Check next function.
    // onlyPictures.defaultPicture = #imageLiteral(resourceName: "defaultProfilePicture")
    
    //    func pictureViews(index: Int) -> UIImage {
    //        return objProfileModel?.arrMyFriends[index]
    //    }
    //
    
    // ---------------------------------------------------
    // If you do have URLs of images. Use below function to have UIImageView instance and index insted of 'pictureViews(index: Int) -> UIImage'
    // NOTE: It's your resposibility to assign any placeholder image till download & assignment completes.
    // I've used AlamofireImage here for image async downloading, assigning & caching, Use any library to allocate your image from url to imageView.
    
    func pictureViews(_ imageView: UIImageView, index: Int) {
        
        // Use 'index' to receive specific url from your collection. It's similar to indexPath.row in UITableView.
        /* let url = URL(string: self.objProfileModel?.arrMyFriends[index].profile_image_thumb ?? "")
         
         imageView.image = #imageLiteral(resourceName: "defaultProfilePicture")   // placeholder image
         imageView.af_setImage(withURL: url!)*/
        imageView.setImageWithURL(self.objOtherProfile?.arrMyFriends[index].profile_image, "dp_profile")
        
    }
}
class OtherUserProfileModel {
    
    var about_me =  ""
    var about_my_profession =  ""
    var address =  ""
    // var apply_reset_pwd =  ""
    var category_status =  ""
    var cover_image =  ""
    var date =  ""
    var email =  ""
    var firstname =  ""
    var gender =  ""
    var lastname =  ""
    var media_id =  ""
    var media_type =  ""
    var member_since =  ""
    var phone =  ""
    var phone_code =  ""
    var profile_image =  ""
    var profile_image_thumb =  ""
    var profile_updated =  ""
    var token =  ""
    var followers =  ""
    var total_following =  ""
    var total_friends =  ""
    var user_id =  ""
    var user_post =  ""
    var professional_qualification = ""
    var profession = ""
    var request_status =  false
    var is_friend =  false
    var is_following =  false
    var is_expert =  false
    var status =  false
    var notification_status =  false
    
    var arrInt = [objCategoryInterested]()
    var arrMyFriends = [objMYFriends]()
    var arrProfessionalQua = [objProfessionalQuali]()
    
    var aboutEducation = ""
    
    var sender_id =  ""

    init(_ dict : [String:Any])
    {
        //For Interested Category
        if let arr = dict["interest_categories"] as? [[String:Any]] {
            self.arrInt = arr.map({objCategoryInterested.init($0)})
        }
        
        //For My Friend list
        if let arr = dict["my_friends"] as? [[String:Any]] {
            self.arrMyFriends = arr.map({objMYFriends.init($0)})
        }
        
        //For Professional
        if let arr = dict["professional_qualification"] as? [[String:Any]] {
            self.arrProfessionalQua = arr.map({objProfessionalQuali.init($0)})
        }
        
        //For Education
        if let arr = dict["educational_qualification"] as? [[String:Any]] {
            if let dic = arr.first{
                self.aboutEducation = toString(dic["qualification"])
            }
        }
        
        self.sender_id = toString(dict["sender_id"])

        self.user_id = toString(dict["user_id"])
        self.email = toString(dict["email"])
        self.firstname = toString(dict["firstname"])
        self.lastname = toString(dict["lastname"])
        self.about_my_profession = toString(dict["about_my_profession"])
        self.about_me = toString(dict["about_me"])
        self.address = toString(dict["address"])
        self.date = toString(dict["date"])
        self.total_following = toString(dict["total_following"])
        self.followers = toString(dict["followers"])
        self.cover_image = toString(dict["cover_image"])
        self.profile_image = toString(dict["profile_image"])
        self.profile_image_thumb = toString(dict["profile_image_thumb"])
        self.category_status = toString(dict["category_status"])
        self.gender = toString(dict["gender"])

        self.member_since = toString(dict["member_since"]).dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate:  Constant.date_MMMddyyyy)
        self.phone = toString(dict["phone"])
        self.phone_code = toString(dict["phone_code"])
        self.professional_qualification = toString(dict["professional_qualification"])
        self.profession = toString(dict["profession"])

        
        //BOOL
        self.is_expert = toString(dict["is_expert"]) == "1"
        self.status = toString(dict["status"]) == "1"
        self.notification_status = toString(dict["notification_status"]) == "1"
        
        self.is_friend = ConvertToBool(toString(dict["is_friend"]))
        self.is_following = ConvertToBool(toString(dict["is_following"]))
        
        self.request_status = ConvertToBool(toString(dict["request_status"]))


    }
    
}
