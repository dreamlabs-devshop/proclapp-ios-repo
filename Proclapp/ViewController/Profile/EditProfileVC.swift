
//
//  EditProfileVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/18/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import CountryPicker
import SVProgressHUD

class EditProfileVC: UIViewController, CountryPickerDelegate {
    
    var updateProfileCompletion: (()->())?
    
    let arrFields = ["First Name", "Last Name", "Email Address", "Residential Address", "Gender", "Are you expert?", "About me"]
    //ms_ios
    //remove "Mobile Number" is after email
    
    let arrGender = ["Male","Female"]
    let arrOption = ["Yes","No"]
    
    var getProfile: ProfileModel?
    
    var getCC = "+11"
    
    var mobileFieldCell: editMobileCell?
    var headerCell: editProfileImageCell?
    
    var pickedProfile: UIImage?
    var pickedCover: UIImage?
    
    var pickProfile = true
    
    var isProfileFromLogin = false
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var countryPicker: CountryPicker!
    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBOutlet weak var btnback: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = true
        
        //        pickerView.delegate = self
        //        pickerView.dataSource = self
        
        
        
        headerCell = (tableView.dequeueReusableCell(withIdentifier: "editProfileImageCell") as! editProfileImageCell)
        DispatchQueue.main.async {
            self.headerCell?.imgProfile.setCornerRaduisToRound()
        }
        mobileFieldCell = (tableView.dequeueReusableCell(withIdentifier: "editMobileCell") as! editMobileCell)
        mobileFieldCell!.txtCC.inputView = countryPicker
        mobileFieldCell!.txtCC.tintColor = UIColor.clear
        
        tableView.reloadData()
        
        if isProfileFromLogin
        {
            self.webCall_GetProfile()
        }
    }
    
    
    func webCall_GetProfile()
    {
        WebService.shared.RequesURL(ServerURL.GetProfile, Perameters: ["user_id":globalUserId,"profile_id":globalUserId],showProgress: true, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true{
                if let profile = dicRes["user_data"] as? [String : Any] {
                    self.getProfile = ProfileModel.init(profile)
                    
                    self.tableView.reloadData()
                }
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        btnback.isHidden = isProfileFromLogin
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
        if let gesture = navigationController?.interactivePopGestureRecognizer
        {
            view.addGestureRecognizer(gesture)
        }
    }
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
    }
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveClicked(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if validateFields
        {
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.callUpdateProfileAPI()
            }
        }
       
    }
    
    var validateFields: Bool{
        
        if ToString(getProfile?.firstname).isBlank {
            showOkAlert(msg: "Please enter a first name")
            return false
        }
        else if ToString(getProfile?.lastname).isBlank{
            showOkAlert(msg: "Please enter a last name")
            return false
        }
        else if ToString(getProfile?.email).isEmail == false {
            showOkAlert(msg: "Please enter a valid email adress")
            return false
        }
       /* else if ToString(getProfile?.phone).isBlank {
            showOkAlert(msg: "Please enter a valid phone number")
            return false
        }*/
            
        else{
            return true
        }
    }
    
    @IBAction func btnPickProfileImage(_ sender: UIButton) {
        pickProfile = true
        PickImage()
    }
    
    @IBAction func btnPickCoverImage(_ sender: UIButton) {
        pickProfile = false
        PickImage()
    }
    
    
    func PickImage(){
        DispatchQueue.main.async {
            let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let camera = UIAlertAction.init(title: "Camera", style: .default) { _ in
                self.openImagePicker(type: .camera)
            }
            
            let photos = UIAlertAction.init(title: "Photos", style: .default) { _ in
                self.openImagePicker(type: .photoLibrary)
            }
            
            let cancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(camera)
            alert.addAction(photos)
            alert.addAction(cancel)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- CountryPicker Delegate
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        
        mobileFieldCell?.txtCC.text = phoneCode
    }
    
    //MARK:- Web service
    func callUpdateProfileAPI(){
        
        var dict = [String: Any]()
        dict["user_id"] = globalUserId
        dict["firstname"] = toString(getProfile?.firstname)
        dict["lastname"] = toString(getProfile?.lastname)
        dict["email"] = toString(getProfile?.email)
        //        dict["cover_image"] = ""
        dict["about_me"] = toString(getProfile?.about_me)
        dict["about_my_profession"] = toString(getProfile?.about_my_profession)
        //        dict["profile_image"] = ""
        dict["gender"] = toString(getProfile?.gender)
//        dict["phone"] = toString(getProfile?.phone)
//        dict["phone_code"] = toString(getProfile?.phone_code)
        dict["address"] = toString(getProfile?.address)
        dict["is_expert"] = getProfile?.is_expert == true ? "1" : "0"
//        dict["education"] = (getProfile?.arrEducationQuali.map({$0.toDict_Education}))?.jsonString
        dict["profession"] = (getProfile?.arrProfessionalQua.map({$0.toDict_Professional}))?.jsonString
        
        var arrUpload = [UploadImage]()
        
        if let imgPic = pickedProfile {
            arrUpload.append(UploadImage.init(imgPic.pngData(), Name: "profile_image", FileName: UploadImage.getFileName + ".png"))
        }
        
        if let imgCover = pickedCover {
            arrUpload.append(UploadImage.init(imgCover.pngData(), Name: "cover_image", FileName: UploadImage.getFileName + ".png"))
        }
        
        SVProgressHUD.show()
        SVProgressHUD.setDefaultAnimationType(.native)
        SVProgressHUD.setDefaultMaskType(.black)
        
        WebService.shared.uploadData(arrUpload, urlString: ServerURL.UpdateProfile, Perameters: dict, progressCompletionBlock: nil, completion: { (responseDict, isStatus) in
            
            debugPrint(responseDict)
            
            if isStatus{
                self.showOkAlertWithHandler(msg: toString(responseDict[Constant.responsemsg]), handler: {
                  
                    if self.isProfileFromLogin
                    {
                        if let dictData = responseDict[Constant.userDeafult_ProfileDic] as? [String : Any]
                        {
                            setUserDefault(dictData as AnyObject, Key: Constant.userDeafult_LoginDic)
                            setLoginData(dictData)
                        }
                        
                        if  toString(UserDefaults.standard.value(forKey: Constant.userDeafult_isFirstTimeInstall)).isValid
                        {
                           self.navigationController?.popViewController(animated: true)
                        }
                        else
                        {
                            let vcInstace = StoryBoard.Main.instantiateViewController(withIdentifier: "LetsStartedVC") as! LetsStartedVC
                            self.navigationController?.pushViewController(vcInstace, animated: true)
                        }
                    }
                    else
                    {
                        self.navigationController?.popViewController(animated: true)
                        self.updateProfileCompletion?()
                    }
                })
            }else{
                self.showOkAlert(msg: toString(responseDict[Constant.responsemsg]))
            }
            SVProgressHUD.dismiss()
        }) { (error) in
            SVProgressHUD.dismiss()
        }
    }
    
}

//MARK: - Table view data source
extension EditProfileVC: UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 0 {return 1}
            
        else if section == 1 {return arrFields.count}
            
            //        else if section == 2 {return 1}
            
        else if section == 2 {return getProfile?.arrEducationQuali.count ?? 0}
            
        else if section == 3 {return getProfile?.arrProfessionalQua.count ?? 0}
            
        else if section == 4 {return 1}
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            //            let cell = tableView.dequeueReusableCell(withIdentifier: "editProfileImageCell") as! editProfileImageCell
            if pickedCover == nil{
                headerCell!.imgCover.setImageWithURL(getProfile?.cover_image,"video_placeholder")
            }
            else{
                headerCell!.imgCover.image = pickedCover
            }
            
            if pickedProfile == nil{
                headerCell!.imgProfile.setImageWithURL(getProfile?.profile_image_thumb,"dp_profile")
            }
            else{
                headerCell!.imgProfile.image = pickedProfile
            }
            
            
            return headerCell!
        }
            
        else if indexPath.section == 1
        {
            if arrFields[indexPath.row] == "First Name"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "editTextFiledCell") as! editTextFiledCell
                cell.lblTitle.text = arrFields[indexPath.row]
                cell.txtField.text = toString(getProfile?.firstname)
                cell.txtField.accessibilityHint = arrFields[indexPath.row]
                cell.txtField.inputView = nil
                cell.txtField.tintColor = UIColor.App166
                cell.txtField.isUserInteractionEnabled = true
                return cell
            }
            else if arrFields[indexPath.row] == "Last Name"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "editTextFiledCell") as! editTextFiledCell
                cell.lblTitle.text = arrFields[indexPath.row]
                cell.txtField.text = toString(getProfile?.lastname)
                cell.txtField.accessibilityHint = arrFields[indexPath.row]
                cell.txtField.inputView = nil
                cell.txtField.tintColor = UIColor.App166
                cell.txtField.isUserInteractionEnabled = true
                return cell
            }
            else if arrFields[indexPath.row] == "Email Address"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "editTextFiledCell") as! editTextFiledCell
                cell.lblTitle.text = arrFields[indexPath.row]
                cell.txtField.text = toString(getProfile?.email)
                cell.txtField.accessibilityHint = arrFields[indexPath.row]
                cell.txtField.isUserInteractionEnabled = cell.txtField.text?.isBlank == false ? false : true
                cell.txtField.inputView = nil
                cell.txtField.tintColor = UIColor.App166
                return cell
            }
            else if arrFields[indexPath.row] == "Mobile Number"{
                mobileFieldCell!.lblTitle.text = arrFields[indexPath.row]
                mobileFieldCell?.txtMobile.text = toString(getProfile?.phone)
                mobileFieldCell?.txtCC.text = toString(getProfile?.phone_code)
                mobileFieldCell?.txtMobile.accessibilityHint = arrFields[indexPath.row]
                mobileFieldCell?.txtMobile.keyboardType = .numberPad
                mobileFieldCell?.txtCC.accessibilityHint = "CC"
                
                if toString(self.getProfile?.phone_code) == ""{
                    countryPicker.setCountry(Locale.current.regionCode?.uppercased() ?? "IN")
                    self.getProfile?.phone_code = (mobileFieldCell?.txtCC.text) ?? "+91"
                }
                else{
                    countryPicker.setCountryByPhoneCode(toString(self.getProfile?.phone_code))
                }
                return mobileFieldCell!
                
            }
            else if arrFields[indexPath.row] == "Residential Address"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "editTextFiledCell") as! editTextFiledCell
                cell.lblTitle.text = arrFields[indexPath.row]
                cell.txtField.text = toString(getProfile?.address)
                cell.txtField.accessibilityHint = arrFields[indexPath.row]
                cell.txtField.inputView = nil
                cell.txtField.tintColor = UIColor.App166
                cell.txtField.isUserInteractionEnabled = true
                return cell
            }
            else if arrFields[indexPath.row] == "Gender"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "editTextFiledCell") as! editTextFiledCell
                cell.lblTitle.text = arrFields[indexPath.row]
                
                let getGender =  toString(getProfile?.gender)
                if getGender.isValid
                {
                    cell.txtField.text = toString(getProfile?.gender) == "1" ? "Male" : "Female"
                }
                else
                {
                     cell.txtField.text = ""
                }
                cell.txtField.accessibilityHint = arrFields[indexPath.row]
                cell.picker.accessibilityHint = arrFields[indexPath.row]
                cell.txtField.inputView = cell.picker
                cell.txtField.tintColor = .clear
                cell.txtField.isUserInteractionEnabled = true
                return cell
            }
            else if arrFields[indexPath.row] == "Are you expert?"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "editTextFiledCell") as! editTextFiledCell
                cell.lblTitle.text = arrFields[indexPath.row]
                cell.txtField.text = toString(getProfile?.is_expert) == "1" ? "Yes" : "No"
                cell.txtField.accessibilityHint = arrFields[indexPath.row]
                cell.picker.accessibilityHint = arrFields[indexPath.row]
                cell.txtField.inputView = cell.picker
                cell.txtField.tintColor = .clear
                cell.txtField.isUserInteractionEnabled = true
                return cell
            }
            else{
                //"About me"
                let cell = tableView.dequeueReusableCell(withIdentifier: "editTextFiledCell") as! editTextFiledCell
                cell.lblTitle.text = arrFields[indexPath.row]
                cell.txtField.accessibilityHint = arrFields[indexPath.row]
                cell.txtField.text = toString(getProfile?.about_me)
                cell.txtField.inputView = nil
                cell.txtField.tintColor = UIColor.App166
                cell.txtField.isUserInteractionEnabled = true
                return cell
            }
        }
        else if indexPath.section == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "editQualificationCell") as! editQualificationCell
            
            cell.lblTitleOne.text = "Edu. Qualification"
            cell.lblTitleTwo.text = "Passing Year"
            
            cell.txtFieldOne.accessibilityHint = "qua_one"
            cell.txtFieldTwo.accessibilityHint = "qua_two"
            
            cell.txtFieldOne.text = getProfile?.arrEducationQuali[indexPath.row].qualification
            cell.txtFieldTwo.text = getProfile?.arrEducationQuali[indexPath.row].year
            
            return cell
        }
        else if indexPath.section == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "editProfessionalCell") as! editProfessionalCell
            
            cell.lblTitleOneP.text = "Job Title"
            cell.lblTitleTwoP.text = "Experience"
            cell.lblTitleThreeP.text = "Is this your current job?"
            
            cell.txtFieldOneP.accessibilityHint = "txtFieldOneP"
            cell.txtFieldTwoP.accessibilityHint = "txtFieldTwoP"
            cell.txtFieldThreeP.accessibilityHint = "txtFieldThreeP"
            
            cell.txtFieldOneP.text = getProfile?.arrProfessionalQua[indexPath.row].job_title
            cell.txtFieldTwoP.text = getProfile?.arrProfessionalQua[indexPath.row].experience
            cell.txtFieldThreeP.text = toString(getProfile?.arrProfessionalQua[indexPath.row].is_current_job) == "1" ? "Yes" : "No"
            return cell
        }
        else //"About my profession"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "editTextFiledCell") as! editTextFiledCell
            cell.lblTitle.text = "About my profession"
            cell.txtField.accessibilityHint = "About my profession"
            cell.txtField.text = toString(getProfile?.about_my_profession)
            cell.txtField.inputView = nil
            cell.txtField.tintColor = UIColor.App166
            cell.txtField.isUserInteractionEnabled = true
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        if indexPath.section == 0 { return 150 * screenscale }
            
        else if indexPath.section == 1 { return 61 * screenscale }
            
            
        else if indexPath.section == 2 {return 0}
            //ms_ios
        //remove edu so return 134 * screenscale replace by 0
            
        else if indexPath.section == 3 {return 201 * screenscale}
            
        
        else if indexPath.section == 4 { return 61 * screenscale }

        return 0
        
    }
   /* func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0 { return 150 * screenscale }
            
        else if indexPath.section == 1 { return 61 * screenscale }
            
            //        else if indexPath.section == 2 {return 40}
            
        else if indexPath.section == 2 {return 134 * screenscale}
            
        else if indexPath.section == 3 {return 201 * screenscale}
            
        else if indexPath.section == 4 {return 40}
        
        return 0
        
    }*/
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if section == 1 || section == 2 || section == 3{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "editHeaderCell") as! editHeaderCell
            
            if section == 1{
                cell.lblTitle.text = "PERSONAL INFO."
            }
            else if section == 2{//height 0 set
                cell.lblTitle.text = "EDUCATIONAL QUALIFICATION"
            }
            else{
                cell.lblTitle.text = "PROFESSIONAL QUALIFICATION"
            }
            
            return cell
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 1 || section == 3{
            //section == 2
            //ms_ios
            return 40 * screenscale
        }
        return  CGFloat.leastNormalMagnitude // Removes extra padding in Grouped style
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if section == 2{//remove set height 0
            let cell = tableView.dequeueReusableCell(withIdentifier: "editFooterCell") as! editFooterCell
            cell.btnAdd.addTarget(self, action: #selector(btnAddEducationAction), for: .touchUpInside)
            return cell
        }
        else if section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "editFooterCell") as! editFooterCell
            cell.btnAdd.addTarget(self, action: #selector(btnAddProfessionalAction), for: .touchUpInside)
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "editFooterCell") as! editFooterCell
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        if  section == 3{//section == 2
            return 40 * screenscale
        }
        return  CGFloat.leastNormalMagnitude // Removes extra padding in Grouped style
        
    }
    
    
    @IBAction func btnDeleteQualification(_ sender: UIButton){
        if let indexPath = self.tableView.indexPathForView(sender){
            getProfile?.arrEducationQuali.remove(at: indexPath.row)
            self.tableView.reloadData()
        }
    }
    
    @IBAction func btnDeleteProfessional(_ sender: UIButton){
        if let indexPath = self.tableView.indexPathForView(sender){
            getProfile?.arrProfessionalQua.remove(at: indexPath.row)
            self.tableView.reloadData()
        }
    }
    
    @objc func btnAddEducationAction(_ sender: UIButton){
        let objEdu = objEducationQuali.init("", year: "")
        getProfile?.arrEducationQuali.append(objEdu)
        self.tableView.reloadData()
    }
    
    @objc func btnAddProfessionalAction(_ sender: UIButton){
        let objProf = objProfessionalQuali.init()
        getProfile?.arrProfessionalQua.append(objProf)
        self.tableView.reloadData()
    }
}

extension EditProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func openImagePicker(type: UIImagePickerController.SourceType){
        DispatchQueue.main.async {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = type
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            
            if pickProfile{
                headerCell!.imgProfile.image = image
                self.pickedProfile = image
//                self.pickedProfile = resizePortraidImage(image, maxHeight: 1080, maxWidth: 1080)
            }else{
                headerCell!.imgCover.image = image
                self.pickedCover = image
//                self.pickedCover = resizePortraidImage(image, maxHeight: 1080, maxWidth: 1080)
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func resizePortraidImage(_ image: UIImage , maxHeight: Float , maxWidth: Float) -> UIImage
    {
        var actualHeight: Float = Float(image.size.height)
        var actualWidth: Float = Float(image.size.width)
        //    let maxHeight: Float = 1920
        //    let maxWidth: Float = 1080
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.75
        //50 percent compression
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect = CGRect(x: 0, y: 0, width:  CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img?.jpegData(compressionQuality: CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!)!
    }
}


extension EditProfileVC: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.accessibilityHint == "Gender"{
            
        }
        else if textField.accessibilityHint == "Are you expert?"{
            
        }
        //        else if textField.accessibilityHint == "txtFieldThreeP"{
        //            pickerView.accessibilityHint = "txtFieldThreeP"
        //            pickerView.reloadAllComponents()
        //        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.accessibilityHint == "First Name"{
            getProfile?.firstname = toString(textField.text)
        }
        else if textField.accessibilityHint == "Last Name"{
            getProfile?.lastname = toString(textField.text)
        }
        else if textField.accessibilityHint == "Email Address"{
            getProfile?.email = toString(textField.text)
        }
        else if textField.accessibilityHint == "CC"{
            getProfile?.phone_code = toString(textField.text)
        }
        else if textField.accessibilityHint == "Mobile Number"{
            getProfile?.phone = toString(textField.text)
        }
        else if textField.accessibilityHint == "Residential Address"{
            getProfile?.address = toString(textField.text)
        }
        else if textField.accessibilityHint == "Gender"{
            getProfile?.gender = toString(textField.text) == "Male" ? "1" : "2"
        }
        else if textField.accessibilityHint == "Are you expert?"
        {
            getProfile?.is_expert = toString(textField.text) == "Yes" ? true : false
            if getProfile?.is_expert ?? false
            {
                let vcInstace = StoryBoard.Wallet.instantiateViewController(withIdentifier: "WebviewPaymentVC") as! WebviewPaymentVC
                vcInstace.strPaymentType = "1"
                vcInstace.callbackPayment = { status in
                    
                    if status == Constant.kPayment_Success{
                        textField.text = "Yes"
                        self.getProfile?.is_expert = toString(textField.text) == "Yes" ? true : false
                    }
                    else
                    {
                        textField.text = "No"
                        self.getProfile?.is_expert = toString(textField.text) == "Yes" ? true : false
                    }
                }
                self.pushTo(vcInstace)
            }
            
        }
        else if textField.accessibilityHint == "About my profession"{
            getProfile?.about_my_profession = toString(textField.text)
        }
        else if textField.accessibilityHint == "About me"{
            getProfile?.about_me = toString(textField.text)
        }
            //education qualification
        else if textField.accessibilityHint == "qua_one"{
            if let indexPath = self.tableView.indexPathForView(textField){
                getProfile?.arrEducationQuali[indexPath.row].qualification = toString(textField.text)
            }
        }
        else if textField.accessibilityHint == "qua_two"{
            if let indexPath = self.tableView.indexPathForView(textField){
                getProfile?.arrEducationQuali[indexPath.row].year = toString(textField.text)
            }
        }
            //professional qualification
        else if textField.accessibilityHint == "txtFieldOneP"{
            if let indexPath = self.tableView.indexPathForView(textField){
                getProfile?.arrProfessionalQua[indexPath.row].job_title = toString(textField.text)
            }
        }
        else if textField.accessibilityHint == "txtFieldTwoP"{
            if let indexPath = self.tableView.indexPathForView(textField){
                getProfile?.arrProfessionalQua[indexPath.row].experience = toString(textField.text)
            }
        }
        else if textField.accessibilityHint == "txtFieldThreeP"{
            if let indexPath = self.tableView.indexPathForView(textField){
                getProfile?.arrProfessionalQua[indexPath.row].is_current_job = toString(textField.text) == "Yes" ? "1" : "0"
            }
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.accessibilityHint == "qua_two"{
            // get the current text, or use an empty string if that failed
            let currentText = textField.text ?? ""
            
            // attempt to read the range they are trying to change, or exit if we can't
            guard let stringRange = Range(range, in: currentText) else { return false }
            
            // add their new text to the existing text
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            
            // make sure the result is under 16 characters
            return updatedText.count <= 4
        }
        return true
    }
}

class editHeaderCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    
}

class editFooterCell: UITableViewCell {
    @IBOutlet weak var btnAdd: UIButton!
}

class editTextFiledCell: UITableViewCell,UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtField: UITextField!
    
    let picker = UIPickerView()
    
    let arrGender = ["Male","Female"]
    let arrOption = ["Yes","No"]
    
    override func awakeFromNib() {
        picker.delegate = self
        picker.dataSource = self
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.accessibilityHint == "Gender"{
            return arrGender.count
        }
        else{
            return arrOption.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.accessibilityHint == "Gender"{
            txtField.text = arrGender[row]
            return arrGender[row]
        }
        else{
            txtField.text = arrOption[row]
            return arrOption[row]
        }
    }
    
    //    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    //        if pickerView.accessibilityHint == "Gender"{
    //            txtField.text = arrGender[row]
    //        }
    //        else{
    //            txtField.text = arrOption[row]
    //        }
    //    }
}

class editAboutCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDes: UILabel!
}

class editQualificationCell: UITableViewCell {
    
    @IBOutlet weak var lblTitleOne: UILabel!
    @IBOutlet weak var txtFieldOne: UITextField!
    @IBOutlet weak var lblTitleTwo: UILabel!
    @IBOutlet weak var txtFieldTwo: UITextField!
    
    @IBOutlet weak var btnDelete: UIButton!
}

class editProfessionalCell: UITableViewCell, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var lblTitleOneP: UILabel!
    @IBOutlet weak var txtFieldOneP: UITextField!
    
    @IBOutlet weak var lblTitleTwoP: UILabel!
    @IBOutlet weak var txtFieldTwoP: UITextField!
    
    @IBOutlet weak var lblTitleThreeP: UILabel!
    @IBOutlet weak var txtFieldThreeP: UITextField!
    
    let picker = UIPickerView()
    
    let arrOption = ["Yes","No"]
    
    
    override func awakeFromNib() {
        
        picker.delegate = self
        picker.dataSource = self
        txtFieldThreeP.inputView = picker
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrOption.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        txtFieldThreeP.text = arrOption[row]
        return arrOption[row]
    }
    
    //    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    //        txtFieldThreeP.text = arrOption[row]
    //    }
}

class editProfileImageCell: UITableViewCell {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgCover: UIImageView!
}

class editMobileCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtCC: UITextField!
    
    override func awakeFromNib() {
        
    }
    
}



