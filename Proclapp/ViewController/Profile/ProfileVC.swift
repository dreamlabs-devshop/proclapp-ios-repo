
//
//  ProfileVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/16/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import OnlyPictures

class ProfileVC: UIViewController {
    
    @IBOutlet weak var tblview: UITableView!
    
    @IBOutlet weak var coverImg: UIImageView!
    
    

    @IBOutlet weak var indicator: UIActivityIndicatorView!

    var isAvailavbleMoreDat = false
    var cellTag : TagListTableViewCell?
    
    var objProfileModel : ProfileModel?
    
    var arrALLHomePost = [allPostModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.Delete_Post), name: .Delete_Post, object: nil)

//        NotificationCenter.default.addObserver(self, selector: #selector(self.DeclineQuestion), name: .DeclineQuestion, object: nil)

//        NotificationCenter.default.addObserver(self, selector: #selector(self.DeclineArticle), name: .DeclineArticle, object: nil)

        
//        self.tblview.isHidden = true
        
        tblview.register(UINib.init(nibName: "downloadPDFTableCell", bundle: nil), forCellReuseIdentifier: "downloadPDFTableCell")
        
        tblview.register(UINib.init(nibName: "QuestionAnswerTableViewCell", bundle: nil), forCellReuseIdentifier: "QuestionAnswerTableViewCell")
        
        tblview.register(UINib.init(nibName: "ArticleDeclineTableCell", bundle: nil), forCellReuseIdentifier: "ArticleDeclineTableCell")
        
        tblview.register(UINib.init(nibName: "AudioTableViewCell", bundle: nil), forCellReuseIdentifier: "AudioTableViewCell")
        
        tblview.register(UINib.init(nibName: "VideoTableViewCell", bundle: nil), forCellReuseIdentifier: "VideoTableViewCell")
        
        
        cellTag = tblview.dequeueReusableCell(withIdentifier: "TagListTableViewCell") as? TagListTableViewCell
        
        let RegularFont = UIFont.FontLatoRegular(size: 13 * screenscale )
        cellTag?.sTagListView.textFont = RegularFont
        cellTag?.sTagListView.alignment = .left
        cellTag?.sTagListView.removeAllTags()
        
        self.tblview.refreshControl = UIRefreshControl()
        self.tblview.refreshControl?.tintColor = .AppSkyBlue
        self.tblview.refreshControl?.addTarget(self, action: #selector(refreshCalled), for: UIControl.Event.valueChanged)
        
        self.refreshCalled()
        
        DispatchQueue.main.async {
            self.indicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tblview.bounds.width, height: CGFloat(30))
            self.tblview.tableFooterView = self.indicator
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        webCall_GetProfile()
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //    override func viewWillDisappear(_ animated: Bool) {
    //        super.viewWillDisappear(animated)
    //        self.navigationController?.isNavigationBarHidden = false
    //    }
    
   /* override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
        if let gesture = navigationController?.interactivePopGestureRecognizer
        {
            view.addGestureRecognizer(gesture)
        }
    }
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
    }*/
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func Delete_Post(notification: NSNotification){
        
        if  let dic = notification.object as? [String : Any]{
            
            let removePostId = (toString(dic["id"]))
//            let getType = PostTypeEnum.init(rawValue: ToInt(dic["type"]))
//            debugPrint(toString(dic["type"]))
            if let find_index = (self.arrALLHomePost).firstIndex(where: {$0.post_id == removePostId}) {
                self.arrALLHomePost.remove(at: find_index)
            }
            self.tblview.reloadData()
        }
    }
    
    /*@objc func DeclineQuestion(notification: NSNotification){
        
        DispatchQueue.main.async {
            if let find_index = (self.arrALLHomePost).firstIndex(where: {$0.post_id == toString(notification.object)}) {
                self.arrALLHomePost.remove(at: find_index)
            }
            self.tblview.reloadData()
        }
    }
    @objc func DeclineArticle(notification: NSNotification){
        
        DispatchQueue.main.async {
            if let find_index = (self.arrALLHomePost).firstIndex(where: {$0.post_id == toString(notification.object)}) {
                self.arrALLHomePost.remove(at: find_index)
            }
            self.tblview.reloadData()
        }
    }*/
    
    @IBAction func clickEdit(_ sender: Any) {
        if let obj = objProfileModel {
            let editProfile = StoryBoard.Profile.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
            editProfile.getProfile = ProfileModel.init(obj)
            editProfile.updateProfileCompletion = {
                self.webCall_GetProfile()
            }
            pushTo(editProfile)
        }
    }
    
    @IBAction func clickEditInterestedCategory(_ sender: Any) {
        let vcInstace = StoryBoard.More.instantiateViewController(withIdentifier: "CategoryVC") as! CategoryVC
        vcInstace.callback =  { getInteretCateName in
            self.cellTag?.sTagListView.removeAllTags()
            self.cellTag?.sTagListView.addTags(getInteretCateName)
            self.tblview.reloadData()
        }
        self.navigationController?.pushViewController(vcInstace, animated: true)
    }
    @IBAction func clickViewMoreAbout(_ sender: UIButton)
    {
        if let getTitle = sender.titleLabel?.text
        {
            if getTitle == "View More"
            {
                sender.setTitle("View Less", for: .normal)
            }
            else
            {
                sender.setTitle("View More", for: .normal)
            }

            UIView.animate(withDuration: 0.3, animations: {
                self.tblview.beginUpdates()
                self.tblview.setContentOffset(CGPoint.zero, animated: false)
                self.tblview.endUpdates()
                
            }) { (Bool) in
                self.tblview.reloadData()
            }
            
        }
    }
    
    @IBAction func clickViewMore(_ sender: UIButton) {
        
        if let getTitle = cellTag?.btnViewMore.titleLabel?.text
        {
            if getTitle == "View More"
            {
                cellTag?.btnViewMore.setTitle("View Less", for: .normal)
            }
            else
            {
                cellTag?.btnViewMore.setTitle("View More", for: .normal)
            }

            UIView.animate(withDuration: 0.3, animations: {
                self.tblview.beginUpdates()
                self.tblview.setContentOffset(CGPoint.zero, animated: false)
                self.tblview.endUpdates()
                
            }) { (Bool) in
                self.tblview.reloadData()
            }

        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
//MARK: - Table view data source
extension ProfileVC: UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  section == 0 ? 2 : arrALLHomePost.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            if indexPath.row == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell") as! ProfileTableViewCell
                
                if let obj = self.objProfileModel{
                    
                    //For Category
                    
                   cell.imgProfile.setImageWithURL(obj.profile_image,"dp_profile")
                    
                    cell.lblUserName.text = (obj.firstname + " " + obj.lastname).capitalized
                    cell.lblFollowingCount.text = obj.total_following
                    cell.lblFollowerCount.text = obj.followers
                    cell.lblAbout.text = obj.about_me
                    cell.lblAboutProfession.text = obj.about_my_profession
                    cell.lblJoinDate.text = obj.member_since
                    
                    cell.viewExpertDotborder.isHidden = !obj.is_expert
                    cell.lblPost.text = obj.arrProfessionalQua.first?.job_title
                    
//                    cell.lblAboutEducation.text = obj.aboutEducation
                    
                    cell.lblAddress.text = (obj.address).capitalized
                    
                    cell.btnEdit.addTarget(self, action: #selector(clickEditInterestedCategory(_:)), for: .touchUpInside)
                    
                    cell.onlyPictures.dataSource = self
                    

                                   if obj.gender.isValid
                                   {
                                        cell.lblGender.text = obj.gender == "1" ? "Male" : "Female"
                                   }
                                   else{
                                        cell.lblGender.text = ""
                                   }
                    cell.btnViewMoreAbout.addTarget(self, action: #selector(clickViewMoreAbout(_:)), for: .touchUpInside)

                    if let getTitle = cell.btnViewMoreAbout.titleLabel?.text
                    {
                        if getTitle == "View More"
                        {
                            cell.lblAbout.numberOfLines = 3
//                           cell.btnViewMore.setTitle("View Less", for: .normal)
                        }
                        else
                        {
                            cell.lblAbout.numberOfLines = 0
//                            cell.btnViewMore.setTitle("View More", for: .normal)
                        }
                    }
                    cell.btnViewMoreAbout.isHidden = cell.lblAbout.maxNumberOfLines > 3 ? false : true
                }
                
                return cell
            }
            else
            {
                /* let cell = tableView.dequeueReusableCell(withIdentifier: "TagListTableViewCell") as! TagListTableViewCell
                 
                 let RegularFont = UIFont.FontLatoRegular(size: 13 * screenscale )
                 cell.sTagListView.textFont = RegularFont*/
                
                return cellTag!
            }
        }
        else
        {
            switch self.arrALLHomePost[indexPath.row].postType {
                
            case .Article:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleDeclineTableCell") as! ArticleDeclineTableCell
                
                cell.model = arrALLHomePost[indexPath.row]
                
                /* cell.btnFlash.addTarget(self, action: #selector(tapFlash(_:)), for: .touchUpInside)
                 cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
                 cell.btnDot.accessibilityHint = "tableHome"*/
                
                
                return cell
                
            case .Question:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionAnswerTableViewCell") as! QuestionAnswerTableViewCell
                
                
                cell.model = arrALLHomePost[indexPath.row]
                
                
                /*cell.btnFlash.addTarget(self, action: #selector(tapFlash(_:)), for: .touchUpInside)
                 cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
                 cell.btnDot.accessibilityHint = "tableHome"*/
                return cell
                
            case .VoiceVideo:
                
                switch self.arrALLHomePost[indexPath.row].audioVideoType{
                    
                case .audio:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "AudioTableViewCell") as! AudioTableViewCell
                    
                    cell.model = arrALLHomePost[indexPath.row]
                    
                    /*cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
                     cell.btnDot.accessibilityHint = "tableHome"*/
                    
                    return cell
                    
                case .video:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell") as! VideoTableViewCell
                    
                    cell.model = arrALLHomePost[indexPath.row]
                    
                    /* cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
                     cell.btnDot.accessibilityHint = "tableviewVV"*/
                    
                    return cell
                }
                
                
            case .ResearchPapers:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "downloadPDFTableCell") as! downloadPDFTableCell
                
                cell.model = arrALLHomePost[indexPath.row]
                
                
                /*cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
                 cell.btnDot.accessibilityHint = "tableHome"*/
                
                return cell
                
            case .Advert:
                return UITableViewCell()
            }
            
            
            //comment answer mate
            
            /* let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerTableCell") as! AnswerTableCell
             
             cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
             cell.btnDot.accessibilityHint = "tableHome"
             
             
             cell.selectionStyle = .none
             return cell*/
            
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0,indexPath.row == 1{
            if let cellNew = cellTag
            {
                if let getTitle = cellTag?.btnViewMore.titleLabel?.text
                {
                    if getTitle == "View More"
                    {
                        let fixRow = 6
                        let noOfRow = cellNew.sTagListView.rows
                        if noOfRow > fixRow {
                            let height = CGFloat(fixRow) * (30 + cellNew.sTagListView.marginY)
                            return height + 40
                        }
                    }
                }
            }
        }
        return UITableView.automaticDimension
        
        /*  if indexPath.row == 0
         {
         return UITableView.automaticDimension
         }
         
         let cell = tableView.dequeueReusableCell(withIdentifier: "TagListTableViewCell") as! TagListTableViewCell
         let sH = cell.sTagListView.frame.height * screenscale
         let height = CGFloat(cell.sTagListView.rows) * (sH + cell.sTagListView.marginY)
         return height*/
    }
    
    /*func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
     {
     if indexPath.row == 0
     {
     return 577
     }
     return 100
     }*/
    func scrollViewDidEndDragging(_ aScrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if aScrollView == self.tblview{
            let offset: CGPoint = aScrollView.contentOffset
            let bounds: CGRect = aScrollView.bounds
            let size: CGSize = aScrollView.contentSize
            let inset: UIEdgeInsets = aScrollView.contentInset
            let y = Float(offset.y + bounds.size.height - inset.bottom)
            let h = Float(size.height)
            let reload_distance: Float = 0
            //            print("load more data!!!!!")
            if y > h + reload_distance{
                if self.isAvailavbleMoreDat == true , self.tblview.accessibilityHint == nil{
                    
                    self.indicator.startAnimating()
                    self.tblview.accessibilityHint = "service_calling"
                    webCall_UserPost(self.arrALLHomePost.count)
                }
            }
        }
    }
    
    
    @IBAction func clickFollwing(_ sender: Any) {
        let vc = StoryBoard.Other.instantiateViewController(withIdentifier: "FollowingVC") as! FollowingVC
        vc.isFollowing = true
        self.pushTo(vc)
    }
    
    @IBAction func clickFollower(_ sender: Any) {
        let vc = StoryBoard.Other.instantiateViewController(withIdentifier: "FollowingVC") as! FollowingVC
        vc.isFollowing = false
        self.pushTo(vc)
    }
       
}


class ProfileTableViewCell : UITableViewCell {
    
    
    @IBOutlet weak var lblGender: UILabel!

    @IBOutlet weak var lblFollowerCount: UILabel!
    @IBOutlet weak var lblFollowingCount: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblExpert: UILabel!
    @IBOutlet weak var lblAboutProfession: UILabel!
    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var lblJoinDate: UILabel!
    @IBOutlet weak var lblPost: UILabel!
    @IBOutlet weak var lblAboutEducation: UILabel!
    @IBOutlet weak var lblAddress: UILabel!

    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var onlyPictures: OnlyHorizontalPictures!
    @IBOutlet weak var viewExpertDotborder: UIView!
    
    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var btnViewMoreAbout: UIButton!

    
    //OtherProfile
    @IBOutlet weak var btnAddFriend: UIButton!
    @IBOutlet weak var btnFollow: UIButton!
    
    
    
    override func awakeFromNib() {
        
        
        imgProfile.setCornerRaduisToRound()
        
        onlyPictures.alignment = .left
        onlyPictures.countPosition = .right
        onlyPictures.spacing = 0
        onlyPictures.backgroundColorForCount = .AppSkyBlue
        onlyPictures.textColorForCount = .white
        onlyPictures.fontForCount = UIFont.FontLatoRegular(size: 14)
        
        DispatchQueue.main.async {
            self.viewExpertDotborder.addDashedBorderNew()
        }
        
        imgProfile.setupImageViewer(options: [.theme(.dark)])

        
    }
    
    
    
}
class TagListTableViewCell : UITableViewCell {
    
    @IBOutlet weak var sTagListView: TagListView!
    
    @IBOutlet weak var btnViewMore: UIButton!

    override func awakeFromNib() {
    }
}

extension ProfileVC: OnlyPicturesDataSource {
    
    // ---------------------------------------------------
    // returns the total no of pictures
    
    func numberOfPictures() -> Int {
        return objProfileModel?.arrMyFriends.count ?? 0
    }
    
    // ---------------------------------------------------
    // returns the no of pictures should be visible in screen.
    // In above preview, Left & Right formats are example of visible pictures, if you want pictures to be shown without count, remove this function, it's optional.
    
    func visiblePictures() -> Int {
        return 3
    }
    
    
    // ---------------------------------------------------
    // return the images you want to show. If you have URL's for images, use next function instead of this.
    // use .defaultPicture property to set placeholder image. This only work with local images. for URL's images we provided imageView instance, it's your responsibility to assign placeholder image in it. Check next function.
    // onlyPictures.defaultPicture = #imageLiteral(resourceName: "defaultProfilePicture")
    
    //    func pictureViews(index: Int) -> UIImage {
    //        return objProfileModel?.arrMyFriends[index]
    //    }
    //
    
    // ---------------------------------------------------
    // If you do have URLs of images. Use below function to have UIImageView instance and index insted of 'pictureViews(index: Int) -> UIImage'
    // NOTE: It's your resposibility to assign any placeholder image till download & assignment completes.
    // I've used AlamofireImage here for image async downloading, assigning & caching, Use any library to allocate your image from url to imageView.
    
    func pictureViews(_ imageView: UIImageView, index: Int) {
        
        // Use 'index' to receive specific url from your collection. It's similar to indexPath.row in UITableView.
        /* let url = URL(string: self.objProfileModel?.arrMyFriends[index].profile_image_thumb ?? "")
         
         imageView.image = #imageLiteral(resourceName: "defaultProfilePicture")   // placeholder image
         imageView.af_setImage(withURL: url!)*/
        imageView.setImageWithURL(self.objProfileModel?.arrMyFriends[index].profile_image, "dp_profile")
        
    }
}



//MARK: - API call
extension ProfileVC
{
    @objc func refreshCalled()
       {
           self.webCall_GetProfile()
       }
    func webCall_GetProfile(_ offset : Int = 0){
        
        var delayTime = DispatchTime.now()
        
        if self.tblview.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblview.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            
            WebService.shared.RequesURL(ServerURL.GetProfile, Perameters: ["user_id":globalUserId,"profile_id":globalUserId,"offset":offset],showProgress: false, completion: { (dicRes, success) in
//                debugPrint(dicRes)
                if success == true{
                    if let profile = dicRes["user_data"] as? [String : Any] {
                        self.objProfileModel = ProfileModel.init(profile)
                        self.sideMenuDataSet()
                        self.webCall_UserPost()
                    }
                    DispatchQueue.main.async {
                        UIView.performWithoutAnimation {
                            self.tblview.reloadData()
                        }
                        self.tblview.refreshControl?.endRefreshing()
                    }
                    self.setupData()
                }
                else{
                    self.tblview.refreshControl?.endRefreshing()
                }
            }) { (err) in
                debugPrint(err)
            }
        }
    }
    func webCall_UserPost(_ offset : Int = 0){
        
        var delayTime = DispatchTime.now()
        
        if self.tblview.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblview.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            
            WebService.shared.RequesURL(ServerURL.GetUserPosts, Perameters: ["user_id":globalUserId,"other_person_id":globalUserId,"offset":offset],showProgress: false, completion: { (dicRes, success) in
//                debugPrint(dicRes)
                if success == true{
                    if let arr = dicRes["detail"] as? [[String:Any]] {
                        if offset == 0{
                            self.arrALLHomePost = arr.map({allPostModel.init($0)})
                        }
                        else{
                            self.arrALLHomePost.append(contentsOf: arr.map({allPostModel.init($0)}))
                        }
                    }
                    DispatchQueue.main.async {
                        self.isAvailavbleMoreDat = self.arrALLHomePost.count >= ToInt(dicRes["offset"])
                        UIView.performWithoutAnimation {
                            self.tblview.reloadData()
                        }
                        self.tblview.accessibilityHint = nil
                        self.tblview.refreshControl?.endRefreshing()
                        self.indicator.stopAnimating()
                    }
                    
                }
                else{
                    //                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
                    self.tblview.accessibilityHint = nil
                    self.tblview.refreshControl?.endRefreshing()
                    self.indicator.stopAnimating()
                    
                }
            }) { (err) in
                debugPrint(err)
            }
        }
    }
    
    
    func sideMenuDataSet()
    {
        if var dicGetLogin = getUserDefault(Key: Constant.userDeafult_LoginDic) as? [String : Any] {
//            cell.imgProfile.setImageWithURL(obj.profile_image,"dp_profile")
            if let obj = self.objProfileModel{
                
                dicGetLogin["profile_image"] = obj.profile_image
                dicGetLogin["firstname"] = obj.firstname
                dicGetLogin["lastname"] = obj.lastname
                dicGetLogin["fullName"] = obj.firstname + " " + obj.lastname
                loginData?.profession = obj.arrProfessionalQua.first?.job_title ?? ""
                loginData?.fullName =  obj.firstname + " " + obj.lastname
                
                loginData?.profile_image = obj.profile_image
                
                setUserDefault(dicGetLogin as AnyObject, Key: Constant.userDeafult_LoginDic)
            }
        }
    }
    
    
    func setupData(){
        if let obj = self.objProfileModel{
            self.coverImg.setImageWithURL(obj.cover_image,"video_placeholder")
            
        }
        //For Category
        self.cellTag?.sTagListView.removeAllTags()
        if let arrCat = self.objProfileModel?.arrInt {
            self.cellTag?.sTagListView.addTags(arrCat.map({$0.category_name}))
        }
        
        self.tblview.reloadData()
        self.tblview.isHidden = false
    }
    
}

class ProfileModel {
    
    var about_me =  ""
    var about_my_profession =  ""
    var address =  ""
    // var apply_reset_pwd =  ""
    var category_status =  ""
    var cover_image =  ""
    var date =  ""
    var email =  ""
    var firstname =  ""
    var lastname =  ""
    var fullName =  ""
    var followers =  ""
    var gender =  ""
    var media_id =  ""
    var media_type =  ""
    var member_since =  ""
    var phone =  ""
    var phone_code =  ""
    var profile_image =  ""
    var profile_image_thumb =  ""
    var profile_updated =  ""
    var token =  ""
    var total_following =  ""
    var total_friends =  ""
    var user_id =  ""
    var user_post =  ""
    var screen_code =  ""
    var profession =  ""


    var is_expert =  false
    var status =  false
    var notification_status =  false
    
    var arrInt = [objCategoryInterested]()
    var arrMyFriends = [objMYFriends]()
    var arrProfessionalQua = [objProfessionalQuali]()
    
    var arrEducationQuali = [objEducationQuali]()
    
    var aboutEducation = ""

    
    init(_ dict : [String:Any])
    {
        //For Interested Category
        if let arr = dict["interest_categories"] as? [[String:Any]] {
            self.arrInt = arr.map({objCategoryInterested.init($0)})
        }
        
        //For My Friend list
        if let arr = dict["my_friends"] as? [[String:Any]] {
            self.arrMyFriends = arr.map({objMYFriends.init($0)})
        }
        
        //For Professional
        if let arr = dict["professional_qualification"] as? [[String:Any]] {
            self.arrProfessionalQua = arr.map({objProfessionalQuali.init($0)})
        }
        
        //For Education
        if let arr = dict["educational_qualification"] as? [[String:Any]] {
            self.arrEducationQuali = arr.map({objEducationQuali.init($0)})
        }
        
        //For Education
        if let arr = dict["educational_qualification"] as? [[String:Any]] {
            if let dic = arr.first
            {
                self.aboutEducation = toString(dic["qualification"])
            }
        }
        
        self.user_id = toString(dict["user_id"])
        self.token = toString(dict["token"])
        self.screen_code = toString(dict["screen_code"])
        self.profession = toString(dict["profession"])
        self.email = toString(dict["email"])
        self.firstname = toString(dict["firstname"])
        self.lastname = toString(dict["lastname"])
       self.fullName = self.firstname + " " + self.lastname
        self.about_my_profession = toString(dict["about_my_profession"])
        self.about_me = toString(dict["about_me"])
        self.address = toString(dict["address"])
        self.date = toString(dict["date"])
        self.total_following = toString(dict["total_following"])
        self.followers = toString(dict["followers"])
        self.cover_image = toString(dict["cover_image"])
        self.profile_image = toString(dict["profile_image"])
        self.profile_image_thumb = toString(dict["profile_image_thumb"])
        self.category_status = toString(dict["category_status"])
        self.gender =  toString(dict["gender"])
        
        self.member_since = toString(dict["member_since"]).dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_MMMddyyyy)
        self.phone = toString(dict["phone"])
        self.phone_code = toString(dict["phone_code"])
        
        
        //BOOL
        self.is_expert = toString(dict["is_expert"]) == "1"
        self.status = toString(dict["status"]) == "1"
        self.notification_status = toString(dict["notification_status"]) == "1"
        
    }
    
    init(_ other : ProfileModel) {
        
        self.token = other.token
        self.arrInt = other.arrInt
        self.arrMyFriends = other.arrMyFriends
        self.arrProfessionalQua = other.arrProfessionalQua
        self.arrEducationQuali = other.arrEducationQuali
        self.user_id = other.user_id
        self.email = other.email
        self.firstname = other.firstname
        self.lastname = other.lastname
        self.about_my_profession = other.about_my_profession
        self.about_me = other.about_me
        self.address = other.address
        self.date = other.date
        self.total_following = other.total_following
        self.followers = other.followers
        self.cover_image = other.cover_image
        self.profile_image = other.profile_image
        self.profile_image_thumb = other.profile_image_thumb
        self.category_status = other.category_status
        self.member_since = other.member_since
        self.phone = other.phone
        self.phone_code = other.phone_code
        self.is_expert = other.is_expert
        self.status = other.status
        self.notification_status = other.notification_status
        self.gender =  other.gender
    }
}

//Selected category
class objCategoryInterested
{
    var category_id = ""
    var category_name = ""
    
    init(_ dict : [String:Any]) {
        self.category_id = toString(dict["category_id"])
        self.category_name = toString(dict["category_name"])
    }
}

//My Friends
class objMYFriends
{
    //    var cover_image = ""
    //    var email = ""
    //    var firstname = ""
    //    var is_expert = ""
    //    var lastname = ""
    var name = ""
    //    var notification_status = ""
    //    var profession = ""
    var profile_image = ""
    var profile_image_thumb = ""
    
    
    init(_ dict : [String:Any]) {
        self.name = toString(dict["name"])
        self.profile_image_thumb = toString(dict["profile_image_thumb"])
        self.profile_image = toString(dict["profile_image"])
    }
}

//
class objProfessionalQuali
{
    var date = ""
    var experience = ""
    var is_current_job = ""
    var job_title = ""
    var user_id = ""
    var user_profession_id = ""
    
    
    init(_ dict : [String:Any]) {
        self.date = toString(dict["date"])
        self.experience = toString(dict["experience"])
        self.is_current_job = toString(dict["is_current_job"])
        self.job_title = toString(dict["job_title"])
        self.user_id = toString(dict["user_id"])
        self.user_profession_id = toString(dict["user_profession_id"])
    }
    
    init(_ temp: String = "blank_init") {
        self.date = ""
        self.experience = ""
        self.is_current_job = ""
        self.job_title = ""
        self.user_id = ""
        self.user_profession_id = ""
    }
    
    var toDict_Professional : [String : Any] {
        return ["date":self.date, "experience" : self.experience, "is_current_job": self.is_current_job, "job_title": self.job_title, "user_id": self.user_id, "user_profession_id": self.user_profession_id]
    }
}

class objEducationQuali
{
    var qualification = ""
    var year = ""
    
    
    init(_ dict : [String:Any]) {
        self.qualification = toString(dict["qualification"])
        self.year = toString(dict["year"])
    }
    
    init(_ qua: String, year: String) {
        self.qualification = qua
        self.year = year
    }
    
    var toDict_Education : [String : Any] {
        return ["edu_qua":self.qualification,"year" : self.year]
    }
}
