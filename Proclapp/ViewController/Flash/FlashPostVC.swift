
//
//  FlashPostVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/11/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class FlashPostVC: UIViewController {

    var callback : ((String) -> Void)?

    @IBOutlet weak var clsSelectExpert: UICollectionView!
    var arrFlashUserList = [FlashUserListModel]()

    var getPostID = ""
    @IBOutlet weak var btnDone: UIButton!
    
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    
    var isFromRecommended = false
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var isAvailavbleMoreDat = false
    var arrNoti = [NotificationListModel]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromRecommended
        {
            lbl1.text = "Recommand your post to"
            lbl2.text = "Recommand users to remind them to respond on your post."
            
            DispatchQueue.main.async {
            self.indicator.frame = CGRect(x: CGFloat(0), y: CGFloat(screenheight - 30 - BottomPadding), width: screenwidth, height: CGFloat(30))
            }
            
            self.clsSelectExpert.refreshControl = UIRefreshControl()
            self.clsSelectExpert.refreshControl?.tintColor = .AppSkyBlue
            self.clsSelectExpert.refreshControl?.addTarget(self, action: #selector(refreshCalledRecommanded), for: UIControl.Event.valueChanged)
            self.getRecommanedList()
        }
        else
        {
            self.clsSelectExpert.refreshControl = UIRefreshControl()
            self.clsSelectExpert.refreshControl?.tintColor = .AppSkyBlue
            self.clsSelectExpert.refreshControl?.addTarget(self, action: #selector(refreshCalled), for: UIControl.Event.valueChanged)
            self.getExpertList()
        }
        btnDone.alpha = 0.5
        btnDone.isUserInteractionEnabled = false
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func clickDone(_ sender: Any) {

        webCallFlashPost()
    }
  
    
    @objc func refreshCalled() {
        self.getExpertList()
    }
    
    @objc func getExpertList(_ serviceCount : Int = 0) {
        
        if self.clsSelectExpert.accessibilityHint == "service_calling" { return }
        
        self.clsSelectExpert.accessibilityHint = "service_calling"
        var delayTime = DispatchTime.now()
        
        if self.clsSelectExpert.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.clsSelectExpert.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            
            WebService.shared.RequesURL(ServerURL.FlashUserList, Perameters: ["user_id" : globalUserId],showProgress: false,completion: { (dicRes, success) in
                
                debugPrint(dicRes)
                
                if success == true
                {
                    if let arrData = dicRes["users"] as? [[String:Any]] {
                        self.arrFlashUserList = arrData.map({FlashUserListModel.init($0)})
                    }
                    self.clsSelectExpert.reloadData()
                }
                DispatchQueue.main.async {
                    UIView.performWithoutAnimation {
                        self.clsSelectExpert.reloadData()
                    }
                    self.clsSelectExpert.accessibilityHint = nil
                    self.clsSelectExpert.refreshControl?.endRefreshing()
                }
                
                
            }) { (err) in
                debugPrint("\(ServerURL.Interested_Category):-->",err)
                self.clsSelectExpert.accessibilityHint = nil
                self.clsSelectExpert.refreshControl?.endRefreshing()
            }
        }
    }
    //MARK: API Call
    func webCallFlashPost()
    {
        WebService.shared.RequesURL(isFromRecommended == true ? ServerURL.RecommendPost : ServerURL.FlashPost, Perameters: ["user_id" : globalUserId,"post_id":getPostID,"user":self.arrFlashUserList.filter({$0.is_selected}).map({$0.user_id}).joined(separator: ",")],showProgress: true,completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true
            {
                self.showOkAlertWithHandler(msg: toString(dicRes.object(forKey: "response_msg"))) {
                    self.clickBack(self)
                }
                
                if self.isFromRecommended
                {
                    self.callback?("true")
                }
                
            }
            else{
                self.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
            }
        }) { (err) in
        }
    }
    
    
    @objc func refreshCalledRecommanded() {
        self.getRecommanedList()
    }
    
    @objc func getRecommanedList(_ offset : Int = 0) {
        
        var delayTime = DispatchTime.now()
        
        if self.clsSelectExpert.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.clsSelectExpert.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.GetFriends, Perameters: ["user_id" : globalUserId,"offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                    debugPrint(dicRes)
                    debugPrint("Offset= \(offset)")
                    if let arrPrice = dicRes["friends_list"] as? [[String:Any]] {
                        
                        if offset == 0{
                            self.arrFlashUserList = arrPrice.map({FlashUserListModel.init($0)})
                        }
                        else
                        {
                            self.arrFlashUserList.append(contentsOf: arrPrice.map({FlashUserListModel.init($0)}))
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.isAvailavbleMoreDat = self.arrNoti.count >= ToInt(dicRes["offset"])
                    UIView.performWithoutAnimation {
                        self.clsSelectExpert.reloadData()
                    }
                    self.clsSelectExpert.accessibilityHint = nil
                    self.clsSelectExpert.refreshControl?.endRefreshing()
                    self.indicator.stopAnimating()
                }
            }) { (err) in
                self.clsSelectExpert.accessibilityHint = nil
                self.clsSelectExpert.refreshControl?.endRefreshing()
                self.indicator.stopAnimating()
            }
        }
    }
    
}
extension FlashPostVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrFlashUserList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "FlashCollectionCell", for: indexPath) as! FlashCollectionCell
        
        let obj = arrFlashUserList[indexPath.row]
        
        cell.lblName.text = obj.name
        cell.img.setImageWithURL(obj.profile_image, "experts_placeholder")
        cell.btnActive.isSelected = obj.is_selected
        cell.lblFlashCount.text = isFromRecommended == true ? "" : toString(obj.flashed) + " Flashed"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.width/3, height: 127 * screenscale)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        arrFlashUserList[indexPath.row].is_selected = !arrFlashUserList[indexPath.row].is_selected
        collectionView.reloadItems(at: [indexPath])
        
        let expert_id = self.arrFlashUserList.filter({$0.is_selected}).map({$0.user_id})
        debugPrint(expert_id)
        btnDone.alpha = expert_id.count == 0 ? 0.5 : 1
        btnDone.isUserInteractionEnabled = expert_id.count != 0
    }
    
    func scrollViewDidEndDragging(_ aScrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if self.isAvailavbleMoreDat == true , self.clsSelectExpert.accessibilityHint == nil,self.isFromRecommended{
            self.indicator.startAnimating()
            self.clsSelectExpert.accessibilityHint = "service_calling"
            self.getRecommanedList(arrFlashUserList.count)
        }
    }
    
}
class FlashCollectionCell: UICollectionViewCell {
    
    @IBOutlet var img: UIImageView!
    @IBOutlet var viewRound: UIView!
    @IBOutlet var btnActive: UIButton!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblFlashCount: UILabel!

    
    override func awakeFromNib() {
        
        DispatchQueue.main.async {
            self.img.layer.cornerRadius = self.img.frame.height / 2
            self.img.layer.masksToBounds = true
            self.viewRound.layer.cornerRadius = self.viewRound.frame.height/2
        }
    }
    
    
}
class FlashUserListModel {
    
    var user_id =  ""
    var name =  ""
    var profile_image =  ""
    var is_selected =  false
    var flashed = 0
    
    init(_ dict : [String:Any]) {
        self.user_id =  toString(dict["user_id"])
        self.name = toString(dict["name"])
        self.profile_image = toString(dict["profile_image"])
        self.flashed = ToInt(toString(dict["flashed"]))
        //        self.is_selected = toString(dict["is_selected"]) == "1"
    }
}


