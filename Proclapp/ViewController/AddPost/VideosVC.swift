//
//  VideosVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/24/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class VideosVC: UIViewController {

//    static let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "VideosVC") as! VideosVC

    @IBOutlet weak var tableviewVV: UITableView!

    var arrVoiceVideo = [allPostModel]()
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var isMore = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        DispatchQueue.main.async {
            
            self.indicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tableviewVV.bounds.width, height: CGFloat(30))
            self.tableviewVV.tableFooterView = self.indicator
        }
   
        
        self.tableviewVV.refreshControl = UIRefreshControl()
        self.tableviewVV.refreshControl?.tintColor = .AppSkyBlue
        self.tableviewVV.refreshControl?.addTarget(self, action: #selector(refreshCalled_VoiceVideo), for: UIControl.Event.valueChanged)
        self.getVoiceVideoList()
        
        tableviewVV.register(UINib.init(nibName: "VideoTableViewCell", bundle: nil), forCellReuseIdentifier: "VideoTableViewCell")
        
        tableviewVV.register(UINib.init(nibName: "AudioTableViewCell", bundle: nil), forCellReuseIdentifier: "AudioTableViewCell")


        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func refreshCalled_VoiceVideo() {
        self.getVoiceVideoList()
    }
    
    @objc func getVoiceVideoList(_ offset : Int = 0) {
        
        var delayTime = DispatchTime.now()
        
        if self.tableviewVV.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tableviewVV.refreshControl?.beginRefreshingManually()
        }
        
        debugPrint("Offset= \(offset)")
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.vv_general_list, Perameters: ["user_id" : globalUserId,"vv_type":"2","offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                    debugPrint(dicRes)
                    
                    if let arrData = dicRes["vv_list"] as? [[String:Any]] {
                        
                        if offset == 0{
                            self.arrVoiceVideo = arrData.map({allPostModel.init($0)})
                        }
                        else{
                            self.arrVoiceVideo.append(contentsOf: arrData.map({allPostModel.init($0)}))
                        }
                    }
                    
                    self.isMore = self.arrVoiceVideo.count >= ToInt(dicRes["offset"])
                    
                    
                }
                else{
                    self.isMore = false
                    self.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
                }
                
                DispatchQueue.main.async {
                    
                    debugPrint("isAvailableMoreData= \(self.isMore)")
                    
                    debugPrint("arr= \(self.arrVoiceVideo.count)")
                    
                    debugPrint("toint= \(ToInt(dicRes["offset"]))")
                    
                    
                    UIView.performWithoutAnimation {
                        self.tableviewVV.reloadData()
                    }
                    self.tableviewVV.accessibilityHint = nil
                    self.tableviewVV.refreshControl?.endRefreshing()
                    self.indicator.stopAnimating()
                }
            }) { (err) in
                self.tableviewVV.accessibilityHint = nil
                self.tableviewVV.refreshControl?.endRefreshing()
                self.indicator.stopAnimating()
            }
        }
    }


}
//MARK: - Table view data source
extension VideosVC: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrVoiceVideo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch self.arrVoiceVideo[indexPath.row].audioVideoType{
        case .audio:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AudioTableViewCell") as! AudioTableViewCell
            cell.model = arrVoiceVideo[indexPath.row]
            return cell
            
        case .video:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell") as! VideoTableViewCell
            cell.model = arrVoiceVideo[indexPath.row]
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableView.automaticDimension
    }
    func scrollViewDidEndDragging(_ aScrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        let offset: CGPoint = aScrollView.contentOffset
        let bounds: CGRect = aScrollView.bounds
        let size: CGSize = aScrollView.contentSize
        let inset: UIEdgeInsets = aScrollView.contentInset
        let y = Float(offset.y + bounds.size.height - inset.bottom)
        let h = Float(size.height)
        let reload_distance: Float = 0
        //            print("load more data!!!!!")
        if y > h + reload_distance{
            
            if self.isMore == true , self.tableviewVV.accessibilityHint == nil{
                
                self.indicator.startAnimating()
                self.tableviewVV.accessibilityHint = "service_calling"
                self.getVoiceVideoList(self.arrVoiceVideo.count)
            }
        }
    }
}
