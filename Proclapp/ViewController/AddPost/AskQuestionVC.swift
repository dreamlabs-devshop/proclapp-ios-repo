//
//  AskQuestionVC.swift
//  Proclapp
//
//  Created by Prismetric Tech on 8/23/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class AskQuestionVC: UIViewController {

    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var txtCategory:UITextField!
    @IBOutlet weak var txtAddinfo:UITextField!
    var pickerView = UIPickerView()
    var categoryaSelectedId = String()
    
    var editQuesDict : allPostModel!

    var arrCategoryList = [InterestedCategoryListModel]()

    let defultTextDes = "Write your question here"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if editQuesDict != nil
        {
            categoryaSelectedId = editQuesDict.categories_id
            txtAddinfo.text = editQuesDict.additional_info
            txtView.text = editQuesDict.post_title
            txtCategory.text = editQuesDict.category_name
        }
        else
        {
            categoryaSelectedId = ""
            txtView.text = defultTextDes
            txtView.textColor = UIColor.lightGray
        }
        
        txtView.layer.borderColor = UIColor.lightGray.cgColor
        txtView.layer.borderWidth = 1
        
        pickerView.dataSource = self
        pickerView.delegate = self
        txtCategory.inputView = pickerView
        txtCategory.delegate = self
        
        webCall_InterestedCategory()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func postBtnClicked(_ sender: Any) {
        if validationAskQuestion == true
        {
            if editQuesDict != nil
            {
                postOrEditQuestionService(url: ServerURL.edit_post)
            }
            else
            {
                postOrEditQuestionService(url: ServerURL.add_QA_post)
            }
        }
    }
}

extension AskQuestionVC //Services
{
    func webCall_InterestedCategory()
    {
        WebService.shared.RequesURL(ServerURL.Interested_Category, Perameters: ["user_id" : globalUserId],showProgress: true,completion: { (dicRes, success) in
            
            debugPrint("\(ServerURL.Interested_Category):-->",dicRes)
            
            if success == true
            {
                if let arrPrice = dicRes["interested_category"] as? [[String:Any]] {
                    self.arrCategoryList = arrPrice.map({InterestedCategoryListModel.init($0)})
                }
                self.pickerView.reloadAllComponents()
            }
            
        }) { (err) in
            debugPrint("\(ServerURL.Interested_Category):-->",err)
        }
    }

    var validationAskQuestion: Bool{
        
        self.view.endEditing(true)
        
        var mess = ""
        
        if categoryaSelectedId.isBlank {
            mess = "Please select category"
        }
        else if ToString(txtView.text).isBlank || ToString(txtView.text) == defultTextDes{
            mess = "Please enter question"
        }
        if mess != ""{
            self.showOkAlert(msg: mess)
            return false
        }
        return true
    }

    func postOrEditQuestionService(url : String)
    {
        var param = Dictionary<String, Any>()
        if(editQuesDict != nil)
        {
            param = ["user_id":globalUserId, "post_title": ToString(txtView.text), "additional_info": ToString(txtAddinfo.text), "post_category": categoryaSelectedId, "post_id" : editQuesDict.post_id]
        }
        else
        {
            param = ["user_id":globalUserId, "post_title": ToString(txtView.text), "additional_info": ToString(txtAddinfo.text), "post_category": categoryaSelectedId]
        }
        
        WebService.shared.RequesURL(url, Perameters: param,showProgress: true, completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            if success == true
            {
                self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                    
                    NotificationCenter.default.post(name: .NewDataLoadHome, object:nil)

                    self.navigationController?.popViewController(animated: true)
                })
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }

}
extension  AskQuestionVC : UITextViewDelegate,UITextFieldDelegate
{
    //MARK: - UITextview Delegate Methods
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == defultTextDes {
            txtView.text = nil
        }
        txtView.textColor = UIColor.Appcolor51
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text.isEmpty {
            txtView.textColor = UIColor.lightGray
            txtView.text = defultTextDes
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
           if textField == txtCategory{
               pickerView.selectRow(0, inComponent: 0, animated: false)
               pickerView(pickerView, didSelectRow: 0, inComponent: 0)
           }
           return true
       }
}

extension AskQuestionVC: UIPickerViewDelegate, UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrCategoryList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return  arrCategoryList[row].category_name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if arrCategoryList.count > 0
        {
            txtCategory.text = arrCategoryList[row].category_name
            categoryaSelectedId = arrCategoryList[row].category_id
        }
    }
}
