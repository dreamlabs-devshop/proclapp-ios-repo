//
//  CreateArticleVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/20/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import AlamofireImage
import SafariServices
import SwiftLinkPreview
import CropViewController

class CreateArticleVC: UIViewController {
    
    
//    private var image: UIImage?
//    private var croppingStyle = CropViewCroppingStyle.default
    
//    private var croppedRect = CGRect.zero
//    private var croppedAngle = 0
    
    
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var txtCategory:UITextField!
    @IBOutlet weak var txtTitle:UITextField!
    @IBOutlet weak var colview:UICollectionView!
    @IBOutlet weak var colviewConst:NSLayoutConstraint!
    @IBOutlet weak var scrollView:UIScrollView!
    
    let imagePicker = UIImagePickerController()
    var imageArray = NSMutableArray()
    
    var pickerView = UIPickerView()
    var categoryaSelectedId = String()
    
    var deleteImageIds = NSMutableArray()
    var arrCategoryList = [InterestedCategoryListModel]()
    var editDict : allPostModel!
    
    let defultTextDes = "Write your article here"
    
    var textField: UITextField?
    
    var strPostLink = ""
    
    
//    private var result = Response()
    private let slp = SwiftLinkPreview(cache: InMemoryCache())
    
    @IBOutlet private weak var previewTitle: UILabel?
    @IBOutlet private weak var previewCanonicalUrl: UILabel?
    @IBOutlet private weak var previewDescription: UILabel?
    @IBOutlet private weak var detailedView: UIView?
    @IBOutlet private weak var favicon: UIImageView?
    @IBOutlet private weak var indiCator: UIActivityIndicatorView?


//    static let vcInstace = StoryBoard.AddPost.instantiateViewController(withIdentifier: "CreateArticleVC") as! CreateArticleVC
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailedView?.isHidden = true
        indiCator?.stopAnimating()
        detailedView?.applyDropShadow()
        
        

        
        colviewConst.constant = 100
        if editDict != nil
        {
            categoryaSelectedId = editDict.categories_id
            txtTitle.text = editDict.post_title
            txtView.text = editDict.post_description
            txtCategory.text = editDict.category_name
            imageArray = NSMutableArray.init(array: editDict.arrImages)
            colview.reloadData()
            
            if let previewUrl = editDict?.dicPostLink?.link, checkValidString(previewUrl)
            {
                self.showPreview(previewUrl)
            }
        }
        else
        {
            categoryaSelectedId = ""
            txtView.text = defultTextDes
            txtView.textColor = UIColor.lightGray
        }
        
        txtView.layer.borderColor = UIColor.lightGray.cgColor
        txtView.layer.borderWidth = 1
        
        pickerView.dataSource = self
        pickerView.delegate = self
        txtCategory.inputView = pickerView
        txtCategory.delegate = self
        
        imagePicker.delegate = self
        
        webCall_InterestedCategory()
      
        self.view.layoutIfNeeded()
        
        // Do any additional setup after loading the view.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
            scrollView.contentOffset.x = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func postBtnClicked(_ sender: Any) {
        if validationAskQuestion == true
        {
            if editDict != nil
            {
                createOrEditArcticleService(url: ServerURL.edit_post)
            }
            else
            {
                createOrEditArcticleService(url: ServerURL.add_article_post)
            }
        }
    }
    
    @IBAction func photoBtnclicked(_ sender: Any) {
        showActionSheet()
    }
    
    @IBAction func closeBtnClicked(_ sender: UIButton) {
        
        let obj = imageArray[sender.tag] as Any
        
        if obj is imagePostModel
        {
            deleteImageIds.add((obj as! imagePostModel).post_img_id)
        }
        
        imageArray.removeObject(at: sender.tag)
        self.colview.reloadData()
    }
   
}
//MARK: Services
extension CreateArticleVC
{
    func webCall_InterestedCategory()
    {        WebService.shared.RequesURL(ServerURL.Interested_Category, Perameters: ["user_id" : globalUserId],showProgress: true,completion: { (dicRes, success) in
        
        debugPrint("\(ServerURL.Interested_Category):-->",dicRes)
        
        if success == true
        {
            if let arrPrice = dicRes["interested_category"] as? [[String:Any]] {
                self.arrCategoryList = arrPrice.map({InterestedCategoryListModel.init($0)})
            }
            self.pickerView.reloadAllComponents()
        }
        
    }) { (err) in
        debugPrint("\(ServerURL.Interested_Category):-->",err)
        }
    }
    
    var validationAskQuestion: Bool{
        
        self.view.endEditing(true)
        
        var mess = ""
        
        if categoryaSelectedId.isBlank {
            mess = "Please select category"
        }
        else if ToString(txtTitle.text).isBlank{
            mess = "Please enter title"
        }
        else if ToString(txtView.text).isBlank || ToString(txtView.text) == defultTextDes{
            mess = "Please enter your article"
        }
        if mess != ""{
            self.showOkAlert(msg: mess)
            return false
        }
        return true
    }
    
    func createOrEditArcticleService(url : String)
    {
        var param = Dictionary<String, Any>()
        if(editDict != nil)
        {
            param = ["user_id":globalUserId, "post_title": ToString(txtTitle.text), "post_description": ToString(txtView.text), "post_category": categoryaSelectedId, "post_id":editDict.post_id, "delete_img": deleteImageIds.count == 0 ? "" : deleteImageIds.componentsJoined(by: ","),"post_link":strPostLink]
        }
        else
        {
            param = ["user_id":globalUserId, "post_title": ToString(txtTitle.text), "post_description": ToString(txtView.text), "post_category": categoryaSelectedId,"post_link":strPostLink]
        }
        
        var imges = [UIImage]()
        imageArray.forEach { (obj) in
            if obj is UIImage
            {
                imges.append(obj as! UIImage)
            }
        }
        WebService.shared.webRequestForMultipleImages(DictionaryImages: imageArray.count > 0 ? ["post_img": imges] : nil, urlString: url, Perameters: param, completion: { (dicRes, success) in
            debugPrint(dicRes)
            
            if success == true
            {
                self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                    NotificationCenter.default.post(name: .NewDataLoadHome, object:nil)
                    self.navigationController?.popViewController(animated: true)

                })
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
            
        }) { (err) in
            debugPrint(err)
        }
    }
}
extension  CreateArticleVC : UITextViewDelegate,UITextFieldDelegate
{
    //MARK: - UITextview Delegate Methods
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == defultTextDes {
            txtView.text = nil
        }
        txtView.textColor = UIColor.Appcolor51
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text.isEmpty {
            txtView.textColor = UIColor.lightGray
            txtView.text = defultTextDes
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtCategory{
            pickerView.selectRow(0, inComponent: 0, animated: false)
            pickerView(pickerView, didSelectRow: 0, inComponent: 0)
        }
        return true
    }
    
    
}
//MARK: picker View methods

extension CreateArticleVC: UIPickerViewDelegate, UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrCategoryList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return  arrCategoryList[row].category_name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if arrCategoryList.count > 0
        {
            txtCategory.text = arrCategoryList[row].category_name
            categoryaSelectedId = arrCategoryList[row].category_id
        }
    }
}

//MARK: Collection view methods
extension CreateArticleVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! imageCell
        
        let obj = imageArray[indexPath.row] as Any
        
        if obj is imagePostModel
        {
            cell.imageView.setImageWithURL((obj as! imagePostModel).image_name, nil)
        }
        else if obj is UIImage
        {
            cell.imageView.image = (imageArray[indexPath.row] as! UIImage)
        }
        
        cell.closeBtn.tag = indexPath.row
        let height = colview.collectionViewLayout.collectionViewContentSize.height
        colviewConst.constant = height
        self.view.layoutIfNeeded()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width - 30)/3, height: (collectionView.frame.width - 30)/3)
    }
    
}

//MARK: Image picker

extension CreateArticleVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate
{
    func camera()
    {
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func photoLibrary()
    {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    /*func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imageArray.add(info[UIImagePickerController.InfoKey.editedImage] as! UIImage)
        self.dismiss(animated: true, completion: nil)
        self.colview.reloadData()
    }*/
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) else { return }
        
        let cropController = CropViewController(croppingStyle: .default, image: image)
        //cropController.modalPresentationStyle = .fullScreen
        cropController.delegate = self
//        self.image = image
        picker.dismiss(animated: true, completion: {
            self.present(cropController, animated: true, completion: nil)
            //self.navigationController!.pushViewController(cropController, animated: true)
        })
    }
    
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        //        self.croppedRect = cropRect
        //        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        //        self.croppedRect = cropRect
        //        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        
        cropViewController.dismiss(animated: true, completion:{
            self.imageArray.add(image)
            self.colview.reloadData()
        })
    }

    
}

class imageCell : UICollectionViewCell
{
    @IBOutlet var imageView : UIImageView!
    @IBOutlet var closeBtn : UIButton!
    
    override func awakeFromNib() {
        imageView.layer.cornerRadius = 8
    }
}

extension String {
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}


extension CreateArticleVC
{
    @IBAction func clickLink(_ sender: UIButton) {
        
        openAlertView()
    }
    func configurationTextField(textField: UITextField!) {
        if (textField) != nil {
            self.textField = textField!
            //Save reference to the UITextField
            self.textField?.placeholder = "Enter a link"
            self.textField?.keyboardType = .URL
        }
    }
    
    func openAlertView() {
        let alert = UIAlertController(title: Constant.appName, message: nil, preferredStyle: .alert)
        alert.addTextField(configurationHandler: configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler:{ (UIAlertAction) in
            self.showPreview(toString(self.textField?.text))
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showPreview(_ linkURL:String) {
        
        if !checkValidString(linkURL)
        {
            return
        }
        
        self.detailedView?.isHidden = true
        indiCator?.startAnimating()
        
        func setDataPreview(_ result: Response) {
            print("url: ", result.url ?? "no url")
            print("finalUrl: ", result.finalUrl ?? "no finalUrl")
            print("canonicalUrl: ", result.canonicalUrl ?? "no canonicalUrl")
            print("title: ", result.title ?? "no title")
            print("images: ", result.images ?? "no images")
            print("image: ", result.image ?? "no image")
            print("video: ", result.video ?? "no video")
            print("icon: ", result.icon ?? "no icon")
            print("description: ", result.description ?? "no description")
            
            self.previewTitle?.text = toString(result.title)
            self.previewCanonicalUrl?.text = toString(result.finalUrl)
            self.previewDescription?.text = toString(result.description)
            
            if let icon = result.image
            {
                self.favicon?.setImageWithURL(toString(icon), "logo_small")
            }
            else
            {
                self.favicon?.setImageWithURL(toString(result.icon), "logo_small")
            }
            
            self.detailedView?.isHidden = false
            self.indiCator?.stopAnimating()
            self.strPostLink =  self.previewCanonicalUrl?.text ?? ""
        }
        
        self.slp.preview(
            linkURL,
            onSuccess: { result in
                setDataPreview(result)
                //                self.result = result
        },
            onError: { error in
                print(error)
                self.showOkAlert(msg: "Invalid URL!")
                self.indiCator?.stopAnimating()
        }
        )
    }
    @IBAction func openWithAction(_ sender: UIButton) {
        if strPostLink.isEmpty == false{
            openURL(URL.init(string: strPostLink)!)
        }
    }
    func openURL(_ url: URL) {
        if ["http", "https"].contains(url.scheme?.lowercased() ?? "") {
            self.present(SFSafariViewController(url: url), animated: true, completion: nil)
        } else {
            linkurlOpen_MS(linkurl: url.absoluteString)
        }
    }
    @IBAction func closePriview(_ sender: UIButton) {
       strPostLink = ""
        detailedView?.isHidden = true
    }
    
}
