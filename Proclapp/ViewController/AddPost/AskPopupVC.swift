//
//  AskPopupVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/17/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class AskPopupVC: UIViewController {
    
    var callback : ((Int) -> Void)?
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var viewShadow: UIView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        viewShadow.addShadow(radius: 3.0,corner: 8.0)
        stackView.spacing = 30 * screenscale
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    
    
    @IBAction func clickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func clickselectList(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.callback?(sender.tag)
        }
    }
    
    
    @IBAction func addAdvert(_ sender: UIButton) {
    }
}

