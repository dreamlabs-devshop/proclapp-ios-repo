
//
//  SelectExpertVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/19/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class SelectExpertVC: UIViewController {
    
    var expert_id = ""
    
    var getObjSelectedCategory: InterestedCategoryListModel?
    
    var postId = ""
    
    var arrExpertList = [ExpertListModel]()
    
    //asha03/09
    var dictCatgeory : InterestedCategoryListModel?
    //asha03/09
    
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var clsSelectExpert: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnNext.setTitle(postId.isEmpty == true ? "Next" : "Request Expert", for: .normal)
        
        self.clsSelectExpert.refreshControl = UIRefreshControl()
        self.clsSelectExpert.refreshControl?.tintColor = .AppSkyBlue
        self.clsSelectExpert.refreshControl?.addTarget(self, action: #selector(refreshCalled), for: UIControl.Event.valueChanged)
        self.getExpertList()
    }
    
    @objc func refreshCalled() {
        self.getExpertList()
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func btnNextClicked(_ sender: UIButton)
    {
    
        if postId.isEmpty == true
        {
            let vc =  StoryBoard.Voice.instantiateViewController(withIdentifier: "VoiceVideoVC") as! VoiceVideoVC
            
            vc.dictExpert = self.arrExpertList[sender.tag]
            //asha03/09
            vc.dictCatgeory = self.dictCatgeory
            //asha03/09
            
            self.pushTo(vc)
        }
        else
        {
            self.webCallRequestToExpert()
        }
    }
    
    //MARK: API Call
    func webCallRequestToExpert(){
//        let expert_id = self.arrExpertList.filter({$0.is_selected}).map({$0.user_id})
        if expert_id.isEmpty{return}
        
        
        WebService.shared.RequesURL(ServerURL.request_to_expert, Perameters: ["user_id" : globalUserId,"post_id":postId,"expert_id":expert_id],showProgress: true,completion: { (dicRes, success) in
            if success == true{
                debugPrint(dicRes)
                self.clickBack(self)
            }
            else{
                self.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
            }
        }) { (err) in
        }
    }
    @objc func getExpertList(_ serviceCount : Int = 0) {
        
        if self.clsSelectExpert.accessibilityHint == "service_calling" { return }
        
        self.clsSelectExpert.accessibilityHint = "service_calling"
        var delayTime = DispatchTime.now()
        
        if self.clsSelectExpert.refreshControl?.isRefreshing == false{
            delayTime = DispatchTime.now() + 0.5
            self.clsSelectExpert.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.ExpertList, Perameters: ["user_id" : globalUserId],showProgress: false,completion: { (dicRes, success) in
                debugPrint(dicRes)
                if success == true
                {
                    if let arrData = dicRes["expert_list"] as? [[String:Any]] {
//                        self.arrExpertList.removeAll()
                        self.arrExpertList = arrData.map({ExpertListModel.init($0)})
                    }
                    
                }
                DispatchQueue.main.async {
                    UIView.performWithoutAnimation {
                        self.clsSelectExpert.reloadData()
                    }
                    self.clsSelectExpert.accessibilityHint = nil
                    self.clsSelectExpert.refreshControl?.endRefreshing()
                }
            }) { (err) in
                if serviceCount < 2 {
                    self.clsSelectExpert.accessibilityHint = nil
                    self.getExpertList(serviceCount + 1)
                } else {
                    debugPrint("\(ServerURL.Interested_Category):-->",err)
                    self.clsSelectExpert.accessibilityHint = nil
                    self.clsSelectExpert.refreshControl?.endRefreshing()
                }
            }
        }
    }
}

extension SelectExpertVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrExpertList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "selectExpertCollectionCell", for: indexPath) as! selectExpertCollectionCell
        
        let obj = arrExpertList[indexPath.row]
        
        cell.lblName.text = obj.FullName
        cell.btnActive.isSelected = obj.is_active
        cell.btnSelect.isSelected = obj.is_selected
        cell.img.setImageWithURL(obj.profile_image, "experts_placeholder")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.width/3, height: 114 * screenscale)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        btnNext.tag = indexPath.row
        btnNext.alpha = 1.0
        btnNext.isUserInteractionEnabled = true
        
        _ = self.arrExpertList.map({ (obj) -> ExpertListModel in
            obj.is_selected = false
            return obj
        })
        arrExpertList[indexPath.row].is_selected = true
        
        expert_id = arrExpertList[indexPath.row].user_id

        
        self.clsSelectExpert.reloadData()
    }
    
}
class selectExpertCollectionCell: UICollectionViewCell {
    
    @IBOutlet var img: UIImageView!
    @IBOutlet var viewRound: UIView!
    @IBOutlet var btnActive: UIButton!
    @IBOutlet var btnSelect: UIButton!
    
    @IBOutlet var lblName: UILabel!
    
    override func awakeFromNib() {
        
        DispatchQueue.main.async {
            self.img.layer.cornerRadius = self.img.frame.height / 2
            self.img.layer.masksToBounds = true
            self.viewRound.layer.cornerRadius = self.viewRound.frame.height/2
        }
    }
    
    
}


