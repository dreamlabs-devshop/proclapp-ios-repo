//
//  BookmarkedPostVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/24/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class BookmarkedPostVC: UIViewController {
    
    var arrBookmarkList = [allPostModel]()
    
    @IBOutlet weak var tblBookmark: UITableView!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var isMore = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblBookmark.register(UINib.init(nibName: "downloadPDFTableCell", bundle: nil), forCellReuseIdentifier: "downloadPDFTableCell")
        
        DispatchQueue.main.async {
            
            self.indicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tblBookmark.bounds.width, height: CGFloat(30))
            self.tblBookmark.tableFooterView = self.indicator
        }
        
        self.tblBookmark.refreshControl = UIRefreshControl()
        self.tblBookmark.refreshControl?.tintColor = .AppSkyBlue
        self.tblBookmark.refreshControl?.addTarget(self, action: #selector(refreshCalled), for: UIControl.Event.valueChanged)
        
        bookmarkListAPI()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func refreshCalled() {
        self.bookmarkListAPI()
    }
    
    @objc func bookmarkListAPI(_ offset : Int = 0) {
        
        var delayTime = DispatchTime.now()
        
        if self.tblBookmark.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblBookmark.refreshControl?.beginRefreshingManually()
        }
        
        debugPrint("Offset= \(offset)")
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.BookmarkedPostList, Perameters: ["user_id" : globalUserId,"offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                    //                    debugPrint(dicRes)
                    
                    if let arrData = dicRes["posts"] as? [[String:Any]] {
                        if offset == 0{
                            self.arrBookmarkList = arrData.map({allPostModel.init($0)})
                        }
                        else
                        {
                        self.arrBookmarkList.append(contentsOf: arrData.map({allPostModel.init($0)}))
                        }
                    }
                    
                    self.isMore = self.arrBookmarkList.count >= ToInt(dicRes["offset"])
                    
                    
                }
                else{
                    self.isMore = false
                    self.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
                }
                
                DispatchQueue.main.async {
                    
                    debugPrint("isAvailableMoreData= \(self.isMore)")
                    
                    debugPrint("arr= \(self.arrBookmarkList.count)")
                    
                    debugPrint("toint= \(ToInt(dicRes["offset"]))")
                    
                    
                    UIView.performWithoutAnimation {
                        self.tblBookmark.reloadData()
                    }
                    self.tblBookmark.accessibilityHint = nil
                    self.tblBookmark.refreshControl?.endRefreshing()
                    self.indicator.stopAnimating()
                }
            }) { (err) in
                self.tblBookmark.accessibilityHint = nil
                self.tblBookmark.refreshControl?.endRefreshing()
                self.indicator.stopAnimating()
            }
        }
    }
}

extension BookmarkedPostVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrBookmarkList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "downloadPDFTableCell") as! downloadPDFTableCell
        cell.model = arrBookmarkList[indexPath.row]
        return cell
    }
    func scrollViewDidEndDragging(_ aScrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        let offset: CGPoint = aScrollView.contentOffset
        let bounds: CGRect = aScrollView.bounds
        let size: CGSize = aScrollView.contentSize
        let inset: UIEdgeInsets = aScrollView.contentInset
        let y = Float(offset.y + bounds.size.height - inset.bottom)
        let h = Float(size.height)
        let reload_distance: Float = 0
        //            print("load more data!!!!!")
        if y > h + reload_distance{
            
            if self.isMore == true , self.tblBookmark.accessibilityHint == nil{
                
                self.indicator.startAnimating()
                self.tblBookmark.accessibilityHint = "service_calling"
                self.bookmarkListAPI(self.arrBookmarkList.count)
            }
        }
    }
}

