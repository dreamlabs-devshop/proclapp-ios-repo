//
//  CreateArticleVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/20/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import AlamofireImage
import SafariServices
import SwiftLinkPreview
import CropViewController
import AVKit
import Social
import CoreServices

class UploadVideoVC: UIViewController {
    
    
    @IBOutlet weak var txtCategory:UITextField!
    @IBOutlet weak var txtTitle:UITextField!
    @IBOutlet weak var colview:UICollectionView!
    @IBOutlet weak var colviewConst:NSLayoutConstraint!
    @IBOutlet weak var scrollView:UIScrollView!
    
    let imagePicker = UIImagePickerController()
    var imageArray = NSMutableArray()
    
    var pickerView = UIPickerView()
    
    var deleteImageIds = NSMutableArray()
    var arrCategoryList = [InterestedCategoryListModel]()
    var editDict : allPostModel!
    
    var textField: UITextField?
    
    var strPostLink = ""
    
    var videoPath: URL? = nil
    
    private let slp = SwiftLinkPreview(cache: InMemoryCache())
    
    @IBOutlet private weak var previewTitle: UILabel?
    @IBOutlet private weak var previewCanonicalUrl: UILabel?
    @IBOutlet private weak var previewDescription: UILabel?
    @IBOutlet private weak var detailedView: UIView?
    @IBOutlet private weak var favicon: UIImageView?
    @IBOutlet private weak var indiCator: UIActivityIndicatorView?


    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailedView?.isHidden = true
        indiCator?.stopAnimating()
        detailedView?.applyDropShadow()
        
        

        
//        colviewConst.constant = 100
        if editDict != nil
        {
            txtTitle.text = editDict.post_title
            txtCategory.text = editDict.category_name
            imageArray = NSMutableArray.init(array: editDict.arrImages)
            colview.reloadData()
            
            if let previewUrl = editDict?.dicPostLink?.link, checkValidString(previewUrl)
            {
                self.showPreview(previewUrl)
            }
        }
       
        imagePicker.delegate = self
        self.view.layoutIfNeeded()
        
        // Do any additional setup after loading the view.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
            scrollView.contentOffset.x = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func postBtnClicked(_ sender: Any) {
        submitVideoService(url: ServerURL.AddVVPost)
    }
    
    @IBAction func photoBtnclicked(_ sender: Any) {
        showActionSheet()
    }
    
    @IBAction func closeBtnClicked(_ sender: UIButton) {
        
        let obj = imageArray[sender.tag] as Any
        
        if obj is imagePostModel
        {
            deleteImageIds.add((obj as! imagePostModel).post_img_id)
        }
        
        imageArray.removeObject(at: sender.tag)
        self.colview.reloadData()
    }
   
}
//MARK: Services
extension UploadVideoVC
{
    
    
    var validationAskQuestion: Bool{
        
        self.view.endEditing(true)
        
        var mess = ""
        
       if ToString(txtTitle.text).isBlank{
            mess = "Please enter title"
        }
        
        if mess != ""{
            self.showOkAlert(msg: mess)
            return false
        }
        return true
    }
    
    func submitVideoService(url : String)
    {
        
        if (self.videoPath == nil){
            self.showOkAlert(msg: "Please select a video")
            return
        }
        
        var param = Dictionary<String, Any>()
        if(editDict != nil)
        {
            param = ["user_id":globalUserId, "post_title": "", "post_description": ToString(txtTitle.text), "vv_type":"2", "post_id":editDict.post_id, "post_link":strPostLink]
        }
        else
        {
            param = ["user_id":globalUserId, "post_title": "", "post_description": ToString(txtTitle.text),"vv_type":"2","post_link":strPostLink]
        }
        
        self.indiCator?.startAnimating();
        WebService.shared.webRequestForUplaodVideo(DictionaryVideo: ["file": videoPath as Any], urlString: url, Perameters: param, completion: { (dicRes, success) in
            debugPrint(dicRes)
            
            if success == true
            {
                self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                    NotificationCenter.default.post(name: .NewDataLoadHome, object:nil)
                    self.navigationController?.popViewController(animated: true)

                })
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
            
            self.indiCator?.stopAnimating();
        }) { (err) in
            debugPrint(err)
            self.indiCator?.stopAnimating();
        }
    }
}

extension UploadVideoVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate
{
    func camera()
    {
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = false
        imagePicker.mediaTypes = [kUTTypeMovie as String]
        imagePicker.videoQuality = .typeIFrame1280x720
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func photoLibrary()
    {
        imagePicker.sourceType = .savedPhotosAlbum
        imagePicker.allowsEditing = false
        imagePicker.mediaTypes = [kUTTypeMovie as String]
        imagePicker.videoQuality = .typeIFrame1280x720
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    /*func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imageArray.add(info[UIImagePickerController.InfoKey.editedImage] as! UIImage)
        self.dismiss(animated: true, completion: nil)
        self.colview.reloadData()
    }*/
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let videoPath = (info[UIImagePickerController.InfoKey.mediaURL] as? URL) else {
//            print("url: ", videoPath );
            self.showOkAlert(msg: "Media not found")
            return
           // return self.dispatchError("Media not found")
        }
//
//        print("url: ", String(describing: videoPath))
        
        self.videoPath = videoPath
//        if let image = UIImage(named: "video_selected") {
//            self.uploadVideoBtn.setImage(image, for: .normal)
//        }
       
       
        
        picker.dismiss(animated: true, completion: {
        })
    }
    
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        //        self.croppedRect = cropRect
        //        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        //        self.croppedRect = cropRect
        //        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        
        cropViewController.dismiss(animated: true, completion:{
            self.imageArray.add(image)
            self.colview.reloadData()
        })
    }

    
}



extension UploadVideoVC
{
    @IBAction func clickLink(_ sender: UIButton) {
        
        openAlertView()
    }
    func configurationTextField(textField: UITextField!) {
        if (textField) != nil {
            self.textField = textField!
            //Save reference to the UITextField
            self.textField?.placeholder = "Enter a link"
            self.textField?.keyboardType = .URL
        }
    }
    
    func openAlertView() {
        let alert = UIAlertController(title: Constant.appName, message: nil, preferredStyle: .alert)
        alert.addTextField(configurationHandler: configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler:{ (UIAlertAction) in
            self.showPreview(toString(self.textField?.text))
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showPreview(_ linkURL:String) {
        
        if !checkValidString(linkURL)
        {
            return
        }
        
        self.detailedView?.isHidden = true
        indiCator?.startAnimating()
        
        func setDataPreview(_ result: Response) {
            print("url: ", result.url ?? "no url")
            print("finalUrl: ", result.finalUrl ?? "no finalUrl")
            print("canonicalUrl: ", result.canonicalUrl ?? "no canonicalUrl")
            print("title: ", result.title ?? "no title")
            print("images: ", result.images ?? "no images")
            print("image: ", result.image ?? "no image")
            print("video: ", result.video ?? "no video")
            print("icon: ", result.icon ?? "no icon")
            print("description: ", result.description ?? "no description")
            
            self.previewTitle?.text = toString(result.title)
            self.previewCanonicalUrl?.text = toString(result.finalUrl)
            self.previewDescription?.text = toString(result.description)
            
            if let icon = result.image
            {
                self.favicon?.setImageWithURL(toString(icon), "logo_small")
            }
            else
            {
                self.favicon?.setImageWithURL(toString(result.icon), "logo_small")
            }
            
            self.detailedView?.isHidden = false
            self.indiCator?.stopAnimating()
            self.strPostLink =  self.previewCanonicalUrl?.text ?? ""
        }
        
        self.slp.preview(
            linkURL,
            onSuccess: { result in
                setDataPreview(result)
                //                self.result = result
        },
            onError: { error in
                print(error)
                self.showOkAlert(msg: "Invalid URL!")
                self.indiCator?.stopAnimating()
        }
        )
    }
    @IBAction func openWithAction(_ sender: UIButton) {
        if strPostLink.isEmpty == false{
            openURL(URL.init(string: strPostLink)!)
        }
    }
    func openURL(_ url: URL) {
        if ["http", "https"].contains(url.scheme?.lowercased() ?? "") {
            self.present(SFSafariViewController(url: url), animated: true, completion: nil)
        } else {
            linkurlOpen_MS(linkurl: url.absoluteString)
        }
    }
    @IBAction func closePriview(_ sender: UIButton) {
       strPostLink = ""
        detailedView?.isHidden = true
    }
    
}
