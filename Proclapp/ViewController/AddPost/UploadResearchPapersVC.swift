//
//  UploadResearchPapersVC.swift
//  Proclapp
//
//  Created by Prismetric Tech on 8/26/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import MobileCoreServices
import PDFKit

class UploadResearchPapersVC: UIViewController {

    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var txtCategory:UITextField!
    @IBOutlet weak var outerView:UIView!

    @IBOutlet weak var pdfImage:UIImageView!
    @IBOutlet weak var pdfName:UILabel!
    @IBOutlet weak var pdfSize:UILabel!
    var editDict : allPostModel!

    var pickerView = UIPickerView()
    var categoryaSelectedId = String()
    
    var pdfURLStr = String()
    var arrCategoryList = [InterestedCategoryListModel]()
    
    let defultTextDes = "Write your title here"

    override func viewDidLoad() {
        super.viewDidLoad()

        if editDict != nil
        {
            categoryaSelectedId = editDict.categories_id
            txtView.text = editDict.post_title
            txtCategory.text = editDict.category_name
            pdfURLStr = editDict.dicPDF!.file_name
            pdfName.text = editDict.dicPDF?.ori_file_name
            pdfSize.text = editDict.dicPDF?.file_size

            pdfImage.isHidden = false
            pdfName.isHidden = false
            pdfSize.isHidden = false
        }
        else
        {
            categoryaSelectedId = ""
            txtView.text = defultTextDes
            txtView.textColor = UIColor.lightGray
            pdfImage.isHidden = true
            pdfName.isHidden = true
            pdfSize.isHidden = true
        }

        outerView.layer.borderColor = UIColor.lightGray.cgColor
        outerView.layer.borderWidth = 1
        
        pickerView.dataSource = self
        pickerView.delegate = self
        txtCategory.inputView = pickerView
        txtCategory.delegate = self

        webCall_InterestedCategory()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func postBtnClicked(_ sender: Any) {
        if validationReasearchPapers == true
        {
            if editDict != nil
            {
                postOrEditRPPostService(url: ServerURL.edit_post)
            }
            else
            {
                postOrEditRPPostService(url: ServerURL.add_RP_post)
            }
        }
    }
    
    @IBAction func addPDFBtnClicked(_ sender: Any)
    {
//        clickFunctionOpenPdf()
        
        let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), "public.text", "public.data"], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    
    func clickFunctionOpenPdf(){
        
        let documentPicker = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF),String(kUTTypeRTFD),String(kUTTypeFlatRTFD)], in: .import)
        /*  let documentPicker = UIDocumentPickerViewController(documentTypes: ["com.apple.iwork.pages.pages", "com.apple.iwork.numbers.numbers", "com.apple.iwork.keynote.key","public.image", "com.apple.application", "public.item","public.data", "public.content", "public.audiovisual-content", "public.movie", "public.audiovisual-content", "public.video", "public.audio", "public.text", "public.data", "public.zip-archive", "com.pkware.zip-archive", "public.composite-content", "public.text"], in: .import) */

//        let documentPicker = UIDocumentPickerViewController(documentTypes: [kUTTypePDF as String], in: .import)
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
}

extension UploadResearchPapersVC //Services
{
    func webCall_InterestedCategory()
    {
        WebService.shared.RequesURL(ServerURL.Interested_Category, Perameters: ["user_id" : globalUserId],showProgress: true,completion: { (dicRes, success) in
            
            debugPrint("\(ServerURL.Interested_Category):-->",dicRes)
            
            if success == true
            {
                if let arrPrice = dicRes["interested_category"] as? [[String:Any]] {
                    self.arrCategoryList = arrPrice.map({InterestedCategoryListModel.init($0)})
                }
                self.pickerView.reloadAllComponents()
            }
            
        }) { (err) in
            debugPrint("\(ServerURL.Interested_Category):-->",err)
        }
    }
    
    var validationReasearchPapers: Bool{
        
        self.view.endEditing(true)
        
        var mess = ""
        
        if categoryaSelectedId.isBlank {
            mess = "Please select category"
        }
        else if ToString(txtView.text).isBlank || ToString(txtView.text) == defultTextDes{
            mess = "Please enter title"
        }
        else if pdfURLStr.isBlank
        {
            mess = "Please choose file"
        }
        if mess != ""{
            self.showOkAlert(msg: mess)
            return false
        }
        return true
    }
    
    func postOrEditRPPostService(url : String)
    {
        var data = Data()
        do {
            data = try! Data(contentsOf: URL(string: pdfURLStr)!)
            print(data)
        } catch {
            print(error)
        }

        var param = Dictionary<String, Any>()
        if(editDict != nil)
        {
            param = ["user_id":globalUserId, "post_title": ToString(txtView.text), "post_category": categoryaSelectedId, "post_id": editDict.post_id]
        }
        else
        {
            param = ["user_id":globalUserId, "post_title": ToString(txtView.text), "post_category": categoryaSelectedId]
        }

        WebService.shared.uploadPDFFile(data, fileName : pdfName.text!, urlString: url, Perameters : param, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true
            {
                self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                    
                    NotificationCenter.default.post(name: .NewDataLoadHome, object:nil)

                    self.navigationController?.popViewController(animated: true)
                })
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }

        }) { (err) in
            debugPrint(err)
        }
    }
}

extension  UploadResearchPapersVC : UITextViewDelegate,UITextFieldDelegate
{
    //MARK: - UITextview Delegate Methods
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == defultTextDes {
            txtView.text = nil
        }
        txtView.textColor = UIColor.Appcolor51
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text.isEmpty {
            txtView.textColor = UIColor.lightGray
            txtView.text = defultTextDes
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtCategory{
            pickerView.selectRow(0, inComponent: 0, animated: false)
            pickerView(pickerView, didSelectRow: 0, inComponent: 0)
        }
        return true
    }
}

extension UploadResearchPapersVC: UIPickerViewDelegate, UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrCategoryList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return  arrCategoryList[row].category_name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if arrCategoryList.count > 0
        {
        txtCategory.text = arrCategoryList[row].category_name
        categoryaSelectedId = arrCategoryList[row].category_id
        }
    }
}

//MARK: Document Picker
extension UploadResearchPapersVC : UIDocumentMenuDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate
{
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
       
        pdfImage.isHidden = false
        pdfName.isHidden = false
        pdfSize.isHidden = false
        
        pdfURLStr = ToString(myURL)
        print("import result : \(pdfURLStr)")
        
        pdfSize.text = ToString(sizePerMB(url: myURL))
        
        let partsArray = pdfURLStr.components(separatedBy: "/")
        pdfName.text = partsArray.last
    }
    
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
    
    func sizePerMB(url: URL?) -> String {
        guard let filePath = url?.path else {
            return "0.0 KB"
        }
        do {
            let attribute = try FileManager.default.attributesOfItem(atPath: filePath)
            if let size = attribute[FileAttributeKey.size] as? NSNumber {
                if size.doubleValue / 1000.0 < 1023
                {
                    return "\(size.doubleValue / 1000.0) KB"
                }
                else
                {
                    return "\(size.doubleValue / 1000000.0) MB"
                }
            }
        } catch {
            print("Error: \(error)")
        }
        return "0.0 KB"
    }

}
