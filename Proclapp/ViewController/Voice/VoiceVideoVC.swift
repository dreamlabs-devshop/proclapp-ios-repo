


//
//  VoiceVideoVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 6/19/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class VoiceVideoVC: UIViewController {
    
    
    @IBOutlet var btnVoice: UIButton!
    @IBOutlet var btnVideo: UIButton!
    
    //asha03/09
    var dictExpert : ExpertListModel?
    var dictCatgeory : InterestedCategoryListModel?
    //asha03/09
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnVoice.isSelected = true
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func clickVoiceVideo(_ sender: UIButton) {
        btnVoice.isSelected = false
        btnVideo.isSelected = false
        sender.isSelected = true
    }
    @IBAction func clickNext(_ sender: Any)
    {
        if btnVoice.isSelected == true
        {
            let vcInstace = StoryBoard.Voice.instantiateViewController(withIdentifier: "VoiceRecordListVC") as! VoiceRecordListVC
            //asha03/09
            vcInstace.dictCatgeory = self.dictCatgeory
            vcInstace.dictExpert = self.dictExpert
            //asha03/09
            self.pushTo(vcInstace)
        }
        else
        {
            let vcInstace = StoryBoard.Voice.instantiateViewController(withIdentifier: "VideoRecordListVC") as! VideoRecordListVC
            //asha03/09
            vcInstace.dictCatgeory = self.dictCatgeory
            vcInstace.dictExpert = self.dictExpert
            //asha03/09
            self.pushTo(vcInstace)
        }
        
    }
    
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
        if let gesture = navigationController?.interactivePopGestureRecognizer
        {
            view.addGestureRecognizer(gesture)
        }
    }
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
}
