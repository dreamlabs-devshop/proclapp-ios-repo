//
//  RecordVideoVC.swift
//  Proclapp
//
//  Created by iMac-4 on 6/19/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import AVFoundation



class RecordVideoVC: UIViewController {
    
    var callback: (()->())?
    var postDescription = "";

    //MARK:- Properties
    let cameraManager = CameraManager()
    var previewLayer: AVCaptureVideoPreviewLayer!
    var timer : Timer?
    var seconds = 0
    var isTimerRunning = false
    var resumeTapped = false
    var isPause = false
    
    var isRecordingStart = false
    
    var isFromExpertReply = false
    var obj : allPostModel?
    
    //asha03/09
    var dictExpert : ExpertListModel?
    var dictCatgeory : InterestedCategoryListModel?
    //asha03/09
    
    //MARK:- IBOutlet
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnStop: UIButton!
    @IBOutlet weak var btnFlip: UIButton!
    
    //MARK:- View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
       
        cameraManager.getpost_id = ToString(obj?.post_id)
        cameraManager.postDescription = ToString(postDescription);
        
        cameraManager.isExpertCameraManager = isFromExpertReply
        
        btnPlay.layer.cornerRadius = btnPlay.frame.height/2
        btnStop.layer.cornerRadius = btnStop.frame.height/2
        btnPlay.setImage(UIImage(named: "play"), for: .normal)
        
        self.setupCameraPreview()
        
        self.cameraManager.cameraOutputQuality = .high
        self.cameraManager.videoStabilisationMode = .auto
        self.cameraManager.cameraOutputMode = .videoWithMic
        self.cameraManager.writeFilesToPhoneLibrary = true
        self.cameraManager.cameraDevice = .back
        self.cameraManager.addPreviewLayerToView(self.previewView)
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.VideoUpload), name: .VideoUpload, object: nil)
    }
    
    @objc func VideoUpload(notification: NSNotification){
        
        
        DispatchQueue.main.async {
            
            if self.isFromExpertReply
            {
                NotificationCenter.default.post(name: .AnswerPost, object:nil)
                self.clickBack(self)
                
            }
            else{
                self.callback?()
                self.clickBack(self)
            }
            
            
            debugPrint("!!!!!!!! VideoUpload !!!!!!!!!!")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK:- IBAction
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFlipCameraClicked(_ sender: UIButton){
        if cameraManager.cameraDevice == .back{
            cameraManager.cameraDevice = .front
        }else{
            cameraManager.cameraDevice = .back
        }
    }
    @IBAction func btnPlayPauseClicked(_ sender: UIButton){
        
        if isRecordingStart{
            pauseVideoRecording()
        }
        else
        {
            btnPlay.setImage(UIImage(named: "pause"), for: .normal)
            btnFlip.isHidden = true
            isRecordingStart = true
            if isTimerRunning == false {
                runTimer()
            }
            
            cameraManager.startRecordingVideo(isFirst: true)
            debugPrint("start_video_record...")
        }
        
    }
    
    func pauseVideoRecording(){
        if isPause{
            
            btnPlay.setImage(UIImage(named: "pause"), for: .normal)
            runTimer()
            cameraManager.startRecordingVideo(isFirst: false)
            isPause = false
            
        }
        else
        {
            
            btnPlay.setImage(UIImage(named: "play"), for: .normal)
            
            StopTimer()
            isTimerRunning = false
            isPause = true
            cameraManager.stopVideoRecordingTemp()
            
        }
    }
    
    
    
    @IBAction func btnStopCameraClicked(_ sender: UIButton){
        //for stop video recording
        if lblDuration.text == "00:00:00"{ return}
        
       
        
        StopTimer()

        isPause = false
        
        seconds = 0
        lblDuration.text = timeString(time: TimeInterval(seconds))
        isTimerRunning = false
        isRecordingStart = false
        btnFlip.isHidden = false
        btnPlay.setImage(UIImage(named: "play"), for: .normal)
        //asha03/09
        self.cameraManager.catgeoryID = self.dictCatgeory?.category_id
        self.cameraManager.expertID = self.dictExpert?.user_id
        //asha03/09
        self.cameraManager.stopVideoRecording(isStop: true)
        
    }
    
    //MARK:- Function
    func setupCameraPreview(){
        previewLayer = AVCaptureVideoPreviewLayer(session: AVCaptureSession())
        previewLayer.frame = self.view.bounds
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        previewView.layer.addSublayer(previewLayer)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //Duration timer function
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
        isTimerRunning = true
    }
    
    @objc func updateTimer() {
        seconds += 1
        self.lblDuration.text = timeString(time: TimeInterval(seconds))
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    func StopTimer(){
        timer?.invalidate()
        timer = nil
    }
    
    
    
}


