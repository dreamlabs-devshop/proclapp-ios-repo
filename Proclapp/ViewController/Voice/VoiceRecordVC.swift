//
//  VoiceRecordVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 5/18/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import AVFoundation
import SVProgressHUD

class VoiceRecordVC: UIViewController , AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    var callback: (()->())?
    
    var postDescription: String = ""

//    static let vcInstace = StoryBoard.Voice.instantiateViewController(withIdentifier: "VoiceRecordVC") as! VoiceRecordVC
    
    @IBOutlet weak var circularSlider: EFCircularSlider?
    
    @IBOutlet var viewPause: UIView!
    @IBOutlet var viewStop: UIView!
    
    @IBOutlet var btnPlayResume: UIButton!
    
    
    //    @IBOutlet var recordButton: UIButton!
    //    @IBOutlet var playButton: UIButton!
    
    
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioPlayer:AVAudioPlayer!
    
    var counter = 0
    var timer : Timer?
    
    var isPause = false
    var isFromExpertReply = false
    var obj : allPostModel?

    //asha03/09
    var dictExpert : ExpertListModel?
    var dictCatgeory : InterestedCategoryListModel?
    //asha03/09
    
    @IBOutlet  var lblTime: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        viewPause.layer.cornerRadius = viewPause.frame.height/2
        viewStop.layer.cornerRadius = viewStop.frame.height/2
        
        //setup Recorder
        self.setupView()
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.clear, UIColor.darkGray]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.locations = [0, 1]
        //        gradientLayer.frame = bounds
        
        circularSlider?.inputView?.layer.insertSublayer(gradientLayer, at: 0)
        circularSlider?.lineWidth = Int32(3 * screenscale)
        circularSlider?.handleType = .bigCircle
        circularSlider?.handleColor = UIColor.white
        circularSlider?.filledColor = UIColor.AppSkyBlue
        circularSlider?.unfilledColor = UIColor(red:0.36, green:0.36, blue:0.36, alpha:1.0)
        //        circularSlider?.layer.shadowOffset = shadowOffset1
        circularSlider?.layer.shadowColor = UIColor(red: 38/255, green: 45/255, blue: 70/255, alpha: 0.9).cgColor
        circularSlider?.minimumValue = 0
        circularSlider?.maximumValue = 360
        circularSlider?.currentValue = 0
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        self.navigationController?.isNavigationBarHidden = false
        StopTimer()
    }
    
    func StrtTimer(){
        StopTimer()
        // start the timer
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    func StopTimer(){
        timer?.invalidate()
        timer = nil
    }
    
    // called every time interval from the timer
    @objc func timerAction() {
        counter += 1
        lblTime.text = timeString(time: TimeInterval(counter))
        circularSlider?.currentValue = Float(counter)
        
        
    }
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    
    func setupView() {
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        //                        self.loadRecordingUI()
                    } else {
                        // failed to record
                    }
                }
            }
        } catch {
            // failed to record
        }
    }
    
    /*func loadRecordingUI() {
     recordButton.isEnabled = true
     playButton.isEnabled = false
     recordButton.setTitle("Tap to Record", for: .normal)
     recordButton.addTarget(self, action: #selector(recordAudioButtonTapped), for: .touchUpInside)
     //        view.addSubview(recordButton)
     }*/
    
    @objc func recordAudioButtonTapped(_ sender: UIButton) {
        if audioRecorder == nil {
            startRecording()
        } else {
            finishRecording(success: true)
        }
    }
    
    //    func recordSound(){
    //        let dirPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
    //
    //        let recordingName = "my_audio.wav"
    //        let pathArray = [dirPath, recordingName]
    //        let filePath = NSURL.fileURLWithPathComponents(pathArray)
    //        let recordSettings = [AVEncoderAudioQualityKey: AVAudioQuality.Min.rawValue,
    //                              AVEncoderBitRateKey: 16,
    //                              AVNumberOfChannelsKey: 2,
    //                              AVSampleRateKey: 44100.0]
    //        print(filePath)
    //
    //        let session = AVAudioSession.sharedInstance()
    //        do {
    //            try session.setCategory(AVAudioSessionCategoryPlayAndRecord)
    //            audioRecorder = try AVAudioRecorder(URL: filePath!, settings: recordSettings as! [String : AnyObject])
    //        } catch _ {
    //            print("Error")
    //        }
    //
    //        audioRecorder.delegate = self
    //        audioRecorder.meteringEnabled = true
    //        audioRecorder.prepareToRecord()
    //        audioRecorder.record()
    //    }
    //
    
    
    func startRecording() {
        
        let audioFilename = getFileURL()
        
        let settings = [
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
            StrtTimer()
            isPause = false
            
        } catch {
            finishRecording(success: false)
        }
        
        
        
    }
    
    func resumeRecording() {
        isPause = false
        StrtTimer()
        audioRecorder.record()
        lblTime.text = timeString(time: TimeInterval(counter))
        circularSlider?.currentValue = Float(counter)
    }
    
    func pauseRecording(){
        isPause = true
        audioRecorder.pause()
        StopTimer()
        lblTime.text = timeString(time: TimeInterval(counter))
        circularSlider?.currentValue = Float(counter)
    }
    
    func finishRecording(success: Bool) {
        
        if lblTime.text == "00:00:00" { return}
        
        isPause = false
        audioRecorder.stop()
        audioRecorder = nil
        
        counter = 0
        lblTime.text = timeString(time: TimeInterval(counter))
        circularSlider?.currentValue = Float(counter)
        
        if success {
            
            
            
            // recording success :(
            
            //asha03/09
            //            preparePlayer()
            //            audioPlayer.play()
            
            self.isFromExpertReply ? ReplyVoice() : callUploadVoiceRecorderWS()
            
            //asha03/09
            
        } else {
            // recording failed :(
        }
        
        StopTimer()
        
    }
    
    //asha03/09
    func callUploadVoiceRecorderWS(){
        
        SVProgressHUD.show()
        SVProgressHUD.setDefaultAnimationType(.native)
        SVProgressHUD.setDefaultMaskType(.black)
        
        WebService.shared.webRequestForUplaodVoice(DictionaryImages:  ["file": getFileURL()], urlString: ServerURL.AddVVPost, Perameters: ["user_id":globalUserId,"vv_type":"1","post_category":ToString(dictCatgeory?.category_id),"expert":ToString(dictExpert?.user_id), "post_description":ToString(self.postDescription)], completion: { (dicRes, success) in
            debugPrint(dicRes)
            
            if success == true
            {
                self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                     self.callback?()
                    self.clickBack(self)
                })
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
            
            SVProgressHUD.dismiss()
            
        }) { (err) in
            debugPrint(err)
            SVProgressHUD.dismiss()
        }
        
    }
    //asha03/09
    func ReplyVoice(){
        
        SVProgressHUD.show()
        SVProgressHUD.setDefaultAnimationType(.native)
        SVProgressHUD.setDefaultMaskType(.black)
        
        WebService.shared.webRequestForUplaodVoice(DictionaryImages:  ["file": getFileURL()], urlString: ServerURL.ExpertReply, Perameters: ["user_id":globalUserId,"vv_type":"1","post_id": ToString(obj?.post_id), "type": "3", "post_description":ToString(self.postDescription)], completion: { (dicRes, success) in
            debugPrint(dicRes)
            
            self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                if success == true{
                    NotificationCenter.default.post(name: .AnswerPost, object:nil)
                    self.clickBack(self)
                }
            })
            
            SVProgressHUD.dismiss()
            
        }) { (err) in
            debugPrint(err)
            SVProgressHUD.dismiss()
        }
        
    }
    
    
    
    
    /* @IBAction func playAudioButtonTapped(_ sender: UIButton) {
     
     if (sender.titleLabel?.text == "Play"){
     sender.setTitle("Stop", for: .normal)
     preparePlayer()
     audioPlayer.play()
     } else {
     audioPlayer.stop()
     sender.setTitle("Play", for: .normal)
     }
     }*/
    
    
    func preparePlayer() {
        var error: NSError?
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: getFileURL() as URL)
        } catch let error1 as NSError {
            error = error1
            audioPlayer = nil
        }
        
        if let err = error {
            print("AVAudioPlayer error: \(err.localizedDescription)")
        } else {
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
            audioPlayer.volume = 10.0//10.0
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func getFileURL() -> URL {
        let path = getDocumentsDirectory().appendingPathComponent("recording.wav")
        return path as URL
    }
    
    //MARK: Delegates
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("Error while recording audio \(error!.localizedDescription)")
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        //        recordButton.isEnabled = true
        //        playButton.setTitle("Play", for: .normal)
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Error while playing audio \(error!.localizedDescription)")
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickStartRecording(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        if audioRecorder == nil {
            startRecording()
        } else {
            if isPause{
                resumeRecording()
            }
            else{
                pauseRecording()
            }
        }
    }
    @IBAction func clickStopRecording(_ sender: Any) {
        finishRecording(success: true)
        
        btnPlayResume.isSelected = false
    }
    
}

extension Double {
    func asString(style: DateComponentsFormatter.UnitsStyle) -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second, .nanosecond]
        formatter.unitsStyle = style
        guard let formattedString = formatter.string(from: self) else { return "" }
        return formattedString
    }
}


