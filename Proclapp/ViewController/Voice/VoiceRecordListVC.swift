
//
//  VoiceRecordListVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/15/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class VoiceRecordListVC: UIViewController {
    
    
    @IBOutlet weak var circleimage: UIView!
    @IBOutlet weak var btnCreateVideo: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var txtDescription: UITextField!
    
    //asha03/09
    var dictExpert : ExpertListModel?
    var dictCatgeory : InterestedCategoryListModel?
    //asha03/09
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var isMore = false
    var arrVoiceList = [allPostModel]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            self.circleimage.layer.cornerRadius = self.circleimage.bounds.height / 2
            self.circleimage.layer.borderColor = UIColor(red:0.36, green:0.36, blue:0.36, alpha:1.0).cgColor
            self.circleimage.layer.borderWidth = 2
            
            self.btnCreateVideo.layer.cornerRadius = self.btnCreateVideo.bounds.height / 2
            
            self.indicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tblView.bounds.width, height: CGFloat(30))
            self.tblView.tableFooterView = self.indicator
        }
     
        
        self.tblView.refreshControl = UIRefreshControl()
        self.tblView.refreshControl?.tintColor = .AppSkyBlue
        self.tblView.refreshControl?.addTarget(self, action: #selector(refreshCalled_VoiceVideo), for: UIControl.Event.valueChanged)
        self.getAudioList()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        self.navigationController?.isNavigationBarHidden = false
    }
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func clickStrtRecording(_ sender: Any) {
        
        let vcInstace = StoryBoard.Voice.instantiateViewController(withIdentifier: "VoiceRecordVC") as! VoiceRecordVC
        //asha03/09
        vcInstace.dictCatgeory = self.dictCatgeory
        vcInstace.dictExpert = self.dictExpert
        vcInstace.postDescription = self.txtDescription!.text!
        
        vcInstace.callback  = {
            self.txtDescription!.text = ""
            self.getAudioList()
        }
        //asha03/09
        self.pushTo(vcInstace)
    }
    
    @objc func refreshCalled_VoiceVideo() {
        self.getAudioList()
    }
    
    @objc func getAudioList(_ offset : Int = 0) {
        
        var delayTime = DispatchTime.now()
        
        if self.tblView.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblView.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.VVList, Perameters: ["user_id" : globalUserId,"vv_type":"1","offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                                        debugPrint(dicRes)
                    
                    if let arrData = dicRes["vv_list"] as? [[String:Any]] {
                        
                        if offset == 0{
                            self.arrVoiceList = arrData.map({allPostModel.init($0)})
                        }
                        else
                        {
                        self.arrVoiceList.append(contentsOf: arrData.map({allPostModel.init($0)}))
                        }
                    }
                    
                    self.isMore = self.arrVoiceList.count >= ToInt(dicRes["offset"])
                    
                    
                }
                else{
                    self.isMore = false
                    self.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
                }
                
                DispatchQueue.main.async {
                    
                    debugPrint("isAvailableMoreData= \(self.isMore)")
                    
                    debugPrint("arr= \(self.arrVoiceList.count)")
                    
                    debugPrint("toint= \(ToInt(dicRes["offset"]))")
                    
                    
                    UIView.performWithoutAnimation {
                        self.tblView.reloadData()
                    }
                    self.tblView.accessibilityHint = nil
                    self.tblView.refreshControl?.endRefreshing()
                    self.indicator.stopAnimating()
                }
            }) { (err) in
                self.tblView.accessibilityHint = nil
                self.tblView.refreshControl?.endRefreshing()
                self.indicator.stopAnimating()
            }
        }
    }
    
    
}


extension VoiceRecordListVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrVoiceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AudioListCell") as! AudioListCell
        cell.model = arrVoiceList[indexPath.row]

      
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68 * screenscale
    }
    func scrollViewDidEndDragging(_ aScrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        let offset: CGPoint = aScrollView.contentOffset
        let bounds: CGRect = aScrollView.bounds
        let size: CGSize = aScrollView.contentSize
        let inset: UIEdgeInsets = aScrollView.contentInset
        let y = Float(offset.y + bounds.size.height - inset.bottom)
        let h = Float(size.height)
        let reload_distance: Float = 0
        //            print("load more data!!!!!")
        if y > h + reload_distance{
            
            if self.isMore == true , self.tblView.accessibilityHint == nil{
                
                self.indicator.startAnimating()
                self.tblView.accessibilityHint = "service_calling"
                self.getAudioList(self.arrVoiceList.count)
            }
        }
    }
}

class AudioListCell: UITableViewCell{
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnSendReq: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            self.imgView.layer.cornerRadius = self.imgView.bounds.height / 2
        }
    }
    
    var model : allPostModel? {
        didSet {
            lblTime.text = model?.post_date.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_HHMMMddyy)
            
            
            
            if toString(model?.dicAudio?.ori_audio_name).isValid
            {
                lblTitle.text = model?.dicAudio?.ori_audio_name
            }
            else
            {
                lblTitle.text = "  "
            }
            
            /*imgView.image = #imageLiteral(resourceName: "music_ic")
            imgView.contentMode = .center
            btnSendReq.setTitle("Send Request", for: .normal)
            btnSendReq.setTitleColor(UIColor.white, for: .normal)
            btnSendReq.backgroundColor = UIColor.AppSkyBlue*/
            
            if  model?.is_requested_to_expert ?? false
            {
                imgView.image = #imageLiteral(resourceName: "delet_ic")
                imgView.contentMode = .scaleAspectFill
                btnSendReq.setTitle("Delete", for: .normal)
                btnSendReq.setTitleColor(UIColor.AppcolorRed, for: .normal)
                btnSendReq.backgroundColor = UIColor.white
            }
            else
            {
                imgView.image = #imageLiteral(resourceName: "music_ic")
                imgView.contentMode = .center
                btnSendReq.setTitle("Send Request", for: .normal)
                btnSendReq.setTitleColor(UIColor.white, for: .normal)
                btnSendReq.backgroundColor = UIColor.AppSkyBlue
            }
            
        }
    }
    
    @IBAction func clickSendRequest(_ sender: UIButton) {
        if let getObj = model{
            
            if  getObj.is_requested_to_expert//delete post
            {
                clickDeletePost(getObj.post_id)
            }
            else // send request
            {
                if let vc = self.tableView?.parentViewController as? VoiceRecordListVC {
                    debugPrint("expert id->",ToString(vc.dictExpert?.user_id))
                    debugPrint("post id->",getObj.post_id)
                    debugPrint("globalUserId->",globalUserId)
                    
                    
                    
                    WebService.shared.RequesURL(ServerURL.request_to_expert, Perameters: ["user_id" : globalUserId,"post_id":getObj.post_id,"expert_id":ToString(vc.dictExpert?.user_id)],showProgress: true,completion: { (dicRes, success) in
                        if success == true
                        {
                            debugPrint(dicRes)
                            self.imgView.image = #imageLiteral(resourceName: "delet_ic")
                            self.imgView.contentMode = .scaleAspectFill
                            self.btnSendReq.setTitle("Delete", for: .normal)
                            self.btnSendReq.setTitleColor(UIColor.AppcolorRed, for: .normal)
                            self.btnSendReq.backgroundColor = UIColor.white
                            getObj.is_requested_to_expert = true
                        }
                        else{
                            vc.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
                        }
                    }) { (err) in
                    }
                    
                }
            }
            
        }
    }
    func clickDeletePost(_ post_id:String)
    {
        if let indexPath = self.tableView?.indexPathForView(self){
            
            if let vc = self.tableView?.parentViewController as? VoiceRecordListVC {
                
                vc.showOkCancelAlertWithAction(msg: Constant.kAlertDeletePost) { (bool) in
                    if bool
                    {
                        self.callDeletePost(post_id)
                        vc.arrVoiceList.remove(at: indexPath.row)
                        self.tableView?.deleteRows(at: [indexPath], with: .left)
                    }
                }
            }
        }
    }
    
    func callDeletePost(_ post_id:String) {
        WebService.shared.RequesURL(ServerURL.DeletePost, Perameters: ["user_id" : globalUserId,"post_id":post_id],showProgress: false,completion: { (dicRes, success) in
            if success == true{
                debugPrint(dicRes)
            }
        }) { (err) in
        }
    }

   
    
}

