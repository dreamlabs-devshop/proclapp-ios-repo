//
//  ExpertWalletVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 23/07/2019.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

var year_G = 0
var month_G = 0
var monthFull_G = ""

class ExpertWalletVC: UIViewController,UITextFieldDelegate {

  var ss: MonthYearPickerView!
    
    @IBOutlet var tableViewExpertWallet: UITableView!
    
    @IBOutlet var lblwalletAmount: UILabel!
    @IBOutlet var txtDate: UITextField!

    var arrWalletHistory = [WalletHistoryModel]()


    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ss = MonthYearPickerView()
        txtDate.inputView = ss
        txtDate.delegate = self
        
        
        year_G = ToInt(Date().Getyear)
        
        month_G = ToInt(Date().Getmonth)
        
        monthFull_G = Date().GetmonthFull

        txtDate.text = "   " + monthFull_G + " " + "\(year_G)"

        
        
        
        self.tableViewExpertWallet.refreshControl = UIRefreshControl()
        self.tableViewExpertWallet.refreshControl?.tintColor = .white
        self.tableViewExpertWallet.refreshControl?.addTarget(self, action: #selector(refreshCalled), for: UIControl.Event.valueChanged)
        self.getExpertWallet()


        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
              super.viewWillAppear(animated)
              self.navigationController?.isNavigationBarHidden = true
          }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func redeemBtnAction(_ sender: Any) {
        let RedeemVC = self.storyboard?.instantiateViewController(withIdentifier: "RedeemAmountVC") as! RedeemAmountVC
        RedeemVC.redemwallet = toString(self.lblwalletAmount.text)
        RedeemVC.callback =  {
            self.getExpertWallet()
        }
        self.navigationController?.pushViewController(RedeemVC, animated: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        if let m = txtDate.text?.components(separatedBy: " ").first,let y = txtDate.text?.components(separatedBy: " ").last{
            if m != monthFull_G || y != toString(year_G){
                self.txtDate.text = "   " + monthFull_G + " " + "\(year_G)"
                self.getExpertWallet()
            }
        }
    }

   @objc func refreshCalled() {
       self.getExpertWallet()
   }
   
   @objc func getExpertWallet() {
       
       var delayTime = DispatchTime.now()
       
       if self.tableViewExpertWallet.refreshControl?.isRefreshing == false
       {
           delayTime = DispatchTime.now() + 0.5
           self.tableViewExpertWallet.refreshControl?.beginRefreshingManually()
       }
       
       
       DispatchQueue.main.asyncAfter(deadline: delayTime) {
        WebService.shared.RequesURL(ServerURL.getExpertwallet, Perameters: ["user_id" : globalUserId,"month":month_G,"year":year_G],showProgress: false,completion: { (dicRes, success) in
               if success == true{
                   debugPrint(dicRes)
                   if let dic = dicRes["detail"] as? NSDictionary
                   {
                    if let arr = dic["history"] as? [[String:Any]]
                    {
                        self.arrWalletHistory = arr.map({WalletHistoryModel.init($0)})

                    }
                    self.lblwalletAmount.text = "$ \(toString(dic["wallet_amount"]))"
                }
               }
               else{
                   self.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
               }
               
               DispatchQueue.main.async {
                   UIView.performWithoutAnimation {
                       self.tableViewExpertWallet.reloadData()
                   }
                   self.tableViewExpertWallet.refreshControl?.endRefreshing()
               }
           }) { (err) in
               self.tableViewExpertWallet.refreshControl?.endRefreshing()
           }
       }
   }

}
extension ExpertWalletVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrWalletHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "expertWalletCell") as! expertWalletCell
        
        let  obj = arrWalletHistory[indexPath.row]
        
        cell.lblDate.text = obj.date.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_HHMMMddyy)
        cell.lblTitle.text = obj.post_title
        cell.lblAmount.text = "$ \(obj.balance_amount)"
        cell.lblName.text = obj.name
        
         cell.profileImg.setImageWithURL(obj.profile_image, "experts_placeholder")

        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 116 * screenscale
    }
    
    
}
class expertWalletCell : UITableViewCell{
    
    
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblAmount: UILabel!
    @IBOutlet var lblName: UILabel!

    @IBOutlet var profileImg: UIImageView!

    @IBOutlet var outerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            
            self.profileImg.layer.masksToBounds = true
            self.profileImg.layer.cornerRadius = self.profileImg.frame.height/2
            self.outerView.applyDropShadow(color: .gray, alpha: 0.5, x: 0, y: 0, blur: 6, spread: 0, radius: 10, borderWidth: 0)
        }
    }
}
class MonthYearPickerView: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var months: [String]!
    var years: [Int]!
    
    var month = Calendar.current.component(.month, from: Date()) {
        didSet {
            selectRow(month-1, inComponent: 0, animated: false)
        }
    }
    
    var year = Calendar.current.component(.year, from: Date()) {
        didSet {
            selectRow(years.firstIndex(of: year)!, inComponent: 1, animated: true)
        }
    }
    
    var onDateSelected: ((_ month: Int, _ year: Int) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonSetup()
    }
    
    func commonSetup() {
        self.years =  (2019...ToInt(Date().Getyear)).compactMap({$0}).reversed()
        
        
        var months: [String] = []
        var month = 0
        for _ in 1...12 {
            months.append(DateFormatter().monthSymbols[month].capitalized)
            month += 1
        }
        self.months = months
        
        self.delegate = self
        self.dataSource = self
        
        let currentMonth = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.month, from: NSDate() as Date)
        self.selectRow(currentMonth - 1, inComponent: 0, animated: false)
    }
    
    // Mark: UIPicker Delegate / Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return months[row]
        case 1:
            return "\(years[row])"
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return months.count
        case 1:
            return years.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let month = self.selectedRow(inComponent: 0)+1
        let year = years[self.selectedRow(inComponent: 1)]
        if let block = onDateSelected {
            block(month, year)
        }
        month_G = month
        year_G = year
        monthFull_G =  months[self.selectedRow(inComponent: 0)]
        self.month = month
        self.year = year
    }
    
}
extension Date {
    var Getmonth: String {
        dFMS.dateFormat = "MM"
        return dFMS.string(from: self)
    }
    var GetmonthFull: String {
        dFMS.dateFormat = "MMMM"
        return dFMS.string(from: self)
    }
    var Getyear: String {
        dFMS.dateFormat = "yyyy"
        return dFMS.string(from: self)
    }
}
class WalletHistoryModel {

    var balance_amount =  ""
    var currency =  ""
    var date =  ""
    var debit_credit_amount =  ""
    var note =  ""
    var post_id =  ""
    var post_title =  ""
    var transaction_id =  ""
    var id =  ""
    var name =  ""
    var profile_image =  ""
    //    var is_selected =  false
    
    init(_ dict : [String:Any]) {
        self.id =  toString(dict["id"])
        self.name = toString(dict["name"])
        self.profile_image = toString(dict["profile_image"])
        
        self.balance_amount =  toString(dict["balance_amount"])
        self.currency = toString(dict["currency"])
        self.date = toString(dict["date"])
        self.debit_credit_amount =  toString(dict["debit_credit_amount"])
        self.note = toString(dict["note"])
        self.post_id = toString(dict["post_id"])
        self.post_title =  toString(dict["post_title"])
        self.transaction_id = toString(dict["transaction_id"])
    }
}
