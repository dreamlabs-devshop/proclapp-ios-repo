//
//  WebviewPaymentVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 21/11/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import WebKit

class WebviewPaymentVC: UIViewController,WKNavigationDelegate {
   
    var callbackPayment : ((String) -> Void)?

    @IBOutlet var webView: WKWebView!
    @IBOutlet var indicator: UIActivityIndicatorView!
    
    var strPaymentType = ""
    var postId = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        var finalURL = ""
        if strPaymentType == "1"//for is expert in edit profile
        {
            finalURL = "\(ServerURL.DomainURL)do_payment?user_id=\(globalUserId)"
        }
        else if strPaymentType == "2"// for download pdf in RP list
        {
            finalURL = "\(ServerURL.DomainURL)do_payment_subscribe_user?user_id=\(globalUserId)&post_id=\(postId)"

        }
        else if strPaymentType == "3"//for Expert answer see
        {
            finalURL = "\(ServerURL.DomainURL)do_payment_view_answer?user_id=\(globalUserId)&post_id=\(postId)"
        }
        else if strPaymentType == "4"//for Advertise
        {
            finalURL = "\(ServerURL.DomainURL)do_advert_payment?user_id=\(globalUserId)&adv=\(postId)"
        }
        webView.load(finalURL)
        webView.navigationDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           self.navigationController?.isNavigationBarHidden = true
       }
    
    @IBAction func clickBack(_ sender: Any) {
        
        if strPaymentType == "4"//for Advertise
        {
            self.showOkCancelAlertWithAction(msg: Constant.kAlertPaymentFaild) { (success) in
                if success
                {
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: AdvertVC.self) {
                            _ =  self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                }
            }
        }
        else
        {
            self.callbackPayment?(Constant.kPayment_Fail)
            self.showOkAlertWithHandler(msg: Constant.kAlertPaymentFaild)
            {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        
    }

        //MARK:- WKNavigationDelegate
        
        func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
            indicator.stopAnimating()
            print(error.localizedDescription)
        }
        func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
            
            indicator.startAnimating()

            /* print("Strat to load")
             if let url = webView.url?.absoluteString{
             print("Strat url = \(url)")
             }*/
        }
        func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            
            indicator.stopAnimating()

            if let url = webView.url?.absoluteString{
                print("finish url = \(url)")
             
                if let statusGet = (url.components(separatedBy: "/").last)
                {
                    print(statusGet)

                    if statusGet ==  Constant.kPayment_Success
                    {
                        
                        self.callbackPayment?(Constant.kPayment_Success)
                        
                        self.showOkAlertWithHandler(msg: Constant.kAlertPaymentSuccess) {
                            self.navigationController?.popViewController(animated: true)
                        }
                      
                    }
                    else if statusGet == Constant.kPayment_Fail
                    {
                        self.callbackPayment?(Constant.kPayment_Fail)

                        self.showOkAlertWithHandler(msg: Constant.kAlertPaymentFaild) {
                            self.navigationController?.popViewController(animated: true)
                        }
                      
                    }
                }
            }
            
        }

}
extension WKWebView {
    func load(_ urlString: String) {
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            load(request)
        }
    }
}
