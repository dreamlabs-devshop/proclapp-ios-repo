//
//  RedeemAmountVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 23/07/2019.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class RedeemAmountVC: UIViewController {

    var redemwallet = ""
    var callback : (() -> Void)?

    @IBOutlet var lblAvailable: UILabel!
    
    @IBOutlet var txtAccountNumber: SkyFloatingLabelTextField!
    @IBOutlet var txtAccountName: SkyFloatingLabelTextField!
    @IBOutlet var txtBankCode: SkyFloatingLabelTextField!
    @IBOutlet var txtAmmount: SkyFloatingLabelTextField!


    override func viewDidLoad() {
        super.viewDidLoad()

        lblAvailable.text = "Available Wallet balance \(redemwallet)"
        // Do any additional setup after loading the view.
    }
    

    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func clickReddemWallet(_ sender: Any)
    {
        if  validation
        {
            getRedeemWallet()
        }
        
    }
    
    var validation: Bool{
        
        self.view.endEditing(true)
        
        var mess = ""
        
        if ToString(txtAccountNumber.text).isBlank {
            mess = "Please enter a account number"
        }
        else if ToString(txtAccountName.text).isBlank{
            mess = "Please enter a account holder name"
        }
        else if ToString(txtBankCode.text).isBlank{
            mess = "Please enter a bank code"
        }
        else if ToString(txtAmmount.text).isBlank{
            mess = "Please enter a amount"
        }
        if mess != ""
        {
            self.showOkAlert(msg: mess)
            return false
        }
            return true
    }
    func getRedeemWallet() {
        
        WebService.shared.RequesURL(ServerURL.getRedeemwallet, Perameters: ["user_id" : globalUserId,"account_no":toString(txtAccountNumber.text),"account_holder_name":toString(txtAccountName.text),"bank_code":toString(txtBankCode.text),"amount":toString(txtAmmount.text)],showProgress: true,completion: { (dicRes, success) in
           
            debugPrint(dicRes)
            self.showOkAlertWithHandler(msg: toString(dicRes.object(forKey: "response_msg")))
            {
                if success
                {
                    self.callback?()
                    self.navigationController?.popViewController(animated: true)
                }
            }
          
        }) { (err) in
        }
    }

}
