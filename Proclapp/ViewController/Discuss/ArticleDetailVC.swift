//
//  ArticleDetailVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 04/09/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class ArticleDetailVC: UIViewController {

    var cellHeader : cellAnswerHeader?
    
//    var cellViewAnswer: cellSeeAnswer?
    
    @IBOutlet weak var tblview: UITableView!
    @IBOutlet weak var tblAnswerList: UITableView!
    
    var dictPost : allPostModel?
    
    var arrDiscussList = [AnswerModel]()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//         cellHeader = tblview.dequeueReusableCell(withIdentifier: "cellAnswerHeader") as? cellAnswerHeader

        tblview.register(UINib.init(nibName: "ArticleDeclineTableCell", bundle: nil), forCellReuseIdentifier: "ArticleDeclineTableCell")
        
        tblview.register(UINib.init(nibName: "ArticleDeclineSingleImageTableCell", bundle: nil), forCellReuseIdentifier: "ArticleDeclineSingleImageTableCell")


        getAnswerListWS()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.DiscussPost), name: .DiscussPost, object: nil)


        // Do any additional setup after loading the view.
    }
    @objc func DiscussPost(notification: NSNotification){
        DispatchQueue.main.async {
            self.getAnswerListWS()
            debugPrint("!!!!!!!! DiscussPost !!!!!!!!!!")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func clickFilter(_ sender: Any) {
        
        DispatchQueue.main.async {
            guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
            popupVC.arrTitle = Constant.karrFilter
            popupVC.callback =  { str in
                debugPrint(str)
                self.getAnswerListWS(str)
            }
            self.present(popupVC, animated: true, completion: nil)
        }
        
    }
    
    //MARK: - Web service
    
    func getAnswerListWS(_ strGet : String = "") {
        
       var passdata = "3"
        if strGet.lowercased() == "User".lowercased(){
            passdata = "4"
        }
        else if strGet.lowercased() == "Views".lowercased() {
            passdata = "2"
        }
        
        if self.tblview.accessibilityHint == "service_calling" { return }
        
        self.tblview.accessibilityHint = "service_calling"
        var delayTime = DispatchTime.now()
        
        if self.tblview.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblview.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.AnswerList, Perameters: ["user_id" : globalUserId,"post_id":toString(self.dictPost?.post_id),"sort_by":passdata],showProgress: false,completion: { (dicRes, success) in
                debugPrint(dicRes)
                if success == true{
                    if let arrTemp = dicRes["answer_list"] as? [[String:Any]] {
                        self.arrDiscussList = arrTemp.map({AnswerModel.init($0)})
                    }
                }
                DispatchQueue.main.async {
                    UIView.performWithoutAnimation {
                        self.tblview.reloadData()
                    }
                    self.tblview.accessibilityHint = nil
                    self.tblview.refreshControl?.endRefreshing()
                }
                
                
            }) { (err) in
                debugPrint(err)
                self.tblview.accessibilityHint = nil
                self.tblview.refreshControl?.endRefreshing()
            }
        }
        
    }
    
    

}

//MARK: - Table view data source
extension ArticleDetailVC: UITableViewDataSource,UITableViewDelegate, UITableUpdateDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return section == 0 ? 1 : self.arrDiscussList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            let imgesCount = ToInt((dictPost?.arrImages.count))
            if imgesCount == 1
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleDeclineSingleImageTableCell") as! ArticleDeclineTableCell
                cell.model = dictPost
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleDeclineTableCell") as! ArticleDeclineTableCell
                
                //            cell.viewPreviewHeightConst.constant = 100
                //            UIView.animate(withDuration: 0.2) {
                //                self.view.layoutIfNeeded()
                //            }
                cell.model = dictPost
                cell.lblTitle.numberOfLines = 0
                //            cell.lblDes.numberOfLines = 0
                //            cell.txtviewDes.textContainer.maximumNumberOfLines = 0
                
                return cell
            }
            
           
        }
        else
        {
           let cell = tableView.dequeueReusableCell(withIdentifier: "cellViewAnswer") as! cellViewAnswer
                        
                         self.tblAnswerList = tableView

                        cell.model = arrDiscussList[indexPath.row]
                        
                        cell.lblanswer.attributedText  = cell.model?.answer.htmlAttributed(family: "Lato-Regular", size: 11 * screenscale, color: .Appcolor51)
                        
                        cell.lblCommentcount.text = cell.model?.answer_comment_count
                        
                        cell.lblLikeCount.text = toString(cell.model?.post_answer_like_count)
                        cell.btnLike.isSelected = cell.model?.is_post_answer_liked ?? false
                        
                        cell.lblUpvotecount.text = toString(cell.model?.answer_upvote_comment_count)
                        cell.lblDownvoteCount.text = toString(cell.model?.answer_downvote_comment_count)
                        cell.btnUpvote.isSelected = cell.model?.is_post_answer_upvote ?? false ? true : false
                        cell.btndownvote.isSelected = cell.model?.is_post_answer_downvote ?? false ? true : false
                        
                        let strUpvote = ToInt(cell.lblUpvotecount.text) > 1 ? "Upvotes" : "Upvote"
                        cell.btnUpvote.setTitle(strUpvote, for: .normal)
                       cell.btnUpvote.setTitle(strUpvote, for: .selected)
                        
                        let strDownvote = ToInt(cell.lblDownvoteCount.text) > 1 ? "Downvotes" : "Downvote"
                       cell.btndownvote.setTitle(strDownvote, for: .normal)
                        cell.btndownvote.setTitle(strDownvote, for: .selected)
                        
                        cell.lblYouTxt.text = ""
                        cell.self.imgYou.isHidden = true
                        if cell.model?.is_post_answer_upvote ?? false{
                            cell.lblYouTxt.text = "You upvoted this"
                            cell.self.imgYou.isHidden = false
                        }
                        else if cell.model?.is_post_answer_downvote ?? false{
                            cell.lblYouTxt.text = "You downvoted this"
                            cell.self.imgYou.isHidden = false
                        }
                        
                        cell.self.imgYou.setImageWithURL(loginData?.profile_image ?? "", "Big_dp_placeholder")
                        
                        if let previewUrl = cell.model?.dicPostLink?.link, checkValidString(previewUrl)
                        {
                            cell.viewPreviewHeightConst.constant = 100
                            cell.self.showPreviewImage(toString(previewUrl))
                        }
                        else
                        {
                            cell.viewPreviewHeightConst.constant = 0
                            cell.detailedView?.isHidden = true
                        }
                        
                        cell.btnViewMore.isHidden = true
                        if(cell.lblanswer.maxNumberOfLines > 6){
                            cell.btnViewMore.isHidden = false
                            cell.lblanswer.numberOfLines = 6
                        }
                        
                        cell.self.updateConstraintsIfNeeded()
                        cell.self.layoutIfNeeded()
//                        
//                        cell.btnShared.addTarget(self, action: #selector(cell.self.btnSharedClicked(_:)), for: .touchUpInside)
//            //
            //            cell.btnUpvote.addTarget(self, action: #selector(cell.self.btnUpVoteClicked(_:)), for: .touchUpInside)
            //            cell.btndownvote.addTarget(self, action: #selector(cell.self.downVoteClick(_:)), for: .touchUpInside)

//                        cell.self.collectionGallary.delegate = cell
//                        cell.self.collectionGallary.dataSource = cell
//
//                        cell.self.collectionGallary.reloadData()

                        cell.self.setUpCollection()

                        
                        return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellAnswerHeader") as! cellAnswerHeader
        cell.lblTitle.text = self.arrDiscussList.count > 1 ? "\(self.arrDiscussList.count) Comment" : "\(self.arrDiscussList.count) Comment"
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? CGFloat.leastNormalMagnitude : 32 * screenscale
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
   /* func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
          return true
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        debugPrint("MYid:-\(globalUserId)","Match:-\(self.arrDiscussList[indexPath.row].user?.user_id)")

        if globalUserId != self.arrDiscussList[indexPath.row].user?.user_id {return [UITableViewRowAction]()}

        
        let delete = UITableViewRowAction.init(style: UITableViewRowAction.Style.destructive, title: "Delete") { (_, getInd) in
            
            self.showOkCancelAlertWithAction(msg: Constant.kAlertdiscuss, handler: { (bool) in
                if bool
                {
                    self.webCall_deleteDiscuss(self.arrDiscussList[getInd.row].post_answers_id,callback: {
                        self.arrDiscussList.remove(at: getInd.row)
                        self.tblview?.deleteRows(at: [indexPath], with: .left)
                    })
                }
            })
            
            
        }
        delete.backgroundColor = .AppcolorRed
        
        
        let edit = UITableViewRowAction.init(style: UITableViewRowAction.Style.default, title: "Edit") { (_, getInd) in
            
            let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "DiscussionVC") as! DiscussionVC
            vcInstace.strPostID = self.arrDiscussList[getInd.row].post_id
            vcInstace.dictAnswer = self.arrDiscussList[getInd.row]
            self.present(vcInstace, animated: true, completion: nil)
        }
        edit.backgroundColor = .AppBlue_Dark
        
        
        return [delete,edit]
    }*/
     /*func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") { (action, sourceView, completionHandler) in
            print("index path of delete: \(indexPath)")
            completionHandler(true)
        }
     
        let rename = UIContextualAction(style: .normal, title: "Edit") { (action, sourceView, completionHandler) in
            print("index path of edit: \(indexPath)")
            completionHandler(true)
        }
        let swipeActionConfig = UISwipeActionsConfiguration(actions: [rename, delete])
        swipeActionConfig.performsFirstActionWithFullSwipe = false
        return swipeActionConfig
    }*/
    
    func webCall_deleteDiscuss(_ delete_answers_id:String ,callback: (()->())?){
        WebService.shared.RequesURL(ServerURL.delete_answer, Perameters: ["user_id":globalUserId,"post_answers_id":delete_answers_id],showProgress: true, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true
            {
                callback?()
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
    
    func reloadTableData() {
        self.tblAnswerList.reloadData()
    }
    
    
}

