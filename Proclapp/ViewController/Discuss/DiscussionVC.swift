//
//  DiscussionVC.swift
//  Proclapp
//
//  Created by Prismetric Tech on 8/29/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import RichEditorView
import IQKeyboardManagerSwift
import Photos

class DiscussionVC: UIViewController {

    var dictAnswer : AnswerListModel?

    @IBOutlet weak var lblHeader: UILabel!

    
    @IBOutlet weak var viewEditor: RichEditorView!
    lazy var toolbar: RichEditorToolbar = {
        let toolbar = RichEditorToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 44))
        toolbar.options = RichEditorDefaultOption.all
        return toolbar
    }()
    
    let imagePicker = UIImagePickerController()
    var arrImages = [UIImage]()
    var strPostID : String = ""
    var strDiscusstionHTMLString : String = ""
    var customeHeaderTitle: String = "";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
        viewEditor.delegate = self
        viewEditor.inputAccessoryView = toolbar
        viewEditor.placeholder = "Type some text..."
        toolbar.delegate = self
        toolbar.editor = viewEditor
        
        strDiscusstionHTMLString = toString(dictAnswer?.answer)
        toolbar.editor?.html = strDiscusstionHTMLString
        
        // We will create a custom action that clears all the input text when it is pressed
        let item = RichEditorOptionItem(image: nil, title: "Clear") { toolbar in
            toolbar.editor?.html = ""
        }
        
        var options = toolbar.options
        options.append(item)
        toolbar.options = options
        
        if(customeHeaderTitle == ""){
            lblHeader.text = dictAnswer == nil ? "Add discussion" : "Edit discussion"
        }
        else {
             lblHeader.text = customeHeaderTitle
        }
        
     
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    //MARk:- btnClosePressed
    @IBAction func btnClosePressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARk:- btnPostPressed
    @IBAction func btnPostPressed(_ sender: Any) {
        if checkValid(strDiscusstionHTMLString){
            if dictAnswer == nil{
                self.jsonAddAnswerDiscusstion()
            }
            else{
                self.jsonEditAnswerDiscusstion()
            }
        }
        else{
            debugPrint("not valid")
        }
    }
    
    func checkValid(_ CheckString : String?) -> Bool
    {
        if let strTemp = CheckString
        {
            return  !(strTemp.isEmpty || strTemp == "" || strTemp == "(null)" || strTemp == "<null>" || strTemp == " " || strTemp == "<br>" ||  strTemp.trimmingCharacters(in: .whitespacesAndNewlines).count == 0)
        }
        else
        {
            return false
        }
    }
    
    func authorizeToAlbum(completion:@escaping (Bool)->Void) {
        
        if PHPhotoLibrary.authorizationStatus() != .authorized {
            NSLog("Will request authorization")
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == .authorized {
                    DispatchQueue.main.async(execute: {
                        completion(true)
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        completion(false)
                    })
                }
            })
            
        } else {
            DispatchQueue.main.async(execute: {
                completion(true)
            })
        }
    }
    
    func imageTapped()
    {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.authorizeToAlbum { (authorized) in
                if authorized == true {
                    self.openCamera()
                }
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.authorizeToAlbum { (authorized) in
                if authorized == true {
                    self.openGallery()
                }
            }
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.delegate = self
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func openGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}



extension DiscussionVC: RichEditorDelegate {
    
    func richEditor(_ editor: RichEditorView, contentDidChange content: String) {

        strDiscusstionHTMLString = content.replacingOccurrences(of: "&nbsp;", with: "")
        debugPrint(strDiscusstionHTMLString)
    }
    
}



extension DiscussionVC: RichEditorToolbarDelegate {
    
    fileprivate func randomColor() -> UIColor {
        let colors: [UIColor] = [
            .red,
            .orange,
            .yellow,
            .green,
            .blue,
            .purple
        ]
        
        let color = colors[Int(arc4random_uniform(UInt32(colors.count)))]
        return color
    }
    
    func richEditorToolbarChangeTextColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextColor(color)
    }
    
    func richEditorToolbarChangeBackgroundColor(_ toolbar: RichEditorToolbar) {
        let color = randomColor()
        toolbar.editor?.setTextBackgroundColor(color)
    }
    
    func richEditorToolbarInsertImage(_ toolbar: RichEditorToolbar) {
        imageTapped()
//        toolbar.editor?.insertImage("https://gravatar.com/avatar/696cf5da599733261059de06c4d1fe22", alt: "Gravatar")
    }
    
//    func richEditorToolbarInsertLink(_ toolbar: RichEditorToolbar) {
//        // Can only add links to selected text, so make sure there is a range selection first
//        if toolbar.editor?.hasRangeSelection == true {
//            toolbar.editor?.insertLink("http://github.com/cjwirth/RichEditorView", title: "Github Link")
//        }
//    }
}


//MARK:- ImagePicker Delegate
extension DiscussionVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//        pickedImage.resizableImage(200.0 , maxWidth:300.0, compression:0.7)
        
//        let imageData = image!.jpegData(compressionQuality: compression)
        
        if let pickedImage = info[.originalImage] as? UIImage {
            
            self.arrImages.append(pickedImage)
            
            dismiss(animated: true, completion: {
                DispatchQueue.main.async {
                    self.jsonUploadDiscusstionImage()
                }
                
            })
            /* if let imageData = pickedImage.jpegData(compressionQuality: 1.0) {
             let filename = NSUUID().uuidString + ".jpg"
             let path = getDocumentsURL()
             let fileURL = path.appendingPathComponent(filename)
             print("file url: \(fileURL)")
             do {
             try imageData.write(to: fileURL!, options: NSData.WritingOptions.atomic)
             }
             catch let error as NSError  {
             print(error)
             }
             self.viewEditor?.insertImage(fileURL!.absoluteString, alt: "new image")
             }*/
        } else {
            dismiss(animated: true, completion: nil)
        }
 
 
      /*  if let imgUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL{
            let imgName = imgUrl.lastPathComponent
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
            let localPath = documentDirectory?.appending(imgName)
            debugPrint("localPath!",localPath!)
            viewEditor.insertImage(localPath!, alt: imgName)
            
            let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            let data = image.pngData()! as NSData
            data.write(toFile: localPath!, atomically: true)
            //let imageData = NSData(contentsOfFile: localPath!)!
            let photoURL = URL.init(fileURLWithPath: localPath!)//NSURL(fileURLWithPath: localPath!)
            print(photoURL)
            
            
        }*/
        
    }
    
    func getDocumentsURL() -> NSURL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL as NSURL
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        print("cancel is clicked")
        dismiss(animated: true, completion: nil)
    }
    
   
    func jsonUploadDiscusstionImage()
    {
        let parameter = NSMutableDictionary()
        
        parameter["user_id"] = globalUserId
        
        let parameterImages = NSMutableDictionary()
        
        if arrImages.count != 0   {
            parameterImages["image"] = arrImages[0].resizeImage(maxHeight: 200, maxWidth: 200, compression: 1)
        }
        
        WebService.shared.WebServiceRequestURLByPassingData(DictionaryImages: parameterImages,urlString: ServerURL.post_content_image, Perameters: parameter as! [String : Any], completion: { (dicRes, errorMsg, isDone) in
            
             self.arrImages.removeAll()
            if let arrRes =  dicRes["response"] as? NSArray {
                
                if let dicData = arrRes[0] as? NSDictionary {
                    
                    debugPrint("dicData text",dicData["text"] as? String ?? "")
                    if let strUrl = dicData["text"] as? String {
                        
                        self.viewEditor.insertImage(strUrl, alt:"\(URL(string: strUrl)?.lastPathComponent ?? "")")

                    }
                    
                }
            }
            
 
        }) { (errorFail) in
            
        }
    }
    
    func jsonAddAnswerDiscusstion()
    {
        let parameter = NSMutableDictionary()
        
        parameter["user_id"] = globalUserId
        parameter["post_id"] = strPostID
        parameter["answer"] = strDiscusstionHTMLString
 
        let parameterImages = NSMutableDictionary()
        
        if arrImages.count != 0   {
            parameterImages["image"] = arrImages[0].resizeImage(maxHeight: 200, maxWidth: 200, compression: 1)
        }
        
        WebService.shared.WebServiceRequestURLByPassingData(DictionaryImages: parameterImages,urlString: ServerURL.add_answer, Perameters: parameter as! [String : Any], completion: { (dicRes, errorMsg, isDone) in
            
          
            
            
            if let arrRes =  dicRes["response"] as? NSArray {
                
                if let dicData = arrRes[0] as? NSDictionary {
                    
                    if ConvertToBool(dicData["status"])
                    {
                        NotificationCenter.default.post(name: .DiscussPost, object: nil)
                        
                        self.showOkAlertWithHandler(msg: Constant.kAlertDiscAdded, handler: {
                            self.btnClosePressed(self)
                        })
                    }
                    else{
                        self.showOkAlertWithHandler(msg: "Comment added", handler: {
                        })
                    }
                    
                 /*   debugPrint("dicData text",dicData["text"] as? String ?? "")
                    if let strUrl = dicData["text"] as? String {
                        
                        self.viewEditor.insertImage(strUrl, alt:"\(URL(string: strUrl)?.lastPathComponent ?? "")")
                        
                    }*/
                    
                }
            }
            
            
        }) { (errorFail) in
            
        }
    }
    
    func jsonEditAnswerDiscusstion()
    {
        let parameter = NSMutableDictionary()
        
        parameter["user_id"] = globalUserId
        parameter["post_answers_id"] = dictAnswer?.post_answers_id
        parameter["answer"] = strDiscusstionHTMLString
        
        let parameterImages = NSMutableDictionary()
        
        if arrImages.count != 0   {
            parameterImages["image"] = arrImages[0].resizeImage(maxHeight: 200, maxWidth: 200, compression: 1)
        }
        
        WebService.shared.WebServiceRequestURLByPassingData(DictionaryImages: parameterImages,urlString: ServerURL.edit_answer, Perameters: parameter as! [String : Any], completion: { (dicRes, errorMsg, isDone) in
            
            
             debugPrint(dicRes)
            
            if let arrRes =  dicRes["response"] as? NSArray {
                
                if let dicData = arrRes[0] as? NSDictionary {
                    
                    if ConvertToBool(dicData["status"])
                    {
                        NotificationCenter.default.post(name: .DiscussPost, object: nil)
                        
                        self.showOkAlertWithHandler(msg: Constant.kAlertDiscUpdate, handler: {
                            self.btnClosePressed(self)
                        })
                    }
                    else{
                        self.showOkAlertWithHandler(msg: toString(dicData["response_msg"]), handler: {
                        })
                    }
                    
                    /*   debugPrint("dicData text",dicData["text"] as? String ?? "")
                     if let strUrl = dicData["text"] as? String {
                     
                     self.viewEditor.insertImage(strUrl, alt:"\(URL(string: strUrl)?.lastPathComponent ?? "")")
                     
                     }*/
                    
                }
            }
            
            
        }) { (errorFail) in
            
        }
    }
}

public extension UIImage {
    
    public func resizeImage(maxHeight:CGFloat, maxWidth:CGFloat, compression:CGFloat)->UIImage? {
        
        var actualHeight = self.size.height
        var actualWidth = self.size.width
        var imgRatio = actualWidth/actualHeight
        let maxRatio = maxWidth/maxHeight
        
        if actualHeight > maxHeight || actualWidth > maxWidth
        {
            if imgRatio < maxRatio
            {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if(imgRatio > maxRatio)
            {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth = maxWidth;
            }
            else
            {
                actualHeight = maxHeight;
                actualWidth = maxWidth;
            }
        }
        /*
         let rect = CGRectMake(0, 0, actualWidth, actualHeight)
         UIGraphicsBeginImageContext(rect.size)
         self.draw(in: rect)
         let image = UIGraphicsGetImageFromCurrentImageContext()
         let imageData = UIImageJPEGRepresentation(image!, compression)
         */
        let rect = CGRect(x: 0, y: 0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        self.draw(in: rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = image!.jpegData(compressionQuality: compression)
        UIGraphicsEndImageContext()
        
        return image! as UIImage
        
    }
    
    
}
