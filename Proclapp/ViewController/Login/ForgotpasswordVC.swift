
//
//  ForgotpasswordVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/1/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class ForgotpasswordVC: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
        if let gesture = navigationController?.interactivePopGestureRecognizer
        {
            view.addGestureRecognizer(gesture)
        }
    }
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func clickForgotPassword(_ sender: Any) {
        if validationLogin{
           webCall_Forgotpassword()
        }
    }
    
    

}
extension ForgotpasswordVC
{
    
    var validationLogin: Bool{
        
        self.view.endEditing(true)
        
        var mess = ""
        
        if ToString(txtEmail.text).isEmail == false {
            mess = "Please enter a valid email adress"
        }
        if mess != ""{
            self.showOkAlert(msg: mess)
            return false
        }
        return true
    }
    
    func webCall_Forgotpassword()
    {
        WebService.shared.RequesURL(ServerURL.ForgotPswd, Perameters: ["email":ToString(txtEmail.text)],showProgress: true, completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            if success == true
            {
                self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                    self.clickBack(self)
                })
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
}
