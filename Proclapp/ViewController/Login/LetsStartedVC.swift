//
//  LetsStartedVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/2/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class LetsStartedVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func clickLetsStarted(_ sender: Any) {
        //For First Time install app
        setUserDefault("no" as AnyObject, Key: Constant.userDeafult_isFirstTimeInstall)
        appDelegate.LoadHomeView()
        
    }
}
