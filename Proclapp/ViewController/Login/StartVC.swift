//
//  StartVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 3/26/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import AuthenticationServices
import SVProgressHUD
import GoogleSignIn

class StartVC: UIViewController {
    
    @IBOutlet weak private var signInButtonStack: UIStackView!

    @IBOutlet weak private var btnApple: UIButton!
    
    var googleSignIn = GIDSignIn.sharedInstance()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.setUpSignInAppleButton()
        
        if #available(iOS 13.0, *) {
            btnApple.isHidden = false
        } else {
            btnApple.isHidden = true
            // Fallback on earlier versions
        }

        
        btnApple.addTarget(self, action: #selector(handleAppleIdRequest), for: .touchUpInside)

        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func googleButton(_ sender: UIButton) {
        googleAuthLogin()
    }
    
    @IBAction func clickSkip(_ sender: Any) {
        resetData()
        appDelegate.LoadHomeView()
    }
    
    @IBAction func fbBtnClicked(_ sender: Any)
    {
        var obj = signUpData()
        SMSocialHelper.Shared.FacebookSignin(delegate: self) { (result, err) in
            
            if result.emailAddress.isEmail == true
            {
                obj = result
                 debugPrint("obj")
                debugPrint(obj)
                
                self.CheckSocialID(mediaType: result.media_type, MediaID: result.media_id, email: result.emailAddress, userName: result.userName, device_token: GlobalDeviceToken)
            }
            else {
                debugPrint(err)
            }
        }
    }
    
    // MARK:- GOOGLE SIGNIN
    func googleAuthLogin() {
        self.googleSignIn?.presentingViewController = self
        self.googleSignIn?.clientID = "935087247654-srf3f4flcor84h6ied7luvnhr5ep8931.apps.googleusercontent.com"
        self.googleSignIn?.delegate = self
        self.googleSignIn?.signIn()
    }
    // MARK:- SOCIAL SIGN IN SERVICES CALL
    
    func CheckSocialID(mediaType:String, MediaID:String, email:String, userName:String, device_token:String)
    {
        let parameter = NSMutableDictionary()
        parameter.setObject(mediaType, forKey: "media_type" as NSCopying)
        parameter.setObject(MediaID, forKey: "media_id" as NSCopying)
        
        debugPrint("parameter")
        debugPrint(email)
        debugPrint(userName)
        debugPrint(device_token)
        debugPrint(mediaType)
        debugPrint(parameter)
        
        
        WebService.shared.RequesURL(ServerURL.check_social_id, Perameters: (parameter as! [String : Any]),showProgress: true, completion: { (dicRes, success) in
            
            debugPrint("dicRes")
            debugPrint(dicRes)
            if success , let dictData = dicRes as? [String : Any]
            {
                setUserDefault(dictData as AnyObject, Key: Constant.userDeafult_LoginDic)
                setLoginData(dictData)
                appDelegate.LoadHomeView()
            }
            else
            {
                self.SocialSignIn(mediaType: mediaType, MediaID: MediaID, email: email, userName: userName, device_token: GlobalDeviceToken, device_type: "ios")
            }
        }) { (err) in
            debugPrint(err)
        }
    }
    
    func SocialSignIn(mediaType:String, MediaID:String, email:String, userName:String, device_token:String, device_type:String)
    {
        let parameter = NSMutableDictionary()
        parameter.setObject(mediaType, forKey: "media_type" as NSCopying)
        parameter.setObject(MediaID, forKey: "media_id" as NSCopying)
        parameter.setObject(email, forKey: "email" as NSCopying)
        parameter.setObject(device_token, forKey: "device_token" as NSCopying)
        parameter.setObject(device_type, forKey: "device_type" as NSCopying)
        parameter.setObject(userName, forKey: "username" as NSCopying)
        
        debugPrint(parameter)
        
        WebService.shared.RequesURL(ServerURL.signin_with_social, Perameters: (parameter as! [String : Any]),showProgress: true, completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            if success , let dictData = dicRes as? [String : Any]
            {
                setUserDefault(dictData as AnyObject, Key: Constant.userDeafult_LoginDic)
                setLoginData(dictData)
                appDelegate.LoadHomeView()
            }
            else
            {
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
            
        }) { (err) in
            debugPrint(err)
        }
    }
    
  
}

class signUpData
{
    var userName: String = ""
    var emailAddress: String = ""
    var password: String = ""
    var confirmPassword: String = ""
    var media_type: String = ""
    var media_id: String = ""
}


// MARK:- GOOGLE SIGNIN DELEGATE
extension StartVC: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard let user = user else {
            print("Uh oh. The user cancelled the Google login.")
            return
        }
        
        let obj = signUpData()
        if(user.profile.email.isEmail == true){
            let userName: String = user.profile.familyName ?? "" + " " + user.profile.givenName
            obj.emailAddress = user.profile.email
            obj.media_id = user.authentication.idToken
            obj.media_type = "google"
            obj.userName = userName
            
            self.CheckSocialID(mediaType: "google", MediaID: user.authentication.accessToken ?? user.userID, email: user.profile.email, userName: userName, device_token: GlobalDeviceToken)
        }
        
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
}

// MARK:-  SIGN IN APPLE

extension StartVC : ASAuthorizationControllerDelegate {
    
    func setUpSignInAppleButton() {
            if #available(iOS 13.0, *) {
    //            let isDarkTheme = view.traitCollection.userInterfaceStyle == .dark
    //            let style: ASAuthorizationAppleIDButton.Style = isDarkTheme ? .white : .black
                let authorizationButton = ASAuthorizationAppleIDButton(type: .default, style: ASAuthorizationAppleIDButton.Style.white)
    //            let authorizationButton = ASAuthorizationAppleIDButton()
                authorizationButton.addTarget(self, action: #selector(handleAppleIdRequest), for: .touchUpInside)
                authorizationButton.cornerRadius = 10
                //Add button on some view or stack
                self.signInButtonStack.addArrangedSubview(authorizationButton)
            } else {
                // Fallback on earlier versions
            }
          
        }
    
    @objc func handleAppleIdRequest() {
        if #available(iOS 13.0, *) {
            
            debugPrint("!!!!!!!!!!!!!!!!!!")
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
                   let authorizationController = ASAuthorizationController(authorizationRequests: [request])
                   authorizationController.delegate = self
                   authorizationController.performRequests()
        } else {
            // Fallback on earlier versions
        }
       
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        
        
        debugPrint("##########")

        /*let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)*/
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            let userName: String = appleIDCredential.fullName?.givenName ?? "" + " " + (appleIDCredential.fullName?.familyName)!
            var email: String = ToString(appleIDCredential.email)
            if(email == ""){
                email =  ToString(userName).replacingOccurrences(of: " ", with: ".") + "@apple.com"
            }
            
            self.CheckSocialID(mediaType: "apple", MediaID: ToString(appleIDCredential.user), email: email, userName: userName, device_token: GlobalDeviceToken)
            debugPrint("@@@@@@@@@@@@@")

            // Create an account in your system.
//            // For the purpose of this demo app, store the these details in the keychain.
//            KeychainItem.currentUserIdentifier = appleIDCredential.user
//            KeychainItem.currentUserFirstName = appleIDCredential.fullName?.givenName
//            KeychainItem.currentUserLastName = appleIDCredential.fullName?.familyName
//            KeychainItem.currentUserEmail = appleIDCredential.email
            
//            print("User Id - \(appleIDCredential.user)")
//            print("User Name - \(appleIDCredential.fullName?.description ?? "N/A")")
//            print("User Email - \(appleIDCredential.email ?? "N/A")")
//            print("Real User Status - \(appleIDCredential.realUserStatus.rawValue)")
            

            
//            if let identityTokenData = appleIDCredential.identityToken,
//                let identityTokenString = String(data: identityTokenData, encoding: .utf8) {
//                print("Identity Token \(identityTokenString)")
//            }
            
        }
        /*else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            
            // For the purpose of this demo app, show the password credential as an alert.
            DispatchQueue.main.async {
                let message = "The app has received your selected credential from the keychain. \n\n Username: \(username)\n Password: \(password)"
                let alertController = UIAlertController(title: "Keychain Credential Received",
                                                        message: message,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
        }*/
    }
}

extension StartVC : ASAuthorizationControllerPresentationContextProviding {
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}
