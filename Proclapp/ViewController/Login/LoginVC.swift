//
//  LoginVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 3/26/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func clickLogin(_ sender: Any) {
        if validationLogin{
            webCall_signin()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        #if DEBUG
        txtEmail.text = "maulik@prismetric.com"
        txtPassword.text = "123123"
        #else
        #endif
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
}
extension LoginVC
{
    
    var validationLogin: Bool{
        
        self.view.endEditing(true)
        
        var mess = ""
        
        if ToString(txtEmail.text).isEmail == false {
            mess = "Please enter a valid email adress"
        }
        else if ToString(txtPassword.text).isBlank {
            mess = "Please enter a password"
        }
        
        if mess != ""{
            self.showOkAlert(msg: mess)
            return false
        }
        return true
    }
    
    func webCall_signin()
    {
        let dicParameter = ["email":ToString(txtEmail.text),"password":ToString(txtPassword.text),"device_token":GlobalDeviceToken]
        
        WebService.shared.RequesURL(ServerURL.SignIn, Perameters: dicParameter,showProgress: true, completion: { (dicRes, success) in
            
            debugPrint(dicRes)

            if success , let dictData = dicRes as? [String : Any]
            {
                setUserDefault(dictData as AnyObject, Key: Constant.userDeafult_LoginDic)
                setLoginData(dictData)
               appDelegate.LoadHomeView()
                
//                if loginData?.screen_code == "111"{//change password
//                    self.pushTo(ChangePassVC.vcInstace)
//                }
//                else if loginData?.screen_code == "222"{//edit profile
//                    let editProfile = StoryBoard.Profile.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
//                    editProfile.isProfileFromLogin = true
//                    self.pushTo(editProfile)
//                }
//                else
//                {
//                    if  toString(UserDefaults.standard.value(forKey: Constant.userDeafult_isFirstTimeInstall)).isValid
//                    {
//                        appDelegate.LoadHomeView()
//                    }
//                    else
//                    {
//                        let vcInstace = StoryBoard.Main.instantiateViewController(withIdentifier: "LetsStartedVC") as! LetsStartedVC
//                        self.navigationController?.pushViewController(vcInstace, animated: true)
//                    }
//                }
            }
            else
            {
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
                
               /* if screen_code == "333"{
                    let alertView = UIAlertController(title: Constant.appName, message: toString(dicRes[Constant.responsemsg]), preferredStyle: .alert)
                    
                    let Cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
                    })
                    
                    let Resend = UIAlertAction(title: "Resend mail", style: .default, handler: { (alert) in
                        
                    })
                    alertView.addAction(Resend)
                    alertView.addAction(Cancel)
                    self.present(alertView, animated: true, completion: nil)
                }
                else
                {
                    self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
                }*/
            }
        }) { (err) in
            debugPrint(err)
        }
    }
}
