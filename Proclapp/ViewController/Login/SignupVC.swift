//
//  SignupVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 3/26/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import CountryPicker
import AlamofireImage
import SVProgressHUD

class SignupVC: UIViewController, CountryPickerDelegate {
    
    var getClick = tapPage.click1
    
    struct tapPage {
        static let click1 = "click1"
        static let click2 = "click2"
        static let click3 = "click3"
    }
    

    var arrCategoryList = [InterestedCategoryListModel]()

    var arrQualification = [signupEducationModel]()


    @IBOutlet weak var collectionView: UICollectionView!

    @IBOutlet weak var scrviewMain: UIScrollView!

    @IBOutlet weak var tableview: UITableView!

    @IBOutlet weak var dashView: UIView!

    @IBOutlet weak var stckview: UIStackView!
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    
    @IBOutlet weak var btnSkip: UIButton!
    
    @IBOutlet var searchbar: UISearchBar!
    
    @IBOutlet weak var txtFname: UITextField!
    @IBOutlet weak var txtLname: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtCPassword: UITextField!
    
    @IBOutlet weak var pickerCountry: CountryPicker!
    
    let pickerView = UIPickerView()
    
    
    let arrGender = ["Male","Female"]
    
//    var qualiFicationCount = 1
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(UINib.init(nibName: "categorySelectCLVCell", bundle: nil), forCellWithReuseIdentifier: "categorySelectCLVCell")

        self.arrQualification.append(signupEducationModel())
        
        webCall_InterestedCategory()
        
        
        pickerCountry.countryPickerDelegate = self
        txtCode.inputView = pickerCountry
        pickerView.delegate = self
        pickerView.dataSource = self
        txtGender.inputView = pickerView
        btnSkip.isHidden = true
        scrviewMain.isScrollEnabled = false

        dashView.addDashedBorder()

        stckview.spacing = 60*screenscale
        
        self.btn1.layer.cornerRadius = self.btn2.frame.height/2
        self.btn2.layer.cornerRadius = self.btn2.frame.height/2
        self.btn3.layer.cornerRadius = self.btn2.frame.height/2

        click1(btn1)
        self.pickerCountry.setCountry(Locale.current.regionCode?.uppercased() ?? "IN")
//        if let country = self.pickerCountry.countries.first(where: {$0.code.rawValue.lowercased() == Locale.current.regionCode?.lowercased()}) {
//
//            self.txtCode.text = country.phoneCode
////            self.txtCode.placeholder = country.phoneCode
////            selectedCounty = country
////            self.updatePlaceholder()
//        }
        
        // Do any additional setup after loading the view.
    }
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        txtCode.text = phoneCode
    }

    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
        if let gesture = navigationController?.interactivePopGestureRecognizer
        {
            view.addGestureRecognizer(gesture)
        }
        txtCode.tintColor = UIColor.clear
    }
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func click1(_ sender: UIButton) {
        
        
        getClick = tapPage.click1
        
        btnSkip.isHidden = true
        scrviewMain.setContentOffset(CGPoint.init(x: 0 , y: 0), animated: true)

        sender.backgroundColor = UIColor.AppBlue_Dark
        sender.setTitleColor(UIColor.AppSkyBlue, for: .normal)
        sender.layer.borderWidth = 0
        btn2.backgroundColor = UIColor.white
        btn2.setTitleColor(UIColor.Appcolor102, for: .normal)
        btn2.layer.borderColor = UIColor.Appcolor153.cgColor
        btn2.layer.borderWidth = 1
        
        
        btn3.backgroundColor = UIColor.white
        btn3.layer.borderColor = UIColor.Appcolor153.cgColor
        btn3.layer.borderWidth = 1
        btn3.setTitleColor(UIColor.Appcolor102, for: .normal)
        
        
        
    }
    @IBAction func click2(_ sender: UIButton) {
        
        if  validationSignup == true
        {
            self.check_email_exists(ToString(txtEmail.text)) { (check) in
                if check
                {
                    self.getClick = tapPage.click2
                    
                    self.btnSkip.isHidden = false
                    self.scrviewMain.setContentOffset(CGPoint.init(x: screenwidth , y: 0), animated: true)
                    
                    sender.backgroundColor = UIColor.AppBlue_Dark
                    sender.setTitleColor(UIColor.AppSkyBlue, for: .normal)
                    sender.layer.borderWidth = 0
                    
                    
                    self.btn1.backgroundColor = UIColor.white
                    self.btn1.setTitleColor(UIColor.Appcolor102, for: .normal)
                    self.btn1.layer.borderColor = UIColor.Appcolor153.cgColor
                    self.btn1.layer.borderWidth = 1
                    
                    
                    self.btn3.backgroundColor = UIColor.white
                    self.btn3.layer.borderColor = UIColor.Appcolor153.cgColor
                    self.btn3.layer.borderWidth = 1
                    self.btn3.setTitleColor(UIColor.Appcolor102, for: .normal)                }
            }
            
            
            
        }
 
    }
    @IBAction func click3(_ sender: UIButton) {
        
        if  validationSignup == true
        {
            self.check_email_exists(ToString(txtEmail.text)) { (check) in
                if check
                {
                    self.getClick = tapPage.click3
                    
                    self.btnSkip.isHidden = false
                    
                    self.scrviewMain.setContentOffset(CGPoint.init(x: screenwidth * 1 , y: 0), animated: true)
                    
                    sender.backgroundColor = UIColor.AppBlue_Dark
                    sender.setTitleColor(UIColor.AppSkyBlue, for: .normal)
                    sender.layer.borderWidth = 0
                    
                    
                    self.btn2.backgroundColor = UIColor.white
                    self.btn2.setTitleColor(UIColor.Appcolor102, for: .normal)
                    self.btn2.layer.borderColor = UIColor.Appcolor153.cgColor
                    self.btn2.layer.borderWidth = 1
                    
                    
                    self.btn1.backgroundColor = UIColor.white
                    self.btn1.setTitleColor(UIColor.Appcolor102, for: .normal)
                    self.btn1.layer.borderColor = UIColor.Appcolor153.cgColor
                    self.btn1.layer.borderWidth = 1
                }
            }
        }
    }
    
    @IBAction func clickSkip(_ sender: UIButton)
    {
        
        var gender = ""
        
        if ToString(txtGender.text).isBlank == false
        {
            gender = ToString(txtGender.text).lowercased() == "male" ? "1" : "0"
        }
        
        let dicParameter = ["firstname" : ToString(txtFname.text), "lastname":ToString(txtLname.text), "email":ToString(txtEmail.text),"gender":gender,"phone":ToString(txtMobile.text),"phone_code":ToString(txtCode.text),"address":ToString(txtAddress.text),"password":ToString(txtPassword.text),"education":"","category":""]
        
        debugPrint(dicParameter)
        
        WebService.shared.RequesURL(ServerURL.SignUp, Perameters: dicParameter,showProgress: true, completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            if success == true
            {
                self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                    self.clickBack(self)
                })
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
    @IBAction func clickSave(_ sender: Any)
    {
        if getClick == tapPage.click2
        {
            var isallValcurrect = true
            var msg = ""
            
            for obj in self.arrQualification
            {
                isallValcurrect = obj.selectedDegree.isEmpty == false &&  obj.selectedYear.isEmpty == false
                msg = obj.selectedDegree.isEmpty == true ? "Please enter qualification" : "Please select year"
                if isallValcurrect == false{
                    break
                }
            }
            
            if isallValcurrect == false
            {
                self.showOkAlert(msg: msg)
                return
            }
            else
            {
                self.click3(btn3)
            }
        }
        else
            {
                
                let count = self.arrCategoryList.filter({$0.is_selected}).count
                
                if count == 0
                {
                    self.showOkAlert(msg: "Please select interested category")
                }
                else
                {
                    webCall_signup()
                }
            }
    }

    
    @IBAction func clickSignup(_ sender: UIButton)
    {
        //ms_ios
       self.click3(btn3) // before click2
    }
    
    

    var validationSignup: Bool{
        
        self.view.endEditing(true)
        
        var mess = ""
        
        let newPassLenth = ToString(txtPassword.text).trim

        if ToString(txtFname.text).isBlank {
            mess = "Please enter a first name"
        }
        else if ToString(txtLname.text).isBlank{
            mess = "Please enter a last name"
        }
        else if ToString(txtEmail.text).isEmail == false {
            mess = "Please enter a valid email adress"
        }
            
     /*   else if ToString(txtCode.text).isBlank{
            mess = "Please enter a code"
        }
        else if ToString(txtMobile.text).isBlank{
            mess = "Please enter a mobile number "
        }*/
        /*else if ToString(txtGender.text).isBlank{
            mess = "Please select gender"
        }*/
       /* else if ToString(txtAddress.text).isBlank{
            mess = "Please enter a resedential address "
        }*/
        else if ToString(txtPassword.text).isBlank {
            mess = "Please enter a password"
        }
        else if newPassLenth.count < 6 ||  newPassLenth.count > 15
        {
            mess = "Please enter a password at least 6 to 15 characters"
        }
        else if ToString(txtCPassword.text).isBlank{
            mess = "Please enter a confirm password "
        }
        else if ToString(txtPassword.text) != ToString(txtCPassword.text){
            mess = "Password and confirm password does not match"
        }
        
        
        if mess != ""
        {
            self.showOkAlert(msg: mess)
            return false
        }
            return true
    }

}


//MARK: - API call

extension SignupVC
{
    
    func webCall_signup()
    {
        var gender = ""
        if ToString(txtGender.text).isBlank == false
        {
            gender = ToString(txtGender.text).lowercased() == "male" ? "1" : "0"
        }
        
        let dicParameter = ["firstname" : ToString(txtFname.text), "lastname":ToString(txtLname.text), "email":ToString(txtEmail.text),"gender":gender,"phone":ToString(txtMobile.text),"phone_code":ToString(txtCode.text),"address":ToString(txtAddress.text),"password":ToString(txtPassword.text),"education":(self.arrQualification.map({$0.toDict_Education})).jsonString,"category":self.arrCategoryList.filter({$0.is_selected}).map({$0.category_id}).joined(separator: ",")]
        
        debugPrint(dicParameter)
        
        WebService.shared.RequesURL(ServerURL.SignUp, Perameters: dicParameter,showProgress: true, completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            if success == true
            {
                self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                    self.clickBack(self)
                })
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
    
    
    func webCall_InterestedCategory()
    {
        WebService.shared.RequesURL(ServerURL.Interested_Category, Perameters: ["user_id" : ""],showProgress: false,completion: { (dicRes, success) in
            
            debugPrint("\(ServerURL.Interested_Category):-->",dicRes)
            
            if success == true
            {
                if let arrPrice = dicRes["interested_category"] as? [[String:Any]] {
                    self.arrCategoryList = arrPrice.map({InterestedCategoryListModel.init($0)})
                }
                self.collectionView.reloadData()
            }
            
        }) { (err) in
            debugPrint("\(ServerURL.Interested_Category):-->",err)
        }
    }
    
    func check_email_exists(_ email : String, _ callback: ((Bool)->())?)
    {
        WebService.shared.RequesURL(ServerURL.check_email_exists, Perameters: ["email" : email],showProgress: true,completion: { (dicRes, success) in
            
            if success == false
            {
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
            callback?(success)
            
        }) { (err) in
            debugPrint("\(ServerURL.Interested_Category):-->",err)
            callback?(false)

        }
    }
    
    
    
    
    
}


extension SignupVC
: UIPickerViewDataSource,UIPickerViewDelegate {
    //MARK: - UIPickerView  Methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrGender.count
    }
  
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        txtGender.text = arrGender[row]
        return  arrGender[row]
    }
}

extension SignupVC: UISearchBarDelegate
{
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        //        searchActive = false;
        debugPrint("searchBarCancelButtonClicked->")
        
        searchBar.text = ""
        
//        self.arrFilterRoom.removeAll()
        
//        self.isFilter = false
//        self.tableView.reloadData()
        
        searchBar.resignFirstResponder()
//        tableView.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //        searchActive = false
        debugPrint("searchBarSearchButtonClicked->")
        searchBar.resignFirstResponder()
        
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        //        let search = searchText.trim()
        //        debugPrint("search->",search)
//        self.arrFilterRoom = self.arrAllRoom.filter({$0.name.lowercased().contains(searchText.lowercased()) || $0.des.lowercased().contains(searchText.lowercased())})
//        self.isFilter = searchText.isEmpty == false
//        self.tableView.reloadData()
    }
}

extension SignupVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCategoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "categorySelectCLVCell", for: indexPath) as! categorySelectCLVCell
        
        let obj = arrCategoryList[indexPath.row]
        cell.lblTitle.text = obj.category_name
        
        cell.btnImage.addTarget(self, action: #selector(tapSelectCategory(_:)), for: .touchUpInside)
        

        cell.btnImage.isSelected = obj.is_selected
        
        if let url = URL.init(string: obj.category_img){
            //            cell.btnImage.af_setImage(for: .normal, url: url)
            cell.btnImage.af_setImage(for: .normal, url: url, placeholderImage: UIImage.init(named: "place_logo"))
        }
        if let url = URL.init(string: obj.category_selected_img){
            //            cell.btnImage.af_setImage(for: .selected, url: url)
            cell.btnImage.af_setImage(for: .selected, url: url, placeholderImage: UIImage.init(named: "place_logo"))
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.width/3, height: 106 * screenscale)
    }
    /*func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        debugPrint("selecvt")
        
        arrCategoryList[indexPath.row].is_selected = !arrCategoryList[indexPath.row].is_selected
        collectionView.reloadItems(at: [indexPath])
    }*/
    
    @IBAction func tapSelectCategory(_ sender: UIButton) {
        
        if let getIndexPath = self.collectionView.indexPathForView_Collection(sender)
        {
             arrCategoryList[getIndexPath.row].is_selected = !arrCategoryList[getIndexPath.row].is_selected
            collectionView.reloadItems(at: [getIndexPath])

        }
        
    }
    
}


//MARK: - Table view data source
extension SignupVC: UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arrQualification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "signupTextfiledCell") as! signupTextfiledCell
        
        cell.selectionStyle = .none
        
        cell.model = self.arrQualification[indexPath.row]
        

        cell.btnRemove.accessibilityHint = "\(indexPath.row + 1)"
        cell.btnRemove.isHidden = indexPath.row == 0
        cell.btnRemove.addTarget(self, action: #selector(clickRemove(_:)), for: .touchUpInside)

     
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 91
        
    }
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return UITableView.automaticDimension
//    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "addFooterCell") as! addFooterCell

        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.white
        
//        cell.btnAdd.accessibilityHint = "\(qualiFicationCount)"

        cell.btnAdd.addTarget(self, action: #selector(clickAdd(_:)), for: .touchUpInside)

        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 50
    }
    
    @IBAction func tapAddProfftionalInfo(_ sender: UIButton)
    {
      
        var isallValcurrect = true
        var msg = ""
        
        for obj in self.arrQualification
        {
            isallValcurrect = obj.selectedDegree.isEmpty == false &&  obj.selectedYear.isEmpty == false
            msg = obj.selectedDegree.isEmpty == true ? "Please enter qualification" : "Please select year"
            if isallValcurrect == false{
                break
            }
        }
        
        if isallValcurrect == false
        {
            self.showOkAlert(msg: msg)
            return
        }
        
        

        
        
    }
  
    @IBAction func clickTerms(_ sender: UIButton)
    {
        let vc = StoryBoard.More.instantiateViewController(withIdentifier: "GeneralWebVC") as! GeneralWebVC
               vc.strTitle = "Terms & Conditions"
               vc.key = "2"
               self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickPolicy(_ sender: UIButton)
    {
        let vc = StoryBoard.More.instantiateViewController(withIdentifier: "GeneralWebVC") as! GeneralWebVC
               vc.strTitle = "Privacy Policy"
               vc.key = "3"
               self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func clickAdd(_ sender: UIButton)
    {
//        let get = ToInt(sender.accessibilityHint)
//
//        qualiFicationCount = get + 1
//         qualiFicationCount += 1
        self.arrQualification.append(signupEducationModel())

        self.tableview.reloadData()
    }
    @IBAction func clickRemove(_ sender: UIButton)
    {
        if let getIndex = self.tableview.indexPathForView(sender)
        {
//            qualiFicationCount -= 1
            self.arrQualification.remove(at: getIndex.row)
            self.tableview.deleteRows(at: [getIndex], with: .fade)
        }
    }
    

   
    
}


class signupTextfiledCell: UITableViewCell,UITextFieldDelegate {
    
    @IBOutlet var btnRemove: UIButton!
    
    @IBOutlet var txtYear: UITextField!
    @IBOutlet var txtDegree: UITextField!
    
    var arrYears = [String]()
    let pickerview = UIPickerView()
    
    
    
    private var _model: signupEducationModel?
    var model: signupEducationModel? {
        get {
            return _model
        }
        set(model) {
            _model = model
            if let obj = _model {
                self.txtDegree.text =  obj.selectedDegree
                self.txtYear.text =  obj.selectedYear
            }
        }
    }
    
    override func awakeFromNib() {
        
        let currentYear = Calendar.current.component(.year, from: Date())
        for getYear in currentYear-100...currentYear {
            arrYears.append("\(getYear)")
        }
       arrYears = arrYears.reversed()
        
//        let previous = currentYear - 100
//        arrYears = (previous...currentYear).map { $0 } as NSArray
//        arrYears =  arrYears.reversed() as NSArray

        
        pickerview.delegate = self
        pickerview.dataSource = self
        
        
        self.txtYear.delegate = self
        self.txtDegree.delegate = self
        
        txtYear.tintColor = UIColor.clear
        
       
    }

    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       
        if textField == txtYear
        {
            textField.tintColor = UIColor.clear

//            let currentYear = Calendar.current.component(.year, from: Date())
//            let previous = currentYear - 100
//            arrYears = (previous...currentYear).map { $0 } as NSArray
//            arrYears =  arrYears.reversed() as NSArray
            
            self.pickerview.reloadAllComponents()
            
            /*if self.arrQty.count > 0 , ToInt(self._model?.selectedQty) == 0
            {*/
                self.pickerview.selectRow(0, inComponent: 0, animated: false)
                self.pickerView(self.pickerview, didSelectRow: 0, inComponent: 0)
            //}
            textField.inputView = self.pickerview
        }
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtDegree
        {
            guard let textFieldText = textField.text as NSString?, let modelObj = model else {
                return true
            }
            let newString: String = textFieldText.replacingCharacters(in: range, with: string)
            modelObj.selectedDegree = newString
//            debugPrint(modelObj.selectedDegree)
        }
        
        
        return true
    }
    

}

//MARK: - UIPickerView  Methods
extension signupTextfiledCell : UIPickerViewDelegate, UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrYears.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
//        let year = String(format: "%@", (arrYears[row] as? CVarArg ?? ""))
        self.txtYear.text = arrYears[row]
        self.model?.selectedYear = arrYears[row]
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrYears[row]
//        return  String(format: "%@", (arrYears[row] as? CVarArg ?? ""))
    }
}


class addFooterCell: UITableViewCell {
    
    @IBOutlet var btnAdd: UIButton!
    
}


class signupEducationModel {

    var selectedYear = ""
    var selectedDegree = ""

    var toDict_Education : [String : Any] {
        return ["edu_qua":self.selectedDegree,"year" : self.selectedYear]
    }
    
}

class InterestedCategoryListModel {
    
    var category_id =  ""
    var category_img =  ""
    var category_img_thumb =  ""
    var category_name =  ""
    var category_selected_img =  ""
    var category_selected_img_thumb =  ""
    var date =  ""
    var is_selected =  false
    
    init(_ dict : [String:Any]) {
        self.category_id = toString(dict["category_id"])
        self.category_img = toString(dict["category_img"])
        self.category_img_thumb = toString(dict["category_img_thumb"])
        self.category_name = toString(dict["category_name"])
        self.category_selected_img = toString(dict["category_selected_img"])
        self.category_selected_img_thumb = toString(dict["category_selected_img_thumb"])
        self.date = toString(dict["date"])
        self.is_selected = toString(dict["is_selected"]) == "1"
    }
}
