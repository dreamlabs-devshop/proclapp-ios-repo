
//
//  AddExpertAnswerVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 07/01/20.
//  Copyright © 2020 Ashish Parmar. All rights reserved.
//

import UIKit

class AddExpertAnswerVC: UIViewController {

    var obj : allPostModel?
    
    @IBOutlet weak var txtView: UITextView!
    
    let defultTextDes = "Write your answer here"

    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtView.delegate = self
        txtView.layer.borderColor = UIColor.lightGray.cgColor
               txtView.layer.borderWidth = 1
        

        // Do any additional setup after loading the view.
    }
   
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           self.navigationController?.isNavigationBarHidden = true
       }

   @IBAction func postBtnClicked(_ sender: Any) {
        if validationAskQuestion == true
        {
            ReplyText()
        }
    }
    var validationAskQuestion: Bool{
        
        self.view.endEditing(true)
        
        var mess = ""
        
        if ToString(txtView.text).isBlank || ToString(txtView.text) == defultTextDes{
            mess = "Please enter answer"
        }
        if mess != ""{
            self.showOkAlert(msg: mess)
            return false
        }
        return true
    }
    func ReplyText()
    {
        WebService.shared.RequesURL(ServerURL.ExpertReply, Perameters: ["user_id":globalUserId, "post_id": ToString(obj?.post_id), "type": "3","answer":ToString(txtView.text)],showProgress: true, completion: { (dicRes, success) in
            debugPrint(dicRes)
            self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                if success == true{
                    NotificationCenter.default.post(name: .AnswerPost, object:nil)
                    self.clickBack(self)
                }
            })
        }) { (err) in
            debugPrint(err)
        }
    }

}

extension  AddExpertAnswerVC : UITextViewDelegate
{
    //MARK: - UITextview Delegate Methods
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == defultTextDes {
            txtView.text = nil
        }
        txtView.textColor = UIColor.Appcolor51
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text.isEmpty {
            txtView.textColor = UIColor.lightGray
            txtView.text = defultTextDes
        }
    }
}
