//
//  AddExpertReplyPopupVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 07/01/20.
//  Copyright © 2020 Ashish Parmar. All rights reserved.
//

import UIKit

class AddExpertReplyPopupVC: UIViewController {
    
    var callback : ((Int) -> Void)?
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var viewShadow: UIView!

    @IBOutlet weak var txtAnswer: UITextView!

    var isTxtviewHide = true
    var strAnswer = ""

    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        txtAnswer.isHidden = isTxtviewHide
         txtAnswer.text = strAnswer

        viewShadow.addShadow(radius: 3.0,corner: 8.0)
        stackView.spacing = 30 * screenscale
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    
    
    @IBAction func clickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func clickselectList(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.callback?(sender.tag)
        }
    }
}
