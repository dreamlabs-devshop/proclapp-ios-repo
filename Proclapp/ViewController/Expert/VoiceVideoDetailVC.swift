
//
//  VoiceVideoDetailVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 07/01/20.
//  Copyright © 2020 Ashish Parmar. All rights reserved.
//

import UIKit


class VoiceVideoDetailVC: UIViewController {

    var objPost : allPostModel?
    
    var arrAnswer = [AnswerModel]()

    @IBOutlet weak var tblview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblview.register(UINib.init(nibName: "AudioTableViewCell", bundle: nil), forCellReuseIdentifier: "AudioTableViewCell")
        tblview.register(UINib.init(nibName: "VideoTableViewCell", bundle: nil), forCellReuseIdentifier: "VideoTableViewCell")

        NotificationCenter.default.addObserver(self, selector: #selector(self.AnswerPost), name: .AnswerPost, object: nil)

        
        getAnswerListWS()

        // Do any additional setup after loading the view.
    }
    @objc func AnswerPost(notification: NSNotification){
              DispatchQueue.main.async {
                  self.getAnswerListWS()
                  debugPrint("!!!!!!!! AnswerPost !!!!!!!!!!")
              }
          }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    @IBAction func clickBack(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
    
        @IBAction func clickFilter(_ sender: Any) {
            
            DispatchQueue.main.async {
                guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
                popupVC.arrTitle = Constant.karrFilter
                popupVC.callback =  { str in
    //                debugPrint(str)
                    self.getAnswerListWS(str)
                }
                self.present(popupVC, animated: true, completion: nil)
            }
            
        }
    
    //MARK: - Web service
    
    func getAnswerListWS(_ strGet : String = "") {
        var passdata = "3"
        if strGet.lowercased() == "User".lowercased(){
            passdata = "4"
        }
        else if strGet.lowercased() == "Views".lowercased() {
            passdata = "2"
        }
        
        if self.tblview.accessibilityHint == "service_calling" { return }
        
        self.tblview.accessibilityHint = "service_calling"
        var delayTime = DispatchTime.now()
        
        if self.tblview.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblview.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.vv_answer_list, Perameters: ["user_id" : globalUserId,"post_id":toString(self.objPost?.post_id),"sort_by":passdata],showProgress: false,completion: { (dicRes, success) in
                debugPrint(dicRes)
                if success == true{
                    if let arrTemp = dicRes["answer_list"] as? [[String:Any]] {
                        self.arrAnswer = arrTemp.map({AnswerModel.init($0)})
                    }
                }
                DispatchQueue.main.async {
                    UIView.performWithoutAnimation {
                        self.tblview.reloadData()
                    }
                    self.tblview.accessibilityHint = nil
                    self.tblview.refreshControl?.endRefreshing()
                }
                
                
            }) { (err) in
                debugPrint(err)
                self.tblview.accessibilityHint = nil
                self.tblview.refreshControl?.endRefreshing()
            }
        }
        
    }
    
    func webCall_deleteDiscuss(_ delete_answers_id:String ,callback: (()->())?){
        WebService.shared.RequesURL(ServerURL.delete_answer, Perameters: ["user_id":globalUserId,"post_answers_id":delete_answers_id],showProgress: true, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true
            {
                callback?()
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
}
//MARK: - Table view data source
extension VoiceVideoDetailVC: UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
          return 2
      }
      
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
      {
        return section == 0 ? 1 : arrAnswer.count
      }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if indexPath.section == 0
        {
            if objPost?.audioVideoType == .audio
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AudioTableViewCell") as! AudioTableViewCell
                cell.model = objPost
                cell.shouldPlayAudio = true
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell") as! VideoTableViewCell
                cell.model = objPost
                cell.btnViewDetails.isHidden = true
                cell.shouldPlayVideo = true
                return cell
            }
        }
            
        else
        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSeeAnswer") as! cellSeeAnswer
//            cell.model = arrAnswer[indexPath.row]
//            return cell
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellViewAnswer") as! cellViewAnswer
                         
             cell.model = arrAnswer[indexPath.row]
             
             cell.lblanswer.attributedText  = cell.model?.answer.htmlAttributed(family: "Lato-Regular", size: 11 * screenscale, color: .Appcolor51)
             
             cell.lblCommentcount.text = cell.model?.answer_comment_count
             
             cell.lblLikeCount.text = toString(cell.model?.post_answer_like_count)
             cell.btnLike.isSelected = cell.model?.is_post_answer_liked ?? false
             
             cell.lblUpvotecount.text = toString(cell.model?.answer_upvote_comment_count)
             cell.lblDownvoteCount.text = toString(cell.model?.answer_downvote_comment_count)
             cell.btnUpvote.isSelected = cell.model?.is_post_answer_upvote ?? false ? true : false
             cell.btndownvote.isSelected = cell.model?.is_post_answer_downvote ?? false ? true : false
             
             let strUpvote = ToInt(cell.lblUpvotecount.text) > 1 ? "Upvotes" : "Upvote"
             cell.btnUpvote.setTitle(strUpvote, for: .normal)
            cell.btnUpvote.setTitle(strUpvote, for: .selected)
             
             let strDownvote = ToInt(cell.lblDownvoteCount.text) > 1 ? "Downvotes" : "Downvote"
            cell.btndownvote.setTitle(strDownvote, for: .normal)
             cell.btndownvote.setTitle(strDownvote, for: .selected)
             
             cell.lblYouTxt.text = ""
             cell.self.imgYou.isHidden = true
             if cell.model?.is_post_answer_upvote ?? false{
                 cell.lblYouTxt.text = "You upvoted this"
                 cell.self.imgYou.isHidden = false
             }
             else if cell.model?.is_post_answer_downvote ?? false{
                 cell.lblYouTxt.text = "You downvoted this"
                 cell.self.imgYou.isHidden = false
             }
             
             cell.self.imgYou.setImageWithURL(loginData?.profile_image ?? "", "Big_dp_placeholder")
             
             if let previewUrl = cell.model?.dicPostLink?.link, checkValidString(previewUrl)
             {
                 cell.viewPreviewHeightConst.constant = 100
                 cell.self.showPreviewImage(toString(previewUrl))
             }
             else
             {
                 cell.viewPreviewHeightConst.constant = 0
                 cell.detailedView?.isHidden = true
             }
             
             cell.btnViewMore.isHidden = true
             if(cell.lblanswer.maxNumberOfLines > 6){
                 cell.btnViewMore.isHidden = false
             }
             
             cell.self.updateConstraintsIfNeeded()
             cell.self.layoutIfNeeded()
             cell.self.setUpCollection()
             return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableView.automaticDimension
//        return indexPath.section == 0 ? UITableView.automaticDimension : 68 * screenscale
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellAnswerHeader") as! cellAnswerHeader
        cell.lblTitle.text = self.arrAnswer.count > 1 ? "\(self.arrAnswer.count) ANSWERS" : "\(self.arrAnswer.count) ANSWER"
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ?  CGFloat.leastNormalMagnitude : 32 * screenscale
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
}
