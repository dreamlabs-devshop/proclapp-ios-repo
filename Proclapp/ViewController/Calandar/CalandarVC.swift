//
//  CalandarVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/9/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import BottomPopup

class CalandarVC: BottomPopupViewController {
    

    @IBOutlet weak var viewBorder: UIView!

    @IBOutlet weak var lblCaryDate: UILabel!

    @IBOutlet weak var calendar: FSCalendar!

    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    var dateCarey = ""
    
    var callback : ((String) -> Void)?
    
    var height: CGFloat?
    
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewBorder.addShadow(radius: 3, corner: 10)
       
        self.calendar.select(Date())
        self.calendar.scope = .month
        
        dFMS.dateFormat = "yyyy-MM-dd"
        dateCarey = dFMS.string(from: Date())
        
        
        lblCaryDate.text = dateCarey
        
        
    }
    
    @IBAction func clickDone(_ sender: Any){
       
        dismiss(animated: true) {
            self.callback?(self.dateCarey)
        }
    }
    
    
    @IBAction func clickDismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
   
    deinit {
        print("\(#function)")
    }
    
    
    // Bottom popup attribute methods
    // You can override the desired method to change appearance
    
    override func getPopupHeight() -> CGFloat {
        return height ?? screenheight
    }
    
    override func getPopupTopCornerRadius() -> CGFloat {
        return topCornerRadius ?? CGFloat(0)
    }
    
    override func getPopupPresentDuration() -> Double {
        return presentDuration ?? 0.5
    }
    
    override func getPopupDismissDuration() -> Double {
        return dismissDuration ?? 0.5
    }
    
    override func shouldPopupDismissInteractivelty() -> Bool {
        return shouldDismissInteractivelty ?? true
    }
}

extension CalandarVC : FSCalendarDataSource, FSCalendarDelegate
{
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        //        self.calendarHeightConstraint.constant = bounds.height
        //        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("did select date \(self.dateFormatter.string(from: date))")
        dateCarey = ("\(self.dateFormatter.string(from: date))")
        lblCaryDate.text = dateCarey
//        let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
        if monthPosition == .next || monthPosition == .previous{
            calendar.setCurrentPage(date, animated: true)
        }
    }
    
    
    /*func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        let curDate = Date().addingTimeInterval(-24*60*60)
        if date < curDate {
            return false
        } else {
            return true
        }
    }*/
    func calendarCurrentPageDidChange(_ cal: FSCalendar) {
        print("\(self.dateFormatter.string(from: cal.currentPage))")
    }
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    ;
    
    
    
}
