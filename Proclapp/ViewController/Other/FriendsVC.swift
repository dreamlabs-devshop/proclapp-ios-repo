//
//  FriendsVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/16/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class FriendsVC: UIViewController,UIScrollViewDelegate {
    
    
    var arrSuggestionList = [FriendsModel]()
    var arrFriendList = [FriendsModel]()
    
    @IBOutlet weak var tblSuggestion: UITableView!
    @IBOutlet weak var tblFriends: UITableView!
    
    @IBOutlet weak var Scrollviewmain: UIScrollView!
    
    
    @IBOutlet var btnSuggestion: UIButton!
    @IBOutlet var btnFriends: UIButton!
    
    @IBOutlet var xLineConst: NSLayoutConstraint!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var indicatorSuggeston: UIActivityIndicatorView!

    var isAvailavbleMoreDataFriend = false
    var isAvailavbleMoreDataSuggestion = false

    var isFromNoti = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        DispatchQueue.main.async {
            self.indicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tblFriends.bounds.width, height: CGFloat(30))
            self.tblFriends.tableFooterView = self.indicator
            
            self.indicatorSuggeston.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tblSuggestion.bounds.width, height: CGFloat(30))
            self.tblSuggestion.tableFooterView = self.indicatorSuggeston
        }
        
        self.getSuggestionListAPI()
        self.getFriendsListAPI()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
//        self.getSuggestionListAPI()
//
//        self.getFriendsListAPI()
        
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapTwoButton(_ sender: UIButton)
    {
        btnSuggestion.isSelected = false
        btnFriends.isSelected = false
        
        
        if sender == btnSuggestion{
            sender.isSelected = true
            tblSuggestion.reloadData()
        }
        else if sender == btnFriends{
            sender.isSelected = true
            tblFriends.reloadData()
        }
        
        Scrollviewmain.setContentOffset(CGPoint.init(x: screenwidth * CGFloat(sender.tag), y: 0), animated: true)
        
        xLineConst.constant =  (screenwidth / 2) * CGFloat(sender.tag)
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnAcceptClicked(_ sender: UIButton)
    {
        if let indexPath = tblFriends.indexPathForView(sender){
            self.webCallAcceptRejectFriendRequest("1", senderID: toString(arrFriendList[indexPath.row].sender_id), completion: {
                self.arrFriendList[indexPath.row].request_status = "1"
                self.tblFriends.reloadRows(at: [indexPath], with: .none)
            })
        }
    }
    
    @IBAction func btnRejectClicked(_ sender: UIButton){
        if let indexPath = tblFriends.indexPathForView(sender){
            self.webCallAcceptRejectFriendRequest("0", senderID: toString(arrFriendList[indexPath.row].sender_id), completion: {
                self.arrFriendList.remove(at: indexPath.row)
                self.tblFriends.deleteRows(at: [indexPath], with: .automatic)
            })
        }
    }
    @IBAction func btnAddFriendClicked(_ sender: UIButton)
    {
        if let indexPath = tblSuggestion.indexPathForView(sender){
            self.webCall_AddRemoveFriend( toString(arrSuggestionList[indexPath.row].sender_id), completion: {
                self.arrSuggestionList.remove(at: indexPath.row)
                self.tblSuggestion.deleteRows(at: [indexPath], with:.automatic )
            })
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView == Scrollviewmain
        {
            if Scrollviewmain.contentOffset.x == screenwidth
            {
                tapTwoButton(btnFriends)
            }
            else
            {
                tapTwoButton(btnSuggestion)
            }
        }
    }
    
    //MARK:- API
    /*func getSuggestionListAPI(){
        
        WebService.shared.RequesURL(ServerURL.FriendSuggestionList, Perameters: ["user_id": globalUserId], showProgress: true, completion: { (dictResponse, Status) in
            if Status{
                //                debugPrint(dictResponse)
                
                if let arr = dictResponse.object(forKey: "suggestion_list") as? [[String: Any]]{
                    self.arrSuggestionList = arr.map({FriendsModel.init($0)})
                }
                
                self.tblSuggestion.reloadData()
            }
        }) { (error) in
            debugPrint(error.localizedDescription)
        }
    }*/
    
   /* func getFriendsListAPI(){
        
        WebService.shared.RequesURL(ServerURL.GetFriends, Perameters: ["user_id": globalUserId], showProgress: true, completion: { (dictResponse, Status) in
            if Status{
                debugPrint(dictResponse)
                
                if let arr = dictResponse.object(forKey: "friends_list") as? [[String: Any]]{
                    self.arrFriendList = arr.map({FriendsModel.init($0)})
                }
                
                self.tblFriends.reloadData()
            }
        }) { (error) in
            debugPrint(error.localizedDescription)
        }
    }*/
    @objc func getFriendsListAPI(_ offset : Int = 0) {
        
        
        if offset == 0
        {
            self.arrFriendList.removeAll()
            self.tblFriends.reloadData()
        }
      /*  var delayTime = DispatchTime.now()
        
        if self.tblFriends.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblFriends.refreshControl?.beginRefreshingManually()
        }*/
        
//        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.GetFriends, Perameters: ["user_id" : globalUserId,"offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
//                    debugPrint(dicRes)
                    if let arr = dicRes["friends_list"] as? [[String:Any]] {
                        
                        if offset == 0
                        {
                            self.arrFriendList = arr.map({FriendsModel.init($0)})
                        }
                        else
                        {
                            self.arrFriendList.append(contentsOf: arr.map({FriendsModel.init($0)}))
                        }
                        if self.isFromNoti
                        {
                            self.isFromNoti = false
                            self.tapTwoButton(self.btnFriends)
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.isAvailavbleMoreDataFriend = self.arrFriendList.count >= ToInt(dicRes["offset"])
                    UIView.performWithoutAnimation {
                        self.tblFriends.reloadData()
                    }
                    self.tblFriends.accessibilityHint = nil
//                    self.tblFriends.refreshControl?.endRefreshing()
                    self.indicator.stopAnimating()
                }
            }) { (err) in
                self.tblFriends.accessibilityHint = nil
//                self.tblFriends.refreshControl?.endRefreshing()
                self.indicator.stopAnimating()
            }
//        }
    }
    
    
    @objc func getSuggestionListAPI(_ offset : Int = 0) {
        
        if offset == 0
        {
            self.arrSuggestionList.removeAll()
            self.tblSuggestion.reloadData()
        }
        /*  var delayTime = DispatchTime.now()
         
         if self.tblSuggestion.refreshControl?.isRefreshing == false
         {
         delayTime = DispatchTime.now() + 0.5
         self.tblSuggestion.refreshControl?.beginRefreshingManually()
         }*/
        
        //            DispatchQueue.main.asyncAfter(deadline: delayTime) {
        WebService.shared.RequesURL(ServerURL.FriendSuggestionList, Perameters: ["user_id" : globalUserId,"offset":offset],showProgress: false,completion: { (dicRes, success) in
            
            //                    debugPrint(dicRes)
            
            if success == true{
                if let arr = dicRes["suggestion_list"] as? [[String:Any]] {
                    
                    if offset == 0
                    {
                        self.arrSuggestionList = arr.map({FriendsModel.init($0)})
                    }
                    else
                    {
                        self.arrSuggestionList.append(contentsOf: arr.map({FriendsModel.init($0)}))
                    }
                }
            }
            
            debugPrint("count--> ",self.arrSuggestionList.count)
            
            DispatchQueue.main.async {
                self.isAvailavbleMoreDataSuggestion = self.arrSuggestionList.count >= ToInt(dicRes["offset"])
                UIView.performWithoutAnimation {
                    self.tblSuggestion.reloadData()
                }
                self.tblSuggestion.accessibilityHint = nil
//                self.tblSuggestion.refreshControl?.endRefreshing()
                self.indicatorSuggeston.stopAnimating()
            }
        }) { (err) in
            self.tblSuggestion.accessibilityHint = nil
//            self.tblSuggestion.refreshControl?.endRefreshing()
            self.indicatorSuggeston.stopAnimating()
        }
        //            }
    }
    
    
    func webCallAcceptRejectFriendRequest(_ status: String, senderID: String, completion:(()->())?){
        
        var dict = [String: Any]()
        dict["user_id"] = globalUserId
        dict["sender_id"] = senderID
        dict["status"] = status
        
        WebService.shared.RequesURL(ServerURL.AcceptRejectFriend, Perameters: dict, showProgress: true, completion: { (dictResponse, status) in
            if status{
//                debugPrint(dictResponse)
                completion?()
            }else{
                
            }
        }) { (error) in
            self.showOkAlert(msg: error.localizedDescription)
        }
    }
    func webCall_AddRemoveFriend( _ profileId:String, completion:(()->())?){
        WebService.shared.RequesURL(ServerURL.AddFriendUnFriend, Perameters: ["user_id":globalUserId,"friend_id":profileId,"type":"1"],showProgress: true, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true{
                completion?()
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
    
}

//MARK: - Table view data source
extension FriendsVC: UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblSuggestion{
            return arrSuggestionList.count
        }
        else{
            return arrFriendList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblSuggestion{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSuggestion") as! cellSuggestion
            cell.imgProfile.setImageWithURL_Colletion(arrSuggestionList[indexPath.row].profile_image_thumb, UIImage.init(named: "experts_placeholder"))
            
            cell.imgProfile.setupImageViewer(options: [.theme(.dark)])
            //cell.imgProfile.setImageWithURL(arrSuggestionList[indexPath.row].profile_image_thumb, "experts_placeholder")
            
            cell.lblName.text = arrSuggestionList[indexPath.row].name
            cell.selectionStyle = .none

            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellFriends") as! cellFriends
            cell.selectionStyle = .none
            cell.imgProfile.setupImageViewer(options: [.theme(.dark)])
            cell.model = arrFriendList[indexPath.row]
        
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60 * screenscale
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 42
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dic = tableView == tblSuggestion ? arrSuggestionList[indexPath.row] : arrFriendList[indexPath.row]
        if dic.user_id == globalUserId
        {
            let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            self.navigationController?.pushViewController(vcInstace, animated: true)
        }
        else{
            let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "OtherProfileVC") as! OtherProfileVC
            vcInstace.profileId = dic.user_id
            self.navigationController?.pushViewController(vcInstace, animated: true)
        }
    }
    
    
    func scrollViewDidEndDragging(_ aScrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if aScrollView == self.tblSuggestion{
            let offset: CGPoint = aScrollView.contentOffset
            let bounds: CGRect = aScrollView.bounds
            let size: CGSize = aScrollView.contentSize
            let inset: UIEdgeInsets = aScrollView.contentInset
            let y = Float(offset.y + bounds.size.height - inset.bottom)
            let h = Float(size.height)
            let reload_distance: Float = 0
            //            print("load more data!!!!!")
            if y > h + reload_distance{
                if self.isAvailavbleMoreDataSuggestion == true , self.tblSuggestion.accessibilityHint == nil,aScrollView == tblSuggestion {
                    self.indicatorSuggeston.startAnimating()
                    self.tblSuggestion.accessibilityHint = "service_calling"
                    self.getSuggestionListAPI(self.arrSuggestionList.count)
                }
            }
        }
        else if aScrollView == self.tblFriends{
            let offset: CGPoint = aScrollView.contentOffset
            let bounds: CGRect = aScrollView.bounds
            let size: CGSize = aScrollView.contentSize
            let inset: UIEdgeInsets = aScrollView.contentInset
            let y = Float(offset.y + bounds.size.height - inset.bottom)
            let h = Float(size.height)
            let reload_distance: Float = 0
            //            print("load more data!!!!!")
            if y > h + reload_distance{
                if self.isAvailavbleMoreDataFriend == true , self.tblFriends.accessibilityHint == nil,aScrollView == tblFriends {
                    self.indicator.startAnimating()
                    self.tblFriends.accessibilityHint = "service_calling"
                    self.getFriendsListAPI(arrFriendList.count)
                }
            }
        }
        
    }
    
}
class cellSuggestion : UITableViewCell
{
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imgProfile: UIImageView!
//    @IBOutlet var btnInvite: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            self.imgProfile.layer.cornerRadius = self.imgProfile.frame.height/2
            self.imgProfile.layer.masksToBounds = true
        }
    }
}

class cellFriends : UITableViewCell
{
    @IBOutlet var lblName: UILabel!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var btnInvite: UIButton!
    @IBOutlet var viewAcceptReject: UIView!
    @IBOutlet var viewRequested: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            self.imgProfile.layer.cornerRadius = self.imgProfile.frame.height/2
            self.imgProfile.layer.masksToBounds = true
            
            self.viewRequested.layer.cornerRadius = 5
            self.viewRequested.layer.masksToBounds = true
            self.viewRequested.setborder(1, _color: UIColor(red:0.20, green:0.20, blue:0.20, alpha:1.0))
        }
    }
    
    
    //MARK:- SET MODEL
    
    var _model : FriendsModel?
    var model : FriendsModel? {
        
        get{
            return _model
        }
        set(model)
        {
            _model = model
            
        imgProfile.setImageWithURL_Colletion(model?.profile_image_thumb, UIImage.init(named: "experts_placeholder"))

//            imgProfile.setImageWithURL(model?.profile_image_thumb, "experts_placeholder")
            lblName.text = model?.name
            
            if model?.request_status == "0"{
                if  model?.sender_id == globalUserId{
                    viewRequested.isHidden = false
                    viewAcceptReject.isHidden = true
                }
                else{
                    viewRequested.isHidden = true
                    viewAcceptReject.isHidden = false
                }
                btnInvite.isHidden = true
            }
            else{
                btnInvite.isHidden = false
                viewRequested.isHidden = true
                viewAcceptReject.isHidden = true
            }
            
            
        }
        
    }
    
    @IBAction func clickChat(_ sender: UIButton)
    {
        if let obj = model
        {
            let vcInstace = StoryBoard.Message.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            vcInstace.touserId = obj.user_id
            vcInstace.profileURL = obj.profile_image
            vcInstace.name = obj.name
            self.tableView?.parentViewController?.pushTo(vcInstace)
        }
    }
}

//MARK:- Model
class FriendsModel{
    
    //For Friends
    var cover_image: String = ""
    var email: String = ""
    var firstname: String = ""
    var is_expert: String = ""
    var lastname: String = ""
    var name: String = ""
    var notification_status: String = ""
    var phone: String = ""
    var profession: String = ""
    var profile_image: String = ""
    var profile_image_thumb: String = ""
    var receiver_id: String = ""
    var request_status: String = ""
    var request_status_title: String = ""
    var sender_id: String = ""
    var user_id: String = ""
    var chat_id  = ""

    
    
    //For MsgConver
    var message = ""
    var receiver_message = ""
    var sender_message = ""
    var sent_at = ""
    var to_user_id = ""
    var type = ""
    var unread_msg = 0
    
    var dicSend : SendMsgModel?

    var dicReceive : ReceiveMsgModel?

    var isSender = false

 
    init(_ dict: [String: Any]) {
        
        self.cover_image = toString(dict["cover_image"])
        self.email = toString(dict["email"])
        self.firstname = toString(dict["firstname"])
        self.is_expert = toString(dict["is_expert"])
        self.lastname = toString(dict["lastname"])
        self.name = toString(dict["name"])
        self.notification_status = toString(dict["notification_status"])
        self.phone = toString(dict["phone"])
        self.profession = toString(dict["profession"])
        self.profile_image = toString(dict["profile_image"])
        self.profile_image_thumb = toString(dict["profile_image_thumb"])
        self.receiver_id = toString(dict["receiver_id"])
        self.request_status = toString(dict["request_status"])
        self.request_status_title = toString(dict["request_status_title"])
        self.sender_id = toString(dict["sender_id"])
        self.user_id = toString(dict["user_id"])
        
        self.message = toString(dict["message"])
        self.receiver_message = toString(dict["receiver_message"])
        self.sender_message = toString(dict["sender_message"])
        self.sent_at = toString(dict["sent_at"])
        self.to_user_id = toString(dict["to_user_id"])
        self.type = toString(dict["type"])
        self.unread_msg = ToInt((dict["unread_msg"]))
        
        self.chat_id = toString((dict["chat_id"]))

        
        
        self.isSender = toString(dict["type"]) == "sent"

        
        //For Send
        if let dic = dict["sender"] as? [String:Any] {
            self.dicSend = SendMsgModel.init(dic)
        }
        
        //For Receive
        if let dic = dict["receiver"] as? [String:Any] {
            self.dicReceive = ReceiveMsgModel.init(dic)
        }
    }
}


//MARK:- Model
class SendMsgModel
{
    var about_my_profession = ""
    var date = ""
    var email = ""
    var last_seen = ""
    var name = ""
    var profile_image = ""
    var user_id = ""
    
    var i_hve_blocked = false
    var is_blocked_me = false
    
    init(_ dict: [String: Any]) {
        
        self.about_my_profession = toString(dict["about_my_profession"])
        self.date = toString(dict["date"])
        self.email = toString(dict["email"])
        self.last_seen = toString(dict["last_seen"])
        self.name = toString(dict["name"])
        self.profile_image = toString(dict["profile_image"])
        self.user_id = toString(dict["user_id"])

        self.i_hve_blocked = ConvertToBool(dict["i_hve_blocked"])
        self.is_blocked_me = ConvertToBool(dict["is_blocked_me"])
    }
    
}
class ReceiveMsgModel
{
    var about_my_profession = ""
    var date = ""
    var email = ""
    var last_seen = ""
    var name = ""
    var profile_image = ""
    var user_id = ""
    
    var i_hve_blocked = false
    var is_blocked_me = false
    
    init(_ dict: [String: Any]) {
        
        self.about_my_profession = toString(dict["about_my_profession"])
        self.date = toString(dict["date"])
        self.email = toString(dict["email"])
        self.last_seen = toString(dict["last_seen"])
        self.name = toString(dict["name"])
        self.profile_image = toString(dict["profile_image"])
        self.user_id = toString(dict["user_id"])
        
        self.i_hve_blocked = ConvertToBool(dict["i_hve_blocked"])
        self.is_blocked_me = ConvertToBool(dict["is_blocked_me"])
    }
}

//    blocked = 0;
//    "chat_id" = 2;
//    "deleted_at" = "";
//    "delivered_at" = "";
//    document = "";
//    "document_thumb" = "";
//    "edited_at" = "";
//    "group_id" = "";
//    location = "";
//    message = hi;
//    "message_id" = "36-20191002093126873";
//    "msg_type" = text;
//    "offline_id" = "";
//    receiver =             {
//    "about_my_profession" = "ios prismetric";
//    date = "2019-10-02 13:00:32";
//    email = "maulik@prismetric.com";
//    "i_hve_blocked" = 0;
//    "is_blocked_me" = 0;
//    "last_seen" = Online;
//    name = "maulik patel";
//    phone = 76006694443;
//    "phone_code" = "+1684";
//    "profile_image" = "http://35.181.75.245/upload/20190916093200352958.png";
//    "user_id" = 58;
//    };
////    "seen_at" = "";
//    sender =             {
//    "about_my_profession" = "Android Developer at prismetri technology pvt ltd";
//    date = "2019-10-02 13:01:56";
//    email = "darshan@prismetric.com";
//    "i_hve_blocked" = 0;
//    "is_blocked_me" = 0;
//    "last_seen" = Online;
//    name = "Darshan Sharda";
//    phone = 0123456789;
//    "phone_code" = 91;
//    "profile_image" = "http://35.181.75.245/upload/201904291356581770936.jpeg";
//    "user_id" = 36;
//    };
////    "sent_at" = "2019-10-02 09:31:26";
////    "to_user_id" = 36;
//    type = received;
////    "user_id" = 58;





//    "about_my_profession" = "Android Developer at prismetri technology pvt ltd";
//    blocked = 0;
//    "chat_id" = 2;
//    "deleted_at" = "";
//    "delivered_at" = "";
//    document = "";
//    "document_thumb" = "";
//    "edited_at" = "";
//    email = "darshan@prismetric.com";
//    "group_id" = "";
//    location = "";
//    message = hi;
//    "message_id" = "36-20191002093126873";
//    "msg_type" = text;
//    name = "Darshan Sharda";
//    "offline_id" = "";
//    phone = 0123456789;
//    "phone_code" = 91;
//    "profile_image" = "http://35.181.75.245/upload/201904291356581770936.jpeg";
//    "receiver_message" = "";
//    "seen_at" = "";
//    "sender_message" = hi;
//    "sent_at" = "2019-10-02 09:31:26";
//    "to_user_id" = 36;
//    type = received;
//    "unread_msg" = 1;
//    "user_id" = 58;

