
//
//  SearchVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/23/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class SearchVC: UIViewController,UISearchBarDelegate {

    @IBOutlet var txtSearchBar: UISearchBar?
    @IBOutlet weak var tblview: UITableView?
    @IBOutlet weak var indicator: UIActivityIndicatorView?
    var isAvailavbleMoreDat = false

    var arrSearch = [allPostModel]()
    var arrSearchUser = [OtherUserProfileModel]()
    

//    static let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblview?.contentInset = UIEdgeInsets.init(top: 10, left: 0, bottom: 0, right: 0)

        self.indicator?.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tblview?.bounds.width ?? screenwidth, height: CGFloat(30))
        self.tblview?.tableFooterView = self.indicator

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   /* func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchText \(searchText)")
    }*/
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchText \(toString(searchBar.text))")
        self.view.endEditing(true)
        if toString(searchBar.text).isValid
        {
            getSearchList()
        }
        else
        {
            arrSearch.removeAll()
            arrSearchUser.removeAll()
            tblview?.reloadData()
        }
    }
    
    
   
    
    @objc func getSearchList(_ offset : Int = 0) {
        
        let para = ["user_id" : globalUserId,"offset":offset,"chr":toString(self.txtSearchBar?.text)] as [String : Any]
        
                        debugPrint(para)

        WebService.shared.RequesURL(ServerURL.GetSearchPost, Perameters: para,showProgress: offset == 0 ? true : false ,completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            if success == true{
                if let arrPrice = dicRes["posts_list"] as? [[String:Any]] {
                    if offset == 0{
                        self.arrSearch = arrPrice.map({allPostModel.init($0)})
                    }
                    else
                    {
                        self.arrSearch.append(contentsOf: arrPrice.map({allPostModel.init($0)}))
                    }
                }
                
                // populate users list from search
                if let arrUserList = dicRes["suggested_friends"] as? [[String:Any]] {
                    if offset == 0{
                        self.arrSearchUser = arrUserList.map({OtherUserProfileModel.init($0)})
                    }
                    else
                    {
                        self.arrSearchUser.append(contentsOf: arrUserList.map({OtherUserProfileModel.init($0)}))
                    }
                }
                
            }
            else
            {
                self.arrSearch.removeAll()
                self.arrSearchUser.removeAll()
            }
            
            DispatchQueue.main.async {
                self.isAvailavbleMoreDat = self.arrSearch.count >= ToInt(dicRes["offset"])
                UIView.performWithoutAnimation {
                    self.tblview?.reloadData()
                }
                self.tblview?.accessibilityHint = nil
                self.indicator?.stopAnimating()
            }
        }) { (err) in
            self.tblview?.accessibilityHint = nil
            self.indicator?.stopAnimating()
        }
    }
    
}
//MARK: - Table view data source
extension SearchVC: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        tblview?.isHidden = (arrSearch.count + arrSearchUser.count) == 0

        return (arrSearch.count + arrSearchUser.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
//        debugPrint(indexPath.row)
//        debugPrint(arrSearch.count + indexPath.row)
//        debugPrint(arrSearchUser.count)
    
        if(arrSearch.count > indexPath.row){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSearchData") as! cellSearchData
            let obj = arrSearch[indexPath.row]
            cell.imgProfile.setImageWithURL(obj.profile_image, "Big_dp_placeholder")
            cell.lblDes.text = obj.post_title
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSearchUserData") as! cellSearchUserData
            let obj = arrSearchUser[indexPath.row - arrSearch.count]
            cell.imgProfile.setImageWithURL(obj.profile_image, "Big_dp_placeholder")
            cell.lblDes.text = obj.firstname + " " + obj.lastname
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (arrSearch.count > indexPath.row){
            let obj = arrSearch[indexPath.row]
            let post_type = obj.postType
            if post_type == .Article
            {
                let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "ArticleDetailVC") as! ArticleDetailVC
                vcInstace.dictPost = obj
                self.pushTo(vcInstace)
            }
            else if post_type == .Question ||  post_type == .ResearchPapers
            {
                let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerListVC") as! AnswerListVC
                vcInstace.dictPost = obj
                self.pushTo(vcInstace)
            }
            else if post_type == .VoiceVideo
            {
                let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "VoiceVideoDetailVC") as! VoiceVideoDetailVC
                vcInstace.objPost = obj
                self.pushTo(vcInstace)
            }
            else if post_type == .Advert
            {
                let vcInstace = StoryBoard.Advert.instantiateViewController(withIdentifier: "AdvertDetailsVC") as! AdvertDetailsVC
                vcInstace.dictPost = obj
                self.pushTo(vcInstace)
            }
        }
        else {
            let obj = arrSearchUser[indexPath.row - arrSearch.count]
            let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "OtherProfileVC") as! OtherProfileVC
                vcInstace.profileId = obj.user_id
                           self.pushTo(vcInstace)
        }
        
        
//        switch self.arrSearch[indexPath.row].postType {
//
//        case .Article:
//            let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "ArticleDetailVC") as! ArticleDetailVC
//            vcInstace.dictPost = self.arrSearch[indexPath.row]
//            self.pushTo(vcInstace)
//
//        case .Question:
//            let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerListVC") as! AnswerListVC
//            vcInstace.dictPost = self.arrSearch[indexPath.row]
//            self.pushTo(vcInstace)
//
//        case .VoiceVideo:
//            debugPrint("voice")
//
//        case .ResearchPapers:
//            let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerListVC") as! AnswerListVC
//            vcInstace.dictPost = self.arrSearch[indexPath.row]
//            self.pushTo(vcInstace)
//
//
//        case .Advert:
//            debugPrint("Advert")
//        }
        
    }
   
    
    func scrollViewDidEndDragging(_ aScrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if aScrollView == self.tblview{
            let offset: CGPoint = aScrollView.contentOffset
            let bounds: CGRect = aScrollView.bounds
            let size: CGSize = aScrollView.contentSize
            let inset: UIEdgeInsets = aScrollView.contentInset
            let y = Float(offset.y + bounds.size.height - inset.bottom)
            let h = Float(size.height)
            let reload_distance: Float = 0
            //            print("load more data!!!!!")
            if y > h + reload_distance{
                if self.isAvailavbleMoreDat == true , self.tblview?.accessibilityHint == nil{
                    
                    self.indicator?.startAnimating()
                    self.tblview?.accessibilityHint = "service_calling"
                    getSearchList(self.arrSearch.count)
                }
            }
        }
    }
}
class cellSearchData : UITableViewCell
{
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblDes: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            self.imgProfile.layer.cornerRadius =  self.imgProfile.frame.height / 2
        }
    }
}

class cellSearchUserData : UITableViewCell
{
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblDes: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            self.imgProfile.layer.cornerRadius =  self.imgProfile.frame.height / 2
        }
    }
}
