//
//  FollowersVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/24/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class FollowersVC: UIViewController {

//    static let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "FollowersVC") as! FollowersVC

    @IBOutlet weak var tblview: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.isNavigationBarHidden = false
    }

}
//MARK: - Table view data source
extension FollowersVC: UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellFollowers") as! cellFollowers
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 68 * screenscale
    }
    
    
    
}
class cellFollowers : UITableViewCell
{
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblPost: UILabel!
    
    @IBOutlet var imgProfile: UIImageView!
   
}
