//
//  ReportabuseDesVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 17/09/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class ReportabuseDesVC: UIViewController {

    let defultTextDes = "Write desciption"

    @IBOutlet weak var txtview: UITextView!

    var post_id = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        txtview.setBorder()
        txtview.text = defultTextDes
        txtview.textColor = UIColor.lightGray
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickReport(_ sender: Any){
        self.view.endEditing(true)
        WebService.shared.RequesURL(ServerURL.post_report_abuse, Perameters: ["user_id" : globalUserId,"post_id":post_id,"description":toString(txtview.text) == defultTextDes ? "" :  toString(txtview.text)],showProgress: true,completion: { (dicRes, success) in
            self.showOkAlertWithHandler(msg: toString(dicRes.object(forKey: "response_msg"))){
                if success{
                    self.clickBack(self)
                }
            }
        }) { (err) in
        }
    }
    

}

extension  ReportabuseDesVC : UITextViewDelegate
{
    //MARK: - UITextview Delegate Methods
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == defultTextDes {
            txtview.text = nil
        }
        txtview.textColor = UIColor.Appcolor51
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text.isEmpty {
            txtview.textColor = UIColor.lightGray
            txtview.text = defultTextDes
        }
    }
}
