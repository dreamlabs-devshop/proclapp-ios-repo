//
//  FollowingVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/24/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class FollowingVC: UIViewController {
    
    @IBOutlet weak var lblHeader: UILabel!


@IBOutlet weak var indicator: UIActivityIndicatorView!

var isFollowing = false
    
var isAvailavbleMoreDat = false
var arrFollowers = [ExpertListModel]()

    
    @IBOutlet weak var tblview: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblHeader.text = self.isFollowing ? "Followings" : "Followers"

        self.tblview.refreshControl = UIRefreshControl()
        self.tblview.refreshControl?.tintColor = .AppSkyBlue
        self.tblview.refreshControl?.addTarget(self, action: #selector(refreshCalled), for: UIControl.Event.valueChanged)
        
        self.refreshCalled()
        
        
        DispatchQueue.main.async {
            self.indicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tblview.bounds.width, height: CGFloat(30))
            self.tblview.tableFooterView = self.indicator
        }

        
        // Do any additional setup after loading the view.
    }
    

    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    @objc func refreshCalled()
    {
        self.getFollowingList()
    }
    
    @objc func getFollowingList(_ offset : Int = 0) {
        
        var delayTime = DispatchTime.now()
        
        if self.tblview.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblview.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            
            
            WebService.shared.RequesURL(self.isFollowing ? ServerURL.Following_list : ServerURL.Follower_list, Perameters: ["user_id" : globalUserId,"offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                    debugPrint(dicRes)
                    
                    let dictKey = self.isFollowing ? "following_list" : "follower_list"
                    
                    if let arr = dicRes[dictKey] as? [[String:Any]] {
                        if offset == 0{
                            self.arrFollowers = arr.map({ExpertListModel.init($0)})
                        }
                       /* else
                        {
                            self.arrNoti.append(contentsOf: arrPrice.map({NotificationListModel.init($0)}))
                        }*/
                    }
                }
                DispatchQueue.main.async {
                    self.isAvailavbleMoreDat = self.arrFollowers.count >= ToInt(dicRes["offset"])
                    UIView.performWithoutAnimation {
                        self.tblview.reloadData()
                    }
                    self.tblview.accessibilityHint = nil
                    self.tblview.refreshControl?.endRefreshing()
                    self.indicator.stopAnimating()
                }
            }) { (err) in
                self.tblview.accessibilityHint = nil
                self.tblview.refreshControl?.endRefreshing()
                self.indicator.stopAnimating()
            }
        }
    }
    
    
}

//MARK: - Table view data source
extension FollowingVC: UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrFollowers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellFollowings") as! cellFollowings
        
        let obj = arrFollowers[indexPath.row]
        
        cell.lblName.text = obj.FullName
        cell.lblPost.text = obj.profession

        cell.imgProfile.setImageWithURL(obj.profile_image, "Big_dp_placeholder")

        cell.btnFollowings.isHidden = !isFollowing
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dic = arrFollowers[indexPath.row]
        if dic.user_id == globalUserId
        {
            let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            self.navigationController?.pushViewController(vcInstace, animated: true)
        }
        else{
            let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "OtherProfileVC") as! OtherProfileVC
            vcInstace.profileId = dic.user_id
            self.navigationController?.pushViewController(vcInstace, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 68 * screenscale
    }
  /*func scrollViewDidEndDragging(_ aScrollView: UIScrollView, willDecelerate decelerate: Bool)
   {
       if aScrollView == self.tblview{
           let offset: CGPoint = aScrollView.contentOffset
           let bounds: CGRect = aScrollView.bounds
           let size: CGSize = aScrollView.contentSize
           let inset: UIEdgeInsets = aScrollView.contentInset
           let y = Float(offset.y + bounds.size.height - inset.bottom)
           let h = Float(size.height)
           let reload_distance: Float = 0
           //            print("load more data!!!!!")
           if y > h + reload_distance{
               if self.isAvailavbleMoreDat == true , self.tblview.accessibilityHint == nil{
                   
                   self.indicator.startAnimating()
                   self.tblview.accessibilityHint = "service_calling"
                   getFollowingList(self.arrFollowers.count)
               }
           }
       }
   }*/
   
    
}

class cellFollowings : UITableViewCell
{
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblPost: UILabel!
    
    @IBOutlet var imgProfile: UIImageView!

    @IBOutlet weak var btnFollowings: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnFollowings.layer.cornerRadius = 5
        DispatchQueue.main.async {
            self.imgProfile.layer.cornerRadius =  self.imgProfile.frame.height / 2
            self.imgProfile.layer.masksToBounds = true
        }
    }

        
        
        func callDeletePost(_ post_id:String,callback: (()->())?)
        {
            WebService.shared.RequesURL(ServerURL.DeletePost, Perameters: ["user_id" : globalUserId,"post_id":post_id],showProgress: true,completion: { (dicRes, success) in
                if success == true
                {
                    callback?()
                }
                else{
                    self.tableView?.parentViewController?.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
                }
            }) { (err) in
            }
        }
    
}

extension FollowingVC
{
    @IBAction func clickUnFollow(_ sender: UIButton) {
        
        if let indexPath = self.tblview.indexPathForView(sender){
            
            WebService.shared.RequesURL(ServerURL.FollowUnFollow, Perameters: ["user_id":globalUserId,"follower_id":arrFollowers[indexPath.row].user_id,"type":"0"],showProgress: true, completion: { (dicRes, success) in
                debugPrint(dicRes)
                if success == true{
                                    self.arrFollowers.remove(at: indexPath.row)
                    self.tblview.deleteRows(at: [indexPath], with: .left)

                }
                else{
                    self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
                }
            }) { (err) in
                debugPrint(err)
            }
            
            
//            webCall_FollowUnFollow("0",arrFollowers[indexPath.row].user_id) {
//                self.arrFollowers.remove(at: indexPath.row)
//                self.tblview.deleteRows(at: [indexPath], with: .left)
//            }
        }
    }
    func webCall_FollowUnFollow( _ type:String,_ follower_id:String,callback: (()->())?)
    {
        WebService.shared.RequesURL(ServerURL.FollowUnFollow, Perameters: ["user_id":globalUserId,"follower_id":follower_id,"type":type],showProgress: true, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true{
                callback?()
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
    
}
