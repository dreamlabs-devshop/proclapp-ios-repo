//
//  AnswerDetailVC.swift
//  Proclapp
//
//  Created by iMac-4 on 4/19/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import SwiftLinkPreview
import SafariServices

class AnswerDetailVC: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblProfession: UILabel!
    
    @IBOutlet weak var quoteView: UIView!
    @IBOutlet weak var actionsView: UIView!
    
    var dictAnswer : AnswerModel?
    var post_answers_id = ""
    var strComment = ""
    var replyId = ""
    
    var header : AnswerDetailHeaderCell!
    
    
    var editCommentId = ""
    var replyCommentId = "";
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        header = tblView.dequeueReusableCell(withIdentifier: "AnswerDetailHeaderCell") as? AnswerDetailHeaderCell

        header.collectionClickBlock = {
            if let img = self.dictAnswer?.arrImages {
                let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "imageGalleryVC") as! imageGalleryVC
                vcInstace.getarrImages = img
                    self.navigationController?.pushViewController(vcInstace, animated: true)
            }
        }
        
        self.imgProfile.layer.cornerRadius = (0.1 * screenwidth)  / 2
        self.imgProfile.layer.masksToBounds = true
        


        tblView.sectionHeaderHeight = UITableView.automaticDimension
        tblView.estimatedSectionHeaderHeight = 100
        
        tblView.rowHeight = UITableView.automaticDimension
        tblView.estimatedRowHeight = 100
        answerDetailWS()
    }
       override func viewWillDisappear(_ animated: Bool)
       {
        super.viewWillDisappear(animated)
        isDisplayEmoji = false
    }
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.imgProfile.layer.cornerRadius = self.imgProfile.frame.height / 2
            self.imgProfile.layer.masksToBounds = true
        }
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - other
    
    @objc func btnUpvoteClicked(_ sender: UIButton) {
        
        if  dictAnswer?.is_post_answer_upvote == false
        {
            if  dictAnswer?.is_post_answer_downvote == true{
                dictAnswer?.answer_downvote_comment_count -= 1
            }
            
            PostUpvoteDownvotekWS(status: "1")
            dictAnswer?.is_post_answer_upvote = true
            dictAnswer?.is_post_answer_downvote = false
            
            dictAnswer?.answer_upvote_comment_count += 1
            
            self.tblView.reloadData()
        }
    }
    //MARK:- Share
     @objc func tapOnShare(_ sender: Any)
    {
        if let getObj = dictAnswer
        {
            self.shareMaulik(shareText: getObj.answer.htmlToString
                , shareImage: nil,shreUrl:nil)
        }
    }
    
    @objc func btnDownvoteClicked(_ sender: UIButton) {
        
        if dictAnswer?.is_post_answer_downvote == false
        {
            if  dictAnswer?.is_post_answer_upvote == true{
                 dictAnswer?.answer_upvote_comment_count -= 1
            }
            PostUpvoteDownvotekWS(status: "2")
            dictAnswer?.is_post_answer_upvote = false
            dictAnswer?.is_post_answer_downvote = true
            dictAnswer?.answer_downvote_comment_count += 1
            self.tblView.reloadData()
        }
    }
    
    @objc func btnBookmarkClicked(_ sender: UIButton) {
        if dictAnswer?.is_post_answer_bookmark == true{
            sender.isSelected = false
            BookmarkUnbookmarkWS(isBookmark: "0")
            dictAnswer?.is_post_answer_bookmark = false
        }
        else
        {
            sender.isSelected = true
            BookmarkUnbookmarkWS(isBookmark: "1")
            dictAnswer?.is_post_answer_bookmark = true
        }
    }
    
    @objc func btnEmojiClicked(_ sender: UIButton) {
        isDisplayEmoji = true
        //        txtViewMsg.resignFirstResponder()
        header.txtComment.reloadInputViews()
        header.txtComment.becomeFirstResponder()
    }
    
    @objc func btnSendCommentClicked(_ sender: UIButton) {
        if validationComment{
            
            /*let comment = AnswerCommentListModel([:])
             let user = OtherUserProfileModel([:])
             user.firstname = loginData?.firstname ?? ""
             user.lastname = loginData?.lastname ?? ""
             user.user_id = loginData?.user_id ?? ""
             user.profile_image = loginData?.profile_image ?? ""
             user.profile_image_thumb = loginData?.profile_image_thumb ?? ""
             comment.comment = strComment
             comment.profession =  self.lblProfession.text ?? ""
             comment.answer_id = dictAnswer?.post_answers_id ?? ""
             comment.date = getCurrentDate(formate: Constant.date_ServerFormate)
             comment.user_id = globalUserId
             comment.user = user
             
             dictAnswer?.comments.insert(comment, at: 0)
             self.tblView.beginUpdates()
             self.tblView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
             self.tblView.endUpdates()
             
             UIView.animate(withDuration: 0.2, animations: {
             self.view.layoutIfNeeded()
             }, completion: { _ in
             })*/
            
            
            
            editCommentId.isEmpty == true ? addCommentWS() : editCommentWS()
            
        }
        
    }
    @IBAction func viewMore(_ sender: UIButton)
    {
        let getTitle = sender.titleLabel?.text
        
        if getTitle?.lowercased() == "view more"
        {
            sender.setTitle("View Less", for: .normal)
            header.lblanswer.numberOfLines = 0
        }
        else
        {
            sender.setTitle("View More", for: .normal)
            header.lblanswer.numberOfLines = 6
        }
        
        UIView.animate(withDuration: 0.3) {
              self.tblView.reloadData()
        }
      
        
    }
    var validationComment: Bool{
        
        self.view.endEditing(true)
        
        var mess = ""
        
        if ToString(strComment).isBlank {
            mess = "Please enter your comment"
        }
        
        if mess != ""{
            self.showOkAlert(msg: mess)
            return false
        }
        return true
    }
    
    //MARK: - Web service
    
    func addCommentWS(_ serviceCount : Int = 0) {
        WebService.shared.RequesURL(ServerURL.AddPostAnswerComment, Perameters: ["user_id" : globalUserId,"answer_id":dictAnswer?.post_answers_id ?? "","comment":strComment],showProgress: true,completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            if success{
                self.strComment = ""
                self.answerDetailWS()
            }
            
            
            
        }) { (err) in
            if serviceCount < 1 {
                self.addCommentWS(serviceCount + 1)
            } else {
                debugPrint(err)
            }
        }
    }
    
    func replyCommentWS(_ serviceCount : Int = 0, comment: String) {
        WebService.shared.RequesURL(ServerURL.AddPostAnswerComment, Perameters: ["user_id" : globalUserId,"answer_id":dictAnswer?.post_answers_id ?? "","comment":comment, "reply_id": self.replyId],showProgress: true,completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            if success{
                self.strComment = ""
                self.answerDetailWS()
            }
            
            
            
        }) { (err) in
            if serviceCount < 1 {
                self.addCommentWS(serviceCount + 1)
            } else {
                debugPrint(err)
            }
        }
    }
    
    
    func editCommentWS(_ serviceCount : Int = 0)
    {
        WebService.shared.RequesURL(ServerURL.EditPostAnswerComment, Perameters: ["user_id" : globalUserId,"answer_id":dictAnswer?.post_answers_id ?? "","comment":strComment,"comment_id":editCommentId],showProgress: true,completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            if success
            {
                self.editCommentId = ""
                self.strComment = ""
                self.answerDetailWS()
            }
            
            
            
        }) { (err) in
            if serviceCount < 1 {
                self.editCommentWS(serviceCount + 1)
            } else {
                debugPrint(err)
            }
        }
    }
    
    
    func BookmarkUnbookmarkWS(isBookmark : String,_ serviceCount : Int = 0) {
        WebService.shared.RequesURL(ServerURL.BookmarkUnbookmarkPostAnswer, Perameters: ["user_id" : globalUserId,"answer_id":dictAnswer?.post_answers_id ?? "","post_id":dictAnswer?.post_id ?? "","bookmark_unbookmark":isBookmark],showProgress: false,completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            
        }) { (err) in
            if serviceCount < 1 {
                self.BookmarkUnbookmarkWS(isBookmark: isBookmark, serviceCount + 1)
            } else {
                debugPrint(err)
            }
        }
    }
    
    func PostUpvoteDownvotekWS(status : String,_ serviceCount : Int = 0) {
        WebService.shared.RequesURL(ServerURL.PostUpvoteDownvote, Perameters: ["user_id" : globalUserId,"answer_id":dictAnswer?.post_answers_id ?? "","post_id":dictAnswer?.post_id ?? "","status":status],showProgress: false,completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            
        }) { (err) in
            if serviceCount < 1 {
                self.PostUpvoteDownvotekWS(status: status, serviceCount + 1)
            } else {
                debugPrint(err)
            }
        }
    }
    
    func AnswerReadWS(_ serviceCount : Int = 0) {
        WebService.shared.RequesURL(ServerURL.PostRead, Perameters: ["user_id" : globalUserId,"answer_id":dictAnswer?.post_answers_id ?? "","post_id":dictAnswer?.post_id ?? ""],showProgress: false,completion: { (dicRes, success) in
            
            
            debugPrint(dicRes)
            
        }) { (err) in
            if serviceCount < 1 {
                self.AnswerReadWS(serviceCount + 1)
            } else {
                debugPrint(err)
            }
        }
    }
    
    
    func answerDetailWS(_ serviceCount : Int = 0) {
        
        WebService.shared.RequesURL(ServerURL.GetAnswerDetails, Perameters: ["user_id" : globalUserId,"post_answers_id":ToString(post_answers_id) ],showProgress: false,completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            if success{
                
                if let dictTemp = dicRes["answer_details"] as? [String:Any] {
                    self.dictAnswer = AnswerModel.init(dictTemp)
                    self.AnswerReadWS()
                }
                
                self.lblUserName.text = ToString(self.dictAnswer?.user?.firstname) + " " + ToString(self.dictAnswer?.user?.lastname)
                self.imgProfile.setImageWithURL(self.dictAnswer?.user?.profile_image, "Big_dp_placeholder")
//                self.lblDate.text = self.dictAnswer?.date.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: "dd MMMM yyyy, hh:mm aa")
                self.lblDate.text = calculateTimeDifference(self.dictAnswer!.date)
                self.lblProfession.text = self.dictAnswer?.user?.profession
                
                self.tblView.reloadData()
                
            }
            
            
        }) { (err) in
            if serviceCount < 1 {
                self.answerDetailWS(serviceCount + 1)
            } else {
                debugPrint(err)
            }
        }
    }
}


extension AnswerDetailVC : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        strComment = textField.text ?? ""
        textField.text = ""
        isDisplayEmoji = false
        textField.reloadInputViews()
    }
    
}

class AnswerDetailHeaderCell: UITableViewCell {
    
    @IBOutlet var collectionGallary: UICollectionView!
    
    
    @IBOutlet weak var viewGallary_H_Const: NSLayoutConstraint!
    
    
    @IBOutlet var lblYouTxt: UILabel!
    @IBOutlet var imgYou: UIImageView!
    
    
    @IBOutlet var txtFieldView: UIView!
    @IBOutlet var lblanswer: UILabel!
    @IBOutlet var txtComment: UITextField!
    @IBOutlet var btnEmoji: UIButton!
    @IBOutlet var btnSendComment: UIButton!
    @IBOutlet var lblCommentcount: UILabel!
    @IBOutlet var lblViewCount: UILabel!
    @IBOutlet var lblUpvotecount: UILabel!
    @IBOutlet var lblDownvoteCount: UILabel!
    @IBOutlet var btnUpvote: UIButton!
    @IBOutlet var btndownvote: UIButton!
    @IBOutlet var btnShared: UIButton!
    @IBOutlet var btnViewMore: UIButton!
    
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var lblLikeCount: UILabel!
    
    //For Preview Show
    @IBOutlet weak var viewPreview: UIView!
    private let slp = SwiftLinkPreview(cache: InMemoryCache())
    @IBOutlet private weak var previewTitle: UILabel?
    @IBOutlet private weak var previewCanonicalUrl: UILabel?
    @IBOutlet private weak var previewDescription: UILabel?
    @IBOutlet private weak var detailedView: UIView?
    @IBOutlet private weak var favicon: UIImageView?
    @IBOutlet private weak var indiCator: UIActivityIndicatorView?
    
    @IBOutlet weak var viewPreviewHeightConst: NSLayoutConstraint!
    
    var collectionClickBlock : (() -> ())?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewGallary_H_Const.constant = 0
        self.updateConstraintsIfNeeded()
        self.layoutIfNeeded()
        
        collectionGallary.delegate = self
        collectionGallary.dataSource = self
        self.collectionGallary.register(UINib(nibName: "AdvertPhotoCLVCell", bundle: nil), forCellWithReuseIdentifier: "AdvertPhotoCLVCell")
        
        
        DispatchQueue.main.async {
            self.txtFieldView.layer.cornerRadius = (self.txtFieldView.bounds.height / 2)
            self.txtFieldView.layer.borderColor = UIColor.lightGray.cgColor
            self.txtFieldView.layer.borderWidth = 1
            
            self.imgYou.layer.cornerRadius =  (0.053125 * screenwidth)/2
            self.imgYou.layer.masksToBounds = true
        }
        self.viewPreview.applyDropShadow()
        
    }
    
    var model : AnswerModel? {
        didSet {
//            lblanswer.attributedText = model?.answer.htmlToAttributedString
            lblanswer.attributedText  = model?.answer.htmlAttributed(family: "Lato-Regular", size: 11 * screenscale, color: .Appcolor51)
            
            lblCommentcount.text = model?.answer_comment_count
            lblViewCount.text = model?.post_answer_read_count
            
            lblLikeCount.text = toString(model?.post_answer_like_count)
            btnLike.isSelected = model?.is_post_answer_liked ?? false
            
            lblUpvotecount.text = toString(model?.answer_upvote_comment_count)
            lblDownvoteCount.text = toString(model?.answer_downvote_comment_count)
            btnUpvote.isSelected = model?.is_post_answer_upvote ?? false ? true : false
            btndownvote.isSelected = model?.is_post_answer_downvote ?? false ? true : false
            
            let strUpvote = ToInt(lblUpvotecount.text) > 1 ? "Upvotes" : "Upvote"
            btnUpvote.setTitle(strUpvote, for: .normal)
            btnUpvote.setTitle(strUpvote, for: .selected)
            
            let strDownvote = ToInt(lblDownvoteCount.text) > 1 ? "Downvotes" : "Downvote"
            btndownvote.setTitle(strDownvote, for: .normal)
            btndownvote.setTitle(strDownvote, for: .selected)
            
            lblYouTxt.text = ""
            self.imgYou.isHidden = true
            if model?.is_post_answer_upvote ?? false{
                lblYouTxt.text = "You upvoted this"
                self.imgYou.isHidden = false
            }
            else if model?.is_post_answer_downvote ?? false{
                lblYouTxt.text = "You downvoted this"
                self.imgYou.isHidden = false
            }
            
            self.imgYou.setImageWithURL(loginData?.profile_image ?? "", "Big_dp_placeholder")
            
            if let previewUrl = model?.dicPostLink?.link, checkValidString(previewUrl)
            {
                viewPreviewHeightConst.constant = 100
                self.showPreview(toString(previewUrl))
            }
            else
            {
                viewPreviewHeightConst.constant = 0
                self.detailedView?.isHidden = true
            }
            
            btnViewMore.isHidden = lblanswer.maxNumberOfLines > 6 ? false : true
            
            
            self.updateConstraintsIfNeeded()
            self.layoutIfNeeded()
            
        }
    }
    func setUpCollection() {
        
        let countImages = ToInt(model?.arrImages.count)
        
        self.collectionGallary.reloadData()
        
        viewGallary_H_Const.constant = countImages == 0 ? 0 :  100 * screenscale
        
        /*if  countImages > 0
         {
         viewGallary_H_Const.constant = (100 * screenscale) + 100
         }*/
        
        self.updateConstraintsIfNeeded()
        self.layoutIfNeeded()
    }
    
}
    

class AnswerDetailCommentCell: UITableViewCell {
    
    @IBOutlet weak var commentView: UIView!
    
    @IBOutlet weak var commentWrapper: UIView!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
   // @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblProfession: UILabel!
    @IBOutlet weak var txtviewComment: UITextView?

    @IBOutlet weak var lblDownvoteCount: UILabel!
   
    @IBOutlet weak var btnDot: UIButton!
    
    @IBOutlet weak var btnLike: UIButton!
    
    @IBOutlet weak var lblLikeCount: UILabel!
    
    @IBOutlet weak var txtReply: UITextField!
    
    @IBOutlet weak var quotedCommentTxt: UITextView!
    
    @IBOutlet weak var quotedCommentProfession: UILabel!
    @IBOutlet weak var quotedCommentUsername: UILabel!
    @IBOutlet weak var quotedCommentView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txtviewComment?.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        txtviewComment?.textContainer.lineFragmentPadding = 0

        DispatchQueue.main.async {
            
            self.imgProfile.layer.cornerRadius = self.imgProfile.frame.height / 2
            self.imgProfile.layer.masksToBounds = true
        }
    }
    
    var model : AnswerCommentListModel? {
        didSet {
            
            self.lblUserName.text = ToString(self.model?.user?.firstname) + " " + ToString(self.model?.user?.lastname)
        self.imgProfile.setImageWithURL(self.model?.user?.profile_image, "Big_dp_placeholder")
//            self.lblComment.text = model?.comment
            self.txtviewComment?.text = model?.comment
            self.lblLikeCount.text = toString(model?.comment_like_count)
            
            if(self.model?.user?.profession == nil || self.model?.user?.profession == ""){
                self.lblProfession.text = "..."
            }
            else {
               self.lblProfession.text = self.model?.user?.profession
            }
            
            
            let imgName = model!.is_comment_liked ? "heart_active" : "heart_deactive"
            let btnImage = UIImage(named: "\(imgName).png")!
            self.btnLike.setImage(btnImage, for: .normal)
            
            //populate comment quote
            if( model?.reply?.comment != nil &&  model?.reply?.comment != ""){
                self.quotedCommentTxt?.text = model?.reply?.comment
                            
                self.quotedCommentProfession?.text =  model?.reply?.user?.profession
                self.quotedCommentUsername.text = ToString(self.model?.reply?.user?.firstname) + " " + ToString(self.model?.reply?.user?.lastname)
    //           quotedCommentView: UIView!
            }
            else { quotedCommentView.heightAnchor.constraint(equalToConstant: CGFloat(0)).isActive = true
            }
           
        }
    }
    
    @IBAction func clickProfile(_ sender: UIButton)
    {
       
        guard globalUserId != "" else {
            appDelegate.showAlertGuest()
            return
        }
        
        if let getObje = model
        {
            if getObje.user_id == globalUserId
            {
                let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                self.tableView?.parentViewController?.pushTo(vcInstace)
            }
            else{
                let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "OtherProfileVC") as! OtherProfileVC
                vcInstace.profileId = getObje.user_id
                self.tableView?.parentViewController?.pushTo(vcInstace)
                
            }
        }
    }
    
    
    @IBAction func btnReplyComment(_ sender: UIButton) {
        let answerDetailVC = self.tableView?.parentViewController as? AnswerDetailVC
        
        if let getObj = self.model
        {
            answerDetailVC?.replyId = getObj.comment_id
        }
        
            
       DispatchQueue.main.async {
           guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "ReplyCommentVC") as? ReplyCommentBottomPopup else { return }
            
           popupVC.callback =  { comment in
        
            answerDetailVC?.replyCommentWS( comment: comment);

           }
           self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
       }
    }
    
    
    @IBAction func btnCommentLike(_ sender: UIButton) {
        if  (self.model?.is_comment_liked == false)
                {
                    self.model?.comment_like_count += 1
                     self.model?.is_comment_liked = true
                    lblLikeCount.text = toString(self.model?.comment_like_count)
        
                    CommentLikeUnlikeKWS(status: "1")
                }
        else {
            self.model?.comment_like_count -= 1
             self.model?.is_comment_liked = false
            lblLikeCount.text = toString(self.model?.comment_like_count)

            CommentLikeUnlikeKWS(status: "0")
        }
        
        let imgName = model!.is_comment_liked ? "heart_active" : "heart_deactive"
        let btnImage = UIImage(named: "\(imgName).png")!
        self.btnLike.setImage(btnImage, for: .normal)
    }
    
    
    @IBAction func btnCommentShare(_ sender: UIButton) {
        if let getObj = self.model
        {
            self.parentViewController?.shareMaulik(shareText: getObj.comment.htmlToString
                , shareImage: nil,shreUrl:nil)
        }
    }
    
    //MARK: Click Dot
    @IBAction func clickDot(_ sender: Any) {
        if let indexPath = self.tableView?.indexPathForView(self){
            if let AnswerDetailVC = self.tableView?.parentViewController as? AnswerDetailVC {
                
                DispatchQueue.main.async {
                    
                    guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
                    popupVC.arrTitle = [BottomName.Edit,BottomName.Delete]
                    popupVC.callback =  { str in
                        debugPrint(str)
                        if str == BottomName.Edit
                        {
                            
                            AnswerDetailVC.editCommentId = toString(AnswerDetailVC.dictAnswer?.comments[indexPath.row].comment_id)
                            
                            AnswerDetailVC.header.txtComment.text = AnswerDetailVC.dictAnswer?.comments[indexPath.row].comment ?? ""
                            AnswerDetailVC.header.txtComment.becomeFirstResponder()
                            
                            
                        }
                        else if str == BottomName.Delete
                        {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                AnswerDetailVC.showOkCancelAlertWithAction(msg: Constant.kAlertComment, handler: { (bool) in
                                    if bool
                                    {
                                        AnswerDetailVC.webcallDeleteComment(AnswerDetailVC.dictAnswer?.comments[indexPath.row].comment_id ?? "",callback: {
                                            AnswerDetailVC.dictAnswer?.comments.remove(at: indexPath.row)
                                            AnswerDetailVC.tblView?.deleteRows(at: [indexPath], with: .left)
                                        })
                                    }
                                })
                            })
                        }
                    }
                    self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
                }
            }
        }
    }
    
    func CommentLikeUnlikeKWS(status : String,_ serviceCount : Int = 0) {
       let answerDetailVC = self.tableView?.parentViewController as? AnswerDetailVC
        
        WebService.shared.RequesURL(ServerURL.CommentLikeUnlike, Perameters: ["user_id" : globalUserId, "comment_id":self.model?.comment_id ?? "","like_unlike":status],showProgress: false,completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            
        }) { (err) in
            if serviceCount < 1 {
                self.CommentLikeUnlikeKWS(status: status, serviceCount + 1)
            } else {
                debugPrint(err)
            }
        }
    }
    
}


class AnswerDetailReplyCell: UITableViewCell {
    
    @IBOutlet weak var commentView: UIView!
    
    @IBOutlet weak var commentWrapper: UIView!
    
    @IBOutlet weak var quoteView: UIView!
    @IBOutlet weak var actionsView: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
   // @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblProfession: UILabel!
    @IBOutlet weak var txtviewComment: UITextView?

    @IBOutlet weak var lblDownvoteCount: UILabel!
   
    @IBOutlet weak var btnDot: UIButton!
    
//    @IBOutlet weak var btnLike: UIButton!
    
    @IBOutlet weak var lblLikeCount: UILabel!
    
    @IBOutlet weak var txtReply: UITextField!
    
    @IBOutlet weak var quotedCommentTxt: UITextView!
    
    @IBOutlet weak var quotedCommentProfession: UILabel!
    @IBOutlet weak var quotedCommentUsername: UILabel!
    @IBOutlet weak var quotedCommentView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txtviewComment?.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        txtviewComment?.textContainer.lineFragmentPadding = 0

        DispatchQueue.main.async {
            
            self.imgProfile.layer.cornerRadius = self.imgProfile.frame.height / 2
            self.imgProfile.layer.masksToBounds = true
        }
    }
    
    var model : AnswerCommentListModel? {
        didSet {
            
            self.lblUserName.text = ToString(self.model?.user?.firstname) + " " + ToString(self.model?.user?.lastname)
        self.imgProfile.setImageWithURL(self.model?.user?.profile_image, "Big_dp_placeholder")
            self.txtviewComment?.text = model?.comment
            //self.lblLikeCount.text = toString(model?.comment_like_count)
            
            if(self.model?.user?.profession == nil || self.model?.user?.profession == ""){
                self.lblProfession.text = "..."
            }
            else {
               self.lblProfession.text = self.model?.user?.profession
            }
            
            
            quoteView.borders(for: [.left], width: 2, color: .gray)
            commentView.borders(for: [.left], width: 2, color: .gray)
            actionsView.borders(for: [.left], width: 2, color: .gray)
            
            
            
            //populate comment quote
            if( model?.reply?.comment != nil &&  model?.reply?.comment != ""){
                self.quotedCommentTxt?.text = model?.reply?.comment
                            
                self.quotedCommentProfession?.text =  model?.reply?.user?.profession
                self.quotedCommentUsername.text = ToString(self.model?.reply?.user?.firstname) + " " + ToString(self.model?.reply?.user?.lastname)
    //           quotedCommentView: UIView!
            }
            else { quotedCommentView.heightAnchor.constraint(equalToConstant: CGFloat(0)).isActive = true
            }
           
        }
    }
    
    @IBAction func clickProfile(_ sender: UIButton)
    {
       
        guard globalUserId != "" else {
            appDelegate.showAlertGuest()
            return
        }
        
        if let getObje = model
        {
            if getObje.user_id == globalUserId
            {
                let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                self.tableView?.parentViewController?.pushTo(vcInstace)
            }
            else{
                let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "OtherProfileVC") as! OtherProfileVC
                vcInstace.profileId = getObje.user_id
                self.tableView?.parentViewController?.pushTo(vcInstace)
                
            }
        }
    }
    
    
    @IBAction func btnReplyComment(_ sender: UIButton) {
        
    }
    
    
    @IBAction func btnCommentLike(_ sender: UIButton) {
        if  (self.model?.is_comment_liked == false)
                {
                    self.model?.comment_like_count += 1
                     self.model?.is_comment_liked = true
                    lblLikeCount.text = toString(self.model?.comment_like_count)
        
                    CommentLikeUnlikeKWS(status: "1")
                }
        else {
            self.model?.comment_like_count -= 1
             self.model?.is_comment_liked = false
            lblLikeCount.text = toString(self.model?.comment_like_count)

            CommentLikeUnlikeKWS(status: "0")
        }
        
//        let imgName = model!.is_comment_liked ? "heart_active" : "heart_deactive"
//        let btnImage = UIImage(named: "\(imgName).png")!
//        self.btnLike.setImage(btnImage, for: .normal)
    }
    
    
    @IBAction func btnCommentShare(_ sender: UIButton) {
        if let getObj = self.model
        {
            self.parentViewController?.shareMaulik(shareText: getObj.comment.htmlToString
                , shareImage: nil,shreUrl:nil)
        }
    }
    
    //MARK: Click Dot
    @IBAction func clickDot(_ sender: Any) {
        if let indexPath = self.tableView?.indexPathForView(self){
            if let AnswerDetailVC = self.tableView?.parentViewController as? AnswerDetailVC {
                
                DispatchQueue.main.async {
                    
                    guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
                    popupVC.arrTitle = [BottomName.Edit,BottomName.Delete]
                    popupVC.callback =  { str in
                        debugPrint(str)
                        if str == BottomName.Edit
                        {
                            
                            AnswerDetailVC.editCommentId = toString(AnswerDetailVC.dictAnswer?.comments[indexPath.row].comment_id)
                            
                            AnswerDetailVC.header.txtComment.text = AnswerDetailVC.dictAnswer?.comments[indexPath.row].comment ?? ""
                            AnswerDetailVC.header.txtComment.becomeFirstResponder()
                            
                            
                        }
                        else if str == BottomName.Delete
                        {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                AnswerDetailVC.showOkCancelAlertWithAction(msg: Constant.kAlertComment, handler: { (bool) in
                                    if bool
                                    {
                                        AnswerDetailVC.webcallDeleteComment(AnswerDetailVC.dictAnswer?.comments[indexPath.row].comment_id ?? "",callback: {
                                            AnswerDetailVC.dictAnswer?.comments.remove(at: indexPath.row)
                                            AnswerDetailVC.tblView?.deleteRows(at: [indexPath], with: .left)
                                        })
                                    }
                                })
                            })
                        }
                    }
                    self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
                }
            }
        }
    }
    
    func CommentLikeUnlikeKWS(status : String,_ serviceCount : Int = 0) {
       let answerDetailVC = self.tableView?.parentViewController as? AnswerDetailVC
        
        WebService.shared.RequesURL(ServerURL.CommentLikeUnlike, Perameters: ["user_id" : globalUserId, "comment_id":self.model?.comment_id ?? "","like_unlike":status],showProgress: false,completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            
        }) { (err) in
            if serviceCount < 1 {
                self.CommentLikeUnlikeKWS(status: status, serviceCount + 1)
            } else {
                debugPrint(err)
            }
        }
    }
    
}

extension AnswerDetailVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let header = tableView.dequeueReusableCell(withIdentifier: "AnswerDetailHeaderCell") as! AnswerDetailHeaderCell
        header.model = dictAnswer
        header.txtComment.text = strComment
//        header.btnLike.addTarget(self, action: #selector(clickLikeUnlike(_:)), for: .touchUpInside)
        
        header.btnShared.addTarget(self, action: #selector(tapOnShare(_:)), for: .touchUpInside)

        
        header.btnUpvote.addTarget(self, action: #selector(btnUpvoteClicked(_:)), for: .touchUpInside)
        header.btndownvote.addTarget(self, action: #selector(btnDownvoteClicked(_:)), for: .touchUpInside)
//        header.btnbookmark.addTarget(self, action: #selector(btnBookmarkClicked(_:)), for: .touchUpInside)
        
        header.btnEmoji.addTarget(self, action: #selector(btnEmojiClicked(_:)), for: .touchUpInside)
        header.btnSendComment.addTarget(self, action: #selector(btnSendCommentClicked(_:)), for: .touchUpInside)
        
        header.btnViewMore.addTarget(self, action: #selector(viewMore(_:)), for: .touchUpInside)

        
        
        
        
        
//        header.collectionGallary.delegate = header
//        header.collectionGallary.dataSource = header
        
//        header.collectionGallary.reloadData()
        
        header.setUpCollection()

        
        return header.contentView
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dictAnswer?.comments.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerDetailCommentCell") as! AnswerDetailCommentCell
        cell.model = dictAnswer?.comments[indexPath.row]
        
        if(cell.model?.comment_level == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerDetailReplyCell") as! AnswerDetailReplyCell
            
            cell.model = dictAnswer?.comments[indexPath.row]
            cell.btnDot.isHidden =  globalUserId != self.dictAnswer?.comments[indexPath.row].user_id
            
            return cell
                   
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerDetailCommentCell") as! AnswerDetailCommentCell
            
            cell.model = dictAnswer?.comments[indexPath.row]
            cell.btnDot.isHidden =  globalUserId != self.dictAnswer?.comments[indexPath.row].user_id
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
   /* func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
            return true
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
    debugPrint("MYid:-\(globalUserId)","Match:-\(self.dictAnswer?.comments[indexPath.row].user_id)")

        
        if globalUserId != self.dictAnswer?.comments[indexPath.row].user_id {return [UITableViewRowAction]()}

        let delete = UITableViewRowAction.init(style: UITableViewRowAction.Style.destructive, title: "Delete") { (_, getInd) in
            
            self.showOkCancelAlertWithAction(msg: Constant.kAlertComment, handler: { (bool) in
                if bool
                {
                    self.webcallDeleteComment(self.dictAnswer?.comments[getInd.row].comment_id ?? "",callback: {
                        self.dictAnswer?.comments.remove(at: getInd.row)
                        self.tblView?.deleteRows(at: [indexPath], with: .left)
                    })
                }
            })
            
            
        }
        delete.backgroundColor = .AppcolorRed
        
        
        
        return [delete]
    }*/
    
    func webcallDeleteComment(_ Delete_comment_id:String ,callback: (()->())?){
        WebService.shared.RequesURL(ServerURL.delete_post_answer_comment, Perameters: ["user_id":globalUserId,"comment_id":Delete_comment_id],showProgress: true, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true
            {
                callback?()
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
}


class AnswerModel {
    
    var admin_id =  ""
    var answer =  ""
    var answer_comment_count =  ""
    var answer_downvote_comment_count =  0
    var answer_upvote_comment_count =  0
    var post_like_count =  0
    var post_answer_like_count =  0


    var comments = [AnswerCommentListModel]()
    var date =  ""
    var expert_id = ""
    var post_answer_read_count =  ""
    var post_answers_id = ""
    var post_id = ""
    var status =  ""
    var user : OtherUserProfileModel?
    var user_id = ""
    var vv_type = ""
    var is_post_answer_bookmark =  false
    var is_post_answer_downvote =  false
    var is_post_answer_upvote =  false
    var is_post_liked =  false
    var is_post_answer_liked =  false
    var show_ans =  false
    


    var arrImages = [imagePostModel]()

    var dicPostLink : PostLinkModel?
    var dicAudio : audioPostModel?
    var dicVideo : videoPostModel?
    

    
    init(_ dict : [String:Any]) {
        self.admin_id = toString(dict["admin_id"])
        self.answer = toString(dict["answer"])
        self.answer_comment_count = toString(dict["answer_comment_count"])
        self.answer_downvote_comment_count = ToInt(dict["answer_downvote_comment_count"])
        self.answer_upvote_comment_count = ToInt(dict["answer_upvote_comment_count"])
        self.post_like_count = ToInt(dict["post_like_count"])

        self.date = toString(dict["date"])
        self.expert_id = toString(dict["expert_id"])
        self.is_post_answer_bookmark = ConvertToBool(toString(dict["is_post_answer_bookmark"]))
        self.is_post_answer_downvote = ConvertToBool(toString(dict["is_post_answer_downvote"]))
        self.is_post_answer_upvote = ConvertToBool(toString(dict["is_post_answer_upvote"]))
        self.is_post_liked = ConvertToBool(toString(dict["is_post_liked"]))
        self.is_post_answer_liked = ConvertToBool(toString(dict["is_post_answer_liked"]))

        self.post_answer_like_count = ToInt(dict["post_answer_like_count"])


        self.post_answer_read_count =
            toString(dict["post_answer_read_count"])
        self.post_answers_id = toString(dict["post_answers_id"])
        self.post_id = toString(dict["post_id"])
        self.user_id = toString(dict["user_id"])
        self.status = toString(dict["status"])
        self.vv_type = toString(dict["vv_type"])
        self.show_ans = ConvertToBool(toString(dict["show_ans"]))
        
        //For User
        if let dic = dict["user"] as? [String:Any] {
            self.user = OtherUserProfileModel.init(dic)
        }
        
        //arr
        //comments
        if let arr = dict["comments"] as? [[String:Any]] {
            var temp_comments = [AnswerCommentListModel]()
            arr.forEach { (item) in
                temp_comments.append(AnswerCommentListModel.init(item))
                
                if let new_arr = item["reply_list"] as? [[String:Any]] {
                    temp_comments.append(contentsOf: new_arr.map({AnswerCommentListModel.init($0)}))
                }
            }
            //self.comments = arr.map({AnswerCommentListModel.init($0)})
            self.comments = temp_comments
        }
        
        //For Interested Category
        if let arr = dict["images"] as? [[String:Any]] {
            self.arrImages = arr.map({imagePostModel.init($0)})
        }
        
        //This handle the image list for comment list
        if let arr = dict["answer_img"] as? [[String:Any]] {
            self.arrImages = arr.map({imagePostModel.init($0)})
        }
        
        //For POstLink
        if let dic = dict["link"] as? [String:Any] {
            self.dicPostLink = PostLinkModel.init(dic)
        }
        
        //For audio
        if let dic = dict["audios"] as? [String:Any] {
            self.dicAudio = audioPostModel.init(dic)
        }
        
        //For video
        if let dic = dict["videos"] as? [String:Any] {
            self.dicVideo = videoPostModel.init(dic)
        }
    }
}

class AnswerCommentListModel {
    var comment_like_count =  0
    var is_comment_liked =  false
    var comment_level =  1
    
    var answer_id =  ""
    var profession =  ""
    var comment =  ""
    var comment_id =  ""
    var date =  ""
    var parent_id =  ""
    var reply_list =  ""
    var reply_id =  ""
    var user : OtherUserProfileModel?
    var user_id = ""
    var reply: AnswerCommentListModel?
    
    init(_ dict : [String:Any]) {
        self.comment_like_count = ToInt(dict["comment_like_count"])
        self.date = toString(dict["date"])
        self.user_id = toString(dict["user_id"])
        self.answer_id = toString(dict["answer_id"])
        self.comment = toString(dict["comment"])
        self.profession = toString(dict["profession"])
        self.comment_id = toString(dict["comment_id"])
        self.parent_id = toString(dict["parent_id"])
        self.reply_list = toString(dict["reply_list"])
        self.reply_id = toString(dict["reply_id"])
        self.is_comment_liked = ConvertToBool(toString(dict["is_comment_liked"]))
        
        //For User
        if let dic = dict["user"] as? [String:Any] {
            self.user = OtherUserProfileModel.init(dic)
        }
        
        if let dic = dict["reply_list"] as? [String:Any] {
            self.reply = AnswerCommentListModel.init(dic)
        }
        
        if (dict["is_comment_liked"] == nil) {
            self.comment_level = 2
        }
        else {
            self.comment_level = 1
        }
    }
}


extension AnswerDetailHeaderCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model?.arrImages.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "AdvertPhotoCLVCell", for: indexPath) as! AdvertPhotoCLVCell
        cell.imgGallary.setImageWithURL(model?.arrImages[indexPath.row].image_name, "place_logo")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: ToInt(model?.arrImages.count) == 1 ? collectionView.frame.width : collectionView.frame.height , height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.collectionClickBlock?()
        
       /* if let get = model?.arrImages, get.count > 0
        {
            let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "imageGalleryVC") as! imageGalleryVC
            vcInstace.getarrImages = get
            self.tableView?.parentViewController?.navigationController?.pushViewController(vcInstace, animated: true)
        }*/
        
    }
}
//MARK:- Preview Show

extension AnswerDetailHeaderCell
{
    func showPreview(_ linkURL:String) {
        
        self.detailedView?.isHidden = true
        indiCator?.startAnimating()
        
        func setDataPreview(_ result: Response) {
            print("url: ", result.url ?? "no url")
            print("finalUrl: ", result.finalUrl ?? "no finalUrl")
            print("canonicalUrl: ", result.canonicalUrl ?? "no canonicalUrl")
            print("title: ", result.title ?? "no title")
            print("images: ", result.images ?? "no images")
            print("image: ", result.image ?? "no image")
            print("video: ", result.video ?? "no video")
            print("icon: ", result.icon ?? "no icon")
            print("description: ", result.description ?? "no description")
            
            self.previewTitle?.text = toString(result.title)
            self.previewCanonicalUrl?.text = toString(result.finalUrl)
            self.previewDescription?.text = toString(result.description)
            
            if let icon = result.image
            {
                self.favicon?.setImageWithURL(toString(icon), "logo_small")
            }
            else
            {
                self.favicon?.setImageWithURL(toString(result.icon), "logo_small")
            }
            self.detailedView?.isHidden = false
            self.indiCator?.stopAnimating()
        }
        
        
        self.slp.preview(
            linkURL,
            onSuccess: { result in
                setDataPreview(result)
                //                self.result = result
        },
            onError: { error in
                print(error)
                //                self.showOkAlert(msg: "Invalid URL!")
                self.indiCator?.stopAnimating()
        }
        )
    }
    @IBAction func openWithAction(_ sender: UIButton) {
        if let previewUrl = model?.dicPostLink?.link, checkValidString(previewUrl)
        {
            openURL(URL.init(string: previewUrl)!)
        }
    }
    func openURL(_ url: URL) {
        if ["http", "https"].contains(url.scheme?.lowercased() ?? "") {
            self.tableView?.parentViewController?.present(SFSafariViewController(url: url), animated: true, completion: nil)
        } else {
            linkurlOpen_MS(linkurl: url.absoluteString)
        }
    }
    
}

//MARK:- Like UnLike
extension AnswerDetailVC {
    
    
    @IBAction func openWithAction(_ sender: UIButton) {
          
        if let strLink = dictAnswer?.dicPostLink?.link
        {
           if strLink.hasPrefix("https://") || strLink.hasPrefix("http://"){
                openURL(URL.init(string: strLink)!)
            }else {
                let correctedURL = "http://\(strLink)"
                openURL(URL.init(string: correctedURL)!)
            }
        }

      }
      func openURL(_ url: URL) {
          if ["http", "https"].contains(url.scheme?.lowercased() ?? "") {
              self.present(SFSafariViewController(url: url), animated: true, completion: nil)
          } else {
              linkurlOpen_MS(linkurl: url.absoluteString)
          }
      }
    
    @IBAction func clickLikeUnlike(_ sender: UIButton) {
        if let getObj = dictAnswer
        {
            callLikeUnlikePost(getObj.post_answers_id,getObj.post_id, sender.isSelected == true ? "0" : "1")
            sender.isSelected = !sender.isSelected
            if sender.isSelected{
                getObj.post_answer_like_count += 1
            }
            else{
                getObj.post_answer_like_count -= 1
            }
            getObj.is_post_answer_liked = !getObj.is_post_answer_liked
            header.lblLikeCount.text = toString(getObj.post_answer_like_count)
        }
    }
    
    func callLikeUnlikePost(_ post_AnswerId:String,_ post_id:String, _ like_unlike:String ) {
        WebService.shared.RequesURL(ServerURL.LikeUnlikeAnswer, Perameters: ["user_id" : globalUserId,"post_id":post_id,"like_unlike":like_unlike,"post_answers_id":post_AnswerId],showProgress: false,completion: { (dicRes, success) in
            if success == true{
                debugPrint(dicRes)
            }
        }) { (err) in
        }
    }
    
}

class EmojiTextField: UITextField {
    
    // required for iOS 13
    override var textInputContextIdentifier: String? { "" } // return non-nil to show the Emoji keyboard ¯\_(ツ)_/¯
    
    override var textInputMode: UITextInputMode? {
        if isDisplayEmoji{
            debugPrint("emji@@@@@@@@@@@@@@@@@@@@@@@@")
            for mode in UITextInputMode.activeInputModes {
                if mode.primaryLanguage == "emoji" {
                    return mode
                }
            }
        }
        else
        {
                for mode in UITextInputMode.activeInputModes {
                if mode.primaryLanguage == "en-US" {
                    return mode
                }
            }
        }
        debugPrint("no!!!!!!!!!!!!!!!!!!!!!")
        return nil
    }
}

extension UIView {
    func borders(for edges:[UIRectEdge], width:CGFloat = 1, color: UIColor = .black) {

        if edges.contains(.all) {
            layer.borderWidth = width
            layer.borderColor = color.cgColor
        } else {
            let allSpecificBorders:[UIRectEdge] = [.top, .bottom, .left, .right]

            for edge in allSpecificBorders {
                if let v = viewWithTag(Int(edge.rawValue)) {
                    v.removeFromSuperview()
                }

                if edges.contains(edge) {
                    let v = UIView()
                    v.tag = Int(edge.rawValue)
                    v.backgroundColor = color
                    v.translatesAutoresizingMaskIntoConstraints = false
                    addSubview(v)

                    var horizontalVisualFormat = "H:"
                    var verticalVisualFormat = "V:"

                    switch edge {
                    case UIRectEdge.bottom:
                        horizontalVisualFormat += "|-(0)-[v]-(0)-|"
                        verticalVisualFormat += "[v(\(width))]-(0)-|"
                    case UIRectEdge.top:
                        horizontalVisualFormat += "|-(0)-[v]-(0)-|"
                        verticalVisualFormat += "|-(0)-[v(\(width))]"
                    case UIRectEdge.left:
                        horizontalVisualFormat += "|-(0)-[v(\(width))]"
                        verticalVisualFormat += "|-(0)-[v]-(0)-|"
                    case UIRectEdge.right:
                        horizontalVisualFormat += "[v(\(width))]-(0)-|"
                        verticalVisualFormat += "|-(0)-[v]-(0)-|"
                    default:
                        break
                    }

                    self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: horizontalVisualFormat, options: .directionLeadingToTrailing, metrics: nil, views: ["v": v]))
                    self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: verticalVisualFormat, options: .directionLeadingToTrailing, metrics: nil, views: ["v": v]))
                }
            }
        }
    }
}
