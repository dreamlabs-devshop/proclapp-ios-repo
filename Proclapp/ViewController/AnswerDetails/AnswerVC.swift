
//
//  AnswerVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/22/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SafariServices
import SwiftLinkPreview

class AnswerVC: UIViewController {
    
    //MARK: - outlet
    @IBOutlet weak var lblHeader: UILabel!

    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var btnsBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblProfession: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var colview:UICollectionView!
    @IBOutlet weak var colviewConst:NSLayoutConstraint!
    let imagePicker = UIImagePickerController()
    var imageArray = NSMutableArray()
    var textField: UITextField?
    var deleteImageIds = NSMutableArray()

    //MARK: - properties
    let defultTextDes = "Write your answer"
    var dictPost : allPostModel?
    
    var dictAnswer : AnswerListModel?
    
    var strPostLink = ""
    private let slp = SwiftLinkPreview(cache: InMemoryCache())
    @IBOutlet private weak var previewTitle: UILabel?
    @IBOutlet private weak var previewCanonicalUrl: UILabel?
    @IBOutlet private weak var previewDescription: UILabel?
    @IBOutlet private weak var detailedView: UIView?
    @IBOutlet private weak var favicon: UIImageView?
    @IBOutlet private weak var indiCator: UIActivityIndicatorView?
    

    @IBOutlet weak var viewPhotoLink: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        detailedView?.isHidden = true
        indiCator?.stopAnimating()
        detailedView?.applyDropShadow()

        viewPhotoLink.isHidden = dictPost?.postType != PostTypeEnum.Question
        
        //setup textview
        
       /* NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange), name: UIWindow.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIWindow.keyboardWillHideNotification, object: nil)*/
        
        //setup post detaildp_profile
        lblUserName.text = dictPost?.author
        imgProfile.setImageWithURL(dictPost?.profile_image, "Big_dp_placeholder")
//        lblDate.text = dictPost?.post_date.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_ddMMM)
       lblDate.text = calculateTimeDifference(dictPost!.post_date)
        lblTitle.text = dictPost?.post_title
        lblProfession.text = dictPost?.professional_qualification
        
        self.imgProfile.layer.cornerRadius = (0.1 * screenwidth)  / 2
        self.imgProfile.layer.masksToBounds = true
        
        lblHeader.text = dictAnswer == nil ? "Add Answer" : "Edit Answer"

        txtView.text = dictAnswer == nil ? defultTextDes : dictAnswer?.answer

        txtView.textColor = dictAnswer == nil ? UIColor.lightGray : UIColor.Appcolor51

        imagePicker.delegate = self
        colviewConst.constant = 100
        
        if dictAnswer != nil//for edit
        {
            if dictAnswer!.arrImages.count > 0
            {
                imageArray = NSMutableArray.init(array: dictAnswer!.arrImages)
            }
            colview.reloadData()
            
            if let previewUrl = dictAnswer?.dicPostLink?.link, checkValidString(previewUrl)
            {
                self.showPreview(previewUrl)
            }
        }

        //FOR SAVE BTN
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange), name: .UIResponder.keyboardDidShowNotification, object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: ., object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    //MARK: - Keyboard Methods
    
    /*@objc func keyboardWillChange(notification:NSNotification)
    {
        let userinfo:NSDictionary = (notification.userInfo as NSDictionary?)!
        if let keybordsize = (userinfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.btnsBottomConstraint.constant = keybordsize.height - BottomPadding
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification:NSNotification)
    {
        self.btnsBottomConstraint.constant = 0
        self.view.layoutIfNeeded()
    }*/
    
    //MARK: - action
    
    @IBAction func clickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPostClick(_ sender: Any) {
        if validationAnswer{
            
            if self.dictAnswer == nil{
                addAnswerWS()
            }
            else{
                editAnswerWS()
            }
        }
    }
    
    
    //MARK: - other
    
    var validationAnswer: Bool{
        
        self.view.endEditing(true)
        
//        if txtView.text == defultTextDes {txtView.text = ""}
        
        var mess = ""
        
        if ToString(txtView.text).isBlank || txtView.text == defultTextDes  {
            mess = "Please enter your answer"
        }
        
        if mess != ""{
            self.showOkAlert(msg: mess)
            return false
        }
        return true
    }
    
    //MARK: - Web service
    
    func addAnswerWS()
    {
        let param = ["user_id" : globalUserId,"post_id":toString(dictPost?.post_id),"answer":txtView.text ?? "","link":strPostLink]
        
        var imageArrayNew = [UIImage]()
        imageArray.forEach { (obj) in
            if obj is UIImage
            {
                imageArrayNew.append(obj as! UIImage)
            }
        }
        WebService.shared.webRequestForMultipleImages(DictionaryImages: imageArray.count > 0 ? ["image": imageArrayNew] : nil, urlString: ServerURL.add_answer, Perameters: param, completion: { (dicRes, success) in
            debugPrint(dicRes)
            
            if ConvertToBool(dicRes["status"])
            {
                NotificationCenter.default.post(name: .AnswerPost, object: nil)
                self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                    
                    self.dismiss(animated: true, completion: nil)
                })
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
            
        }) { (err) in
            debugPrint(err)
        }
    }
    
  /*  func addAnswerWS() {
        WebService.shared.RequesURL(ServerURL.AddAnswer, Perameters: ["user_id" : globalUserId,"post_id":toString(dictPost?.post_id),"answer":txtView.text ?? ""],showProgress: false,completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            if ConvertToBool(dicRes["status"])
            {
                NotificationCenter.default.post(name: .AnswerPost, object: nil)
                self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                    
                    self.dismiss(animated: true, completion: nil)
                })
            }
            else{self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))}
        }) { (err) in
                debugPrint(err)
        }
    }*/
    
    
 
    
    func editAnswerWS()
    {
        let param = ["user_id" : globalUserId,"post_answers_id":toString(dictAnswer?.post_answers_id),"answer":txtView.text ?? "","delete_image": deleteImageIds.count == 0 ? "" : deleteImageIds.componentsJoined(by: ","),"link":strPostLink]
        
        var imageArrayNew = [UIImage]()
        imageArray.forEach { (obj) in
            if obj is UIImage
            {
                imageArrayNew.append(obj as! UIImage)
            }
        }
        WebService.shared.webRequestForMultipleImages(DictionaryImages: imageArray.count > 0 ? ["image": imageArrayNew] : nil, urlString: ServerURL.edit_answer, Perameters: param, completion: { (dicRes, success) in
            debugPrint(dicRes)
            
            if ConvertToBool(dicRes["status"])
            {
                NotificationCenter.default.post(name: .AnswerPost, object: nil)
                self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                    
                    self.dismiss(animated: true, completion: nil)
                })
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
            
        }) { (err) in
            debugPrint(err)
        }
    }
   /* func editAnswerWS() {
        WebService.shared.RequesURL(ServerURL.edit_answer, Perameters: ["user_id" : globalUserId,"post_answers_id":toString(dictAnswer?.post_answers_id),"answer":txtView.text ?? ""],showProgress: false,completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            if ConvertToBool(dicRes["status"])
            {
                NotificationCenter.default.post(name: .AnswerPost, object: nil)
                self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                    
                    self.dismiss(animated: true, completion: nil)
                })
            }
            else{self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))}
        }) { (err) in
            debugPrint(err)
        }
    }*/
}

//MARK: - UITextview Delegate Methods

extension AnswerVC: UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard Range(range, in: currentText) != nil else { return false }
        
        //let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        if text == "\n"
        {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == defultTextDes {
            txtView.text = nil
        }
        txtView.textColor = UIColor.Appcolor51
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text.isEmpty {
            txtView.textColor = UIColor.lightGray
            txtView.text = defultTextDes
        }
    }
}

//MARK: Collection view methods
extension AnswerVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! imageCell
        
        let obj = imageArray[indexPath.row] as Any
        
        if obj is imagePostModel
        {
//            cell.imageView.setImageWithURL((obj as! imagePostModel).image_name, nil)
            cell.imageView.setImageWithURL((obj as! imagePostModel).image_name, "place_logo")
        }
        else if obj is UIImage
        {
            cell.imageView.image = (imageArray[indexPath.row] as! UIImage)
        }
        
        cell.closeBtn.tag = indexPath.row
        let height = colview.collectionViewLayout.collectionViewContentSize.height
        colviewConst.constant = height
        self.view.layoutIfNeeded()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width - 30)/3, height: (collectionView.frame.width - 30)/3)
    }
    
}
//MARK: Image picker

extension AnswerVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func camera()
    {
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func photoLibrary()
    {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.camera()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.photoLibrary()
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imageArray.add(info[UIImagePickerController.InfoKey.editedImage] as! UIImage)
        self.dismiss(animated: true, completion: nil)
        self.colview.reloadData()
    }
    
    @IBAction func photoBtnclicked(_ sender: Any) {
        showActionSheet()
    }
    @IBAction func closeBtnClicked(_ sender: UIButton) {
        
        let obj = imageArray[sender.tag] as Any
        
        if obj is imagePostModel
        {
            deleteImageIds.add((obj as! imagePostModel).post_img_id)
        }
        
        imageArray.removeObject(at: sender.tag)
        self.colview.reloadData()
    }
}

extension AnswerVC
{
    @IBAction func clickLink(_ sender: UIButton) {
        
        openAlertView()
    }
    func configurationTextField(textField: UITextField!) {
        if (textField) != nil {
            self.textField = textField!
            //Save reference to the UITextField
            self.textField?.placeholder = "Enter a link"
            self.textField?.keyboardType = .URL
        }
    }
    
    func openAlertView() {
        let alert = UIAlertController(title: Constant.appName, message: nil, preferredStyle: .alert)
        alert.addTextField(configurationHandler: configurationTextField)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler:{ (UIAlertAction) in
            self.showPreview(toString(self.textField?.text))
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showPreview(_ priviewStr : String) {
        
        self.detailedView?.isHidden = true
        indiCator?.startAnimating()
        
        func setDataPreview(_ result: Response) {
            print("url: ", result.url ?? "no url")
            print("finalUrl: ", result.finalUrl ?? "no finalUrl")
            print("canonicalUrl: ", result.canonicalUrl ?? "no canonicalUrl")
            print("title: ", result.title ?? "no title")
            print("images: ", result.images ?? "no images")
            print("image: ", result.image ?? "no image")
            print("video: ", result.video ?? "no video")
            print("icon: ", result.icon ?? "no icon")
            print("description: ", result.description ?? "no description")
            
            self.previewTitle?.text = toString(result.title)
            self.previewCanonicalUrl?.text = toString(result.finalUrl)
            self.previewDescription?.text = toString(result.description)
            
            if let icon = result.image
            {
                self.favicon?.setImageWithURL(toString(icon), "logo_small")
            }
            else
            {
                self.favicon?.setImageWithURL(toString(result.icon), "logo_small")
            }
            
            self.detailedView?.isHidden = false
            self.indiCator?.stopAnimating()
            self.strPostLink =  self.previewCanonicalUrl?.text ?? ""
        }
        
        self.slp.preview(
            priviewStr,
            onSuccess: { result in
                setDataPreview(result)
                //                self.result = result
        },
            onError: { error in
                print(error)
                self.showOkAlert(msg: "Invalid URL!")
                self.indiCator?.stopAnimating()
        }
        )
    }
    @IBAction func openWithAction(_ sender: UIButton) {
        if strPostLink.isEmpty == false{
            openURL(URL.init(string: strPostLink)!)
        }
    }
    func openURL(_ url: URL) {
        if ["http", "https"].contains(url.scheme?.lowercased() ?? "") {
            self.present(SFSafariViewController(url: url), animated: true, completion: nil)
        } else {
            linkurlOpen_MS(linkurl: url.absoluteString)
        }
    }
    @IBAction func closePriview(_ sender: UIButton) {
        strPostLink = ""
        detailedView?.isHidden = true
    }
    
}
