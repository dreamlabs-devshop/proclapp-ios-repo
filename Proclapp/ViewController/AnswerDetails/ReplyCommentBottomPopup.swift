//
//  ReplyCommentBottomPopup.swift
//  Proclapp
//
//  Created by Dreamlabs on 17/03/2021.
//  Copyright © 2021 Ashish Parmar. All rights reserved.
//

import UIKit
import BottomPopup

class ReplyCommentBottomPopup: BottomPopupViewController {


    @IBOutlet weak var txtComment: UITextField!
    
    @IBOutlet weak var replyFieldView: UIView!
    
     var callback : ((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.async {
            self.replyFieldView.layer.cornerRadius = (self.replyFieldView.bounds.height / 2)
            self.replyFieldView.layer.borderColor = UIColor.lightGray.cgColor
            self.replyFieldView.layer.borderWidth = 1
        }
    }
    
    override func getPopupHeight() -> CGFloat {
        return 80
    }
    
    @IBAction func showEmogi(_ sender: UIButton) {
        isDisplayEmoji = true
        //        txtViewMsg.resignFirstResponder()
        txtComment.reloadInputViews()
        txtComment.becomeFirstResponder()
    }
    
    @IBAction func sendReply(_ sender: UIButton) {
        callback!(txtComment.text ?? "")
        //close bottom sheet
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension ReplyCommentBottomPopup : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //commentReply = textField.text ?? ""
        textField.text = ""
        isDisplayEmoji = false
        textField.reloadInputViews()
    }
    
}
