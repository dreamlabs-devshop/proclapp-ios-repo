//
//  SuggestedEdit.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/22/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class SuggestedEdit: UIViewController {
    
    //MARK: - outlet
    @IBOutlet weak var imgProfile: UIImageView!

    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblProfession: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    

    //MARK: - properties
    let defultTextDes = "Write your Suggestion"
    var dictPost : allPostModel?
    


    override func viewDidLoad() {
        super.viewDidLoad()
       
        //setup post detaildp_profile
        lblUserName.text = dictPost?.author
        imgProfile.setImageWithURL(dictPost?.profile_image, "Big_dp_placeholder")
        lblDate.text = dictPost?.post_date.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_ddMMM)
        lblTitle.text = dictPost?.post_title
        lblProfession.text = dictPost?.professional_qualification
        self.imgProfile.layer.cornerRadius = (0.1 * screenwidth)  / 2
        self.imgProfile.layer.masksToBounds = true
        txtView.textColor = UIColor.lightGray
        txtView.text = defultTextDes
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
  
    
    @IBAction func clickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPostClick(_ sender: Any)
    {
        if validationAnswer{
            addSuggestedEdit()
        }
    }
    
    
    //MARK: - other
    
    var validationAnswer: Bool{
        self.view.endEditing(true)
        var mess = ""
        if ToString(txtView.text).isBlank || txtView.text == defultTextDes  {
            mess = "Please enter your answer"
        }
        
        if mess != ""{
            self.showOkAlert(msg: mess)
            return false
        }
        return true
    }
    
    //MARK: - Web service
    
    func addSuggestedEdit() {
        WebService.shared.RequesURL(ServerURL.suggest_edit_post, Perameters: ["user_id" : globalUserId,"post_id":toString(dictPost?.post_id),"post_title":txtView.text ?? ""],showProgress: true,completion: { (dicRes, success) in
            debugPrint(dicRes)
            if ConvertToBool(dicRes["status"])
            {
                self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                    self.dismiss(animated: true, completion: nil)
                })
            }
            else{self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))}
        }) { (err) in
                debugPrint(err)
        }
    }
    
}

//MARK: - UITextview Delegate Methods

extension SuggestedEdit: UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard Range(range, in: currentText) != nil else { return false }
        
        //let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        if text == "\n"
        {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == defultTextDes {
            txtView.text = nil
        }
        txtView.textColor = UIColor.Appcolor51
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text.isEmpty {
            txtView.textColor = UIColor.lightGray
            txtView.text = defultTextDes
        }
    }
}





