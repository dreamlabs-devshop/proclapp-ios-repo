//
//  AnswerListVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/23/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import AVKit
import SwiftLinkPreview
import SafariServices

class AnswerListVC: UIViewController {
    

//    static let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerListVC") as! AnswerListVC
//
    @IBOutlet weak var btnAnswer: UIButton!

    @IBOutlet weak var tblAnswer: UITableView!
    @IBOutlet weak var tblAnswerList: UITableView!
    
    var dictPost : allPostModel?
    
    var arrAnswer = [AnswerModel]()
    
    var postType = ""
    
    var answer : AnswerDetailHeaderCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        postType = dictPost?.postType ==  PostTypeEnum.Question ?  "Q" : "R"
        btnAnswer.isHidden = postType == "Q"
        
        
        
        tblAnswer.register(UINib.init(nibName: "downloadPDFTableCell", bundle: nil), forCellReuseIdentifier: "downloadPDFTableCell")

        tblAnswer.register(UINib.init(nibName: "QuestionAnswerTableViewCell", bundle: nil), forCellReuseIdentifier: "QuestionAnswerTableViewCell")
        getAnswerListWS()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.AnswerPost), name: .AnswerPost, object: nil)

        // Do any additional setup after loading the view.
    }
    @objc func AnswerPost(notification: NSNotification){
        DispatchQueue.main.async {
            self.getAnswerListWS()
            debugPrint("!!!!!!!! AnswerPost !!!!!!!!!!")
        }
    }
    @IBAction func clickAnswer(_ sender: Any) {
        let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerVC") as! AnswerVC
        vcInstace.dictPost = dictPost
        self.present(vcInstace, animated: true, completion: nil)

    }
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        self.navigationController?.isNavigationBarHidden = false
    }
    @IBAction func clickFilter(_ sender: Any) {
        
        DispatchQueue.main.async {
            guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
            popupVC.arrTitle = Constant.karrFilter
            popupVC.callback =  { str in
//                debugPrint(str)
                self.getAnswerListWS(str)
            }
            self.present(popupVC, animated: true, completion: nil)
        }
        
    }
    //MARK: - Web service
    
    func getAnswerListWS(_ strGet : String = "") {
        var passdata = "3"
        if strGet.lowercased() == "User".lowercased(){
            passdata = "4"
        }
        else if strGet.lowercased() == "Views".lowercased() {
            passdata = "2"
        }
        
        if self.tblAnswer.accessibilityHint == "service_calling" { return }
        
        self.tblAnswer.accessibilityHint = "service_calling"
        var delayTime = DispatchTime.now()
        
        if self.tblAnswer.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblAnswer.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.AnswerList, Perameters: ["user_id" : globalUserId,"post_id":toString(self.dictPost?.post_id),"sort_by":passdata],showProgress: false,completion: { (dicRes, success) in
                debugPrint(dicRes)
                debugPrint("getAnswerListWS")
                if success == true{
                    if let arrTemp = dicRes["answer_list"] as? [[String:Any]] {
                        self.arrAnswer = arrTemp.map({AnswerModel.init($0)})
                    }
                }
                DispatchQueue.main.async {
                    UIView.performWithoutAnimation {
                        self.tblAnswer.reloadData()
                    }
                    self.tblAnswer.accessibilityHint = nil
                    self.tblAnswer.refreshControl?.endRefreshing()
                }
                
                
            }) { (err) in
                debugPrint(err)
                self.tblAnswer.accessibilityHint = nil
                self.tblAnswer.refreshControl?.endRefreshing()
            }
        }
        
    }
    
}

//MARK: - Delegate to update table
protocol UITableUpdateDelegate: class {
    func reloadTableData()
}

//MARK: - Table view data source
    extension AnswerListVC: UITableViewDataSource,UITableViewDelegate, UITableUpdateDelegate
    {
        
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return section == 0 ? 1 : self.arrAnswer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.section == 0
        {
            if postType == "Q"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionAnswerTableViewCell") as! QuestionAnswerTableViewCell
                cell.model = dictPost
                
                cell.lblTitle.numberOfLines =  cell.btnViewMore.titleLabel?.text?.lowercased() != "view more" ? 0 : 6
                cell.txtviewTitle.textContainer.maximumNumberOfLines = cell.btnViewMore.titleLabel?.text?.lowercased() != "view more" ? 0 : 6

                return cell
            }
            else if postType == "R"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "downloadPDFTableCell") as! downloadPDFTableCell
                cell.lblPDFDes.numberOfLines = 0
                cell.model = dictPost
                return cell
            }
            return UITableViewCell()
        }
        else
        {
           
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellViewAnswer") as! cellViewAnswer
            
             self.tblAnswerList = tableView

            cell.model = arrAnswer[indexPath.row]
            
            cell.lblanswer.attributedText  = cell.model?.answer.htmlAttributed(family: "Lato-Regular", size: 11 * screenscale, color: .Appcolor51)
            
            cell.lblCommentcount.text = cell.model?.answer_comment_count
            
            cell.lblLikeCount.text = toString(cell.model?.post_answer_like_count)
            cell.btnLike.isSelected = cell.model?.is_post_answer_liked ?? false
            
            cell.lblUpvotecount.text = toString(cell.model?.answer_upvote_comment_count)
            cell.lblDownvoteCount.text = toString(cell.model?.answer_downvote_comment_count)
            cell.btnUpvote.isSelected = cell.model?.is_post_answer_upvote ?? false ? true : false
            cell.btndownvote.isSelected = cell.model?.is_post_answer_downvote ?? false ? true : false
            
            let strUpvote = ToInt(cell.lblUpvotecount.text) > 1 ? "Upvotes" : "Upvote"
            cell.btnUpvote.setTitle(strUpvote, for: .normal)
           cell.btnUpvote.setTitle(strUpvote, for: .selected)
            
            let strDownvote = ToInt(cell.lblDownvoteCount.text) > 1 ? "Downvotes" : "Downvote"
           cell.btndownvote.setTitle(strDownvote, for: .normal)
            cell.btndownvote.setTitle(strDownvote, for: .selected)
            
            cell.lblYouTxt.text = ""
            cell.self.imgYou.isHidden = true
            if cell.model?.is_post_answer_upvote ?? false{
                cell.lblYouTxt.text = "You upvoted this"
                cell.self.imgYou.isHidden = false
            }
            else if cell.model?.is_post_answer_downvote ?? false{
                cell.lblYouTxt.text = "You downvoted this"
                cell.self.imgYou.isHidden = false
            }
            
            cell.self.imgYou.setImageWithURL(loginData?.profile_image ?? "", "Big_dp_placeholder")
            
            if let previewUrl = cell.model?.dicPostLink?.link, checkValidString(previewUrl)
            {
                cell.viewPreviewHeightConst.constant = 100
                cell.self.showPreviewImage(toString(previewUrl))
            }
            else
            {
                cell.viewPreviewHeightConst.constant = 0
                cell.detailedView?.isHidden = true
            }
            
            cell.btnViewMore.isHidden = true
            if(cell.lblanswer.maxNumberOfLines > 6){
                cell.btnViewMore.isHidden = false
//                cell.lblanswer.numberOfLines = 6
            }
            
            
            cell.self.updateConstraintsIfNeeded()
            cell.self.layoutIfNeeded()
            
//            cell.btnShared.addTarget(self, action: #selector(cell.self.tapOnShare(_:)), for: .touchUpInside)
//    
//            cell.btnUpvote.addTarget(self, action: #selector(cell.self.btnUpVoteClicked(_:)), for: .touchUpInside)
//            cell.btndownvote.addTarget(self, action: #selector(cell.self.downVoteClick(_:)), for: .touchUpInside)

//            cell.self.collectionGallary.delegate = cell
//            cell.self.collectionGallary.dataSource = cell
//
//            cell.self.collectionGallary.reloadData()

            cell.self.setUpCollection()

            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
//        return indexPath.section == 0 ? UITableView.automaticDimension : 68 * screenscale
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellAnswerHeader") as! cellAnswerHeader
        cell.lblTitle.text = self.arrAnswer.count > 1 ? "\(self.arrAnswer.count) ANSWERS" : "\(self.arrAnswer.count) ANSWER"
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ?  CGFloat.leastNormalMagnitude : 32 * screenscale
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return CGFloat.leastNormalMagnitude
    }
    
   /* func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        debugPrint("MYid:-\(globalUserId)","Match:-\(self.arrAnswer[indexPath.row].user?.user_id)")

        if globalUserId != self.arrAnswer[indexPath.row].user?.user_id {return [UITableViewRowAction]()}
        
        let delete = UITableViewRowAction.init(style: UITableViewRowAction.Style.destructive, title: "Delete") { (_, getInd) in
            
            self.showOkCancelAlertWithAction(msg: Constant.kAlertanswer, handler: { (bool) in
                if bool
                {
                    self.webCall_deleteDiscuss(self.arrAnswer[getInd.row].post_answers_id,callback: {
                        self.arrAnswer.remove(at: getInd.row)
                        self.tblAnswer?.deleteRows(at: [indexPath], with: .left)
                    })
                }
            })
            
            
        }
        delete.backgroundColor = .AppcolorRed
        
        
        let edit = UITableViewRowAction.init(style: UITableViewRowAction.Style.default, title: "Edit") { (_, getInd) in
            
            let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerVC") as! AnswerVC
            vcInstace.dictPost = self.dictPost
            vcInstace.dictAnswer = self.arrAnswer[getInd.row]
            self.present(vcInstace, animated: true, completion: nil)
        }
        edit.backgroundColor = .AppBlue_Dark
        

        return [delete,edit]
    }
    */
    func webCall_deleteDiscuss(_ delete_answers_id:String ,callback: (()->())?){
        WebService.shared.RequesURL(ServerURL.delete_answer, Perameters: ["user_id":globalUserId,"post_answers_id":delete_answers_id],showProgress: true, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true
            {
                callback?()
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
    
    func reloadTableData() {
        self.tblAnswerList.reloadData()
    }
}
/*class cellAnswer : UITableViewCell
{
    @IBOutlet var lblTitle: UILabel!
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var imgProfileWidthConst: NSLayoutConstraint!
    @IBOutlet weak var imgProfileHeightConst: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            
            self.imgProfileWidthConst.constant = 30 * screenscale
            self.imgProfileHeightConst.constant = 30 * screenscale
            
            self.imgProfile.layer.cornerRadius =  self.imgProfileWidthConst.constant / 2
        }
    }
}*/

class cellViewAnswer: UITableViewCell {
     let avPlayerController = AVPlayerViewController()
    
    @IBOutlet var collectionGallary: UICollectionView!
    
    
    @IBOutlet weak var viewGallary_H_Const: NSLayoutConstraint!
    
    
    @IBOutlet var lblYouTxt: UILabel!
    @IBOutlet var imgYou: UIImageView!
    
    
    @IBOutlet var txtFieldView: UIView!
    @IBOutlet var lblanswer: UILabel!
    @IBOutlet var txtComment: UITextField!
    @IBOutlet var btnEmoji: UIButton!
    @IBOutlet var btnSendComment: UIButton!
    
    @IBOutlet var lblCommentcount: UILabel!
    @IBOutlet var lblViewCount: UILabel!
    @IBOutlet var lblUpvotecount: UILabel!
    @IBOutlet var lblDownvoteCount: UILabel!
    @IBOutlet var btnUpvote: UIButton!
    @IBOutlet var btndownvote: UIButton!
    @IBOutlet var btnShared: UIButton!
    @IBOutlet var btnViewMore: UIButton!
    
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var lblLikeCount: UILabel!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!
    //For Preview Show
    @IBOutlet weak var viewPreview: UIView!
    private let slp = SwiftLinkPreview(cache: InMemoryCache())
    @IBOutlet private weak var previewTitle: UILabel?
    @IBOutlet private weak var previewCanonicalUrl: UILabel?
    @IBOutlet private weak var previewDescription: UILabel?
    @IBOutlet weak var detailedView: UIView?
    @IBOutlet private weak var favicon: UIImageView?
    @IBOutlet private weak var indiCator: UIActivityIndicatorView?
    
    @IBOutlet weak var viewPreviewHeightConst: NSLayoutConstraint!
    
    
    @IBOutlet weak var imageCollectionView: UIView!
    
    var collectionClickBlock : (() -> ())?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewGallary_H_Const.constant = 0
        self.updateConstraintsIfNeeded()
        self.layoutIfNeeded()
        
        collectionGallary.delegate = self
        collectionGallary.dataSource = self
        self.collectionGallary.register(UINib(nibName: "AdvertPhotoCLVCell", bundle: nil), forCellWithReuseIdentifier: "AdvertPhotoCLVCell")
        
        
        DispatchQueue.main.async {
//            self.txtFieldView.layer.cornerRadius = (self.txtFieldView.bounds.height / 2)
//            self.txtFieldView.layer.borderColor = UIColor.lightGray.cgColor
//            self.txtFieldView.layer.borderWidth = 1
            
            self.imgYou.layer.cornerRadius =  (0.053125 * screenwidth)/2
            self.imgYou.layer.masksToBounds = true
        }
        self.viewPreview.applyDropShadow()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(cellViewAnswer.viewAnswerFunction))
               lblanswer.isUserInteractionEnabled = true
               lblanswer.addGestureRecognizer(tap)
        
    }
    
    var model : AnswerModel? {
        didSet {
//            lblanswer.attributedText = model?.answer.htmlToAttributedString
            lblName.text = ToString(model?.user?.firstname) + " " + ToString(model?.user?.lastname)
//            lblDate.text = model?.date.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_HHMMMdd)
            lblDate.text = calculateTimeDifference(model!.date)
            lblanswer.attributedText  = model?.answer.htmlAttributed(family: "Lato-Regular", size: 11 * screenscale, color: .Appcolor51)
            
            
            
            if(model?.user?.profession == nil || model?.user?.profession == ""){
                lblTitle.text = "..."
            }
            else{
                lblTitle.text = model?.user?.profession
            }
            
            lblCommentcount.text = toString(model?.answer_comment_count ?? 0)
            
            lblLikeCount.text = toString(model?.post_answer_like_count)
            btnLike.isSelected = model?.is_post_answer_liked ?? false
            
            lblUpvotecount.text = toString(model?.answer_upvote_comment_count)
            lblDownvoteCount.text = toString(model?.answer_downvote_comment_count)
            btnUpvote.isSelected = model?.is_post_answer_upvote ?? false ? true : false
            btndownvote.isSelected = model?.is_post_answer_downvote ?? false ? true : false
            
            let strUpvote = ToInt(lblUpvotecount.text) > 1 ? "Upvotes" : "Upvote"
            btnUpvote.setTitle(strUpvote, for: .normal)
            btnUpvote.setTitle(strUpvote, for: .selected)
            
            let strDownvote = ToInt(lblDownvoteCount.text) > 1 ? "Downvotes" : "Downvote"
            btndownvote.setTitle(strDownvote, for: .normal)
            btndownvote.setTitle(strDownvote, for: .selected)
            
            lblYouTxt.text = ""
            self.imgYou.isHidden = true
            if model?.is_post_answer_upvote ?? false{
                lblYouTxt.text = "You upvoted this"
                self.imgYou.isHidden = false
            }
            else if model?.is_post_answer_downvote ?? false{
                lblYouTxt.text = "You downvoted this"
                self.imgYou.isHidden = false
            }
            
            self.imgYou.setImageWithURL(loginData?.profile_image ?? "", "Big_dp_placeholder")
            
            if let previewUrl = model?.dicPostLink?.link, checkValidString(previewUrl)
            {
                viewPreviewHeightConst.constant = 100
                self.showPreviewImage(toString(previewUrl))
            }
            else
            {
                viewPreviewHeightConst.constant = 0
                self.detailedView?.isHidden = true
            }
            
            btnViewMore.isHidden = true
            if(lblanswer.maxNumberOfLines > 6){
                btnViewMore.isHidden = false
//                lblanswer.numberOfLines = 6
            }
            
            if((model?.arrImages.count)! > 0){
                 imageCollectionView?.frame.size.height = 82.5;
            }
            else {
                imageCollectionView?.frame.size.height = 0;
            }
            
            
            
            self.updateConstraintsIfNeeded()
            self.layoutIfNeeded()
            
            setUpCollection()
            
        }
    }
    func setUpCollection() {
        
        let countImages = ToInt(model?.arrImages.count)
        self.collectionGallary.reloadData()
        
        viewGallary_H_Const.constant = countImages == 0 ? 0 :  100 * screenscale
        
//        if  countImages > 0
//         {
//            viewGallary_H_Const.constant = (100 * screenscale) + 100
//         }
        
        self.updateConstraintsIfNeeded()
        self.layoutIfNeeded()
    }
    
    @IBAction func btnSharedClicked(_ sender: UIButton) {
         if let getObj = self.model
        {
        self.parentViewController?.shareMaulik(shareText: getObj.answer.htmlToString
            , shareImage: nil,shreUrl:nil)
        }
    }
    
    @IBAction func viewMoreClicked(_ sender: UIButton) {
        weak var delegate: UITableUpdateDelegate?
        let getTitle = sender.titleLabel?.text
       
       if getTitle?.lowercased() == "view more"
       {
           sender.setTitle("View Less", for: .normal)
           lblanswer.numberOfLines = lblanswer.maxNumberOfLines
           lblanswer.sizeToFit()
       }
       else
       {
           sender.setTitle("View More", for: .normal)
           lblanswer.numberOfLines = 6
       }
       
       UIView.animate(withDuration: 0.3) {
             delegate?.reloadTableData()
       }
            
    }
    
}

extension cellViewAnswer: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model?.arrImages.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "AdvertPhotoCLVCell", for: indexPath) as! AdvertPhotoCLVCell
        cell.imgGallary.setImageWithURL(model?.arrImages[indexPath.row].image_name, "place_logo")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: ToInt(model?.arrImages.count) == 1 ? collectionView.frame.width : collectionView.frame.height , height: collectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.collectionClickBlock?()
        
       /* if let get = model?.arrImages, get.count > 0
        {
            let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "imageGalleryVC") as! imageGalleryVC
            vcInstace.getarrImages = get
            self.tableView?.parentViewController?.navigationController?.pushViewController(vcInstace, animated: true)
        }*/
        
    }
    
    @IBAction func viewAnswerFunction(sender: UITapGestureRecognizer)
       {
           if (self.tableView?.parentViewController as? VoiceVideoDetailVC) != nil
           {
               if let obj = model
               {
                   // Download file
                   if obj.show_ans == false
                   {
                       self.tableView?.parentViewController?.showOkCancelAlertWithAction(msg: Constant.kAlertExpertAnswerSee, handler: { (success) in
                           if success
                           {
                               let vcInstace = StoryBoard.Wallet.instantiateViewController(withIdentifier: "WebviewPaymentVC") as! WebviewPaymentVC
                               vcInstace.strPaymentType = "3"
                               vcInstace.postId = obj.post_id
                               vcInstace.callbackPayment = { status in
                                   if status == Constant.kPayment_Success
                                   {
                                       obj.show_ans = true
                                   }
                                   
                               }
                               self.tableView?.parentViewController?.pushTo(vcInstace)
                           }
                       })
                       return
                   }
                   
                   if obj.vv_type == "1"//audio
                   {
                       if let get = obj.dicAudio{
                           guard let url = URL(string: get.audio_url) else {
                               return
                           }
                           let player = AVPlayer(url: url)
                           avPlayerController.player = player
                           self.tableView?.parentViewController?.present(avPlayerController, animated: true) {
                               player.play()
                           }
                       }
                   }
                   else if obj.vv_type == "2"//video
                   {
                       if let get = obj.dicVideo{
                           guard let url = URL(string: get.video_url) else {
                               return
                           }
                           let player = AVPlayer(url: url)
                           avPlayerController.player = player
                        self.tableView?.parentViewController?.present(avPlayerController, animated: true) {
                               player.play()
                           }
                       }
                   }
                   else
                   {
                       
                       let popVC = StoryBoard.AddPost.instantiateViewController(withIdentifier: "AddExpertReplyPopupVC") as! AddExpertReplyPopupVC
                       popVC.isTxtviewHide = false
                       popVC.strAnswer = obj.answer
                       popVC.modalPresentationStyle = .overCurrentContext
                       popVC.modalTransitionStyle = .crossDissolve
                       self.tableView?.parentViewController?.tabBarController?.present(popVC, animated: true, completion: nil)
                       
                       /* if let nav = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerDetailNAV") as? UINavigationController, let vcInstace = nav.viewControllers.first as? AnswerDetailVC
                        {
                        vcInstace.post_answers_id = model?.post_answers_id ?? ""
                        self.tableView?.parentViewController?.present(nav, animated: true, completion: nil)
                        }*/
                   }
                   
               }
           }
           else
           {
               if let nav = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerDetailNAV") as? UINavigationController, let vcInstace = nav.viewControllers.first as? AnswerDetailVC
               {
                   vcInstace.post_answers_id = model?.post_answers_id ?? ""
                   self.tableView?.parentViewController?.present(nav, animated: true, completion: nil)
               }
           }
           //        let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerDetailVC") as! AnswerDetailVC
           //        vcInstace.post_answers_id = model?.post_answers_id ?? ""
           //        self.tableView?.parentViewController?.present(vcInstace, animated: true, completion: nil)
       }
}
//MARK:- Preview Show

extension cellViewAnswer
{
    func showPreviewImage(_ linkURL:String) {
        
        self.detailedView?.isHidden = true
        indiCator?.startAnimating()
        
        func setDataPreview(_ result: Response) {
            print("url: ", result.url ?? "no url")
            print("finalUrl: ", result.finalUrl ?? "no finalUrl")
            print("canonicalUrl: ", result.canonicalUrl ?? "no canonicalUrl")
            print("title: ", result.title ?? "no title")
            print("images: ", result.images ?? "no images")
            print("image: ", result.image ?? "no image")
            print("video: ", result.video ?? "no video")
            print("icon: ", result.icon ?? "no icon")
            print("description: ", result.description ?? "no description")
            
            self.previewTitle?.text = toString(result.title)
            self.previewCanonicalUrl?.text = toString(result.finalUrl)
            self.previewDescription?.text = toString(result.description)
            
            if let icon = result.image
            {
                self.favicon?.setImageWithURL(toString(icon), "logo_small")
            }
            else
            {
                self.favicon?.setImageWithURL(toString(result.icon), "logo_small")
            }
            self.detailedView?.isHidden = false
            self.indiCator?.stopAnimating()
        }
        
        
        self.slp.preview(
            linkURL,
            onSuccess: { result in
                setDataPreview(result)
                //                self.result = result
        },
            onError: { error in
                print(error)
                //                self.showOkAlert(msg: "Invalid URL!")
                self.indiCator?.stopAnimating()
        }
        )
    }
    
    @IBAction func openWithAction(_ sender: UIButton) {
        if let previewUrl = model?.dicPostLink?.link, checkValidString(previewUrl)
        {
            openURL(URL.init(string: previewUrl)!)
        }
    }
    
    func openURL(_ url: URL) {
        if ["http", "https"].contains(url.scheme?.lowercased() ?? "") {
            self.tableView?.parentViewController?.present(SFSafariViewController(url: url), animated: true, completion: nil)
        } else {
            linkurlOpen_MS(linkurl: url.absoluteString)
        }
    }
    
    
    
//    @objc func btnUpVoteClick(_ sender: UIButton) {
//            weak var delegate: UITableUpdateDelegate?
//
//            if  self.model?.is_post_answer_upvote == false
//            {
//                if  self.model?.is_post_answer_downvote == true{
//                    self.model?.answer_downvote_comment_count -= 1
//                }
//
//                PostUpvoteDownvotekWS(status: "1")
//                self.model?.is_post_answer_upvote = true
//                self.model?.is_post_answer_downvote = false
//
//                self.model?.answer_upvote_comment_count += 1
//
////                delegate?.reloadTableData()
//            }
//        }
//
//        @objc func downVoteClick(_ sender: UIButton) {
//            weak var delegate: UITableUpdateDelegate?
//
//            if self.model?.is_post_answer_downvote == false
//            {
//                if  self.model?.is_post_answer_upvote == true{
//                     self.model?.answer_upvote_comment_count -= 1
//                }
//                PostUpvoteDownvotekWS(status: "2")
//                self.model?.is_post_answer_upvote = false
//                self.model?.is_post_answer_downvote = true
//                self.model?.answer_downvote_comment_count += 1
//                delegate?.reloadTableData()
//            }
//        }
    
        @IBAction func btnUpVoteClicked(_ sender: UIButton) {
            weak var delegate: UITableUpdateDelegate?
                    
            if  self.model?.is_post_answer_upvote == false
            {
                if  self.model?.is_post_answer_downvote == true{
                    lblDownvoteCount.text = toString(self.model!.answer_downvote_comment_count - 1)
                    
                    self.model?.answer_downvote_comment_count -= 1
                    
                    
                }
    
                PostUpvoteDownvotekWS(status: "1")
                self.model?.is_post_answer_upvote = true
                self.model?.is_post_answer_downvote = false
    
                self.model?.answer_upvote_comment_count += 1
                
                lblUpvotecount.text = toString(self.model?.answer_upvote_comment_count)
                
                
    
                delegate?.reloadTableData()
            }
        }
    
        @IBAction func downVote(_ sender: UIButton) {
            weak var delegate: UITableUpdateDelegate?
            
            if self.model?.is_post_answer_downvote == false
            {
                if  self.model?.is_post_answer_upvote == true{
                    lblUpvotecount.text = toString(self.model!.answer_upvote_comment_count - 1)
                    
                     self.model?.answer_upvote_comment_count -= 1
                }
                PostUpvoteDownvotekWS(status: "2")
                self.model?.is_post_answer_upvote = false
                self.model?.is_post_answer_downvote = true
                self.model?.answer_downvote_comment_count += 1
                lblDownvoteCount.text = toString(self.model?.answer_downvote_comment_count)
                
                delegate?.reloadTableData()
            }
        }
    
       
    func PostUpvoteDownvotekWS(status : String,_ serviceCount : Int = 0) {
        WebService.shared.RequesURL(ServerURL.PostUpvoteDownvote, Perameters: ["user_id" : globalUserId,"answer_id":self.model?.post_answers_id ?? "","post_id":self.model?.post_id ?? "","status":status],showProgress: false,completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            
        }) { (err) in
            if serviceCount < 1 {
                self.PostUpvoteDownvotekWS(status: status, serviceCount + 1)
            } else {
                debugPrint(err)
            }
        }
    }
    
}



class cellSeeAnswer : UITableViewCell
{
    let avPlayerController = AVPlayerViewController()

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet weak var btnSeeanswer: UIButton!
    
    @IBOutlet weak var btnDot: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            self.imgProfile.layer.cornerRadius =  self.imgProfile.frame.height / 2
        }
        
        self.btnSeeanswer.layer.cornerRadius = 5
        self.btnSeeanswer.layer.borderWidth  = 1
        self.btnSeeanswer.layer.borderColor = UIColor.AppSkyBlue.cgColor
        
    }
    
    var model : AnswerListModel? {
        didSet {
            lblTitle.text = ToString(model?.user?.firstname) + " " + ToString(model?.user?.lastname)
            imgProfile.setImageWithURL(model?.user?.profile_image_thumb, "Big_dp_placeholder")
//            lblDate.text = model?.date.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_HHMMMddyy)
            lblDate.text = calculateTimeDifference(model!.date)
            btnDot.isHidden =  globalUserId != model?.user?.user_id
        }
    }
    @IBAction func clickProfile(_ sender: UIButton)
    {
       
        guard globalUserId != "" else {
            appDelegate.showAlertGuest()
            return
        }
        
        if let getObje = model
        {
            if getObje.user_id == globalUserId
            {
                let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                self.tableView?.parentViewController?.pushTo(vcInstace)
            }
            else{
                let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "OtherProfileVC") as! OtherProfileVC
                vcInstace.profileId = getObje.user_id
                self.tableView?.parentViewController?.pushTo(vcInstace)
                
            }
        }
    }
    
    @IBAction func btnSeeAnswerClick(_ sender: Any)
    {
        if (self.tableView?.parentViewController as? VoiceVideoDetailVC) != nil
        {
            if let obj = model
            {
                // Download file
                if obj.show_ans == false
                {
                    self.tableView?.parentViewController?.showOkCancelAlertWithAction(msg: Constant.kAlertExpertAnswerSee, handler: { (success) in
                        if success
                        {
                            let vcInstace = StoryBoard.Wallet.instantiateViewController(withIdentifier: "WebviewPaymentVC") as! WebviewPaymentVC
                            vcInstace.strPaymentType = "3"
                            vcInstace.postId = obj.post_id
                            vcInstace.callbackPayment = { status in
                                if status == Constant.kPayment_Success
                                {
                                    obj.show_ans = true
                                }
                                
                            }
                            self.tableView?.parentViewController?.pushTo(vcInstace)
                        }
                    })
                    return
                }
                
                if obj.vv_type == "1"//audio
                {
                    if let get = obj.dicAudio{
                        guard let url = URL(string: get.audio_url) else {
                            return
                        }
                        let player = AVPlayer(url: url)
                        avPlayerController.player = player
                        self.tableView?.parentViewController?.present(avPlayerController, animated: true) {
                            player.play()
                        }
                    }
                }
                else if obj.vv_type == "2"//video
                {
                    if let get = obj.dicVideo{
                        guard let url = URL(string: get.video_url) else {
                            return
                        }
                        let player = AVPlayer(url: url)
                        avPlayerController.player = player
                        self.tableView?.parentViewController?.present(avPlayerController, animated: true) {
                            player.play()
                        }
                    }
                }
                else
                {
                    
                    let popVC = StoryBoard.AddPost.instantiateViewController(withIdentifier: "AddExpertReplyPopupVC") as! AddExpertReplyPopupVC
                    popVC.isTxtviewHide = false
                    popVC.strAnswer = obj.answer
                    popVC.modalPresentationStyle = .overCurrentContext
                    popVC.modalTransitionStyle = .crossDissolve
                    self.tableView?.parentViewController?.tabBarController?.present(popVC, animated: true, completion: nil)
                    
                    /* if let nav = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerDetailNAV") as? UINavigationController, let vcInstace = nav.viewControllers.first as? AnswerDetailVC
                     {
                     vcInstace.post_answers_id = model?.post_answers_id ?? ""
                     self.tableView?.parentViewController?.present(nav, animated: true, completion: nil)
                     }*/
                }
                
            }
        }
        else
        {
            if let nav = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerDetailNAV") as? UINavigationController, let vcInstace = nav.viewControllers.first as? AnswerDetailVC
            {
                vcInstace.post_answers_id = model?.post_answers_id ?? ""
                self.tableView?.parentViewController?.present(nav, animated: true, completion: nil)
            }
        }
        //        let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerDetailVC") as! AnswerDetailVC
        //        vcInstace.post_answers_id = model?.post_answers_id ?? ""
        //        self.tableView?.parentViewController?.present(vcInstace, animated: true, completion: nil)
    }
    
    //MARK: Click Dot
    @IBAction func clickDot(_ sender: Any) {
        
        if let indexPath = self.tableView?.indexPathForView(self){
            if let getVC = self.tableView?.parentViewController as? AnswerListVC {
                
                DispatchQueue.main.async {
                    
                    guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
                    popupVC.arrTitle = [BottomName.Edit,BottomName.Delete]
                    popupVC.callback =  { str in
                        debugPrint(str)
                        if str == BottomName.Edit
                        {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerVC") as! AnswerVC
                                vcInstace.dictPost = getVC.dictPost
                                
//                                vcInstace.dictAnswer = getVC.arrAnswer[indexPath.row]
                                getVC.present(vcInstace, animated: true, completion: nil)
                            })
                            
                        }
                        else if str == BottomName.Delete
                        {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                getVC.showOkCancelAlertWithAction(msg: Constant.kAlertanswer, handler: { (bool) in
                                    if bool
                                    {
                                        getVC.webCall_deleteDiscuss(getVC.arrAnswer[indexPath.row].post_answers_id,callback: {
                                            getVC.arrAnswer.remove(at: indexPath.row)
                                            getVC.tblAnswer?.deleteRows(at: [indexPath], with: .left)
                                        })
                                    }
                                })
                            })
                        }
                    }
                    self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
                }
            }
            
            if let getVC1 = self.tableView?.parentViewController as? ArticleDetailVC {
                
                DispatchQueue.main.async {
                    
                    guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
                    popupVC.arrTitle = [BottomName.Edit,BottomName.Delete]
                    popupVC.callback =  { str in
                        debugPrint(str)
                        if str == BottomName.Edit
                        {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "DiscussionVC") as! DiscussionVC
                                vcInstace.strPostID = getVC1.arrDiscussList[indexPath.row].post_id
//                                vcInstace.dictAnswer = getVC1.arrDiscussList[indexPath.row]
                                getVC1.present(vcInstace, animated: true, completion: nil)                            })
                            
                        }
                        else if str == BottomName.Delete
                        {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                getVC1.showOkCancelAlertWithAction(msg: Constant.kAlertdiscuss, handler: { (bool) in
                                    if bool
                                    {
                                        getVC1.webCall_deleteDiscuss(getVC1.arrDiscussList[indexPath.row].post_answers_id,callback: {
                                            getVC1.arrDiscussList.remove(at: indexPath.row)
                                            getVC1.tblview?.deleteRows(at: [indexPath], with: .left)
                                        })
                                    }
                                })
                            })
                        }
                    }
                    self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
                }
            }
            
            if let getVC = self.tableView?.parentViewController as? AdvertDetailsVC {
                
                DispatchQueue.main.async {
                    
                    guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
                    popupVC.arrTitle = [BottomName.Edit,BottomName.Delete]
                    popupVC.callback =  { str in
                        debugPrint(str)
                        if str == BottomName.Edit
                        {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerVC") as! AnswerVC
                                vcInstace.dictPost = getVC.dictPost
                                vcInstace.dictAnswer = getVC.arrAnswer[indexPath.row]
                                getVC.present(vcInstace, animated: true, completion: nil)
                            })
                            
                        }
                        else if str == BottomName.Delete
                        {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                getVC.showOkCancelAlertWithAction(msg: Constant.kAlertanswer, handler: { (bool) in
                                    if bool
                                    {
                                        getVC.webCall_deleteDiscuss(getVC.arrAnswer[indexPath.row].post_answers_id,callback: {
                                            getVC.arrAnswer.remove(at: indexPath.row)
                                            getVC.tblAdvert?.deleteRows(at: [indexPath], with: .left)
                                        })
                                    }
                                })
                            })
                        }
                    }
                    self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
                }
            }
            
            
            if let getVC = self.tableView?.parentViewController as? VoiceVideoDetailVC {
                
                DispatchQueue.main.async {
                    
                    guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
                    popupVC.arrTitle = [BottomName.Delete]
                    popupVC.callback =  { str in
                        debugPrint(str)
                        
                        if str == BottomName.Delete
                        {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                getVC.showOkCancelAlertWithAction(msg: Constant.kAlertanswer, handler: { (bool) in
                                    if bool
                                    {
                                        getVC.webCall_deleteDiscuss(getVC.arrAnswer[indexPath.row].post_answers_id,callback: {
                                            getVC.arrAnswer.remove(at: indexPath.row)
                                            getVC.tblview?.deleteRows(at: [indexPath], with: .left)
                                        })
                                    }
                                })
                            })
                        }
                    }
                    self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
                }
            }
        }
    }
}

class cellAnswerHeader : UITableViewCell
{
    @IBOutlet var lblTitle: UILabel!

    @IBOutlet var viewCorner: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            self.viewCorner.layer.cornerRadius = self.viewCorner.frame.height/2
        }
    }
    
}



class AnswerListModel {
    
    var admin_id =  ""
    var answer =  ""
    var date =  ""
    var expert_id =  ""
    var is_following =  ""
    var post_answers_id =  ""
    var post_id =  ""
    var status = ""
    var total_upvotes = ""
    var user : OtherUserProfileModel?
    var user_id = ""
    var vv_type = ""
    
    var show_ans = false
    
    var arrImages = [imagePostModel]()
    
    var dicPostLink : PostLinkModel?
    
    var dicVideo : videoPostModel?
    var dicAudio : audioPostModel?

    
    
    init(_ dict : [String:Any]) {
        self.admin_id = toString(dict["admin_id"])
        self.answer = toString(dict["answer"])
        self.date = toString(dict["date"])
        self.expert_id = toString(dict["expert_id"])
        self.is_following = toString(dict["is_following"])
        self.post_answers_id = toString(dict["post_answers_id"])
        self.post_id = toString(dict["post_id"])
        self.status = toString(dict["status"])
        self.total_upvotes = toString(dict["total_upvotes"])
        self.user_id = toString(dict["user_id"])
        
        self.vv_type = toString(dict["vv_type"])
        
        self.show_ans = ConvertToBool(toString(dict["show_ans"]))
        
        
        //For User
        if let dic = dict["user"] as? [String:Any] {
            self.user = OtherUserProfileModel.init(dic)
        }
        if let arr = dict["answer_img"] as? [[String:Any]] {
            self.arrImages = arr.map({imagePostModel.init($0)})
        }
        
        //For POstLink
        if let dic = dict["link"] as? [String:Any] {
            self.dicPostLink = PostLinkModel.init(dic)
        }
        
        //For video
        if let dic = dict["videos"] as? [String:Any] {
            self.dicVideo = videoPostModel.init(dic)
        }
        
        //For audio
           if let dic = dict["audios"] as? [String:Any] {
               self.dicAudio = audioPostModel.init(dic)
           }
        
        
    }
}


