//
//  ExpertVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/4/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class ExpertVC: UIViewController {

    @IBOutlet weak var badgeMsg: UIButton!
    @IBOutlet weak var badgeNoti: UIButton!
    @IBOutlet weak var badgeFriend: UIButton!

//    var refreshControl = UIRefreshControl()
    
    var arrExpertList = [ExpertListModel]()
    
    @IBOutlet weak var colletionExpert: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        badgeMsg.layer.cornerRadius = badgeMsg.frame.height/2
        badgeMsg.layer.masksToBounds = true
        badgeNoti.layer.cornerRadius = badgeNoti.frame.height/2
        badgeNoti.layer.masksToBounds = true
        badgeFriend.layer.cornerRadius = badgeFriend.frame.height/2
        badgeFriend.layer.masksToBounds = true
        self.colletionExpert.refreshControl = UIRefreshControl()
        self.colletionExpert.refreshControl?.tintColor = .AppSkyBlue
        self.colletionExpert.refreshControl?.addTarget(self, action: #selector(refreshCalled), for: UIControl.Event.valueChanged)
        self.getExpertList()
        
        // Do any additional setup after loading the view.
    }
    @objc func refreshCalled() {
        self.getExpertList()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.barTintColor = UIColor.AppSkyBlue
        
       self.setBages()
        appDelegate.funGetnoticount{
            self.setBages()
        }
    }
    func setBages(){
        self.badgeMsg.setTitle(global_count_Msg, for: .normal)
        self.badgeNoti.setTitle(global_count_Noti, for: .normal)
        self.badgeFriend.setTitle(global_count_Friend, for: .normal)
        
        self.badgeMsg.isHidden = global_count_Msg == "0"
        self.badgeNoti.isHidden = global_count_Noti == "0"
        self.badgeFriend.isHidden = global_count_Friend == "0"
    }
    @IBAction func btnSearch(_ sender: UIButton) {
        let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        self.pushTo(vcInstace)
    }
    @IBAction func btnProfile(_ sender: Any) {
        let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigationController?.pushViewController(vcInstace, animated: true)
    }
    @IBAction func btnNotification(_ sender: UIButton) {
        let vcInstace = StoryBoard.Notification.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        self.pushTo(vcInstace)
        
    }
    @IBAction func btnMessage(_ sender: UIButton) {
        let vcInstace = StoryBoard.Message.instantiateViewController(withIdentifier: "MessageVC") as! MessageVC
        self.pushTo(vcInstace)
    }
    @IBAction func btnFriends(_ sender: UIButton) {
        let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "FriendsVC") as! FriendsVC
        self.pushTo(vcInstace)
    }
   
    
    //MARK: API Call
    @objc func getExpertList(_ serviceCount : Int = 0) {
        
        if self.colletionExpert.accessibilityHint == "service_calling" { return }
        
        self.colletionExpert.accessibilityHint = "service_calling"
        var delayTime = DispatchTime.now()
        
        if self.colletionExpert.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.colletionExpert.refreshControl?.beginRefreshingManually()
        }
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.ExpertList, Perameters: ["user_id" : globalUserId],showProgress: false,completion: { (dicRes, success) in
                //                debugPrint(dicRes)
                if let arrData = dicRes["expert_list"] as? [[String:Any]] {
                    self.arrExpertList = arrData.map({ExpertListModel.init($0)})
                }
                
                DispatchQueue.main.async {
                    self.colletionExpert.reloadData()
                                   self.colletionExpert.accessibilityHint = nil
                                   self.colletionExpert.refreshControl?.endRefreshing()
                }
                
            }) { (err) in
                self.colletionExpert.accessibilityHint = nil
                self.colletionExpert.refreshControl?.endRefreshing()
            }
        }
    }
    
    
    
    

    
}


extension ExpertVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrExpertList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "ExportCollectionCell", for: indexPath) as! ExportCollectionCell
        let obj = arrExpertList[indexPath.row]
        cell.lblName.text = obj.FullName
        cell.btnActive.isSelected = obj.is_active
        cell.img.setImageWithURL_Colletion(obj.profile_image,Image.experts_placeholder)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.width/3, height: 114 * screenscale)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if arrExpertList[indexPath.row].user_id == globalUserId{
            self.btnProfile(self)
        }
        else{
            let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "OtherProfileVC") as! OtherProfileVC
            vcInstace.profileId = arrExpertList[indexPath.row].user_id
            self.navigationController?.pushViewController(vcInstace, animated: true)
        }
    }
}



class ExportCollectionCell: UICollectionViewCell {
    
    @IBOutlet var lblName: UILabel!

    @IBOutlet var img: UIImageView!
    @IBOutlet var viewRound: UIView!
    @IBOutlet var btnActive: UIButton!
    
    override func awakeFromNib() {
        
        DispatchQueue.main.async {
            self.img.layer.cornerRadius = self.img.frame.height / 2
            self.img.layer.masksToBounds = true
            self.viewRound.layer.cornerRadius = self.viewRound.frame.height/2
        }
    }
    
    
}


class ExpertListModel {
    
    var user_id =  ""
    var firstname =  ""
    var lastname =  ""
    var FullName =  ""
    var profile_image =  ""
    var profile_image_thumb =  ""
    var is_selected =  false
    var email =  ""
    var is_active =  false
    var profession = ""


    init(_ dict : [String:Any]) {
        
        self.user_id =  toString(dict["user_id"])
        self.firstname = toString(dict["firstname"])
        self.lastname = toString(dict["lastname"])
        self.profession = toString(dict["profession"])
        self.FullName =  (self.firstname + " " + self.lastname).capitalized
        self.profile_image = toString(dict["profile_image"])
        self.profile_image_thumb = toString(dict["profile_image_thumb"])
//        self.is_selected = toString(dict["is_selected"]) == "1"
        self.is_active = ConvertToBool(toString((dict["is_active"])))
    }
   /* "about_me" = "Employee of the African American Horror Story hotel episode of the African American Horror Story hotel episode of the African American Horror Story hotel episode of the African.";
    "about_my_profession" = "Senior android developer";
    address = gandhinagar;
    "apply_reset_pwd" = 0;
    "cover_image" = "http://180.211.113.98:8080/proclapp/upload/201904250948012637411.jpeg";
    date = "2019-03-20 14:31:21";
    "educational_qualification" =             (
    {
    qualification = 10th;
    year = 2008;
    }
    );
    email = "krima@prismetric.com";
    firstname = "Krima 11.";
    gender = 1;
    "interest_categories" =             (
    "Automobile Systems",
    "Autonomous Systems & Tech.",
    "Music & Entertainment",
    "Fashion Industry & Business",
    "Cultures & Traditions",
    "Mobile Technology & Applications",
    "Media & Cinematography"
    );
    "is_expert" = 1;
    lastname = M11;
    "media_id" = "";
    "media_type" = "";
    "member_since" = "2019-03-20 14:31:21";
    "notification_status" = 1;
    password = 7c4a8d09ca3762af61e59520943dc26494f8941b;
    "password_updated" = 1;
    phone = 986564679457088;
    "phone_code" = IN;
    "professional_experience" = "4 years";
    "professional_qualification" = "Android dev.";
    "profile_image" = "http://180.211.113.98:8080/proclapp/upload/201908031153021520611.jpeg";
    "profile_image_thumb" = "http://180.211.113.98:8080/proclapp/upload/thumb/201908031153021520611.jpeg";
    "profile_updated" = 1;
    status = 1;
    token = d24a461cccc2725966d546309aaaab50;
    "total_followers" = 2;
    "total_following" = 1;
    "user_id" = 11;*/
    
}
