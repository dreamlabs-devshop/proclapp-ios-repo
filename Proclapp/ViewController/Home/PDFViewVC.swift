//
//  PDFViewVC.swift
//  Proclapp
//
//  Created by Dreamlabs on 07/04/2021.
//  Copyright © 2021 Ashish Parmar. All rights reserved.
//

import UIKit
import PDFKit

class PDFViewVC: UIViewController {
    
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var pdfUrl:String = ""
    var pdfTitle:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pdfView = PDFView(frame: view.frame)
        indicator?.startAnimating()
        self.indicator?.hidesWhenStopped = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            
            if let url = URL(string: self.pdfUrl),
                let pdfDocument = PDFDocument(url: url) {
                pdfView.displayMode = .singlePageContinuous
                pdfView.autoScales = true
                pdfView.displayDirection = .vertical
                pdfView.document = pdfDocument
                
                self.mainView.addSubview(pdfView)
                
                self.indicator?.stopAnimating()
                
            }
        }
    }
    
    
}
    
