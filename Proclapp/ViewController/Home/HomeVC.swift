//
//  HomeVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 3/27/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import QuickLook

var arrSuggestedFriends = [SuggestedFriendModel]()
var arrSuggestedExpert = [SuggestedExpertModel]()
var arrSuggestedTopics = [TrendingListModel]()

class HomeVC: UIViewController,UIScrollViewDelegate,UITabBarControllerDelegate {
    
    var isMore_Home = false
    var isMore_QA = false
    var isMore_AD = false
    var isMore_VV = false
    var isMore_RP = false

    
    @IBOutlet weak var indicator_Home: UIActivityIndicatorView!
    @IBOutlet weak var indicator_QA: UIActivityIndicatorView!
    @IBOutlet weak var indicator_AD: UIActivityIndicatorView!
    @IBOutlet weak var indicator_VV: UIActivityIndicatorView!
    @IBOutlet weak var indicator_RP: UIActivityIndicatorView!

    @IBOutlet weak var tabHeaderScrollView: UIScrollView!
    
    @IBOutlet weak var viewHome: UIView!
    @IBOutlet weak var viewMenuHeader: UIView!
    @IBOutlet weak var viewOnlineDot: UIView!
    
   
    @IBOutlet weak var tableMenu: UITableView!
    @IBOutlet weak var tableHome: UITableView!
    @IBOutlet weak var tableviewRP: UITableView!
    @IBOutlet weak var tableviewQA: UITableView!
    @IBOutlet weak var tableviewAD: UITableView!
    @IBOutlet weak var tableviewVV: UITableView!
    
    @IBOutlet weak var scrollMain: UIScrollView!
    
    @IBOutlet weak var btnfeed: UIButton!
    @IBOutlet weak var btnqa: UIButton!
    @IBOutlet weak var btnad: UIButton!
    @IBOutlet weak var btnvv: UIButton!
    @IBOutlet weak var btnrp: UIButton!
    @IBOutlet weak var btnPost: UIButton!

    @IBOutlet weak var imageProfile: UIImageView!
    
    @IBOutlet weak var btnLogoutBottomConst: NSLayoutConstraint!

    @IBOutlet weak var lblconstantcenter: NSLayoutConstraint!
    
    //Side Menu
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPost: UILabel!
    
    @IBOutlet weak var badgeMsg: UIButton!
    @IBOutlet weak var badgeNoti: UIButton!
    @IBOutlet weak var badgeFriend: UIButton!
    

    @IBOutlet weak var menuView: UIView!
    
    @IBOutlet weak var qaView: UIView!
    
    @IBOutlet weak var adView: UIView!
    
    @IBOutlet weak var mainStackView: UIStackView!
    
    var arrMenu = menuModel.getTempStructure
    
    //Home
    var arrALLHomePost = [allPostModel]()
    var arrArticle = [allPostModel]()
    var arrQuestion = [allPostModel]()
    var arrVoiceVideo = [allPostModel]()
    var arrResearchPaper = [allPostModel]()



    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
       badgeMsg.layer.cornerRadius = badgeMsg.frame.height/2
       badgeMsg.layer.masksToBounds = true

       badgeNoti.layer.cornerRadius = badgeNoti.frame.height/2
       badgeNoti.layer.masksToBounds = true
       
  
       badgeFriend.layer.cornerRadius = badgeFriend.frame.height/2
       badgeFriend.layer.masksToBounds = true
        
        navigationController?.navigationBar.barTintColor = UIColor.AppSkyBlue
        self.navigationController?.isNavigationBarHidden = false
        
        DispatchQueue.main.async {
            let tabBarHeight = CGFloat((self.tabBarController?.tabBar.frame.size.height)!)
            self.btnLogoutBottomConst.constant =  tabBarHeight + 70 
            self.view.layoutIfNeeded()
            
//            self.tableHome.reloadData()
            
            self.imageProfile.layer.cornerRadius =  self.imageProfile.frame.height/2
            self.viewOnlineDot.layer.cornerRadius = self.viewOnlineDot.frame.height/2
            self.viewOnlineDot.isHidden = globalUserId == ""
           self.indiCatorSetup()

        }
        
        
        

        viewMenuHeader.layer.masksToBounds = true
        viewMenuHeader.clipsToBounds = true
        
        viewMenuHeader.frame = CGRect.init(x: 0, y: 0, width: screenwidth, height: 110 * screenscale)
        
        tableMenu.tableHeaderView = viewMenuHeader
        
        callTableCellRegister()
        
        self.tabBarController?.delegate = self
        
       
        
        self.tableHome.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 120, right: 0)
        self.tableviewQA.contentInset = self.tableHome.contentInset
        self.tableviewAD.contentInset = self.tableHome.contentInset
        self.tableviewVV.contentInset = self.tableHome.contentInset
        self.tableviewRP.contentInset = self.tableHome.contentInset
        self.tableMenu.contentInset = self.tableHome.contentInset



        self.tableHome.refreshControl = UIRefreshControl()
        self.tableHome.refreshControl?.tintColor = .AppSkyBlue
        self.tableHome.refreshControl?.addTarget(self, action: #selector(refreshCalled_AllPost), for: UIControl.Event.valueChanged)
        self.getAllPost()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.NewDataLoadHome), name: .NewDataLoadHome, object: nil)

//        NotificationCenter.default.addObserver(self, selector: #selector(self.DeclineQuestion), name: .DeclineQuestion, object: nil)

//        NotificationCenter.default.addObserver(self, selector: #selector(self.DeclineArticle), name: .DeclineArticle, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.Delete_Post), name: .Delete_Post, object: nil)

        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.funUpdateToken()
            connectSocket()
        }
        
        
       
//        mainStackView.frame.size.width = 3500
//        menuView.frame.size.width = screenwidth;
//        qaView.frame.size.width = screenwidth;
//        adView.frame.size.width = screenwidth;
        // Do any additional setup after loading the view.
    }
    
    @objc func NewDataLoadHome(notification: NSNotification){
        DispatchQueue.main.async {
            self.homeLoad()
            self.getAllPost()
        }
    }
    
    func homeLoad()
    {
        self.btnPost.isHidden = false
        self.tableHome.reloadData()
        self.viewHome.isHidden = false
        self.lblconstantcenter.constant = -100
        self.view.layoutIfNeeded()
    }
  /*  @objc func DeclineQuestion(notification: NSNotification){
        
        DispatchQueue.main.async {
            let removePostId = toString(notification.object)
            if let find_index = (self.arrALLHomePost).firstIndex(where: {$0.post_id == removePostId}) {
                self.arrALLHomePost.remove(at: find_index)
            }
            if let find_index = (self.arrQuestion).firstIndex(where: {$0.post_id == removePostId}) {
                self.arrQuestion.remove(at: find_index)
            }
            self.tableHome.reloadData()
            self.tableviewQA.reloadData()
        }
    }

    @objc func DeclineArticle(notification: NSNotification){
        DispatchQueue.main.async {
            let removePostId = toString(notification.object)
            if let find_index = (self.arrALLHomePost).firstIndex(where: {$0.post_id == removePostId}) {
                self.arrALLHomePost.remove(at: find_index)
            }
            if let find_index = (self.arrArticle).firstIndex(where: {$0.post_id == removePostId}) {
                self.arrArticle.remove(at: find_index)
            }
            self.tableHome.reloadData()
            self.tableviewAD.reloadData()
        }
    }*/
    
    @objc func Delete_Post(notification: NSNotification){
        
        if  let dic = notification.object as? [String : Any]{
            let removePostId = (toString(dic["id"]))
            let getType = PostTypeEnum.init(rawValue: ToInt(dic["type"]))
//            debugPrint(toString(dic["type"]))
            
            if getType == .ResearchPapers{
                DispatchQueue.main.async {
                    if let find_index = (self.arrALLHomePost).firstIndex(where: {$0.post_id == removePostId}) {
                        self.arrALLHomePost.remove(at: find_index)
                    }
                    if let find_index = (self.arrResearchPaper).firstIndex(where: {$0.post_id == removePostId}) {
                        self.arrResearchPaper.remove(at: find_index)
                    }
                    self.tableHome.reloadData()
                    self.tableviewRP.reloadData()
                }
            }
            else if getType == .Question
            {
                DispatchQueue.main.async {
                    if let find_index = (self.arrALLHomePost).firstIndex(where: {$0.post_id == removePostId}) {
                        self.arrALLHomePost.remove(at: find_index)
                    }
                    if let find_index = (self.arrQuestion).firstIndex(where: {$0.post_id == removePostId}) {
                        self.arrQuestion.remove(at: find_index)
                    }
                    self.tableHome.reloadData()
                    self.tableviewQA.reloadData()
                }
            }
            else if getType == .Article
            {
                DispatchQueue.main.async {
                    if let find_index = (self.arrALLHomePost).firstIndex(where: {$0.post_id == removePostId}) {
                        self.arrALLHomePost.remove(at: find_index)
                    }
                    if let find_index = (self.arrArticle).firstIndex(where: {$0.post_id == removePostId}) {
                        self.arrArticle.remove(at: find_index)
                    }
                    self.tableHome.reloadData()
                    self.tableviewAD.reloadData()
                }
            }
            
        }
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = UIColor.AppSkyBlue
        sideMenuDataSet()
        self.navigationController?.isNavigationBarHidden = false
        
       self.setBages()
        appDelegate.funGetnoticount{
            self.setBages()
        }
        /*if loginData?.screen_code == "222"{
         self.btnProfile(self)
         }*/
        
    }
    func setBages(){
        self.badgeMsg.setTitle(global_count_Msg, for: .normal)
        self.badgeNoti.setTitle(global_count_Noti, for: .normal)
        self.badgeFriend.setTitle(global_count_Friend, for: .normal)
        
        self.badgeMsg.isHidden = global_count_Msg == "0"
        self.badgeNoti.isHidden = global_count_Noti == "0"
        self.badgeFriend.isHidden = global_count_Friend == "0"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setTabBarHideGBL(navCtrl: self.navigationController!, isHide: false)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setTabBarHideGBL(navCtrl: self.navigationController!, isHide: true)
    }
        
    
    func sideMenuDataSet(){
            self.lblUserName.text = loginData?.fullName.capitalized
            self.lblPost.text = loginData?.profession
            self.imageProfile.setImageWithURL(loginData?.profile_image, "dp_menu")
    }
 
    
    
    
    
    func funUpdateToken(_ serviceCount : Int = 0) {
        
        if !globalUserId.isValid{ return }
        
        let dic = ["user_id" : globalUserId,"device_type":"ios","device_name":ToString(UIDevice.current.name),"device_id":ToString(UIDevice.current.systemVersion)  ,"device_token":GlobalDeviceToken]
//        debugPrint(dic)
        WebService.shared.RequesURL(ServerURL.UpdateDeviceToken, Perameters: dic,showProgress: false,completion: { (dicRes, success) in
//            debugPrint(dicRes)
        }) { (err) in
            if serviceCount < 1 {
                self.funUpdateToken(serviceCount + 1)
            } else {
//                debugPrint(err)
            }
        }
    }
    
    
    //Indicator
    func indiCatorSetup() {
        
        self.indicator_Home.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tableHome.bounds.width, height: CGFloat(30))
        self.tableHome.tableFooterView = self.indicator_Home
        
        self.indicator_QA.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tableviewQA.bounds.width, height: CGFloat(30))
        self.tableviewQA.tableFooterView = self.indicator_QA
        
//        self.tableviewQA.contentSize = CGSize(width: screenwidth, height: CGFloat((self.tabBarController?.tabBar.frame.size.height)!))
        
        
        self.indicator_AD.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tableviewAD.bounds.width, height: CGFloat(30))
        self.tableviewAD.tableFooterView = self.indicator_AD
        
        self.indicator_VV.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tableviewVV.bounds.width, height: CGFloat(30))
        self.tableviewVV.tableFooterView = self.indicator_VV
        
        self.indicator_RP.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tableviewRP.bounds.width, height: CGFloat(30))
        self.tableviewRP.tableFooterView = self.indicator_RP
    }
    
    func callTableCellRegister(){
        
        //tableHome.register(UINib.init(nibName: "AnswerTableCell", bundle: nil), forCellReuseIdentifier: "AnswerTableCell")
        
       /* tableHome.register(UINib.init(nibName: "downloadPDFTableCell", bundle: nil), forCellReuseIdentifier: "downloadPDFTableCell")
        tableHome.register(UINib.init(nibName: "QuestionAnswerTableViewCell", bundle: nil), forCellReuseIdentifier: "QuestionAnswerTableViewCell")
        tableHome.register(UINib.init(nibName: "ArticleDeclineTableCell", bundle: nil), forCellReuseIdentifier: "ArticleDeclineTableCell")
        tableHome.register(UINib.init(nibName: "AudioTableViewCell", bundle: nil), forCellReuseIdentifier: "AudioTableViewCell")
        tableHome.register(UINib.init(nibName: "VideoTableViewCell", bundle: nil), forCellReuseIdentifier: "VideoTableViewCell")
        tableHome.register(UINib.init(nibName: "AdvertTableCell", bundle: nil), forCellReuseIdentifier: "AdvertTableCell")

        tableHome.register(UINib.init(nibName: "SuggestedTblCell", bundle: nil), forCellReuseIdentifier: "SuggestedTblCell")

        
        
        //QA
        tableviewQA.register(UINib.init(nibName: "QuestionAnswerTableViewCell", bundle: nil), forCellReuseIdentifier: "QuestionAnswerTableViewCell")
        tableviewQA.register(UINib.init(nibName: "AdvertTableCell", bundle: nil), forCellReuseIdentifier: "AdvertTableCell")
      
        tableviewQA.register(UINib.init(nibName: "SuggestedTblCell", bundle: nil), forCellReuseIdentifier: "SuggestedTblCell")

        
        //RP
        tableviewRP.register(UINib.init(nibName: "downloadPDFTableCell", bundle: nil), forCellReuseIdentifier: "downloadPDFTableCell")
        tableviewRP.register(UINib.init(nibName: "AdvertTableCell", bundle: nil), forCellReuseIdentifier: "AdvertTableCell")

        
        //AD
        tableviewAD.register(UINib.init(nibName: "ArticleDeclineTableCell", bundle: nil), forCellReuseIdentifier: "ArticleDeclineTableCell")
        
       
//        tableviewAD.register(UINib.init(nibName: "ArticleDeclineSingleImageTableCell", bundle: nil), forCellReuseIdentifier: "ArticleDeclineSingleImageTableCell")

        
        
        tableviewAD.register(UINib.init(nibName: "AdvertTableCell", bundle: nil), forCellReuseIdentifier: "AdvertTableCell")
        
        tableviewAD.register(UINib.init(nibName: "SuggestedTblCell", bundle: nil), forCellReuseIdentifier: "SuggestedTblCell")


        //VV
        tableviewVV.register(UINib.init(nibName: "VideoTableViewCell", bundle: nil), forCellReuseIdentifier: "VideoTableViewCell")
        tableviewVV.register(UINib.init(nibName: "AudioTableViewCell", bundle: nil), forCellReuseIdentifier: "AudioTableViewCell")
        tableviewVV.register(UINib.init(nibName: "AdvertTableCell", bundle: nil), forCellReuseIdentifier: "AdvertTableCell")*/

        
        [tableviewAD, tableHome,tableviewRP,tableviewQA,tableviewVV].forEach { (tbl) in
                   
                   
                   tbl?.register(UINib.init(nibName: "downloadPDFTableCell", bundle: nil), forCellReuseIdentifier: "downloadPDFTableCell")
                   
                   tbl?.register(UINib.init(nibName: "QuestionAnswerTableViewCell", bundle: nil), forCellReuseIdentifier: "QuestionAnswerTableViewCell")
                   
                   tbl?.register(UINib.init(nibName: "ArticleDeclineTableCell", bundle: nil), forCellReuseIdentifier: "ArticleDeclineTableCell")
                   
                   tbl?.register(UINib.init(nibName: "AudioTableViewCell", bundle: nil), forCellReuseIdentifier: "AudioTableViewCell")
                   
                   tbl?.register(UINib.init(nibName: "VideoTableViewCell", bundle: nil), forCellReuseIdentifier: "VideoTableViewCell")
                  
                   tbl?.register(UINib.init(nibName: "AdvertTableCell", bundle: nil), forCellReuseIdentifier: "AdvertTableCell")

                   tbl?.register(UINib.init(nibName: "SuggestedTblCell", bundle: nil), forCellReuseIdentifier: "SuggestedTblCell")

                   tbl?.register(UINib.init(nibName: "ArticleDeclineSingleImageTableCell", bundle: nil), forCellReuseIdentifier: "ArticleDeclineSingleImageTableCell")
               }
        
        //register the header view
        
        let nib = UINib(nibName: "MenuHeader", bundle: nil)
        tableMenu.register(nib, forHeaderFooterViewReuseIdentifier: "MenuHeader")
        
        
    }
    
    //MARK:- Feed ACTION SECTION
    
    @IBAction func btnallFeed(_ sender: UIButton) {
        self.btnPost.isHidden = true
        self.ScrollButton(sender: sender)
        scrollMain.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
    }
    
    @IBAction func btnQA(_ sender: UIButton) {
        self.ScrollButton(sender: sender)
        scrollMain.setContentOffset(CGPoint.init(x: screenwidth, y: 0), animated: true)
//        menuView.bounds.size.width = screenwidth
//        qaView.bounds.size.width = UIScreen.main.bounds.width
//        adView.frame.size.width = screenwidth
        
    }
    
    @IBAction func btnVV(_ sender: UIButton) {
        self.ScrollButton(sender: sender)
        scrollMain.setContentOffset(CGPoint.init(x: screenwidth*3, y: 0), animated: true)
    }
    
    @IBAction func btnAD(_ sender: UIButton) {
        
        self.ScrollButton(sender: sender)
        scrollMain.setContentOffset(CGPoint.init(x: screenwidth*2, y: 0), animated: true)
        
    }
    @IBAction func btnRP(_ sender: UIButton) {
        self.ScrollButton(sender: sender)
        scrollMain.setContentOffset(CGPoint.init(x: screenwidth*4, y: 0), animated: true)
    }
    @IBAction func btnSearch(_ sender: UIButton) {
        
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        
        let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        self.pushTo(vcInstace)
    }
    @IBAction func btnProfile(_ sender: Any)
    {
        guard globalUserId != "" else {
            appDelegate.showAlertGuest()
            return
        }
        let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigationController?.pushViewController(vcInstace, animated: true)
    }
    @IBAction func btnNotification(_ sender: UIButton) {
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        let vcInstace = StoryBoard.Notification.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        self.pushTo(vcInstace)
        
    }
    @IBAction func btnMessage(_ sender: UIButton) {
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        let vcInstace = StoryBoard.Message.instantiateViewController(withIdentifier: "MessageVC") as! MessageVC
        self.pushTo(vcInstace)
    }
    @IBAction func btnFriends(_ sender: UIButton) {
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "FriendsVC") as! FriendsVC
        self.pushTo(vcInstace)
    }
    
    @IBAction func btnLogout(_ sender: UIButton) {
        if globalUserId == ""{
            resetData()
            appDelegate.LoadLoginView()
        }
        else{
            self.showOkCancelAlertWithAction(msg: Constant.kAlertLogout) { (bool) in
                if bool{
                    self.logoutWebserviceCall()
                }
            }
        }
    }
    
    func logoutWebserviceCall(_ serviceCount : Int = 0) {
        WebService.shared.RequesURL(ServerURL.LogOut, Perameters: ["user_id" : globalUserId,"device_token":GlobalDeviceToken],showProgress: true,completion: { (dicRes, success) in
//            debugPrint(dicRes)
            if success{
                resetData()
                appDelegate.LoadLoginView()
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            if serviceCount < 1 {
                self.logoutWebserviceCall(serviceCount + 1)
            } else {
                debugPrint(err)
            }
        }
    }

    
    
    @IBAction func btnAddPost(_ sender: UIButton) {

        guard globalUserId != "" else {
            appDelegate.showAlertGuest()
            return
        }
        
        let popVC = StoryBoard.AddPost.instantiateViewController(withIdentifier: "AskPopupVC") as! AskPopupVC
        popVC.modalPresentationStyle = .overCurrentContext
        popVC.modalTransitionStyle = .crossDissolve
        popVC.callback =  { sender in

            if sender == 1{
                let vc = StoryBoard.AddPost.instantiateViewController(withIdentifier: "AskQuestionVC") as! AskQuestionVC
                self.pushTo(vc)
            }
            else if sender == 2{
                let vc = StoryBoard.AddPost.instantiateViewController(withIdentifier: "CreateArticleVC") as! CreateArticleVC
                self.pushTo(vc)
                
            }
            else if sender == 3{
                let vc =  StoryBoard.AddPost.instantiateViewController(withIdentifier: "SelectCategoryVC") as! SelectCategoryVC
                self.pushTo(vc)
                
            }
            else if sender == 4{
                let vc = StoryBoard.AddPost.instantiateViewController(withIdentifier: "UploadResearchPapersVC") as! UploadResearchPapersVC
                
                self.pushTo(vc)
            }
            else if sender == 5{
                let vc = StoryBoard.Advert.instantiateViewController(withIdentifier: "SelectTwoCategoryVC") as! SelectTwoCategoryVC
                
                self.pushTo(vc)
            }
            else if sender == 6{
                let vc =  StoryBoard.AddPost.instantiateViewController(withIdentifier: "UploadVideoVC") as! UploadVideoVC
                
                self.pushTo(vc)
                
            }

        }
        self.tabBarController?.present(popVC, animated: true, completion: nil)

//        self.addChild(popVC)
////        popVC.view.frame = self.view.frame
//        self.view.addSubview(popVC.view)
//        popVC.didMove(toParent: self)
    }
    
    
    
    @objc func btnMenuPressed(_ sender: UIButton) {
        
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        
        let objSubMenu = sender.accessibilityHint ?? ""
        
//        debugPrint("Title",objSubMenu)
        
        if objSubMenu == "Answered" {
            let vc = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerUnanswerVC") as! AnswerUnanswerVC
            vc.isPost = true
            self.pushTo(vc)
        }
        else if objSubMenu == "Unanswered" {
            let vc = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerUnanswerVC") as! AnswerUnanswerVC
            self.pushTo(vc)
        }
        else if objSubMenu == "Discussed" {
            let vc = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerUnanswerVC") as! AnswerUnanswerVC
            vc.isPost = true
            vc.isFromDiscuss = true
            self.pushTo(vc)
        }
        else if objSubMenu == "Undiscussed" {
            let vc = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerUnanswerVC") as! AnswerUnanswerVC
            vc.isFromDiscuss = true
            self.pushTo(vc)
        }
    }
    
    //MARK: CHNAGE LABLE ANIMATION
    func ScrollButton(sender:UIButton){
        
        self.viewHome.isHidden = true
        UIView.animate(withDuration: 0.2) {
            self.lblconstantcenter.constant = sender.frame.origin.x
        }
        
        if sender == btnad
        {
             self.tableviewAD.reloadData()

            if arrArticle.count == 0 {
                self.tableviewAD.refreshControl = UIRefreshControl()
                self.tableviewAD.refreshControl?.tintColor = .AppSkyBlue
                self.tableviewAD.refreshControl?.addTarget(self, action: #selector(refreshCalled_Article), for: UIControl.Event.valueChanged)
                self.getArticleList()
            }
        }
       else if sender == btnqa
        {
            self.tableviewQA.reloadData()
            
            if arrQuestion.count == 0 {
                self.tableviewQA.refreshControl = UIRefreshControl()
                self.tableviewQA.refreshControl?.tintColor = .AppSkyBlue
                self.tableviewQA.refreshControl?.addTarget(self, action: #selector(refreshCalled_Question), for: UIControl.Event.valueChanged)
                self.getQuestionList()
            }
        }
        else if sender == btnvv
        {
            self.tableviewVV.reloadData()
            
            if arrVoiceVideo.count == 0 {
                self.tableviewVV.refreshControl = UIRefreshControl()
                self.tableviewVV.refreshControl?.tintColor = .AppSkyBlue
                self.tableviewVV.refreshControl?.addTarget(self, action: #selector(refreshCalled_VoiceVideo), for: UIControl.Event.valueChanged)
                self.getVoiceVideoList()
            }
        }
        else if sender == btnrp
        {
             self.tableviewRP.reloadData()
            
            if arrResearchPaper.count == 0 {
                self.tableviewRP.refreshControl = UIRefreshControl()
                self.tableviewRP.refreshControl?.tintColor = .AppSkyBlue
                self.tableviewRP.refreshControl?.addTarget(self, action: #selector(refreshCalled_Researchpaper), for: UIControl.Event.valueChanged)
                self.getResearchPaperList()
            }
        }
        
        self.view.layoutIfNeeded()
    }
    
    //MARK:-  SCROLLVIEW Delegate
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView == scrollMain{
            let  intpage = scrollView.contentOffset.x / UIScreen.main.bounds.width
            if intpage == 0{
//                self.tableMenu.reloadData()
                self.ScrollButton(sender: btnfeed)
            }
            else if intpage == 1{
//                self.tableviewQA.reloadData()
                self.ScrollButton(sender: btnqa)
            }
            else if intpage == 2{
//                self.tableviewAD.reloadData()
                self.ScrollButton(sender: btnad)
            }
            else if intpage == 3{
//                self.tableviewVV.reloadData()
                self.ScrollButton(sender: btnvv)
            }
            else{
//                self.tableviewRP.reloadData()
                self.ScrollButton(sender: btnrp)
            }
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == scrollMain{
            self.btnPost.isHidden = ToInt(scrollView.contentOffset.x / UIScreen.main.bounds.width) == 0
        }
    }
    
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        
        if tabBarIndex == 2
        {
           self.homeLoad()
        }
    }
    
    
    
    /* guard let popupVC = storyboard?.instantiateViewController(withIdentifier: "CalandarVC") as? CalandarVC else { return }
     present(popupVC, animated: true, completion: nil)*/
}
    

//MARK: - Table view data source
extension HomeVC: UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == tableMenu{return arrMenu.count}
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tableHome{
            return arrALLHomePost.count
        }
        else  if tableView == tableviewQA{
            return arrQuestion.count
        }
        else  if tableView == tableviewAD{
            return arrArticle.count
        }
        else  if tableView == tableviewVV{
            return arrVoiceVideo.count
        }
        else  if tableView == tableviewRP{
            return arrResearchPaper.count
        }
        else if tableView == tableMenu {
            let objMenu = arrMenu[section]
            return objMenu.isOpened ? objMenu.submenu.count : 0
        }
        else
        {
            return 0
        }
    }
    
    /*func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tableMenu
        {
            let objSubMenu = arrMenu[indexPath.section].submenu[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "subMenuTablecell") as! subMenuTablecell
            cell.lblTitle.text = objSubMenu.title
            
            cell.btnTitleClick.accessibilityHint = objSubMenu.title
            
            cell.btnTitleClick.tag = indexPath.row
            cell.btnTitleClick.addTarget(self, action: #selector(btnMenuPressed(_:)), for: .touchUpInside)
            
            cell.selectionStyle = .none
            cell.backgroundColor = .white
            return cell
        }
            else if tableView == tableHome
        {
            let dic = self.arrALLHomePost[indexPath.row]
            
            if dic.postType == .Article, dic.suggested_Friend == false,dic.suggested_Expert == false,dic.suggested_Topics == false
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleDeclineTableCell") as! ArticleDeclineTableCell
                cell.model = dic
                return cell
            }
            else if dic.postType == .Question, dic.suggested_Friend == false,dic.suggested_Expert == false,dic.suggested_Topics == false
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionAnswerTableViewCell") as! QuestionAnswerTableViewCell
                cell.model = dic
                return cell
            }
            else if dic.postType == .VoiceVideo, dic.suggested_Friend == false,dic.suggested_Expert == false,dic.suggested_Topics == false
            {
                let dic = self.arrVoiceVideo[indexPath.row]
                
                if dic.postType == .VoiceVideo
                {
                    if dic.audioVideoType == .audio
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "AudioTableViewCell") as! AudioTableViewCell
                        cell.model = dic
                        return cell
                    }
                    else
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell") as! VideoTableViewCell
                        cell.model = dic
                        return cell
                    }
                }
                
            }
            else if dic.postType == .ResearchPapers, dic.suggested_Friend == false,dic.suggested_Expert == false,dic.suggested_Topics == false
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "downloadPDFTableCell") as! downloadPDFTableCell
                cell.model = dic
                return cell
            }
            else if dic.postType == .Advert, dic.suggested_Friend == false,dic.suggested_Expert == false,dic.suggested_Topics == false
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AdvertTableCell") as! AdvertTableCell
                cell.model = dic
                cell.setUpCollection()
                return cell
            }
            else if dic.suggested_Friend == true
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestedTblCell") as! SuggestedTblCell
                cell.clvSuggested.accessibilityHint = "Friend"
                cell.setUpCollection()
                return cell
            }
            else if dic.suggested_Expert == true
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestedTblCell") as! SuggestedTblCell
                //                cell.model = dic
                cell.clvSuggested.accessibilityHint = "Expert"
                cell.setUpCollection()
                return cell
            }
            else if dic.suggested_Topics == true
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestedTblCell") as! SuggestedTblCell
                cell.clvSuggested.accessibilityHint = "Topics"
                cell.setUpCollection()
                return cell
            }
            return UITableViewCell()
            
        }
            
        /*else if tableView == tableHome
        {
            if self.arrALLHomePost.count == 0 { return UITableViewCell()}
            
            switch self.arrALLHomePost[indexPath.row].postType {
                
            case .Article:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleDeclineTableCell") as! ArticleDeclineTableCell
                
                cell.model = arrALLHomePost[indexPath.row]
                
                /* cell.btnFlash.addTarget(self, action: #selector(tapFlash(_:)), for: .touchUpInside)
                 cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
                 cell.btnDot.accessibilityHint = "tableHome"*/
                
                
                return cell
                
            case .Question:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionAnswerTableViewCell") as! QuestionAnswerTableViewCell
                
                
                cell.model = arrALLHomePost[indexPath.row]
                
                
                /*cell.btnFlash.addTarget(self, action: #selector(tapFlash(_:)), for: .touchUpInside)
                 cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
                 cell.btnDot.accessibilityHint = "tableHome"*/
                return cell
                
            case .VoiceVideo:
                
                switch self.arrALLHomePost[indexPath.row].audioVideoType{
                    
                case .audio:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "AudioTableViewCell") as! AudioTableViewCell
                    
                    cell.model = arrALLHomePost[indexPath.row]
                    
                    /*cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
                     cell.btnDot.accessibilityHint = "tableHome"*/
                    
                    return cell
                    
                case .video:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell") as! VideoTableViewCell
                    
                    cell.model = arrALLHomePost[indexPath.row]
                    
                    /* cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
                     cell.btnDot.accessibilityHint = "tableviewVV"*/
                    
                    return cell
                }
                
                
            case .ResearchPapers:
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "downloadPDFTableCell") as! downloadPDFTableCell
                
                cell.model = arrALLHomePost[indexPath.row]
                
                
                /*cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
                 cell.btnDot.accessibilityHint = "tableHome"*/
                
                return cell
                
            case .Advert:
                let cell = tableView.dequeueReusableCell(withIdentifier: "AdvertTableCell") as! AdvertTableCell
                cell.model = arrALLHomePost[indexPath.row]
                cell.setUpCollection()
                return cell
            }
            
            //comment answer mate
            
            /* let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerTableCell") as! AnswerTableCell
             
             cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
             cell.btnDot.accessibilityHint = "tableHome"
             
             
             cell.selectionStyle = .none
             return cell*/
            
            
            
        }*/
        else if tableView == tableviewRP
        {
            let dic = self.arrResearchPaper[indexPath.row]
            
            if dic.postType == .ResearchPapers
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "downloadPDFTableCell") as! downloadPDFTableCell
                cell.model = dic
                return cell
            }
            else  if dic.postType == .Advert
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AdvertTableCell") as! AdvertTableCell
                cell.model = dic
                cell.setUpCollection()
                return cell
                
                
            }
            return UITableViewCell()
        }
        else if tableView == tableviewAD
        {
            let dic = self.arrArticle[indexPath.row]
            
            if dic.postType == .Article, dic.suggested_Friend == false,dic.suggested_Expert == false,dic.suggested_Topics == false
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleDeclineTableCell") as! ArticleDeclineTableCell
                cell.model = dic
                return cell
            }
            else if dic.postType == .Advert, dic.suggested_Friend == false,dic.suggested_Expert == false,dic.suggested_Topics == false
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AdvertTableCell") as! AdvertTableCell
                cell.model = dic
                cell.setUpCollection()
                return cell
            }
            else if dic.suggested_Friend == true
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestedTblCell") as! SuggestedTblCell
                cell.clvSuggested.accessibilityHint = "Friend"
                cell.setUpCollection()
                return cell
            }
            else if dic.suggested_Expert == true
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestedTblCell") as! SuggestedTblCell
                //                cell.model = dic
                cell.clvSuggested.accessibilityHint = "Expert"
                cell.setUpCollection()
                return cell
            }
            else if dic.suggested_Topics == true
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestedTblCell") as! SuggestedTblCell
                cell.clvSuggested.accessibilityHint = "Topics"
                cell.setUpCollection()
                return cell
            }
            return UITableViewCell()
            
            /*let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleDeclineTableCell") as! ArticleDeclineTableCell
             
             cell.btnDot.addTarget(self, action: #selector(tapThreeDot(_:)), for: .touchUpInside)
             cell.btnDot.accessibilityHint = "tableviewAD"
             
             cell.btnFlash.addTarget(self, action: #selector(tapFlash(_:)), for: .touchUpInside)
             cell.selectionStyle = .none
             return cell*/
        }
        else if tableView == tableviewVV
        {
            let dic = self.arrVoiceVideo[indexPath.row]
            
            if dic.postType == .VoiceVideo
            {
                if dic.audioVideoType == .audio
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "AudioTableViewCell") as! AudioTableViewCell
                    cell.model = dic
                    return cell
                }
                else
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell") as! VideoTableViewCell
                    cell.model = dic
                    return cell
                }
            }
            else  if dic.postType == .Advert
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AdvertTableCell") as! AdvertTableCell
                cell.model = dic
                cell.setUpCollection()
                return cell
            }
            return UITableViewCell()
        }
        else 
        {
            let dic = self.arrQuestion[indexPath.row]
            
            if dic.postType == .Question, dic.suggested_Friend == false,dic.suggested_Expert == false,dic.suggested_Topics == false
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionAnswerTableViewCell") as! QuestionAnswerTableViewCell
                cell.model = dic
                return cell
            }
            else  if dic.postType == .Advert, dic.suggested_Friend == false,dic.suggested_Expert == false,dic.suggested_Topics == false
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AdvertTableCell") as! AdvertTableCell
                cell.model = dic
                cell.setUpCollection()
                return cell
            }
            else if dic.suggested_Friend == true
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestedTblCell") as! SuggestedTblCell
                cell.clvSuggested.accessibilityHint = "Friend"
                cell.setUpCollection()
                return cell
            }
            else if dic.suggested_Expert == true
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestedTblCell") as! SuggestedTblCell
                cell.clvSuggested.accessibilityHint = "Expert"
                cell.setUpCollection()
                return cell
            }
            else if dic.suggested_Topics == true
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestedTblCell") as! SuggestedTblCell
                cell.clvSuggested.accessibilityHint = "Topics"
                cell.setUpCollection()
                return cell
            }
            return UITableViewCell()
        }
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tableMenu
        {
            let objSubMenu = arrMenu[indexPath.section].submenu[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "subMenuTablecell") as! subMenuTablecell
            cell.lblTitle.text = objSubMenu.title
            
            cell.btnTitleClick.accessibilityHint = objSubMenu.title
            
            cell.btnTitleClick.tag = indexPath.row
            cell.btnTitleClick.addTarget(self, action: #selector(btnMenuPressed(_:)), for: .touchUpInside)
            
            cell.selectionStyle = .none
            cell.backgroundColor = .white
            return cell
        }
        else
        {
            var tempDict : allPostModel?
            
            if tableView == tableHome
            {
                tempDict = self.arrALLHomePost[indexPath.row]
            }
            else if tableView == tableviewRP
            {
                tempDict = self.arrResearchPaper[indexPath.row]
            }
            else if tableView == tableviewAD
            {
                tempDict = self.arrArticle[indexPath.row]
            }
            else if tableView == tableviewVV
            {
                tempDict = self.arrVoiceVideo[indexPath.row]
            }
            else  if tableView == tableviewQA
            {
                tempDict = self.arrQuestion[indexPath.row]
            }

            guard let dic = tempDict else {
                return UITableViewCell()
            }
            
            if dic.postType == .Article, dic.suggested_Friend == false,dic.suggested_Expert == false,dic.suggested_Topics == false
            {
                
               /* let imgesCount = ToInt((dic.arrImages.count))
                if imgesCount == 1
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleDeclineSingleImageTableCell") as! ArticleDeclineTableCell
                    cell.model = dic
                    return cell
                }
                else
                {*/
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleDeclineTableCell") as! ArticleDeclineTableCell
                    cell.model = dic
                    return cell
               // }
                
                
            }
            else if dic.postType == .Question, dic.suggested_Friend == false,dic.suggested_Expert == false,dic.suggested_Topics == false
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionAnswerTableViewCell") as! QuestionAnswerTableViewCell
                cell.model = dic
                return cell
            }
            else if dic.postType == .VoiceVideo, dic.suggested_Friend == false,dic.suggested_Expert == false,dic.suggested_Topics == false
            {
                
                if dic.postType == .VoiceVideo
                {
                    if dic.audioVideoType == .audio
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "AudioTableViewCell") as! AudioTableViewCell
                        cell.model = dic
                        
                        return cell
                    }
                    else
                    {
                        let cell =
                            tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell") as! VideoTableViewCell
                        cell.model = dic
                        return cell
                    }
                }
                
            }
            else if dic.postType == .ResearchPapers, dic.suggested_Friend == false,dic.suggested_Expert == false,dic.suggested_Topics == false
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "downloadPDFTableCell") as! downloadPDFTableCell
                cell.model = dic
                return cell
            }
            else if dic.postType == .Advert, dic.suggested_Friend == false,dic.suggested_Expert == false,dic.suggested_Topics == false
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "AdvertTableCell") as! AdvertTableCell
                cell.model = dic
                cell.setUpCollection()
                return cell
            }
            else if dic.suggested_Friend == true
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestedTblCell") as! SuggestedTblCell
                cell.clvSuggested.accessibilityHint = "Friend"
                cell.setUpCollection()
                return cell
            }
            else if dic.suggested_Expert == true
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestedTblCell") as! SuggestedTblCell
                //                cell.model = dic
                cell.clvSuggested.accessibilityHint = "Expert"
                cell.setUpCollection()
                return cell
            }
            else if dic.suggested_Topics == true
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestedTblCell") as! SuggestedTblCell
                cell.clvSuggested.accessibilityHint = "Topics"
                cell.setUpCollection()
                return cell
            }
            return UITableViewCell()
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
       
            
        //For suggested // msios
        if tableView == tableviewQA || tableView == tableviewAD || tableView == tableHome
        {
            
            let dic = tableView == tableviewQA ? self.arrQuestion[indexPath.row] : (tableView == tableviewAD ? self.arrArticle[indexPath.row] : self.arrALLHomePost[indexPath.row])
            
            if dic.suggested_Friend == true
            {
                return arrSuggestedFriends.count == 0 ? 0 : 196 * screenscale
            }
            else if dic.suggested_Expert == true
            {
                return arrSuggestedExpert.count == 0 ? 0 : 196 * screenscale
            }
            else if dic.suggested_Topics == true
            {
                return arrSuggestedTopics.count == 0 ? 0 : 196 * screenscale
            }
            else
            {
                
//                return 500
                return UITableView.automaticDimension
            }
        }
        /* else if tableView == tableviewAD
         {
         let dic = self.arrArticle[indexPath.row]
         
         if dic.suggested_Friend == true
         {
         return arrSuggestedFriends.count == 0 ? 0 : 196 * screenscale
         }
         else if dic.suggested_Expert == true
         {
         return arrSuggestedExpert.count == 0 ? 0 : 196 * screenscale
         }
         else if dic.suggested_Topics == true
         {
         return arrSuggestedTopics.count == 0 ? 0 : 196 * screenscale
         }
         else
         {
         return UITableView.automaticDimension
         
         }
         }*/
        return UITableView.automaticDimension
    }
  
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if tableView == tableMenu
        {
            let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "MenuHeader") as? MenuHeader
            
            headerView?.btnDownArrow.isHidden = section != 0 && section != 1
            
            headerView?.btnTitle.accessibilityHint = "\(section)"
            let objMenu = arrMenu[section]
            
            headerView?.btnTitle.setTitle(objMenu.title, for: .normal)
            headerView?.btnTitle.accessibilityValue = objMenu.title
            headerView?.btnTitle.addTarget(self, action: #selector(clickMenuExpand(_:)), for: .touchUpInside)
            
             headerView?.btnTitle.setImage(objMenu.image, for: .normal)
            
            headerView?.btnDownArrow.isSelected = objMenu.isOpened
         

            headerView?.backgroundColor = UIColor.white

            return headerView
        }
        return nil
    }
    @objc func clickMenuExpand(_ sender: UIButton)
    {
//        debugPrint("clickMenuExpand",sender.accessibilityValue as Any)
          guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
              
        
        if sender.accessibilityValue == Languages.trendingTopics {
            let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "TrendingVC") as! TrendingVC
            self.navigationController?.pushViewController(vcInstace, animated: true)
        }
        else if sender.accessibilityValue == Languages.voiceVideoContent
        {
            self.btnVV(btnvv)
        }
        else if sender.accessibilityValue == Languages.reserchPaper
        {
            self.btnRP(btnrp)
        }
        else if sender.accessibilityValue == Languages.experts
        {
            self.tabBarController?.selectedIndex = 1
        }
        else if sender.accessibilityValue == Languages.adverts
        {
            self.tabBarController?.selectedIndex = 3
        }
        
        
        if let section = Int (sender.accessibilityHint ?? "")
        {
            if arrMenu[section].submenu.count == 0{return}
            arrMenu[section].isOpened = !arrMenu[section].isOpened
            //            UIView.performWithoutAnimation {
            //                self.tableMenu.reloadData()
            //            }
            
            self.tableMenu.reloadSections(IndexSet(integersIn: section...section), with: .none)

        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if tableView == tableMenu
        {
        return 44 * screenscale
        }
        return 0
    }
    
    
    func scrollViewDidEndDragging(_ aScrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if aScrollView == self.tableHome || aScrollView == self.tableviewQA ||  aScrollView == self.tableviewAD || aScrollView == self.tableviewVV || aScrollView == self.tableviewRP
        {
            let offset: CGPoint = aScrollView.contentOffset
            let bounds: CGRect = aScrollView.bounds
            let size: CGSize = aScrollView.contentSize
            let inset: UIEdgeInsets = aScrollView.contentInset
            let y = Float(offset.y + bounds.size.height - inset.bottom)
            let h = Float(size.height)
            let reload_distance: Float = 0
            //            print("load more data!!!!!")
            if y > h + reload_distance{
                
                if aScrollView == self.tableHome
                {
                    if self.isMore_Home == true , self.tableHome.accessibilityHint == nil{
                        
                        self.indicator_Home.startAnimating()
                        self.tableHome.accessibilityHint = "service_calling"
                        self.getAllPost(self.arrALLHomePost.count)
                    }
                }
                else if aScrollView == self.tableviewQA
                {
                    if self.isMore_QA == true , self.tableviewQA.accessibilityHint == nil{
                        
                        self.indicator_QA.startAnimating()
                        self.tableviewQA.accessibilityHint = "service_calling"
                        self.getQuestionList(self.arrQuestion.count)
                    }
                    
//                    let aRect : CGRect = self.view.frame
                    
//                    if (!aRect.contains(self.indicator_QA.frame.origin)) {
//                        tabHeaderScrollView.setContentOffset(CGPoint(x:self.indicator_QA!.frame.origin.x, y:0), animated: true)
//                    }
                    
                    tabHeaderScrollView.scrollRectToVisible(CGRect(origin: self.indicator_QA.frame.origin, size: self.indicator_QA.frame.size), animated: true)
                }
                
                else if aScrollView == self.tableviewAD
                {
                    if self.isMore_AD == true , self.tableviewAD.accessibilityHint == nil{
                        
                        self.indicator_AD.startAnimating()
                        self.tableviewAD.accessibilityHint = "service_calling"
                        self.getArticleList(self.arrArticle.count)
                    }
                }
                else if aScrollView == self.tableviewVV
                {
                    if self.isMore_VV == true , self.tableviewVV.accessibilityHint == nil{
                        
                        self.indicator_VV.startAnimating()
                        self.tableviewVV.accessibilityHint = "service_calling"
                        self.getVoiceVideoList(self.arrVoiceVideo.count)
                    }
                }
                else if aScrollView == self.tableviewRP
                {
                    if self.isMore_RP == true , self.tableviewRP.accessibilityHint == nil{
                        
                        self.indicator_RP.startAnimating()
                        self.tableviewRP.accessibilityHint = "service_calling"
                        self.getResearchPaperList(self.arrResearchPaper.count)
                    }
                }
            }
           
//            (at:selectedIndexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
}
class MenuHeader: UITableViewHeaderFooterView
{
    
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var btnDownArrow: UIButton!

}
class subMenuTablecell: UITableViewCell
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnTitleClick: UIButton!
    
}

class menuModel {
    var title = ""
    var image : UIImage?
    var isOpened = false
    var submenu : [subMenuModel] = []
    
    init(_ Title : String , Img : UIImage? , SubMenus : [subMenuModel] = []) {
        self.title = Title
        self.image = Img
        self.submenu = SubMenus
    }
    
    class var getTempStructure : [menuModel] {
        var arrMenu = [menuModel]()
        arrMenu.append(menuModel(Languages.queAns, Img: AppImage.menuQueAns, SubMenus: [subMenuModel(Languages.ansered) , subMenuModel(Languages.unanswered)]))
        arrMenu.append(menuModel(Languages.articles, Img: AppImage.menuArticles, SubMenus: [subMenuModel(Languages.discussed) , subMenuModel(Languages.undiscussed)]))
        arrMenu.append(menuModel(Languages.voiceVideoContent, Img: AppImage.menuArticles))
        arrMenu.append(menuModel(Languages.reserchPaper, Img: AppImage.menuResearchPaper))
        arrMenu.append(menuModel(Languages.experts, Img: AppImage.menuExperts))
        arrMenu.append(menuModel(Languages.trendingTopics, Img: AppImage.menuTrending))
        arrMenu.append(menuModel(Languages.adverts, Img: AppImage.menuAdverts))
        return arrMenu
    }
}
class subMenuModel {
    var title = ""
    init(_ Title : String ) {
        self.title = Title
    }
}

struct Languages {
    static let queAns = "Question & Answer"
    static let articles = "Articles & Discussions"
    static let voiceVideoContent = "Voice/ Video Contents"
    static let reserchPaper = "Research Papers"
    static let experts = "Experts"
    static let trendingTopics = "Trending Topics"
    static let adverts = "Adverts"
    
    static let ansered = "Answered"
    static let unanswered = "Unanswered"
    
    static let discussed = "Discussed"
    static let undiscussed = "Undiscussed"
    
}

struct AppImage {
    static let menuQueAns = UIImage.init(named: "question_ans")
    static let menuArticles = UIImage.init(named: "articles&discussions")
    static let menuVoiceVideo = UIImage.init(named: "voice_video")
    static let menuResearchPaper = UIImage.init(named: "research_paper")
    static let menuExperts = UIImage.init(named: "experts")
    static let menuTrending = UIImage.init(named: "tranding_topics")
    static let menuAdverts = UIImage.init(named: "advert")
}

class customNavgation : UINavigationController
{
    override func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer?.isEnabled = true
    }
}


//MARK: API Call

extension HomeVC
{
    /*@objc func refreshCalled_AllPost() {
        self.getAllPost()
    }
    
    @objc func getAllPost(_ serviceCount : Int = 0) {
        
        if self.tableHome.accessibilityHint == "service_calling" { return }
        
        self.tableHome.accessibilityHint = "service_calling"
        var delayTime = DispatchTime.now()
        
        if self.tableHome.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tableHome.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.GetallPost, Perameters: ["user_id" : globalUserId,"chr":""],showProgress: false,completion: { (dicRes, success) in
                debugPrint(dicRes)
                if success == true
                {
                   if let arrData = dicRes["posts_list"] as? [[String:Any]] {
                        self.arrALLHomePost.removeAll()
                        self.arrALLHomePost = arrData.map({allPostModel.init($0)})
                    }

                }
                DispatchQueue.main.async {
                    UIView.performWithoutAnimation {
                        self.tableHome.reloadData()
                    }
                    self.tableHome.accessibilityHint = nil
                    self.tableHome.refreshControl?.endRefreshing()
                }
                
                
            }) { (err) in
                if serviceCount < 1 {
                    self.tableHome.accessibilityHint = nil
                    self.getAllPost(serviceCount + 1)
                } else {
                    debugPrint(err)
                    self.tableHome.accessibilityHint = nil
                    self.tableHome.refreshControl?.endRefreshing()
                }
            }
        }
    }*/
    //
    
    
    @objc func refreshCalled_AllPost() {
        self.getAllPost()
    }
    
    @objc func getAllPost(_ offset : Int = 0) {
        
        var delayTime = DispatchTime.now()
        
        if self.tableHome.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tableHome.refreshControl?.beginRefreshingManually()
        }
        

        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.GetallPost, Perameters: ["user_id" : globalUserId,"offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                    debugPrint(dicRes)
                    
                    
                    if let arrData = dicRes["posts_list"] as? [[String:Any]] {
                        
                        if offset == 0{
                            self.arrALLHomePost = arrData.map({allPostModel.init($0)})
                        }
                        else
                        {
                        self.arrALLHomePost.append(contentsOf: arrData.map({allPostModel.init($0)}))
                        }
                    }

                    if let arrSugF = dicRes["suggested_friends"] as? [[String:Any]]
                                       {
                                           arrSuggestedFriends = arrSugF.map({SuggestedFriendModel.init($0)})
                                       }
                                       
                                       if let arrSugE = dicRes["suggested_experts"] as? [[String:Any]]
                                       {
                                           arrSuggestedExpert = arrSugE.map({SuggestedExpertModel.init($0)})
                                       }
                                       if let arrSugT = dicRes["trending_topics"] as? [[String:Any]]
                                       {
                                           arrSuggestedTopics = arrSugT.map({TrendingListModel.init($0)})
                                       }
                    
                   /* if let arrData = dicRes["posts_list"] as? [[String:Any]] {
                        self.arrALLHomePost.removeAll()
                        self.arrALLHomePost = arrData.map({allPostModel.init($0)})
                    }*/
                    
                }
                DispatchQueue.main.async {
                    self.isMore_Home = self.arrALLHomePost.count >= ToInt(dicRes["offset"])
                    
//                    debugPrint("isAvailableMoreData= \(self.isMore_Home)")
//
//                    debugPrint("arr= \(self.arrALLHomePost.count)")
//
//                    debugPrint("toint= \(ToInt(dicRes["offset"]))")

                    
                    UIView.performWithoutAnimation {
                        self.tableHome.reloadData()
                    }
                    self.tableHome.accessibilityHint = nil
                    self.tableHome.refreshControl?.endRefreshing()
                    self.indicator_Home.stopAnimating()
                }
            }) { (err) in
                self.tableHome.accessibilityHint = nil
                self.tableHome.refreshControl?.endRefreshing()
                self.indicator_Home.stopAnimating()
            }
        }
    }
    
    //
    
    @objc func refreshCalled_Article() {
        self.getArticleList()
    }
    @objc func getArticleList(_ offset : Int = 0) {
        
        var delayTime = DispatchTime.now()
        
        if self.tableviewAD.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tableviewAD.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.GetallPost_typeWise, Perameters: ["user_id" : globalUserId,"type":"1","offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                    //                                        debugPrint(dicRes)
                    
                    
                    if let arrData = dicRes["posts"] as? [[String:Any]] {
                        
                        if offset == 0{
                            self.arrArticle = arrData.map({allPostModel.init($0)})
                        }
                        else
                        {
                            self.arrArticle.append(contentsOf: arrData.map({allPostModel.init($0)}))
                        }
                    }
                    
                    if let arrSugF = dicRes["suggested_friends"] as? [[String:Any]]
                    {
                        arrSuggestedFriends = arrSugF.map({SuggestedFriendModel.init($0)})
                    }
                    
                    if let arrSugE = dicRes["suggested_experts"] as? [[String:Any]]
                    {
                        arrSuggestedExpert = arrSugE.map({SuggestedExpertModel.init($0)})
                    }
                    if let arrSugT = dicRes["trending_topics"] as? [[String:Any]]
                    {
                        arrSuggestedTopics = arrSugT.map({TrendingListModel.init($0)})
                    }
                }
                else
                {
                    if offset == 0
                    {
                        self.arrArticle.removeAll()
                    }
                }
                DispatchQueue.main.async {
                    self.isMore_AD = self.arrArticle.count == 0 ? false : self.arrArticle.count >= ToInt(dicRes["offset"])
//                    debugPrint("isAvailableMoreData= \(self.isMore_AD)")
//                    debugPrint("arr= \(self.arrArticle.count)")
//                    debugPrint("toint= \(ToInt(dicRes["offset"]))")
                    
                    UIView.performWithoutAnimation {
                        self.tableviewAD.reloadData()
                    }
                    self.tableviewAD.accessibilityHint = nil
                    self.tableviewAD.refreshControl?.endRefreshing()
                    self.indicator_AD.stopAnimating()
                }
            }) { (err) in
                self.tableviewAD.accessibilityHint = nil
                self.tableviewAD.refreshControl?.endRefreshing()
                self.indicator_AD.stopAnimating()
            }
        }
    }
    
    @objc func refreshCalled_Question() {
        self.getQuestionList()
    }
    
    @objc func getQuestionList(_ offset : Int = 0) {
        
        var delayTime = DispatchTime.now()
        
        if self.tableviewQA.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tableviewQA.refreshControl?.beginRefreshingManually()
        }
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.GetallPost_typeWise, Perameters: ["user_id" : globalUserId,"type":"2","offset":offset],showProgress: false,completion: { (dicRes, success) in
                
//                debugPrint(dicRes)

                if success == true{
                    if let arrData = dicRes["posts"] as? [[String:Any]] {
                        
                        if offset == 0{
                            self.arrQuestion = arrData.map({allPostModel.init($0)})
                        }
                        else
                        {
                            self.arrQuestion.append(contentsOf: arrData.map({allPostModel.init($0)}))
                        }
                    }
                    
                    if let arrSugF = dicRes["suggested_friends"] as? [[String:Any]]
                    {
                        arrSuggestedFriends = arrSugF.map({SuggestedFriendModel.init($0)})
                    }
                    
                    if let arrSugE = dicRes["suggested_experts"] as? [[String:Any]]
                    {
                        arrSuggestedExpert = arrSugE.map({SuggestedExpertModel.init($0)})
                    }
                    if let arrSugT = dicRes["trending_topics"] as? [[String:Any]]
                    {
                        arrSuggestedTopics = arrSugT.map({TrendingListModel.init($0)})
                    }
                }
                else
                {
                    if offset == 0
                    {
                        self.arrQuestion.removeAll()
                    }
                }
                DispatchQueue.main.async {
//                    self.isMore_QA = self.arrQuestion.count >= ToInt(dicRes["offset"])
                    
                    self.isMore_QA = self.arrQuestion.count  == 0 ? false : self.arrQuestion.count >= ToInt(dicRes["offset"])
//                    debugPrint("isAvailableMoreData= \(self.isMore_QA)")
//
//                    debugPrint("arr= \(self.arrQuestion.count)")
//
//                    debugPrint("toint= \(ToInt(dicRes["offset"]))")
                    
                    
                    UIView.performWithoutAnimation {
                        self.tableviewQA.reloadData()
                    }
                    self.tableviewQA.accessibilityHint = nil
                    self.tableviewQA.refreshControl?.endRefreshing()
                    self.indicator_QA.stopAnimating()
                }
            }) { (err) in
                self.tableviewQA.accessibilityHint = nil
                self.tableviewQA.refreshControl?.endRefreshing()
                self.indicator_QA.stopAnimating()
            }
        }
    }
    
    @objc func refreshCalled_VoiceVideo() {
        self.getVoiceVideoList()
    }
    
    @objc func getVoiceVideoList(_ offset : Int = 0) {
        
        var delayTime = DispatchTime.now()
        
        if self.tableviewVV.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tableviewVV.refreshControl?.beginRefreshingManually()
        }
        
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.GetallPost_typeWise, Perameters: ["user_id" : globalUserId,"type":"3","offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                    debugPrint(dicRes)
                    if let arrData = dicRes["posts"] as? [[String:Any]] {
                        
                        if offset == 0{
                            self.arrVoiceVideo = arrData.map({allPostModel.init($0)})
                        }
                        else
                        {
                            self.arrVoiceVideo.append(contentsOf: arrData.map({allPostModel.init($0)}))
                        }
                    }
                }
                else
                {
                    if offset == 0
                    {
                        self.arrVoiceVideo.removeAll()
                    }
                }
                DispatchQueue.main.async {
                    self.isMore_VV =  self.arrVoiceVideo.count == 0 ? false :  self.arrVoiceVideo.count >= ToInt(dicRes["offset"])
//                    debugPrint("isAvailableMoreData= \(self.isMore_VV)")
//                    debugPrint("arr= \(self.arrVoiceVideo.count)")
//                    debugPrint("toint= \(ToInt(dicRes["offset"]))")
                    UIView.performWithoutAnimation {
                        self.tableviewVV.reloadData()
                    }
                    self.tableviewVV.accessibilityHint = nil
                    self.tableviewVV.refreshControl?.endRefreshing()
                    self.indicator_VV.stopAnimating()
                }
            }) { (err) in
                self.tableviewVV.accessibilityHint = nil
                self.tableviewVV.refreshControl?.endRefreshing()
                self.indicator_VV.stopAnimating()
            }
        }
    }
    @objc func refreshCalled_Researchpaper() {
        self.getResearchPaperList()
    }
    
    @objc func getResearchPaperList(_ offset : Int = 0) {
        var delayTime = DispatchTime.now()
        if self.tableviewRP.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tableviewRP.refreshControl?.beginRefreshingManually()
        }
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.GetallPost_typeWise, Perameters: ["user_id" : globalUserId,"type":"4","offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
//                                                            debugPrint(dicRes)
                    if let arrData = dicRes["posts"] as? [[String:Any]] {
                        if offset == 0{
                            self.arrResearchPaper = arrData.map({allPostModel.init($0)})
                        }
                        else{
                            self.arrResearchPaper.append(contentsOf: arrData.map({allPostModel.init($0)}))
                        }
                    }
                }
                else
                {
                    if offset == 0
                    {
                        self.arrResearchPaper.removeAll()
                    }
                }
                DispatchQueue.main.async {
                    self.isMore_RP = self.arrResearchPaper.count == 0 ? false : self.arrResearchPaper.count >= ToInt(dicRes["offset"])
//                    debugPrint("isAvailableMoreData= \(self.isMore_RP)")
//                    debugPrint("arr= \(self.arrResearchPaper.count)")
//                    debugPrint("toint= \(ToInt(dicRes["offset"]))")
                    UIView.performWithoutAnimation {
                        self.tableviewRP.reloadData()
                    }
                    self.tableviewRP.accessibilityHint = nil
                    self.tableviewRP.refreshControl?.endRefreshing()
                    self.indicator_RP.stopAnimating()
                }
            }) { (err) in
                self.tableviewRP.accessibilityHint = nil
                self.tableviewRP.refreshControl?.endRefreshing()
                self.indicator_RP.stopAnimating()
            }
        }
    }
    
}


class allPostModel {
//    var user_id =  ""
    var profile_image =  ""
    var profile_image_thumb =  ""
    var additional_info =  " "
    var admin_id =  ""
    var author =  ""
    var author_id =  ""
    var categories_id =  ""
    var category_name =  ""
    var category_status =  ""
    var cover_image =  ""
//    var expert_id =  ""
    
    var post_comment_count =  ""
    var post_date =  ""
    var post_description =  ""
    var post_flashed_count =  ""
    var post_id =  ""
    var post_like_count =  0
    var post_title =  ""
    var total_answers =  0

    
    var post_type =  ""
    var postType = PostTypeEnum.Article
    
    var professional_qualification =  ""
    
    var type =  ""
    var updated_date =  ""
    
    var audioVideoType = audioVideoEnum.audio
    var vv_type =  ""

    //Bool
    var is_shared =  false
    var status =  false
    var is_following =  false
    var is_post_flashed =  false
    var is_post_liked =  false
    var is_post_bookmark =  false
    var is_user =  false
    var is_carryover =  false
    var is_carryover_expire =  false
    var is_post_recommended =  false
    var is_requested_to_expert   =  false
    
    var suggested_Friend   =  false
    var suggested_Expert   =  false
    var suggested_Topics   =  false
    var is_subscribe   =  false
    var is_expert = false
    var is_Mute = false




    var arrImages = [imagePostModel]()
    var dicPDF : PDFPostModel?
    var dicAudio : audioPostModel?
    var dicVideo : videoPostModel?
    
    var dicPostLink : PostLinkModel?
    

    //for only QA
    var post_share_count =  ""
    
    
    //FOR ADVERT
    var time_in_min = ""
    var advert_payment_status_label =  ""
    var advert_time_slot_id =  ""
    var amount =  ""
    var category =  ""
    var category_label =  ""
    var currency =  ""
    var net_amount =  ""
    var no_of_days =  ""
    var payment_done_label =  ""
    var payment_mode_label =  ""
    var advert_url =  ""


    //For vv
    
    var request_to_reply =  0

    init(_ dict : [String:Any]) {
        
//        self.user_id =  toString(dict["user_id"])
        self.profile_image = toString(dict["profile_image"])
        self.profile_image_thumb = toString(dict["profile_image_thumb"])
        self.additional_info = toString(dict["additional_info"])
        
        self.admin_id = toString(dict["admin_id"])
        self.author = toString(dict["author"])
        self.author_id = toString(dict["author_id"])
        self.is_expert = toString(dict["is_expert"]) == "1"

//        self.expert_id = toString(dict["expert_id"])
        
        self.categories_id = toString(dict["categories_id"])
        self.category_name = toString(dict["category_name"])
        self.category_status = toString(dict["category_status"])
        self.cover_image = toString(dict["cover_image"])
        
        //noti count
        
        self.request_to_reply = ToInt(dict["request_to_reply"])

        self.total_answers = ToInt(dict["total_answers"])
        self.post_comment_count = toString(dict["post_comment_count"])
        self.post_like_count = ToInt(dict["post_like_count"])
        self.post_share_count = toString(dict["post_share_count"])
        self.post_flashed_count = toString(dict["post_flashed_count"])
        
        self.post_date = toString(dict["post_date"])
        self.post_description = toString(dict["post_description"]).htmlToString
        self.post_id = toString(dict["post_id"])
        self.post_title = toString(dict["post_title"])
        self.post_type = toString(dict["post_type"])
        if let getType = PostTypeEnum.init(rawValue: self.post_type.integerValue){
            self.postType = getType
        }
        self.professional_qualification = toString(dict["professional_qualification"])
        
        self.type = toString(dict["type"])
        self.updated_date = toString(dict["updated_date"])
        
        self.vv_type = toString(dict["vv_type"])
        if let getType = audioVideoEnum.init(rawValue: self.vv_type.integerValue){
            self.audioVideoType = getType
        }
        //bool
        self.suggested_Friend = ConvertToBool(toString(dict["sfriend"]))
        self.suggested_Expert = ConvertToBool(toString(dict["sexpert"]))
        self.suggested_Topics = ConvertToBool(toString(dict["tr_topics"]))

        self.is_shared = ConvertToBool(toString(dict["is_shared"]))
        self.status = ConvertToBool(toString(dict["status"]))
        self.is_following = ConvertToBool(toString(dict["is_following"]))
        self.is_post_flashed = ConvertToBool(toString(dict["is_post_flashed"]))
        self.is_post_liked = ConvertToBool(toString(dict["is_post_liked"]))
        self.is_post_bookmark = ConvertToBool(toString(dict["is_post_bookmark"]))
        self.is_user = ConvertToBool(toString(dict["is_user"]))

        self.is_carryover = ConvertToBool(toString(dict["is_carryover"]))
        self.is_carryover_expire = ConvertToBool(toString(dict["is_carryover_expire"]))

        self.is_post_recommended = ConvertToBool(toString(dict["is_post_recommended"]))
        self.is_requested_to_expert = ConvertToBool(toString(dict["is_requested_to_expert"]))

        self.is_subscribe = ConvertToBool(toString(dict["is_subscribe"]))
        self.is_Mute = ConvertToBool(toString(dict["is_mute"]))

        //arr
        //For Interested Category
        if let arr = dict["images"] as? [[String:Any]] {
            self.arrImages = arr.map({imagePostModel.init($0)})
        }
        
        //For PDF
        if let dic = dict["files"] as? [String:Any] {
            self.dicPDF = PDFPostModel.init(dic)
        }
        
        //For audio
        if let dic = dict["audios"] as? [String:Any] {
            self.dicAudio = audioPostModel.init(dic)
        }
        
        //For video
        if let dic = dict["videos"] as? [String:Any] {
            self.dicVideo = videoPostModel.init(dic)
        }
        
        //For POstLink
        if let dic = dict["post_link"] as? [String:Any] {
            self.dicPostLink = PostLinkModel.init(dic)
        }
        
        
        
        //FOR ADVERT
        self.time_in_min = toString(dict["time_in_min"])
        self.advert_payment_status_label = toString(dict["advert_payment_status_label"])
        self.advert_time_slot_id = toString(dict["advert_time_slot_id"])
        self.amount = toString(dict["amount"])
        self.category = toString(dict["category"])
        self.category_label = toString(dict["category_label"])
        self.currency = toString(dict["currency"])
        self.net_amount = toString(dict["net_amount"])
        self.no_of_days = toString(dict["no_of_days"])
        self.payment_done_label = toString(dict["payment_done_label"])
        self.payment_mode_label = toString(dict["payment_mode_label"])
        self.advert_url = toString(dict["advert_url"])
        
        


    }
}

class imagePostModel
{
    
    var date =  ""
    var image_name =  ""
    var image_name_thumb =  ""
    var post_id =  ""
    var post_img_id =  ""
    
//    var imgAdvert =  ""

    //Bool
    var status =  false
    
    init(_ dict : [String:Any]) {
        
        self.date =  toString(dict["date"])
        self.image_name = toString(dict["image_name"])
        self.image_name_thumb = toString(dict["image_name_thumb"])
        self.post_id = toString(dict["post_id"])
        self.post_img_id = toString(dict["post_img_id"])
        self.status = ConvertToBool(toString(dict["status"]))
//        self.imgAdvert = toString(dict["image"])

        
    }
}

class PDFPostModel
{
    var file_id =  ""
    var file_name =  ""
    var file_size =  ""
    var ori_file_name =  ""
    var rp_post_id =  ""

    init(_ dict : [String:Any]) {
        
        self.file_id =  toString(dict["file_id"])
        self.file_name = toString(dict["file_name"])
        self.file_size = toString(dict["file_size"])
        self.ori_file_name = toString(dict["ori_file_name"])
        self.rp_post_id = toString(dict["rp_post_id"])
    }
}

class audioPostModel
{
    var audio_name =  ""
    var audio_size =  ""
    var audio_url =  ""
    var date =  ""
    var ori_audio_name =  ""
    var post_audio_id =  ""
    var post_id =  ""
    var audio_duration = ""
    
    init(_ dict : [String:Any]) {
        self.audio_name =  toString(dict["audio_name"])
        self.audio_size = toString(dict["audio_size"])
        self.audio_url = toString(dict["audio_url"])
        self.date = toString(dict["date"])
        self.ori_audio_name = toString(dict["ori_audio_name"])
        self.post_audio_id = toString(dict["post_audio_id"])
        self.post_id = toString(dict["post_id"])
        self.audio_duration = toString(dict["audio_duration"])
        
    }
}

class videoPostModel
{
    var video_name =  ""
    var video_size =  ""
    var video_url =  ""
    var date =  ""
    var ori_video_name =  ""
    var post_video_id =  ""
    var post_id =  ""
    var video_img =  ""
    var video_thumb_img =  ""
    
    init(_ dict : [String:Any]) {
        
        self.video_name =  toString(dict["video_name"])
        self.video_size = toString(dict["video_size"])
        self.video_url = toString(dict["video_url"])
        self.date = toString(dict["date"])
        self.ori_video_name = toString(dict["ori_video_name"])
        self.post_video_id = toString(dict["post_video_id"])
        self.post_id = toString(dict["post_id"])
        self.video_img = toString(dict["video_img"])
        self.video_thumb_img = toString(dict["video_thumb_img"])

        
    }
}
class PostLinkModel
{
    var link =  ""
    var date =  ""
    var post_id =  ""
    var post_link_id =  ""
    
    init(_ dict : [String:Any]) {
        
        self.link =  toString(dict["link"])
        self.date = toString(dict["date"])
        self.post_id = toString(dict["post_id"])
        self.post_link_id = toString(dict["post_link_id"])
        
    }
}


enum PostTypeEnum : Int {
    case Article = 1
    case Question = 2
    case VoiceVideo = 3
    case ResearchPapers = 4
    case Advert = 5

}




enum audioVideoEnum : Int {
    case audio = 1
    case video = 2
}


class SuggestedFriendModel
{
    var name =  ""
    var profile_image =  ""
    var user_id =  ""
    var total_followers =  ""
    var profession = ""
    
    init(_ dict : [String:Any]) {
        
        self.name =  toString(dict["firstname"]) + " " + toString(dict["lastname"])
        self.profile_image = toString(dict["profile_image"])
        self.user_id = toString(dict["user_id"])
        self.total_followers = toString(dict["total_followers"])
        self.profession = toString(dict["profession"])
    }
}

class SuggestedExpertModel
{
    var name =  ""
    var profile_image =  ""
    var user_id =  ""
    var total_followers =  ""
    var profession = ""
    
    init(_ dict : [String:Any]) {
        
        self.name =  toString(dict["firstname"]) + " " + toString(dict["lastname"])
        self.profile_image = toString(dict["profile_image"])
        self.user_id = toString(dict["user_id"])
        self.total_followers = toString(dict["total_followers"])
        self.profession = toString(dict["profession"])
    }
}

