
//
//  AdvertVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/4/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class AdvertVC: UIViewController {
    
    @IBOutlet weak var badgeMsg: UIButton!
    @IBOutlet weak var badgeNoti: UIButton!
    @IBOutlet weak var badgeFriend: UIButton!
    
    
    @IBOutlet weak var tblAdvert: UITableView!
    
    var arrAdvert = [allPostModel]()
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var isMore = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.Delete_Post), name: .Delete_Post, object: nil)

        badgeMsg.layer.cornerRadius = badgeMsg.frame.height/2
        badgeMsg.layer.masksToBounds = true
        
        badgeNoti.layer.cornerRadius = badgeNoti.frame.height/2
        badgeNoti.layer.masksToBounds = true
        
        badgeFriend.layer.cornerRadius = badgeFriend.frame.height/2
        badgeFriend.layer.masksToBounds = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadNewData), name: .NewDataLoadAdvert, object: nil)
        
        self.tblAdvert.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 80, right: 0)
        
        // Do any additional setup after loading the view.
        tblAdvert.register(UINib.init(nibName: "AdvertTableCell", bundle: nil), forCellReuseIdentifier: "AdvertTableCell")
        tblAdvert.estimatedRowHeight = 100
        tblAdvert.rowHeight = UITableView.automaticDimension
        
        DispatchQueue.main.async {
            
            self.indicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tblAdvert.bounds.width, height: CGFloat(30))
            self.tblAdvert.tableFooterView = self.indicator
        }
        
        
        self.tblAdvert.refreshControl = UIRefreshControl()
        self.tblAdvert.refreshControl?.tintColor = .AppSkyBlue
        self.tblAdvert.refreshControl?.addTarget(self, action: #selector(refreshCalled), for: UIControl.Event.valueChanged)
        self.getAdvertList()
        
        
    }
    
    @objc func Delete_Post(notification: NSNotification){
        
        if  let dic = notification.object as? [String : Any]{
            let removePostId = (toString(dic["id"]))
            let getType = PostTypeEnum.init(rawValue: ToInt(dic["type"]))
            debugPrint(toString(dic["type"]))
            if getType == .Advert{
                DispatchQueue.main.async {
                    if let find_index = (self.arrAdvert).firstIndex(where: {$0.post_id == removePostId}) {
                        self.arrAdvert.remove(at: find_index)
                    }
                    self.tblAdvert.reloadData()
                }
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = UIColor.AppSkyBlue
        self.navigationController?.isNavigationBarHidden = false
        
       self.setBages()
        appDelegate.funGetnoticount{
            self.setBages()
        }
        
        self.tblAdvert.reloadData()
        
    }
    func setBages(){
        self.badgeMsg.setTitle(global_count_Msg, for: .normal)
        self.badgeNoti.setTitle(global_count_Noti, for: .normal)
        self.badgeFriend.setTitle(global_count_Friend, for: .normal)
        
        self.badgeMsg.isHidden = global_count_Msg == "0"
        self.badgeNoti.isHidden = global_count_Noti == "0"
        self.badgeFriend.isHidden = global_count_Friend == "0"
    }
    /* override func viewDidAppear(_ animated: Bool) {
     super.viewDidAppear(animated)
     setTabBarHideGBL(navCtrl: self.navigationController!, isHide: false)
     }
     override func viewWillDisappear(_ animated: Bool) {
     super.viewWillDisappear(animated)
     setTabBarHideGBL(navCtrl: self.navigationController!, isHide: true)
     }*/
    
    @IBAction func btnSearch(_ sender: UIButton) {
        let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        self.pushTo(vcInstace)
    }
    @IBAction func btnProfile(_ sender: UIButton) {
        let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigationController?.pushViewController(vcInstace, animated: true)
    }
    @IBAction func btnNotification(_ sender: UIButton) {
        let vcInstace = StoryBoard.Notification.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        self.pushTo(vcInstace)
        
    }
    @IBAction func btnMessage(_ sender: UIButton) {
        let vcInstace = StoryBoard.Message.instantiateViewController(withIdentifier: "MessageVC") as! MessageVC
        self.pushTo(vcInstace)
    }
    @IBAction func btnFriends(_ sender: UIButton) {
        let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "FriendsVC") as! FriendsVC
        self.pushTo(vcInstace)
    }
    @IBAction func btnAddPost(_ sender: UIButton) {
        
        objAdvert = nil
        
        let vcInstace = StoryBoard.Advert.instantiateViewController(withIdentifier: "SelectTwoCategoryVC") as! SelectTwoCategoryVC
        self.pushTo(vcInstace)
    }
    
}

extension AdvertVC:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAdvert.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdvertTableCell") as! AdvertTableCell
        
        cell.model = arrAdvert[indexPath.row]
        
        cell.clvAdvertPhoto.accessibilityHint = ToString(indexPath.row)
        
        cell.setUpCollection()
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func scrollViewDidEndDragging(_ aScrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        let offset: CGPoint = aScrollView.contentOffset
        let bounds: CGRect = aScrollView.bounds
        let size: CGSize = aScrollView.contentSize
        let inset: UIEdgeInsets = aScrollView.contentInset
        let y = Float(offset.y + bounds.size.height - inset.bottom)
        let h = Float(size.height)
        let reload_distance: Float = 0
        //            print("load more data!!!!!")
        if y > h + reload_distance{
            if self.isMore == true , self.tblAdvert.accessibilityHint == nil{
                self.indicator.startAnimating()
                self.tblAdvert.accessibilityHint = "service_calling"
                self.getAdvertList(self.arrAdvert.count)
            }
        }
    }
    
}
//MARK: - Button Event IBAction

extension AdvertVC
{
    @objc func loadNewData(notification: NSNotification){
        DispatchQueue.main.async {
            self.getAdvertList()
        }
    }
    
    @objc func refreshCalled() {
        self.getAdvertList()
    }
    @objc func getAdvertList(_ offset : Int = 0) {
        
        var delayTime = DispatchTime.now()
        
        if self.tblAdvert.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblAdvert.refreshControl?.beginRefreshingManually()
        }
        
        debugPrint("Offset= \(offset)")
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.AdvertList, Perameters: ["user_id" : globalUserId,"offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                    debugPrint(dicRes)
                    
                    if let arrData = dicRes["advert_list"] as? [[String:Any]] {
                        if offset == 0{
                            self.arrAdvert = arrData.map({allPostModel.init($0)})
                        }
                        else{
                            self.arrAdvert.append(contentsOf: arrData.map({allPostModel.init($0)}))
                        }
                    }
                    
                    self.isMore = self.arrAdvert.count >= ToInt(dicRes["offset"])
                    
                    
                }
                else{
                    self.isMore = false
                    self.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
                }
                
                DispatchQueue.main.async {
                    
                    debugPrint("isAvailableMoreData= \(self.isMore)")
                    
                    debugPrint("arr= \(self.arrAdvert.count)")
                    
                    debugPrint("toint= \(ToInt(dicRes["offset"]))")
                    
                    
                    UIView.performWithoutAnimation {
                        self.tblAdvert.reloadData()
                    }
                    self.tblAdvert.accessibilityHint = nil
                    self.tblAdvert.refreshControl?.endRefreshing()
                    self.indicator.stopAnimating()
                }
            }) { (err) in
                self.tblAdvert.accessibilityHint = nil
                self.tblAdvert.refreshControl?.endRefreshing()
                self.indicator.stopAnimating()
            }
        }
    }
}
/*class AdvertModel {
 
 
 
 
 //    var user_id =  ""
 
 var time_in_min = ""
 var profile_image =  ""
 var profile_image_thumb =  ""
 var additional_info =  ""
 var admin_id =  ""
 var author =  ""
 var author_id =  ""
 var categories_id =  ""
 var category_name =  ""
 var category_status =  ""
 var cover_image =  ""
 //    var expert_id =  ""
 
 var post_comment_count =  ""
 var post_date =  ""
 var post_description =  ""
 var post_flashed_count =  ""
 var post_id =  ""
 var post_like_count =  0
 var post_title =  ""
 var total_answers =  0
 
 
 var post_type =  ""
 var postType = PostTypeEnum.Article
 var professional_qualification =  ""
 var type =  ""
 var updated_date =  ""
 
 var vv_type =  ""
 
 //Bool
 //    var is_shared =  false
 //    var status =  false
 //    var is_following =  false
 //    var is_post_flashed =  false
 var is_post_liked =  false
 var is_post_bookmark =  false
 var is_user =  false
 
 var arrImages = [imagePostModel]()
 
 
 //for only QA
 //    var post_share_count =  ""
 
 var advert_payment_status_label =  ""
 var advert_time_slot_id =  ""
 var amount =  ""
 var category =  ""
 var category_label =  ""
 var currency =  ""
 var net_amount =  ""
 var no_of_days =  ""
 var payment_done_label =  ""
 var payment_mode_label =  ""
 
 init(_ dict : [String:Any]) {
 
 
 self.time_in_min = toString(dict["time_in_min"])
 self.advert_payment_status_label = toString(dict["advert_payment_status_label"])
 self.advert_time_slot_id = toString(dict["advert_time_slot_id"])
 self.amount = toString(dict["amount"])
 self.category = toString(dict["category"])
 self.category_label = toString(dict["category_label"])
 self.currency = toString(dict["currency"])
 self.net_amount = toString(dict["net_amount"])
 self.no_of_days = toString(dict["no_of_days"])
 self.payment_done_label = toString(dict["payment_done_label"])
 self.payment_mode_label = toString(dict["payment_mode_label"])
 
 
 //        self.user_id =  toString(dict["user_id"])
 self.profile_image = toString(dict["profile_image"])
 self.profile_image_thumb = toString(dict["profile_image_thumb"])
 self.additional_info = toString(dict["additional_info"])
 
 self.admin_id = toString(dict["admin_id"])
 self.author = toString(dict["author"])
 self.author_id = toString(dict["author_id"])
 
 //        self.expert_id = toString(dict["expert_id"])
 
 self.categories_id = toString(dict["categories_id"])
 self.category_name = toString(dict["category_name"])
 self.category_status = toString(dict["category_status"])
 self.cover_image = toString(dict["cover_image"])
 
 //noti count
 self.total_answers = ToInt(dict["total_answers"])
 self.post_comment_count = toString(dict["post_comment_count"])
 self.post_like_count = ToInt(dict["post_like_count"])
 //        self.post_share_count = toString(dict["post_share_count"])
 self.post_flashed_count = toString(dict["post_flashed_count"])
 
 self.post_date = toString(dict["post_date"])
 self.post_description = toString(dict["post_description"]).htmlToString
 self.post_id = toString(dict["post_id"])
 self.post_title = toString(dict["post_title"])
 self.post_type = toString(dict["post_type"])
 if let getType = PostTypeEnum.init(rawValue: self.post_type.integerValue){
 self.postType = getType
 }
 self.professional_qualification = toString(dict["professional_qualification"])
 
 self.type = toString(dict["type"])
 self.updated_date = toString(dict["updated_date"])
 
 self.vv_type = toString(dict["vv_type"])
 
 //bool
 //        self.is_shared = ConvertToBool(toString(dict["is_shared"]))
 //        self.status = ConvertToBool(toString(dict["status"]))
 //        self.is_following = ConvertToBool(toString(dict["is_following"]))
 //        self.is_post_flashed = ConvertToBool(toString(dict["is_post_flashed"]))
 self.is_post_liked = ConvertToBool(toString(dict["is_post_liked"]))
 self.is_post_bookmark = ConvertToBool(toString(dict["is_post_bookmark"]))
 self.is_user = ConvertToBool(toString(dict["is_user"]))
 
 //arr
 //For Interested Category
 if let arr = dict["images"] as? [[String:Any]] {
 self.arrImages = arr.map({imagePostModel.init($0)})
 }
 
 
 
 }
 }*/

