//
//  AdvertTableCell.swift
//  Proclapp
//
//  Created by Mac-4 on 05/06/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import SafariServices

class AdvertTableCell: UITableViewCell {

    @IBOutlet weak var viewHeightConst: NSLayoutConstraint!

    @IBOutlet weak var imgProfileWidthConst: NSLayoutConstraint!
    @IBOutlet weak var imgProfileHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var lblCommentCount: UILabel!

    @IBOutlet weak var btnLike: UIButton!

    @IBOutlet var txtviewURL: UITextView!

    
    @IBOutlet weak var clvAdvertPhoto: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        

        self.viewHeightConst.constant = 106 * screenscale

        self.imgProfileWidthConst.constant = 0 * screenscale
        self.imgProfileHeightConst.constant = 30 * screenscale
        self.imgProfile.layer.cornerRadius =  self.imgProfileWidthConst.constant / 2

        self.clvAdvertPhoto.delegate = self
        self.clvAdvertPhoto.dataSource = self
        self.clvAdvertPhoto.register(UINib(nibName: "AdvertPhotoCLVCell", bundle: nil), forCellWithReuseIdentifier: "AdvertPhotoCLVCell")
        
        DispatchQueue.main.async {
//            self.imgProfile.setCornerRaduisToRound()
             self.clvAdvertPhoto.reloadData()
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(didTapTextView))
        tap.numberOfTapsRequired = 1
        txtviewURL.addGestureRecognizer(tap)
        txtviewURL.font = UIFont.FontLatoRegular(size: 11 * screenscale)
//        txtviewURL.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        txtviewURL.textContainer.lineFragmentPadding = 0

       
    }

    func setUpCollection() {
        self.clvAdvertPhoto.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    var _model : allPostModel?
    
    
    var model : allPostModel? {
        
        get{
            return _model
        }
        set(model)
        {
            _model = model
            lblTitle.text = model?.post_title
            lblDate.text = model?.post_date.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_ddMMM)
//            lblCategory.text = "Sponsored | " + toString(model?.category_label) + " | \(toString(model?.time_in_min)) min"

              lblCategory.text = "Sponsored"
//            imgProfile.setImageWithURL(model?.profile_image, "Big_dp_placeholder")
            
            lblLikeCount.text = toString(model?.post_like_count)
            lblCommentCount.text = toString(model?.total_answers)
            
            btnLike.isSelected = model?.is_post_liked ?? false
            
            lblDesc.lineBreakMode = .byTruncatingTail
            lblDesc.numberOfLines = 2

            
            if (self.tableView?.parentViewController as? AdvertDetailsVC) != nil
            {
                lblDesc.numberOfLines = 0
            }
            lblDesc.text = model?.post_description
            
            txtviewURL.textContainer.lineBreakMode = .byTruncatingTail
            txtviewURL.text = checkValidString(toString(model?.advert_url)) ? toString(model?.advert_url) : ""

          
        }
    }
    
}

extension AdvertTableCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model?.arrImages.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "AdvertPhotoCLVCell", for: indexPath) as! AdvertPhotoCLVCell
        
        let obj = model?.arrImages[indexPath.row]
        
        cell.imgGallary.setImageWithURL(obj?.image_name, "place_logo")
        
        if let allURL = model?.arrImages.map({$0.image_name}).compactMap({URL (string: $0)})
        {
            cell.imgGallary.setupImageViewer(urls: allURL, initialIndex: indexPath.item, options: [.theme(.dark)], placeholder: nil, from: self.tableView?.parentViewController)
        }
        

//        cell.imgGallary.image = UIImage.init(named: "fruts")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let countImges = model?.arrImages.count
        if countImges == 1
        {
            return CGSize.init(width: clvAdvertPhoto.frame.width , height: clvAdvertPhoto.frame.height)
        }
        return CGSize.init(width: clvAdvertPhoto.frame.height , height: clvAdvertPhoto.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       /* if let get = model?.arrImages, get.count > 0
        {
            let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "imageGalleryVC") as! imageGalleryVC
            vcInstace.getarrImages = get
            self.tableView?.parentViewController?.navigationController?.pushViewController(vcInstace, animated: true)
            
        }*/
        
        if (self.tableView?.parentViewController as? AdvertDetailsVC) != nil
        {
            if let get = model?.arrImages, get.count > 0
            {
                let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "imageGalleryVC") as! imageGalleryVC
                vcInstace.getarrImages = get
                self.tableView?.parentViewController?.navigationController?.pushViewController(vcInstace, animated: true)
                
            }
        }
        else
        {
        let vcInstace = StoryBoard.Advert.instantiateViewController(withIdentifier: "AdvertDetailsVC") as! AdvertDetailsVC
        vcInstace.dictPost = model
        self.tableView?.parentViewController?.pushTo(vcInstace)
        }
        
        
    }
}




//MARK:- Like UnLike
extension AdvertTableCell {
    
    @IBAction func clickLikeUnlike(_ sender: UIButton) {
        if let getObj = model
        {
            callLikeUnlikePost(getObj.post_id, sender.isSelected == true ? "0" : "1")
            sender.isSelected = !sender.isSelected
            if sender.isSelected{
                getObj.post_like_count += 1
            }
            else{
                getObj.post_like_count -= 1
            }
            getObj.is_post_liked = !getObj.is_post_liked
            lblLikeCount.text = toString(model?.post_like_count)
        }
    }
    
    func callLikeUnlikePost(_ post_id:String, _ like_unlike:String ) {
        WebService.shared.RequesURL(ServerURL.LikeUnlikePost, Perameters: ["user_id" : globalUserId,"post_id":post_id,"like_unlike":like_unlike],showProgress: false,completion: { (dicRes, success) in
            if success == true{
                debugPrint(dicRes)
            }
        }) { (err) in
        }
    }
    
}

//MARK:- Three Dot

extension AdvertTableCell {
    
    @IBAction func clickDot(_ sender: UIButton) {
        if let getObj = model
        {
            if getObj.is_user, getObj.author_id == globalUserId{
                self.tapDotMenuMy(postDict: getObj)
            }
            else{
                self.tapDotMenuOtherUser(postDict: getObj)
            }
        }
    }
    func tapDotMenuMy(postDict : allPostModel) {
        
        DispatchQueue.main.async {
            guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
            popupVC.arrTitle = [BottomName.Edit,BottomName.Share,BottomName.Delete]
            //            popupVC.HeightHeader = 50 * screenscale
            popupVC.callback =  { str in
                debugPrint(str)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    if str == BottomName.Edit
                    {
                        objAdvert = nil
                        objAdvert = Advert()
                        
                        let vc = StoryBoard.Advert.instantiateViewController(withIdentifier: "AddProductSummaryVC") as! AddProductSummaryVC
                        vc.dictPost = postDict
//                        vc.getAdvert_id = postDict.post_id
                        vc.isEdit = true
                        self.tableView?.parentViewController?.pushTo(vc)
                    }
                    else if str == BottomName.Delete
                    {
                        self.clickDeletePost(postDict.post_id)
                    }
                    else if str == BottomName.RequestExpert
                    {
                        let selectExpert = StoryBoard.AddPost.instantiateViewController(withIdentifier: "SelectExpertVC") as! SelectExpertVC
                        selectExpert.postId = postDict.post_id
                        self.tableView?.parentViewController?.pushTo(selectExpert)
                    }
                        
                    else if str == BottomName.Share {
                        self.tableView?.parentViewController?.shareMaulik(shareText: postDict.post_title, shareImage: nil, shreUrl: postDict.arrImages.first?.image_name)
                    }
                })
            }
            self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
        }
    }
    
    func tapDotMenuOtherUser(postDict : allPostModel) {
        
        DispatchQueue.main.async {
            guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
            popupVC.arrTitle = [BottomName.BroadcastAll,postDict.is_Mute ? BottomName.UNMute : BottomName.Mute,BottomName.Share,BottomName.ReportAbuse]//BottomName.CompareAnswer
            popupVC.callback =  { str in
                debugPrint(str)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    if str == BottomName.ReportAbuse {
                        let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "ReportabuseDesVC") as! ReportabuseDesVC
                        vcInstace.post_id = postDict.post_id
                        self.tableView?.parentViewController?.pushTo(vcInstace)
                    }
                    else if str == BottomName.RequestExpert
                    {
                        let selectExpert = StoryBoard.AddPost.instantiateViewController(withIdentifier: "SelectExpertVC") as! SelectExpertVC
                        selectExpert.postId = postDict.post_id
                        self.tableView?.parentViewController?.pushTo(selectExpert)
                    }
                        
                    else if str == BottomName.Share {
                        self.tableView?.parentViewController?.shareMaulik(shareText: postDict.post_title, shareImage: nil, shreUrl: postDict.arrImages.first?.image_name)
                    }
                    else if str == BottomName.BroadcastAll {
                        self.callBroadcastAllPost(postDict.post_id)
                    }
                    else if str == BottomName.Mute
                                       {
                                           self.webCall_Mute(postDict.post_id, status: "1") {
                                               postDict.is_Mute = true
                                           }
                                       }
                                       else if str == BottomName.UNMute
                                       {
                                           self.webCall_Mute(postDict.post_id, status: "0") {
                                               postDict.is_Mute = false
                                           }
                                       }
                })
            }
            self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
        }
        
    }
    
    func webCall_Mute(_ post_id:String ,status:String,callback: (()->())?){
        WebService.shared.RequesURL(ServerURL.MuteUnmuteConversation, Perameters: ["user_id":globalUserId,"post_id":post_id,"status":status],showProgress: true, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true
            {
                callback?()
                self.tableView?.parentViewController?.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
            else{
                self.tableView?.parentViewController?.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
}

//MARK:- Delete Post
extension AdvertTableCell {
    
    func clickDeletePost(_ post_id:String)
    {
        if let indexPath = self.tableView?.indexPathForView(self){
            
            if let AdvertVC = self.tableView?.parentViewController as? AdvertVC {
                
                AdvertVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeletePost) { (bool) in
                    if bool
                    {
                        self.callDeletePost(post_id, callback: {
                            AdvertVC.arrAdvert.remove(at: indexPath.row)
                            self.tableView?.deleteRows(at: [indexPath], with: .left)
                        })
                    }
                }
            }
           else if let AdvertDetailsVC = self.tableView?.parentViewController as? AdvertDetailsVC {
                
                AdvertDetailsVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeletePost) { (bool) in
                    if bool
                    {
                        self.callDeletePost(post_id, callback: {
                            AdvertDetailsVC.clickBack(self)
                            NotificationCenter.default.post(name: .Delete_Post, object: ["id":post_id,"type":"5"])
                        })
                    }
                }
            }
        }
    }
    func callDeletePost(_ post_id:String,callback: (()->())?)
    {
        WebService.shared.RequesURL(ServerURL.DeletePost, Perameters: ["user_id" : globalUserId,"post_id":post_id],showProgress: true,completion: { (dicRes, success) in
            if success == true
            {
                callback?()
            }
            else{
                self.tableView?.parentViewController?.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
        }
    }
}

//MARK:- DEtails
extension AdvertTableCell {
    @IBAction func clickDetailPost(_ sender: UIButton) {
        
        if (self.tableView?.parentViewController as? AdvertDetailsVC) != nil
        {
            return
        }
        let vcInstace = StoryBoard.Advert.instantiateViewController(withIdentifier: "AdvertDetailsVC") as! AdvertDetailsVC
        vcInstace.dictPost = model
        self.tableView?.parentViewController?.pushTo(vcInstace)
     
    }
}
//MARK:- BroadcastAll POST
extension AdvertTableCell
{
    func callBroadcastAllPost(_ post_id:String) {
        WebService.shared.RequesURL(ServerURL.BroadCastPost, Perameters: ["user_id" : globalUserId,"post_id":post_id],showProgress: true,completion: { (dicRes, success) in
            self.tableView?.parentViewController?.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
        }) { (err) in
        }
    }
    
    @objc func didTapTextView(sender: UITapGestureRecognizer)
    {
        if checkValidString(model?.advert_url)
        {
            if let urlString = model?.advert_url
            {
                if urlString.hasPrefix("https://") || urlString.hasPrefix("http://"){
                    openURL(URL.init(string: urlString))
                }else {
                    let correctedURL = "http://\(urlString)"
                    openURL(URL.init(string: correctedURL))
                }
            }
        }
    }
    func openURL(_ url: URL?) {
        if ["http", "https"].contains(url?.scheme?.lowercased() ?? "") {
            self.tableView?.parentViewController?.present(SFSafariViewController(url: url!), animated: true, completion: nil)
        } else {
//            linkurlOpen_MS(linkurl: url!.absoluteString)
        }
    }
    
}
