//
//  MoreTblCell.swift
//  Proclapp
//
//  Created by Mac-4 on 05/06/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class MoreTblCell: UITableViewCell {

    @IBOutlet var bottomLline: UIView!

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnCellClick: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
