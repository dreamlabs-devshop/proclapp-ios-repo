//
//  categorySelectCLVCell.swift
//  Proclapp
//
//  Created by Ashish Parmar on 07/08/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class categorySelectCLVCell: UICollectionViewCell {

    
    @IBOutlet var btnImage: UIButton!
    //    @IBOutlet var img: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    
    override func awakeFromNib() {
        
        DispatchQueue.main.async {
            //            self.img.layer.cornerRadius = (self.img.frame.height + self.img.frame.width) / 4
            //            self.img.layer.masksToBounds = true
            
            self.btnImage.layer.cornerRadius = (self.btnImage.frame.height + self.btnImage.frame.width) / 4
            self.btnImage.layer.masksToBounds = true
            
        }
    }

}
