//
//  SuggestedFriendsCell.swift
//  Proclapp
//
//  Created by Ashish Parmar on 10/12/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class SuggestedFriendsCell: UICollectionViewCell {
    @IBOutlet weak var viewMain: UIView!

    @IBOutlet weak var imgProfile: UIImageView!

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblProfesion: UILabel!
    @IBOutlet weak var lblFollower: UILabel!
    @IBOutlet weak var btnConnect: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            
            self.imgProfile.layer.cornerRadius =    self.imgProfile.frame.size.height/2
            self.imgProfile.layer.masksToBounds = true
            
            self.viewMain.layer.cornerRadius = 10
            //self.btnFollowUnFollow.layer.cornerRadius = 4
            self.viewMain.setborder(0.5, _color: UIColor.AppBlack142)
            
        }
        // Initialization code
    }

}
