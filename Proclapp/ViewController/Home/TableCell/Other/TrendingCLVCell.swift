//
//  TrendingCLVCell.swift
//  Proclapp
//
//  Created by Mac-4 on 04/06/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class TrendingCLVCell: UICollectionViewCell {

    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblFollower: UILabel!
    @IBOutlet weak var btnFollowUnFollow: UIButton!
    @IBOutlet weak var imgview: UIImageView!

    
    
//    @IBOutlet weak var constWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            self.viewImage.setCornerRaduisToRound()
            self.viewMain.layer.cornerRadius = 10
            self.btnFollowUnFollow.layer.cornerRadius = 4
            self.viewMain.setborder(0.5, _color: UIColor.AppBlack142)

        }
        
        // Initialization code
    /* self.translatesAutoresizingMaskIntoConstraints = false
        
        let leftConstraint = contentView.leftAnchor.constraint(equalTo: leftAnchor)
        let rightConstraint = contentView.rightAnchor.constraint(equalTo: rightAnchor)
        let topConstraint = contentView.topAnchor.constraint(equalTo: topAnchor)
        let bottomConstraint = contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
        NSLayoutConstraint.activate([leftConstraint, rightConstraint, topConstraint, bottomConstraint])*/
    }

    
}


