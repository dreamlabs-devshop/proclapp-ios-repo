

import UIKit
import SafariServices

class SuggestedTblCell: UITableViewCell {

    @IBOutlet weak var viewHeightConst: NSLayoutConstraint!


    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var clvSuggested: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        

        self.viewHeightConst.constant = 170 * screenscale


        self.clvSuggested.delegate = self
        self.clvSuggested.dataSource = self
        self.clvSuggested.register(UINib(nibName: "SuggestedFriendsCell", bundle: nil), forCellWithReuseIdentifier: "SuggestedFriendsCell")

        


       
    }

    func setUpCollection() {
        self.clvSuggested.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    var _model : allPostModel?
    
    
    var model : allPostModel? {
        
        get{
            return _model
        }
        set(model)
        {
            _model = model
         
        }
    }
    
}

extension SuggestedTblCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView.accessibilityHint == "Friend"
        {
            return arrSuggestedFriends.count
        }
        else if collectionView.accessibilityHint == "Expert"
        {
            return arrSuggestedExpert.count
        }
        return arrSuggestedTopics.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "SuggestedFriendsCell", for: indexPath) as! SuggestedFriendsCell
        
        if collectionView.accessibilityHint == "Friend"
        {
            lblTitle.text = "SUGGESTED FRIENDS"

            let obj = arrSuggestedFriends[indexPath.row]
            cell.lblName.text = obj.name
            cell.imgProfile.setImageWithURL(obj.profile_image, "experts_placeholder")
            cell.lblProfesion.text = obj.profession.isEmpty == true ? " " : obj.profession
            cell.lblName.text = obj.name
            cell.lblFollower.text = obj.total_followers + " Followers"
            cell.lblName.numberOfLines = 1
            
            cell.btnConnect.setTitle(" CONNECT ", for: .normal)
            cell.btnConnect.isSelected = false
            
            cell.btnConnect.accessibilityHint = "Friend"


        }
        else if collectionView.accessibilityHint == "Expert"
        {
            lblTitle.text = "SUGGESTED EXPERTS"

            let obj = arrSuggestedExpert[indexPath.row]
            cell.lblName.text = obj.name
            cell.imgProfile.setImageWithURL(obj.profile_image, "experts_placeholder")
            cell.lblProfesion.text = obj.profession.isEmpty == true ? " " : obj.profession
            cell.lblName.text = obj.name
            cell.lblFollower.text = obj.total_followers + " Followers"
            cell.lblName.numberOfLines = 1
            
            cell.btnConnect.setTitle(" FOLLOW ", for: .normal)
            cell.btnConnect.isSelected = false
            
            cell.btnConnect.accessibilityHint = "Expert"
        }
        else
        {
            lblTitle.text = "SUGGESTED TOPICS"

            let obj = arrSuggestedTopics[indexPath.row]
            
            cell.lblProfesion.text = ""
            cell.lblName.text = obj.category_name
            cell.lblName.numberOfLines = 2
            cell.lblFollower.text = obj.total_followers + " Followers"

            cell.btnConnect.setTitle(" FOLLOW ", for: .normal)
            cell.btnConnect.setTitle("FOLLOWING", for: .selected)
            cell.btnConnect.setImage(UIImage.init(named: "right"), for: .selected)
            cell.btnConnect.accessibilityHint = "topics"
            
            cell.imgProfile.setImageWithURL(obj.category_img, "")
            cell.btnConnect.isSelected = obj.is_selected

            
            
        }
        
        cell.btnConnect.addTarget(self, action: #selector(clickFollow), for: .touchUpInside)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 110 * screenscale , height: 170 * screenscale)
    }
    
    @IBAction func clickFollow(_ sender: UIButton) {
        
        if let getIndexPath = self.clvSuggested.indexPathForView_Collection(sender),sender.accessibilityHint == "topics"
        {
            arrSuggestedTopics[getIndexPath.row].is_selected = !arrSuggestedTopics[getIndexPath.row].is_selected
            webCall_InterestedCategory_Save()
            self.clvSuggested.reloadItems(at: [getIndexPath])
            
        }
        else if let getIndexPath = self.clvSuggested.indexPathForView_Collection(sender),sender.accessibilityHint == "Friend"
        {
            self.webCall_AddRemoveFriend("1", arrSuggestedFriends[getIndexPath.row].user_id) {
                arrSuggestedFriends.remove(at: getIndexPath.row)
                self.clvSuggested.reloadData()
            }
        }
        else if let getIndexPath = self.clvSuggested.indexPathForView_Collection(sender),sender.accessibilityHint == "Expert"
        {
            self.webCall_FollowUnFollow("1", arrSuggestedExpert[getIndexPath.row].user_id) {
                arrSuggestedExpert.remove(at: getIndexPath.row)
                self.clvSuggested.reloadData()
            }
        }
    }
    func webCall_InterestedCategory_Save()
    {
        WebService.shared.RequesURL(ServerURL.Interested_Category_Save, Perameters: ["user_id" : globalUserId,"category":arrSuggestedTopics.filter({$0.is_selected}).map({$0.category_id}).joined(separator: ",")],showProgress: false,completion: { (dicRes, success) in
            debugPrint(dicRes)
        }) { (err) in
            debugPrint("\(ServerURL.Interested_Category):-->",err)
        }
    }
    
    func webCall_AddRemoveFriend( _ type:String,_ profileId:String,callback: (()->())?){
        WebService.shared.RequesURL(ServerURL.AddFriendUnFriend, Perameters: ["user_id":globalUserId,"friend_id":profileId,"type":type],showProgress: true, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true
            {
                callback?()
            }
            
        }) { (err) in
            debugPrint(err)
        }
    }
    func webCall_FollowUnFollow( _ type:String,_ profileId:String,callback: (()->())?){
        WebService.shared.RequesURL(ServerURL.FollowUnFollow, Perameters: ["user_id":globalUserId,"follower_id":profileId,"type":type],showProgress: true, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true{
                callback?()
            }
        }) { (err) in
            debugPrint(err)
        }
    }
}

