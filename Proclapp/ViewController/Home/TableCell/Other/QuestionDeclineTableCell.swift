//
//  QuestionDeclineTableCell.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/6/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class QuestionDeclineTableCell: UITableViewCell {

    @IBOutlet weak var imgProfileWidthConst: NSLayoutConstraint!
    @IBOutlet weak var imgProfileHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var imgQueWidthConst: NSLayoutConstraint!
    @IBOutlet weak var imgQueHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgQue: UIImageView!
    
    @IBOutlet weak var btnDot: UIButton!
    @IBOutlet weak var btnFlash: UIButton!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDes: UILabel!
    
    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var lblCommentCount: UILabel!
    @IBOutlet weak var lblShareCount: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            
            self.imgProfileWidthConst.constant = 30 * screenscale
            self.imgProfileHeightConst.constant = 30 * screenscale
            
            self.imgQueWidthConst.constant = 75 * screenscale
            self.imgQueHeightConst.constant = 75 * screenscale
            
            
            self.imgProfile.layer.cornerRadius =  self.imgProfileWidthConst.constant / 2
            
            
            
        }
        // Initialization code
    }
    
    var model : allPostModel? {
        didSet {
            lblUserName.text = model?.author
        }
    }

    
}
