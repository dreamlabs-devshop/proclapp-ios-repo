//
//  AdvertPhotoCLVCell.swift
//  Proclapp
//
//  Created by Mac-4 on 05/06/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class AdvertPhotoCLVCell: UICollectionViewCell {

    @IBOutlet weak var viewMain: UIView!
    
    @IBOutlet weak var imgGallary: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        DispatchQueue.main.async {
            self.viewMain.setborder(0.5, _color: UIColor.AppBlack230)
        }
    }

}
