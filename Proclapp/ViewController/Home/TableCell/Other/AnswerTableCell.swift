//
//  AnswerTableCell.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/5/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class AnswerTableCell: UITableViewCell {

    @IBOutlet weak var imgWidthConst: NSLayoutConstraint!
    
    @IBOutlet weak var imgProfile1WidthConst: NSLayoutConstraint!
    @IBOutlet weak var imgProfile1HeightConst: NSLayoutConstraint!

    @IBOutlet weak var imgProfile2WidthConst: NSLayoutConstraint!
    @IBOutlet weak var imgProfile2HeightConst: NSLayoutConstraint!

    @IBOutlet weak var imgProfile1: UIImageView!
    @IBOutlet weak var imgProfile2: UIImageView!

    
    @IBOutlet weak var imgBig: UIImageView!

    @IBOutlet weak var viewBorder: UIView!

    @IBOutlet weak var btnDot: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            
            self.imgProfile1WidthConst.constant = 30 * screenscale
            self.imgProfile1HeightConst.constant = 30 * screenscale

            self.imgProfile2WidthConst.constant = 30 * screenscale
            self.imgProfile2HeightConst.constant = 30 * screenscale

            self.imgWidthConst.constant = self.imgBig.frame.height
            
            self.viewBorder.layer.borderWidth = 1
            self.viewBorder.layer.cornerRadius = 5*screenscale
            self.viewBorder.layer.borderColor = UIColor.AppBlack.cgColor
            
            self.imgProfile1.layer.cornerRadius =  self.imgProfile1WidthConst.constant / 2
            self.imgProfile2.layer.cornerRadius =  self.imgProfile2WidthConst.constant / 2

        }
        
      
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
