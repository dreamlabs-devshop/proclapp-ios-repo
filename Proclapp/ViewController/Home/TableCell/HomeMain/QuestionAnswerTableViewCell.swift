//
//  QuestionAnswerTableViewCell.swift
//  Proclapp
//
//  Created by Ashish Parmar on 12/08/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class QuestionAnswerTableViewCell: UITableViewCell,UITextViewDelegate {
    
    @IBOutlet weak var imgProfileWidthConst: NSLayoutConstraint!
    @IBOutlet weak var imgProfileHeightConst: NSLayoutConstraint!
    
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var btnDot: UIButton!
    @IBOutlet weak var btnFlash: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnCaryover: UIButton!
    @IBOutlet weak var btnViewMore: UIButton!

    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblProfession: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAddInfo: UILabel!
    
    @IBOutlet weak var stckCaryover: UIStackView!

    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var lblCommentCount: UILabel!
    @IBOutlet weak var lblShareCount: UILabel!
    @IBOutlet weak var lblFlashedCount: UILabel!
    
    @IBOutlet var txtviewTitle: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()

        self.imgProfileWidthConst.constant = 30 * screenscale
        self.imgProfileHeightConst.constant = 30 * screenscale
        self.imgProfile.layer.cornerRadius =  self.imgProfileWidthConst.constant / 2
        
        let tapImage: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(clickProfile(_:)))
        tapImage.numberOfTapsRequired = 1
        imgProfile.addGestureRecognizer(tapImage)
        imgProfile.isUserInteractionEnabled = true
        
        txtviewTitle.delegate = self
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(didTapTextView))
        tap.numberOfTapsRequired = 1
        txtviewTitle.addGestureRecognizer(tap)
        txtviewTitle.font = UIFont.FontLatoBold(size: 15 * screenscale)
        txtviewTitle.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        txtviewTitle.textContainer.lineFragmentPadding = 0

        // Initialization code
    }
    @objc func didTapTextView(sender: UITapGestureRecognizer){
    
        debugPrint("!!!!!!! didTapTextView !!!!!!!!!!!!")
        clickDetailPost(UIButton())
    
    }
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        let userIdGet = URL.absoluteString
        debugPrint("@@@@@ ",userIdGet)
         let tel = userIdGet.components(separatedBy: ":").first
        if tel == "tel"{
            if let phone = userIdGet.components(separatedBy: ":").last{
                dialNumber_MS(number: phone)
            }
        }
        else{
            linkurlOpen_MS(linkurl: userIdGet)
        }
        return false
    }
    //MARK:- SET MODEL

    var _model : allPostModel?
    var model : allPostModel? {
        
        get{
            return _model
        }
        set(model)
        {
            _model = model
            
            lblUserName.text = model?.author
            imgProfile.setImageWithURL(model?.profile_image_thumb, "Big_dp_placeholder")
//            lblDate.text = model?.post_date.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_ddMMM)
            lblDate.text = calculateTimeDifference(model!.post_date)
            lblTitle.text = model?.post_title
            txtviewTitle.text = lblTitle.text

            lblProfession.text = model?.professional_qualification
            lblCategory.text = "Question | " + toString(model?.category_name)
            
            lblLikeCount.text = toString(model?.post_like_count)
            lblShareCount.text = model?.post_share_count
            lblCommentCount.text = toString(model?.total_answers)
            lblFlashedCount.text = model?.post_flashed_count
            
            btnLike.isSelected = model?.is_post_liked ?? false
            
            btnFlash.isSelected = model?.is_post_flashed ?? false
            
            //Caryover
            btnCaryover.isSelected = model?.is_carryover ?? false
            stckCaryover.isHidden =   model?.is_carryover_expire ?? false
//            btnCaryover.isUserInteractionEnabled = !btnCaryover.isSelected
            
            txtviewTitle.textContainer.lineBreakMode = .byTruncatingTail

            if (self.tableView?.parentViewController as? AnswerListVC) != nil{
                lblAddInfo.text = model?.additional_info
                lblTitle.numberOfLines =  btnViewMore.titleLabel?.text?.lowercased() != "view more" ? 0 : 6
                txtviewTitle.textContainer.maximumNumberOfLines = btnViewMore.titleLabel?.text?.lowercased() != "view more" ? 0 : 6
                btnViewMore.isHidden = txtviewTitle.maxNumberOfLinesTextview > 6 ? false : true
            }
            else{
                lblAddInfo.text = ""
                lblTitle.numberOfLines = 4
                txtviewTitle.textContainer.maximumNumberOfLines = 4
                btnViewMore.isHidden = true
            }
        }
    }
    
   
   
    
    //MARK:- IBAction
    
    @IBAction func viewMore(_ sender: UIButton)
    {
        sender.setTitle(sender.titleLabel?.text?.lowercased() == "view more" ? "View Less" : "View More", for: .normal)
      
        
        UIView.animate(withDuration: 0.3) {
            if let AnswerListVC = self.tableView?.parentViewController as? AnswerListVC {
                AnswerListVC.tblAnswer.reloadData()
            }
        }
    }
    
    @IBAction func btnAnswerClicked(_ sender: UIButton) {
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerVC") as! AnswerVC
        vcInstace.dictPost = model
        self.tableView?.parentViewController?.present(vcInstace, animated: true, completion: nil)
    }
    @IBAction func clickDetailPost(_ sender: UIButton) {
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        if (self.tableView?.parentViewController as? AnswerListVC) != nil
        {
            return 
        }
        let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerListVC") as! AnswerListVC
        vcInstace.dictPost = model
        self.tableView?.parentViewController?.pushTo(vcInstace)
    }
    @IBAction func clickProfile(_ sender: UIButton)
    {
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        
        if let getObje = model
        {
            if getObje.is_user, getObje.author_id == globalUserId
            {
                let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                self.tableView?.parentViewController?.pushTo(vcInstace)
            }
            else{
                let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "OtherProfileVC") as! OtherProfileVC
                vcInstace.profileId = getObje.author_id
                self.tableView?.parentViewController?.pushTo(vcInstace)
                
            }
        }
    }
    @IBAction func clickShare(_ sender: UIButton)
    {
        if let getObje = model
        {
            self.tableView?.parentViewController?.shareMaulik(shareText: getObje.post_title, shareImage: nil, shreUrl: nil)
        }
    }
    
}


//MARK:- Three Dot

extension QuestionAnswerTableViewCell
{
    @IBAction func clickDot(_ sender: UIButton) {
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        if let getObj = model
        {
            if getObj.is_user, getObj.author_id == globalUserId{
                self.tapDotMenuMy(postDict: getObj)
            }
            else{
                self.tapDotMenuOtherUser(postDict: getObj)
            }
        }
    }
    func tapDotMenuMy(postDict : allPostModel) {
        
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        
        DispatchQueue.main.async {
            guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
            popupVC.arrTitle = [BottomName.Edit,BottomName.Share,BottomName.Delete,BottomName.RequestExpert]
            //            popupVC.HeightHeader = 50 * screenscale
            popupVC.callback =  { str in
                debugPrint(str)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    if str == BottomName.Edit
                    {
                        switch postDict.postType
                        {
                        case .Article :
                            let vc = StoryBoard.AddPost.instantiateViewController(withIdentifier: "CreateArticleVC") as! CreateArticleVC
                            vc.editDict = postDict
                            self.tableView?.parentViewController?.pushTo(vc)
                            
                        case .Question :
                            let vc = StoryBoard.AddPost.instantiateViewController(withIdentifier: "AskQuestionVC") as! AskQuestionVC
                            vc.editQuesDict = postDict
                            self.tableView?.parentViewController?.pushTo(vc)
                            
                        case .ResearchPapers :
                            let vc = StoryBoard.AddPost.instantiateViewController(withIdentifier: "UploadResearchPapersVC") as! UploadResearchPapersVC
                            vc.editDict = postDict
                            self.tableView?.parentViewController?.pushTo(vc)
                        case .VoiceVideo:
                            return
                        case .Advert:
                            return
                        }
                    }
                    else if str == BottomName.Delete
                    {
                        self.clickDeletePost(postDict.post_id)
                    }
                    else if str == BottomName.RequestExpert
                    {
                        let selectExpert = StoryBoard.AddPost.instantiateViewController(withIdentifier: "SelectExpertVC") as! SelectExpertVC
                        selectExpert.postId = postDict.post_id
                        self.tableView?.parentViewController?.pushTo(selectExpert)
                    }
                    else if str == BottomName.Share {
                        self.tableView?.parentViewController?.shareMaulik(shareText: postDict.post_title, shareImage: nil, shreUrl: nil)
                    }
                })
            }
            self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
        }
    }
    
    func tapDotMenuOtherUser(postDict : allPostModel) {
        
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        
        DispatchQueue.main.async {
            guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
            popupVC.arrTitle = [BottomName.SuggestEdit,BottomName.BroadcastAll,postDict.is_Mute ? BottomName.UNMute : BottomName.Mute,BottomName.Share,BottomName.ReportAbuse]//BottomName.CompareAnswer//RequestExpert
            popupVC.callback =  { str in
                debugPrint(str)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    if str == BottomName.ReportAbuse {
                        let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "ReportabuseDesVC") as! ReportabuseDesVC
                        vcInstace.post_id = postDict.post_id
                        self.tableView?.parentViewController?.pushTo(vcInstace)
                    }
                    else if str == BottomName.RequestExpert
                    {
                        let selectExpert = StoryBoard.AddPost.instantiateViewController(withIdentifier: "SelectExpertVC") as! SelectExpertVC
                        selectExpert.postId = postDict.post_id
                        self.tableView?.parentViewController?.pushTo(selectExpert)
                    }
                    
                    else if str == BottomName.Share {
                        self.tableView?.parentViewController?.shareMaulik(shareText: postDict.post_title, shareImage: nil, shreUrl: nil)
                    }
                    
                    else if str == BottomName.BroadcastAll {
                        self.callBroadcastAllPost(postDict.post_id)
                    }
                    else if str == BottomName.SuggestEdit
                    {
                        let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "SuggestedEdit") as! SuggestedEdit
                        vcInstace.dictPost = postDict
                        self.tableView?.parentViewController?.present(vcInstace, animated: true, completion: nil)
                    }
                    else if str == BottomName.Mute
                    {
                        self.webCall_Mute(postDict.post_id, status: "1") {
                            postDict.is_Mute = true
                        }
                    }
                    else if str == BottomName.UNMute
                    {
                        self.webCall_Mute(postDict.post_id, status: "0") {
                            postDict.is_Mute = false
                        }
                    }

                })
            }
            self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
        }
        
    }
    
    func webCall_Mute(_ post_id:String ,status:String,callback: (()->())?){
        WebService.shared.RequesURL(ServerURL.MuteUnmuteConversation, Perameters: ["user_id":globalUserId,"post_id":post_id,"status":status],showProgress: true, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true
            {
                callback?()
                self.tableView?.parentViewController?.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
            else{
                self.tableView?.parentViewController?.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
    /*func tapDotMenuMy() {
     
     DispatchQueue.main.async {
     guard let popupVC = self.tableView?.parentViewController?.storyboard?.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
     popupVC.arrTitle = ["Edit","Share","Delete","Request Expert"]
     //            popupVC.HeightHeader = 50 * screenscale
     popupVC.callback =  { str in
     debugPrint(str)
     }
     self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
     }
     }*/
    
}


//MARK:- Like UnLike
extension QuestionAnswerTableViewCell {
    
    @IBAction func clickLikeUnlike(_ sender: UIButton) {
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        
        if let getObj = model
        {
            callLikeUnlikePost(getObj.post_id, sender.isSelected == true ? "0" : "1")
            sender.isSelected = !sender.isSelected
            if sender.isSelected{
                getObj.post_like_count += 1
            }
            else{
                getObj.post_like_count -= 1
            }
            getObj.is_post_liked = !getObj.is_post_liked
            lblLikeCount.text = toString(model?.post_like_count)
        }
    }
    
    func callLikeUnlikePost(_ post_id:String, _ like_unlike:String ) {
        WebService.shared.RequesURL(ServerURL.LikeUnlikePost, Perameters: ["user_id" : globalUserId,"post_id":post_id,"like_unlike":like_unlike],showProgress: false,completion: { (dicRes, success) in
            if success == true{
                debugPrint(dicRes)
            }
        }) { (err) in
        }
    }
    
}


//MARK:- Decline Post
extension QuestionAnswerTableViewCell {
    @IBAction func clickDecline(_ sender: UIButton) {
        
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        
        if let getObj = model
        {
            if let indexPath = self.tableView?.indexPathForView(self){
                
                if let HomeVC = self.tableView?.parentViewController as? HomeVC {
                    
                    
                    if self.tableView == HomeVC.tableviewQA {
                        
                        HomeVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeclinePost) { (bool) in
                            if bool
                            {
                                self.callDeclinePost(getObj.post_id)
                                
                                HomeVC.arrQuestion.remove(at: indexPath.row)
                                self.tableView?.deleteRows(at: [indexPath], with: .left)
                                
                                if let find_index = (HomeVC.arrALLHomePost).firstIndex(where: {$0.post_id == getObj.post_id}) {
                                    HomeVC.arrALLHomePost.remove(at: find_index)
//                                    HomeVC.tableHome.reloadData()
                                }
                            }
                        }
                    }
                        
                    else if self.tableView == HomeVC.tableHome {
                        HomeVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeclinePost) { (bool) in
                            if bool
                            {
                                self.callDeclinePost(getObj.post_id)
                                
                                HomeVC.arrALLHomePost.remove(at: indexPath.row)
                                self.tableView?.deleteRows(at: [indexPath], with: .left)
                                
                                if let find_index = (HomeVC.arrQuestion).firstIndex(where: {$0.post_id == getObj.post_id}) {
                                    HomeVC.arrQuestion.remove(at: find_index)
//                                    HomeVC.tableviewQA.reloadData()
                                }
                            }
                        }
                    }
                }
                    
                else if let ProfileVC = self.tableView?.parentViewController as? ProfileVC {
                    
                    ProfileVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeclinePost) { (bool) in
                        if bool
                        {
                            self.callDeclinePost(getObj.post_id)
                            
                            ProfileVC.arrALLHomePost.remove(at: indexPath.row)
                            self.tableView?.deleteRows(at: [indexPath], with: .left)
                            
                            self.removeFindIndexHomePost(getObj.post_id)
                            
                        }
                        
                    }
                }
                    
                    
                else if let OtherProfileVC = self.tableView?.parentViewController as? OtherProfileVC {
                    OtherProfileVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeclinePost) { (bool) in
                        if bool
                        {
                            self.callDeclinePost(getObj.post_id)
                            
                            OtherProfileVC.arrALLHomePost.remove(at: indexPath.row)
                            self.tableView?.deleteRows(at: [indexPath], with: .left)
                            self.removeFindIndexHomePost(getObj.post_id)
                        }
                    }
                }
                else if let AnswerListVC = self.tableView?.parentViewController as? AnswerListVC {
                    AnswerListVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeclinePost) { (bool) in
                        if bool
                        {
                            self.callDeclinePost(getObj.post_id)
                            //                            AnswerListVC.dictPost = nil
                            AnswerListVC.clickBack(self)
                            self.removeFindIndexHomePost(getObj.post_id)

//                            NotificationCenter.default.post(name: .DeclineQuestion, object: getObj.post_id)
                        }
                    }
                }
                    
                else if let AnswerUnanswerVC = self.tableView?.parentViewController as? AnswerUnanswerVC {
                    AnswerUnanswerVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeclinePost) { (bool) in
                        if bool
                        {
                            self.callDeclinePost(getObj.post_id)
                            
                            AnswerUnanswerVC.arrPost.remove(at: indexPath.row)
                            self.tableView?.deleteRows(at: [indexPath], with: .left)
                            self.removeFindIndexHomePost(getObj.post_id)
                        }
                    }
                }
                
                
            }
        }
    }
    
    
    func removeFindIndexHomePost(_ removePostId:String) {
        
//        NotificationCenter.default.post(name: .DeclineQuestion, object: removePostId)
        
        NotificationCenter.default.post(name: .Delete_Post, object: ["id":removePostId,"type":"2"])

        
        /*if let HomeVC = self.tableView?.parentViewController as? HomeVC {
         
         if let find_index = (HomeVC.arrALLHomePost).firstIndex(where: {$0.post_id == removePostId}) {
         HomeVC.arrALLHomePost.remove(at: find_index)
         }
         
         if let find_index = (HomeVC.arrQuestion).firstIndex(where: {$0.post_id == removePostId}) {
         HomeVC.arrQuestion.remove(at: find_index)
         }
         }*/
    }
    
    func callDeclinePost(_ post_id:String) {
        WebService.shared.RequesURL(ServerURL.DeclineQestion, Perameters: ["user_id" : globalUserId,"post_id":post_id,"status":"1"],showProgress: false,completion: { (dicRes, success) in
            if success == true
            {
                debugPrint(dicRes)
            }
        }) { (err) in
        }
    }
    
}

//MARK:- Delete Post
extension QuestionAnswerTableViewCell
{
    func clickDeletePost(_ post_id:String)
    {
        if let indexPath = self.tableView?.indexPathForView(self){
            
            if let HomeVC = self.tableView?.parentViewController as? HomeVC {
                
                HomeVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeletePost) { (bool) in
                    if bool
                    {
                        self.callDeletePost(post_id)
                        
                        if let find_index = (HomeVC.arrQuestion).firstIndex(where: {$0.post_id == post_id}) {
                            HomeVC.arrQuestion.remove(at: find_index)
                        }
                        
                        
                        if let find_index = (HomeVC.arrALLHomePost).firstIndex(where: {$0.post_id == post_id}) {
                            HomeVC.arrALLHomePost.remove(at: find_index)
                        }
                        
                        self.tableView?.deleteRows(at: [indexPath], with: .left)
                    }
                    
                    /* if self.tableView == HomeVC.tableviewQA {
                     HomeVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeletePost) { (bool) in
                     if bool
                     {
                     self.callDeletePost(post_id)
                     
                     HomeVC.arrQuestion.remove(at: indexPath.row)
                     self.tableView?.deleteRows(at: [indexPath], with: .left)
                     
                     if let find_index = (HomeVC.arrALLHomePost).firstIndex(where: {$0.post_id == post_id}) {
                     HomeVC.arrALLHomePost.remove(at: find_index)
                     }
                     }
                     }
                     }
                     
                     else if self.tableView == HomeVC.tableHome {
                     HomeVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeletePost) { (bool) in
                     if bool
                     {
                     self.callDeletePost(post_id)
                     
                     HomeVC.arrALLHomePost.remove(at: indexPath.row)
                     self.tableView?.deleteRows(at: [indexPath], with: .left)
                     
                     if let find_index = (HomeVC.arrQuestion).firstIndex(where: {$0.post_id == post_id}) {
                     HomeVC.arrQuestion.remove(at: find_index)
                     }
                     }
                     }
                     }*/
                }
            }
                
            else if let ProfileVC = self.tableView?.parentViewController as? ProfileVC {
                
                ProfileVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeletePost) { (bool) in
                    if bool
                    {
                        self.callDeletePost(post_id)
                        
                        ProfileVC.arrALLHomePost.remove(at: indexPath.row)
                        self.tableView?.deleteRows(at: [indexPath], with: .left)
                        
                    }
                }
            }
            else if let AnswerListVC = self.tableView?.parentViewController as? AnswerListVC {
                
                AnswerListVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeletePost) { (bool) in
                    if bool
                    {
                        self.callDeletePost(post_id)
                        AnswerListVC.clickBack(self)
                        NotificationCenter.default.post(name: .Delete_Post, object: ["id":post_id,"type":"2"])
                    }
                }
            }
        }
    }
    
    func callDeletePost(_ post_id:String) {
        WebService.shared.RequesURL(ServerURL.DeletePost, Perameters: ["user_id" : globalUserId,"post_id":post_id],showProgress: false,completion: { (dicRes, success) in
            if success == true{
                debugPrint(dicRes)
            }
        }) { (err) in
        }
    }
}

//MARK:- Flash Post
extension QuestionAnswerTableViewCell
{
    @IBAction func clickFlash(_ sender: UIButton) {
        
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        
        if let getObj = model
        {
            let vcInstace = StoryBoard.Flash.instantiateViewController(withIdentifier: "FlashPostVC") as! FlashPostVC
            vcInstace.getPostID = getObj.post_id
            self.tableView?.parentViewController?.pushTo(vcInstace)
        }
    }
}

//MARK:- BroadcastAll POST
extension QuestionAnswerTableViewCell
{
    func callBroadcastAllPost(_ post_id:String) {
        WebService.shared.RequesURL(ServerURL.BroadCastPost, Perameters: ["user_id" : globalUserId,"post_id":post_id],showProgress: true,completion: { (dicRes, success) in
            self.tableView?.parentViewController?.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
        }) { (err) in
        }
    }
}

//MARK:- Carryover POST
extension QuestionAnswerTableViewCell
{
    @IBAction func clickCarryover(_ sender: UIButton) {
        
        guard globalUserId != "" else {
            appDelegate.showAlertGuest()
            return
        }
        
        if let getObj = model
        {
            if sender.isSelected
            {
                self.tableView?.parentViewController?.showOkAlert(msg: Constant.kAlertCaryover)
            }
            else{
                guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "CalandarVC") as? CalandarVC else { return }
                popupVC.callback =  { date in
                    
                    sender.isSelected = true
                    
                    WebService.shared.RequesURL(ServerURL.CaryOverPost, Perameters: ["user_id" : globalUserId,"post_id":getObj.post_id,"carryover_date":date],showProgress: true,completion: { (dicRes, success) in
                        
                        //                        sender.isUserInteractionEnabled = false
                        
                        self.tableView?.parentViewController?.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
                    }) { (err) in
                    }
                    
                    
                };            self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
            }
        }
    }
    
}
