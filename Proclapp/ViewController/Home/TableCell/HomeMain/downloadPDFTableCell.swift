//
//  downloadPDFTableCell.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/5/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import QuickLook
import PDFKit

class downloadPDFTableCell: UITableViewCell {
    
    let previewController = QLPreviewController()

    var previewItem : NSURL?
    
    var pdfURL = ""
    var pdfName = ""
    
    
    @IBOutlet weak var btnViewPDF: UIButton!
    
    @IBOutlet weak var imgProfileWidthConst: NSLayoutConstraint!
    @IBOutlet weak var imgProfileHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var imgPDFWidthConst: NSLayoutConstraint!
    @IBOutlet weak var imgPDFHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgPDF: UIImageView!
    
    @IBOutlet weak var btnDot: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnBookmark: UIButton!
    @IBOutlet weak var btnRecommended: UIButton!

    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblProfessional : UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblPDFName: UILabel!
    @IBOutlet weak var lblPDFDes: UILabel!
    @IBOutlet weak var lblPDFSize: UILabel!
    
    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var lblCommentCount: UILabel!
    @IBOutlet weak var lblShareCount: UILabel!
    
    @IBOutlet weak var viewBorder: UIView!

    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //        DispatchQueue.main.async {
        
        viewBorder.layer.borderWidth = 1
        viewBorder.layer.borderColor = UIColor.Appcolor204.cgColor
        
        self.imgProfileWidthConst.constant = 30 * screenscale
        self.imgProfileHeightConst.constant = 30 * screenscale
        
        self.imgPDFWidthConst.constant = 34 * screenscale
        self.imgPDFHeightConst.constant = 44 * screenscale
        let tapImage: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(clickProfile(_:)))
        tapImage.numberOfTapsRequired = 1
        imgProfile.addGestureRecognizer(tapImage)
        imgProfile.isUserInteractionEnabled = true
        self.imgProfile.layer.cornerRadius =  self.imgProfileWidthConst.constant / 2
        
        //        }
        // Initialization code
    }
    
    var _model : allPostModel?
    
    
    var model : allPostModel? {
        
        get{
            return _model
        }
        set(model)
        {
            _model = model
            
            imgProfile.setImageWithURL(model?.profile_image_thumb, "Big_dp_placeholder")
            
            lblUserName.text = model?.author
//            lblDate.text = model?.post_date.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_ddMMM)
            lblDate.text = calculateTimeDifference(model!.post_date)
            lblProfessional.text = model?.professional_qualification
            lblCategory.text = "Research Paper | " + toString(model?.category_name)
            
            lblLikeCount.text = toString(model?.post_like_count)
            lblShareCount.text = model?.post_share_count
            lblCommentCount.text =  toString(model?.total_answers)
            
            lblPDFName.text =  model?.dicPDF?.ori_file_name
            lblPDFSize.text = model?.dicPDF?.file_size
            lblPDFDes.text = model?.post_title

            btnLike.isSelected = model?.is_post_liked ?? false

            btnBookmark.isSelected = model?.is_post_bookmark ?? false
            btnRecommended.isSelected = model?.is_post_recommended ?? false

        }
    }
    //MARK:- Details

    @IBAction func clickDetailPost(_ sender: UIButton) {
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        if (self.tableView?.parentViewController as? AnswerListVC) != nil
        {
            return
        }
        let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerListVC") as! AnswerListVC
        vcInstace.dictPost = model
        self.tableView?.parentViewController?.pushTo(vcInstace)
    }
    @IBAction func clickProfile(_ sender: UIButton)
    {
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        if let getObje = model
        {
            if getObje.is_user, getObje.author_id == globalUserId
            {
                let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                self.tableView?.parentViewController?.pushTo(vcInstace)
            }
            else{
                let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "OtherProfileVC") as! OtherProfileVC
                vcInstace.profileId = getObje.author_id
                self.tableView?.parentViewController?.pushTo(vcInstace)
                
            }
        }
    }
    
    
    @IBAction func clickToViewPDF(_ sender: UIButton) {
        let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "PDFViewVC") as! PDFViewVC
        vcInstace.pdfUrl = (_model?.dicPDF!.file_name)!
        vcInstace.pdfTitle = (_model?.dicPDF!.ori_file_name)!
            self.tableView?.parentViewController?.present(vcInstace, animated: true, completion: nil)
    }
    
    //MARK:- Like Unlike
    @IBAction func clickLikeUnlike(_ sender: UIButton) {
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        if let getObj = model
        {
            callLikeUnlikePost(getObj.post_id, sender.isSelected == true ? "0" : "1")
            
            sender.isSelected = !sender.isSelected
            
            if sender.isSelected
            {
                getObj.post_like_count += 1
            }
            else
            {
                getObj.post_like_count -= 1
            }
            getObj.is_post_liked = !getObj.is_post_liked
            lblLikeCount.text = toString(model?.post_like_count)
        }
    }
    
    func callLikeUnlikePost(_ post_id:String, _ like_unlike:String ) {
        WebService.shared.RequesURL(ServerURL.LikeUnlikePost, Perameters: ["user_id" : globalUserId,"post_id":post_id,"like_unlike":like_unlike],showProgress: false,completion: { (dicRes, success) in
            if success == true{
                debugPrint(dicRes)
            }
        }) { (err) in
        }
    }
    
    
    
    //MARK:- Three Dot
    @IBAction func clickDot(_ sender: UIButton) {
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        if let getObj = model
        {
            if getObj.is_user, getObj.author_id == globalUserId{
                self.tapDotMenuMy(postDict: getObj)
            }
            else{
                self.tapDotMenuOtherUser(postDict: getObj)
            }
        }
    }
    func tapDotMenuMy(postDict : allPostModel) {
        
        DispatchQueue.main.async {
            guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
            popupVC.arrTitle = [BottomName.Edit,BottomName.Share,BottomName.Delete,BottomName.RequestExpert]
            
            //            popupVC.HeightHeader = 50 * screenscale
            popupVC.callback =  { str in
                debugPrint(str)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    if str == BottomName.Edit
                    {
                        switch postDict.postType
                        {
                        case .Article :
                            let vc = StoryBoard.AddPost.instantiateViewController(withIdentifier: "CreateArticleVC") as! CreateArticleVC
                            vc.editDict = postDict
                            self.tableView?.parentViewController?.pushTo(vc)
                            
                        case .Question :
                            let vc = StoryBoard.AddPost.instantiateViewController(withIdentifier: "AskQuestionVC") as! AskQuestionVC
                            vc.editQuesDict = postDict
                            self.tableView?.parentViewController?.pushTo(vc)
                            
                        case .ResearchPapers :
                            let vc = StoryBoard.AddPost.instantiateViewController(withIdentifier: "UploadResearchPapersVC") as! UploadResearchPapersVC
                            vc.editDict = postDict
                            self.tableView?.parentViewController?.pushTo(vc)
                        case .VoiceVideo:
                            return
                        case .Advert:
                            return
                        }
                    }
                    else if str == BottomName.Delete
                    {
                        self.clickDeletePost(postDict.post_id)
                    }
                    else if str == BottomName.RequestExpert
                    {
                        let selectExpert = StoryBoard.AddPost.instantiateViewController(withIdentifier: "SelectExpertVC") as! SelectExpertVC
                        selectExpert.postId = postDict.post_id
                        self.tableView?.parentViewController?.pushTo(selectExpert)
                    }
                    else if str == BottomName.Share {
                        self.tableView?.parentViewController?.shareMaulik(shareText: postDict.post_title, shareImage: nil, shreUrl: postDict.dicPDF?.file_name)
                    } 
                })
            }
            self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
        }
    }
    
    func tapDotMenuOtherUser(postDict : allPostModel) {
        
        DispatchQueue.main.async {
            guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
            popupVC.arrTitle = [BottomName.Share,BottomName.ReportAbuse]
            popupVC.callback =  { str in
                debugPrint(str)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    if str == BottomName.ReportAbuse {
                        let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "ReportabuseDesVC") as! ReportabuseDesVC
                        vcInstace.post_id = postDict.post_id
                        self.tableView?.parentViewController?.pushTo(vcInstace)
                    }
                    else if str == BottomName.Share {
                        self.tableView?.parentViewController?.shareMaulik(shareText: postDict.post_title, shareImage: nil, shreUrl: postDict.dicPDF?.file_name)
                    }
                })
            }
            self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
        }
        
    }
    @IBAction func tapShare(_ sender: UIButton) {
        
        if let postDict = model
        {
            self.tableView?.parentViewController?.shareMaulik(shareText: postDict.post_title, shareImage: nil, shreUrl: postDict.dicPDF?.file_name)
        }
    }
    
}
    


//MARK:- QLPreviewController Datasource
/*extension downloadPDFTableCell: QLPreviewControllerDataSource {
    
    @IBAction func tapDownLoadPDF(_ sender: UIButton) {
        if let get = model?.dicPDF{
            
            
            guard let url = URL(string: get.file_name) else {
                return //be safe
            }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
            /*self.pdfURL = get.file_name
            self.pdfName =  self.pdfURL.replacingOccurrences(of: "/", with: "")
            
            debugPrint(self.pdfName)
            
            // Download file
            self.downloadfile(completion: {(success, fileLocationURL) in
                
                if success {
                    // Set the preview item to display======
                    self.previewItem = fileLocationURL! as NSURL
                    self.previewController.dataSource = self
                    // Display file
                    DispatchQueue.main.async {
                        self.tableView?.parentViewController?.present(self.previewController, animated: true, completion: nil)
                    }
                }else{
                    debugPrint("File can't be downloaded")
                }
            })*/
            
        }
    }
    
    
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return self.previewItem as QLPreviewItem
    }
    
    func downloadfile(completion: @escaping (_ success: Bool,_ fileLocation: URL?) -> Void){
        
        let itemUrl = URL(string: self.pdfURL)
        
        // then lets create your document folder url
        let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        // lets create your destination file url
        let destinationUrl = documentsDirectoryURL.appendingPathComponent(self.pdfName)
        
        // to check if it exists before downloading it
        if FileManager.default.fileExists(atPath: destinationUrl.path) {
            debugPrint("The file already exists at path")
            completion(true, destinationUrl)
            
            // if the file doesn't exist
        } else {
            
            // you can use NSURLSession.sharedSession to download the data asynchronously
            URLSession.shared.downloadTask(with: itemUrl!, completionHandler: { (location, response, error) -> Void in
                guard let tempLocation = location, error == nil else { return }
                do {
                    // after downloading your file you need to move it to your destination url
                    try FileManager.default.moveItem(at: tempLocation, to: destinationUrl)
                    print("File moved to documents folder")
                    completion(true, destinationUrl)
                } catch let error as NSError {
                    print(error.localizedDescription)
                    completion(false, nil)
                }
            }).resume()
        }
    }
}*/

//MARK:- QLPreviewController Datasource
extension downloadPDFTableCell: QLPreviewControllerDataSource {
    
    @IBAction func tapDownLoadPDF(_ sender: UIButton) {
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        if let get = model?.dicPDF
        {
            // Download file
           // @Purvi need to remove payment from research paper
            /*if model?.is_subscribe == false
            {
                self.tableView?.parentViewController?.showOkCancelAlertWithAction(msg: Constant.kAlertSubscibe, handler: { (success) in
                    if success
                    {
                        let vcInstace = StoryBoard.Wallet.instantiateViewController(withIdentifier: "WebviewPaymentVC") as! WebviewPaymentVC
                        vcInstace.strPaymentType = "2"
                        vcInstace.postId = ToString(self.model?.post_id)
                        vcInstace.callbackPayment = { status in
                            if status == Constant.kPayment_Success
                            {
                                if let HomeVC = self.tableView?.parentViewController as? HomeVC {
                                    for dic in HomeVC.arrResearchPaper
                                    {
                                        dic.is_subscribe = true
                                    }
                                    for dic in HomeVC.arrALLHomePost
                                    {
                                        dic.is_subscribe = true
                                    }
                                }
                                else if let ProfileVC = self.tableView?.parentViewController as? ProfileVC {
                                    for dic in ProfileVC.arrALLHomePost
                                    {
                                        dic.is_subscribe = true
                                    }
                                }
                                else if let OtherProfileVC = self.tableView?.parentViewController as? OtherProfileVC {
                                    for dic in OtherProfileVC.arrALLHomePost
                                    {
                                        dic.is_subscribe = true
                                    }
                                }
                            }
                           
                        }
                        self.tableView?.parentViewController?.pushTo(vcInstace)
                    }
                })
                return
            }*/
            self.downloadfile(pdfURL: get.file_name, pdfName: get.ori_file_name, completion: {(success, fileLocationURL) in
                
                if success {
                    // Set the preview item to display======
                    self.previewItem = nil
                    
                    if let url = fileLocationURL{
                        DispatchQueue.main.async {
                            let previewController = QLPreviewController()
                            self.previewItem = url as NSURL
                            previewController.dataSource = self
                            // Display file
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                                self.tableView?.parentViewController?.present(previewController, animated: true, completion: nil)
                            })
                        }
                    }
                }else{
                    debugPrint("File can't be downloaded")
                }
            })
            
        }
    }
    
    
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return (self.previewItem ?? NSURL())
    }
    
    func downloadfile(pdfURL : String,pdfName : String ,completion: @escaping (_ success: Bool,_ fileLocation: URL?) -> Void){
        
        let itemUrl = URL(string: pdfURL)
        
        // then lets create your document folder url
        let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        // lets create your destination file url
        let destinationUrl = documentsDirectoryURL.appendingPathComponent(pdfName)
        
        // to check if it exists before downloading it
        if FileManager.default.fileExists(atPath: destinationUrl.path) {
            debugPrint("The file already exists at path")
            completion(true, destinationUrl)
            
            // if the file doesn't exist
        } else {
            
            // you can use NSURLSession.sharedSession to download the data asynchronously
            URLSession.shared.downloadTask(with: itemUrl!, completionHandler: { (location, response, error) -> Void in
                guard let tempLocation = location, error == nil else { return }
                do {
                    // after downloading your file you need to move it to your destination url
                    try FileManager.default.moveItem(at: tempLocation, to: destinationUrl)
                    print("File moved to documents folder")
                    completion(true, destinationUrl)
                } catch let error as NSError {
                    print(error.localizedDescription)
                    completion(false, nil)
                }
            }).resume()
        }
    }
}



//MARK:- Delete Post
extension downloadPDFTableCell {
    
    func clickDeletePost(_ post_id:String)
    {
        if let indexPath = self.tableView?.indexPathForView(self){
            
            if let HomeVC = self.tableView?.parentViewController as? HomeVC {
                
                HomeVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeletePost) { (bool) in
                    if bool
                    {
                        self.callDeletePost(post_id)
                        
                        if let find_index = (HomeVC.arrResearchPaper).firstIndex(where: {$0.post_id == post_id}) {
                            HomeVC.arrResearchPaper.remove(at: find_index)
                            
                        }
                        
                        if let find_index = (HomeVC.arrALLHomePost).firstIndex(where: {$0.post_id == post_id}) {
                            HomeVC.arrALLHomePost.remove(at: find_index)
                        }
                        
                        self.tableView?.deleteRows(at: [indexPath], with: .left)
                        
                    }
                }
            }
            else if let ProfileVC = self.tableView?.parentViewController as? ProfileVC {
                
                ProfileVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeletePost) { (bool) in
                    if bool
                    {
                        self.callDeletePost(post_id)
                        
                        ProfileVC.arrALLHomePost.remove(at: indexPath.row)
                        self.tableView?.deleteRows(at: [indexPath], with: .left)
                    }
                }
            }
            else if let AnswerListVC = self.tableView?.parentViewController as? AnswerListVC {
                
                AnswerListVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeletePost) { (bool) in
                    if bool
                    {
                        self.callDeletePost(post_id)
                        AnswerListVC.clickBack(self)
                        NotificationCenter.default.post(name: .Delete_Post, object: ["id":post_id,"type":"4"])
                    }
                }
            }
        }
    }
    func callDeletePost(_ post_id:String) {
        WebService.shared.RequesURL(ServerURL.DeletePost, Perameters: ["user_id" : globalUserId,"post_id":post_id],showProgress: false,completion: { (dicRes, success) in
            if success == true{
                debugPrint(dicRes)
            }
        }) { (err) in
        }
    }
}

//MARK:- Bookmark Post
extension downloadPDFTableCell {
    @IBAction func clickBookMarkUnBookMark(_ sender: UIButton) {
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        if let getObj = model
        {
            callBookmarkUnBookmark(getObj.post_id, sender.isSelected == true ? "0" : "1")
            sender.isSelected = !sender.isSelected
            getObj.is_post_bookmark = !getObj.is_post_bookmark
            
            if let bookmarkVC = self.tableView?.parentViewController as? BookmarkedPostVC {
                if let indexPath = self.tableView?.indexPathForView(self){
                    bookmarkVC.arrBookmarkList.remove(at: indexPath.row)
                    self.tableView?.deleteRows(at: [indexPath], with: .left)
                }
            }
        }
    }
    
    func callBookmarkUnBookmark(_ post_id:String, _ bookUnbookMark:String ) {
        WebService.shared.RequesURL(ServerURL.BookmarkUnbookmark, Perameters: ["user_id" : globalUserId,"post_id":post_id,"bookmark_unbookmark":bookUnbookMark],showProgress: false,completion: { (dicRes, success) in
            if success == true{
                debugPrint(dicRes)
            }
        }) { (err) in
        }
    }
    
}
//MARK:- BroadcastAll POST
extension downloadPDFTableCell
{
    func callBroadcastAllPost(_ post_id:String) {
        WebService.shared.RequesURL(ServerURL.BroadCastPost, Perameters: ["user_id" : globalUserId,"post_id":post_id],showProgress: true,completion: { (dicRes, success) in
            self.tableView?.parentViewController?.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
        }) { (err) in
        }
    }
}
//MARK:- Recommanded Post
extension downloadPDFTableCell
{
    @IBAction func clickRecommanded(_ sender: UIButton) {
        
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        if let getObj = model
        {
            let vcInstace = StoryBoard.Flash.instantiateViewController(withIdentifier: "FlashPostVC") as! FlashPostVC
            vcInstace.getPostID = getObj.post_id
            vcInstace.isFromRecommended = true
            vcInstace.callback = { _ in
                getObj.is_post_recommended = true
                self.tableView?.reloadData()
            }
            self.tableView?.parentViewController?.pushTo(vcInstace)
        }
    }
}
