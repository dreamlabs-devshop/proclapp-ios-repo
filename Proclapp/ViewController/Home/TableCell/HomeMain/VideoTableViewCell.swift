//
//  VideoTableViewCell.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/10/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import AVKit

class VideoTableViewCell: UITableViewCell {
    
    
    
    let avPlayerController = AVPlayerViewController()
    
    @IBOutlet weak var imgProfileWidthConst: NSLayoutConstraint!
    @IBOutlet weak var imgProfileHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var videoImage: UIImageView!
    
    @IBOutlet weak var btnDot: UIButton!
    
    @IBOutlet weak var btnReply: UIButton!
    @IBOutlet weak var btnViewDetails: UIButton!

    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblProfessional : UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblShareCount: UILabel!
    
    @IBOutlet weak var btnComment: UIButton!
    
    @IBOutlet weak var lblCommentCount: UIButton!
    
    @IBOutlet weak var btnLikeCount: UIButton!
    
    var shouldPlayVideo = false;
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgProfileWidthConst.constant = 30 * screenscale
        self.imgProfileHeightConst.constant = 30 * screenscale
        self.imgProfile.layer.cornerRadius =  self.imgProfileWidthConst.constant / 2
        
        let tapImage: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(clickProfile(_:)))
        tapImage.numberOfTapsRequired = 1
        imgProfile.addGestureRecognizer(tapImage)
        imgProfile.isUserInteractionEnabled = true
        
    }
    
    
    var model : allPostModel? {
        didSet {
            lblUserName.text = model?.author
            imgProfile.setImageWithURL(model?.profile_image_thumb, "Big_dp_placeholder")
//            lblDate.text = model?.post_date.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_ddMMM)
            lblDate.text = calculateTimeDifference(model!.post_date)
            lblTitle.text = model?.post_description
            lblProfessional.text = model?.professional_qualification
            lblShareCount.text = model?.post_share_count
            lblTime.text = ""
            btnLikeCount.setTitle(toString(model?.post_like_count), for: .normal)
            if(model!.is_post_liked){
                btnLikeCount.setImage(UIImage(named:"heart_active"), for: .normal)
            }
            
            videoImage.setImageWithURL(model?.dicVideo?.video_img, "video_placeholder")
            lblCommentCount.setTitle(toString(model?.total_answers), for: .normal)
            
            
            if let obj = model{
                let Request = obj.request_to_reply
                if loginData?.is_expert ?? false{
                    btnReply.isHidden = true
                    btnViewDetails.isHidden = true
                    
                    if Request == 2 || Request == 3{
                        // lblTitle.backgroundColor = .systemRed
                        btnReply.isHidden = false
                        btnViewDetails.isHidden =  false
                        btnComment.isHidden =  false
                    }
                    else if Request == 1 || Request == 4{
                        if obj.is_user, obj.author_id == globalUserId
                        {
                            // lblTitle.backgroundColor = .blue
                            btnViewDetails.isHidden =  false
                        }
                    }
                }
                else{
                    btnReply.isHidden = true
                    btnViewDetails.isHidden =  true
                    if Request == 1 || Request == 4{
                        // lblTitle.backgroundColor = .green
                        btnViewDetails.isHidden =  false
                    }
                }
            }
            
             btnViewDetails.isHidden =  true
          /* if loginData?.is_expert ?? false
            {
                btnReply.isHidden = true

                if Request == 2 || Request == 3
                {
                    btnReply.isHidden = false
                }
                btnViewDetails.isHidden =  btnReply.isHidden
            }
            else
            {
                btnReply.isHidden = true
                btnViewDetails.isHidden =  true

                if Request == 1 || Request == 4
                {
                    btnViewDetails.isHidden =  false
                }
            }*/

            /* if let get = model?.dicVideo{
             guard let url = URL(string: get.video_url) else {
             return
             }
             getThumbnailImageFromVideoUrl(url: url ) { (thumbImage) in
             self.videoImage.image = thumbImage
             }
             }*/
        }
    }
    
    @IBAction func clickProfile(_ sender: UIButton)
    {
        if let getObje = model
        {
            if getObje.is_user, getObje.author_id == globalUserId
            {
                let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                self.tableView?.parentViewController?.pushTo(vcInstace)
            }
            else{
                let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "OtherProfileVC") as! OtherProfileVC
                vcInstace.profileId = getObje.author_id
                self.tableView?.parentViewController?.pushTo(vcInstace)
                
            }
        }
    }
    
    @IBAction func tapPlayVideo(_ sender: UIButton) {
        
        if(shouldPlayVideo == true){
            if let get = model?.dicVideo{
                guard let url = URL(string: get.video_url) else {
                    return
                }
                let player = AVPlayer(url: url)
                avPlayerController.player = player
                self.tableView?.parentViewController?.present(avPlayerController, animated: true) {
                    player.play()
                }
            }
        }
        else {
            if let getObj = model
            {
                let getVC = StoryBoard.Home.instantiateViewController(withIdentifier: "VoiceVideoDetailVC") as! VoiceVideoDetailVC
                getVC.objPost = getObj
                self.tableView?.parentViewController?.pushTo(getVC)
            }
        }
        
    }
    
    @IBAction func btnShare(_ sender: UIButton) {
        if let getObj = model
        {
            self.tableView?.parentViewController?.shareMaulik(shareText: getObj.category_name, shareImage: nil, shreUrl: getObj.dicVideo?.video_url)
        }
    }
    
    
    @IBAction func btnLike(_ sender: UIButton) {
        guard globalUserId != "" else {
                             appDelegate.showAlertGuest()
                             return
                         }
             
             if let getObj = model
             {
                 callLikeUnlikePost(getObj.post_id, sender.isSelected == true ? "0" : "1")
                 
                 sender.isSelected = !sender.isSelected
                 
                 if sender.isSelected
                 {
                     getObj.post_like_count += 1
                 }
                 else
                 {
                     getObj.post_like_count -= 1
                 }
                 getObj.is_post_liked = !getObj.is_post_liked
                btnLikeCount.setTitle(toString(model?.post_like_count), for: .normal)
             }
    }
    
    @IBAction func btnComment(_ sender: UIButton) {
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        if let get = model?.post_id
        {
            let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "DiscussionVC") as! DiscussionVC
            vcInstace.strPostID = get
            vcInstace.customeHeaderTitle = "Add Comment"
            self.tableView?.parentViewController?.present(vcInstace, animated: true, completion: nil)
        }
    }
    
    
    //MARK:-Details
    
    @IBAction func tapReply(_ sender: Any)
    {
        let popVC = StoryBoard.AddPost.instantiateViewController(withIdentifier: "AddExpertReplyPopupVC") as! AddExpertReplyPopupVC
        popVC.modalPresentationStyle = .overCurrentContext
        popVC.modalTransitionStyle = .crossDissolve
        popVC.callback =  { sender in

         if sender == 1{
             let vc = StoryBoard.AddPost.instantiateViewController(withIdentifier: "AddExpertAnswerVC") as! AddExpertAnswerVC
             vc.obj = self.model
             self.tableView?.parentViewController?.pushTo(vc)
         }
            else if sender == 2
         {
             let vc = StoryBoard.Voice.instantiateViewController(withIdentifier: "VoiceRecordVC") as! VoiceRecordVC
             vc.isFromExpertReply = true
             vc.obj = self.model
             self.tableView?.parentViewController?.pushTo(vc)
         }
            else if sender == 3{
             let vc = StoryBoard.Voice.instantiateViewController(withIdentifier: "RecordVideoVC") as! RecordVideoVC
             vc.isFromExpertReply = true
             vc.obj = self.model
             self.tableView?.parentViewController?.pushTo(vc)
         }
         

        }
        self.tableView?.parentViewController?.tabBarController?.present(popVC, animated: true, completion: nil)
     
    }
    @IBAction func tapDetails(_ sender: Any)
    {
        if let getObj = model
        {
            let getVC = StoryBoard.Home.instantiateViewController(withIdentifier: "VoiceVideoDetailVC") as! VoiceVideoDetailVC
            getVC.objPost = getObj
            self.tableView?.parentViewController?.pushTo(getVC)
        }
    }
    
}

  

//MARK:- Three Dot

extension VideoTableViewCell {
    
    func callLikeUnlikePost(_ post_id:String, _ like_unlike:String ) {
        WebService.shared.RequesURL(ServerURL.LikeUnlikePost, Perameters: ["user_id" : globalUserId,"post_id":post_id,"like_unlike":like_unlike],showProgress: false,completion: { (dicRes, success) in
            if success == true{
                debugPrint(dicRes)
            }
        }) { (err) in
        }
    }
    @IBAction func clickDot(_ sender: UIButton) {
        if let getObj = model
        {
            if getObj.is_user, getObj.author_id == globalUserId{
                self.tapDotMenuMy(postDict: getObj)
            }
            else{
                self.tapDotMenuOtherUser(postDict: getObj)
            }
        }
    }
    func tapDotMenuMy(postDict : allPostModel) {
        
        DispatchQueue.main.async {
            guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
            popupVC.arrTitle = [BottomName.Share,BottomName.Delete]
            
            //            popupVC.HeightHeader = 50 * screenscale
            popupVC.callback =  { str in
                debugPrint(str)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    if str == BottomName.Delete
                    {
                        self.clickDeletePost(postDict.post_id)
                    }
                    else if str == BottomName.RequestExpert
                    {
                        let selectExpert = StoryBoard.AddPost.instantiateViewController(withIdentifier: "SelectExpertVC") as! SelectExpertVC
                        selectExpert.postId = postDict.post_id
                        self.tableView?.parentViewController?.pushTo(selectExpert)
                    }
                    else if str == BottomName.Share
                    {
                        self.tapOnShare(self)
                    }
                    
                })
                
            }
            self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
        }
    }
    
    func tapDotMenuOtherUser(postDict : allPostModel) {
        
        DispatchQueue.main.async {
            guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
            popupVC.arrTitle = [BottomName.Share]
            popupVC.callback =  { str in
                debugPrint(str)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    if str == BottomName.ReportAbuse {
                        let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "ReportabuseDesVC") as! ReportabuseDesVC
                        vcInstace.post_id = postDict.post_id
                        self.tableView?.parentViewController?.pushTo(vcInstace)
                    }
                    else if str == BottomName.RequestExpert
                    {
                        let selectExpert = StoryBoard.AddPost.instantiateViewController(withIdentifier: "SelectExpertVC") as! SelectExpertVC
                        selectExpert.postId = postDict.post_id
                        self.tableView?.parentViewController?.pushTo(selectExpert)
                    }
                    else if str == BottomName.Share
                    {
                        self.tapOnShare(self)
                    }
                })
            }
            self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
        }
        
    }
    
    
}

//MARK:- Delete Post
extension VideoTableViewCell {
    func clickDeletePost(_ post_id:String)
    {
        if let indexPath = self.tableView?.indexPathForView(self){
            
            if let HomeVC = self.tableView?.parentViewController as? HomeVC {
                
                HomeVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeletePost) { (bool) in
                    if bool
                    {
                        self.callDeletePost(post_id)
                        
                        if let find_index = (HomeVC.arrVoiceVideo).firstIndex(where: {$0.post_id == post_id}) {
                            HomeVC.arrVoiceVideo.remove(at: find_index)
                            
                        }
                        
                        if let find_index = (HomeVC.arrALLHomePost).firstIndex(where: {$0.post_id == post_id}) {
                            HomeVC.arrALLHomePost.remove(at: find_index)
                        }
                        
                        self.tableView?.deleteRows(at: [indexPath], with: .left)
                        
                    }
                }
            }
            else if let ProfileVC = self.tableView?.parentViewController as? ProfileVC {
                
                ProfileVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeletePost) { (bool) in
                    if bool
                    {
                        self.callDeletePost(post_id)
                        
                        ProfileVC.arrALLHomePost.remove(at: indexPath.row)
                        self.tableView?.deleteRows(at: [indexPath], with: .left)
                    }
                }
            }
            
            else if let VideosVC = self.tableView?.parentViewController as? VideosVC {
                
                VideosVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeletePost) { (bool) in
                    if bool
                    {
                        self.callDeletePost(post_id)
                        
                        VideosVC.arrVoiceVideo.remove(at: indexPath.row)
                        self.tableView?.deleteRows(at: [indexPath], with: .left)
                    }
                }
            }
        }
    }
    func callDeletePost(_ post_id:String) {
        WebService.shared.RequesURL(ServerURL.DeletePost, Perameters: ["user_id" : globalUserId,"post_id":post_id],showProgress: false,completion: { (dicRes, success) in
            if success == true{
                debugPrint(dicRes)
            }
        }) { (err) in
        }
    }
}

//MARK:- Share
extension VideoTableViewCell {
    @IBAction func tapOnShare(_ sender: Any)
    {
        if let getObj = model
        {
            self.tableView?.parentViewController?.shareMaulik(shareText: getObj.category_name, shareImage: nil, shreUrl: getObj.dicVideo?.video_url)
        }
        
    }
    func share(shareText:String?,shareImage:UIImage?,shreUrl:String?){
        
        DispatchQueue.main.async {
            var objectsToShare = [AnyObject]()
            
            if let shareTextObj = shareText{
                objectsToShare.append(shareTextObj as AnyObject)
            }
            
            if let shareImageObj = shareImage{
                objectsToShare.append(shareImageObj)
            }
            if let shareUrlObj = shreUrl{
                objectsToShare.append(shareUrlObj as AnyObject)
            }
            
            if shareText != nil || shareImage != nil || shreUrl != nil {
                let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.tableView?.parentViewController?.view
                activityViewController.setValue(Constant.appName, forKey: "Subject")
                
                self.tableView?.parentViewController?.present(activityViewController, animated: true, completion: nil)
            }else
            {
                print("There is nothing to share")
            }
        }
    }
}
func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
    DispatchQueue.global().async { //1
        let asset = AVAsset(url: url) //2
        let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
        avAssetImageGenerator.appliesPreferredTrackTransform = true //4
        let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
        do {
            let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
            let thumbImage = UIImage(cgImage: cgThumbImage) //7
            DispatchQueue.main.async { //8
                completion(thumbImage) //9
            }
        } catch {
            print(error.localizedDescription) //10
            DispatchQueue.main.async {
                completion(nil) //11
            }
        }
    }
}
