//
//  ArticleDeclineTableCell.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/6/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import SwiftLinkPreview
import SafariServices
import ImageViewer_swift

class ArticleDeclineTableCell: UITableViewCell,UITextViewDelegate {
    


    @IBOutlet weak var wrapperView: UIView!
    
    @IBOutlet weak var imgProfileWidthConst: NSLayoutConstraint!
    @IBOutlet weak var imgProfileHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var imgArticleWidthConst: NSLayoutConstraint!
    @IBOutlet weak var imgArticleHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgArticle: UIImageView!
    
    @IBOutlet weak var btnDot: UIButton!
    @IBOutlet weak var btnFlash: UIButton!
    @IBOutlet weak var btnMultiImage: UIButton?
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnCaryover: UIButton!
    @IBOutlet weak var btnViewMore: UIButton!

    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblProfession: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDes: UILabel!
    @IBOutlet weak var lblImageCount: UILabel?
    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var lblCommentCount: UILabel!
    
    @IBOutlet weak var viewAlpha: UIView?
    
    @IBOutlet var txtviewDes: UITextView!
    
    @IBOutlet weak var stckCaryover: UIStackView!


    let sizeArticleSingleImage = 125 * screenscale
    let sizeArticleImage = 75 * screenscale
    let sizeProfileImage = 30 * screenscale
    
    //For Preview Show
    @IBOutlet weak var viewPreview: UIView!
    private let slp = SwiftLinkPreview(cache: InMemoryCache())
    @IBOutlet private weak var previewTitle: UILabel?
    @IBOutlet private weak var previewCanonicalUrl: UILabel?
    @IBOutlet private weak var previewDescription: UILabel?
    @IBOutlet private weak var detailedView: UIView?
    @IBOutlet private weak var favicon: UIImageView?
    @IBOutlet private weak var indiCator: UIActivityIndicatorView?
    
    @IBOutlet weak var viewPreviewHeightConst: NSLayoutConstraint!




    override func awakeFromNib() {
        super.awakeFromNib()
        
        //        DispatchQueue.main.async {
        
        self.viewPreview.applyDropShadow()
        
        self.imgProfileWidthConst.constant = self.sizeProfileImage
        self.imgProfileHeightConst.constant = self.sizeProfileImage
        
        self.imgArticleWidthConst.constant = self.sizeArticleImage
        self.imgArticleHeightConst.constant = self.sizeArticleImage
        
        
        self.imgProfile.layer.cornerRadius =  self.imgProfileWidthConst.constant / 2
        
        let tapImage: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(clickProfile(_:)))
        tapImage.numberOfTapsRequired = 1
        imgProfile.addGestureRecognizer(tapImage)
        imgProfile.isUserInteractionEnabled = true
        
        //        }
        // Initialization code
        
        txtviewDes.delegate = self
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(didTapTextView))
        tap.numberOfTapsRequired = 1
        txtviewDes.addGestureRecognizer(tap)
        txtviewDes.font = UIFont.FontLatoRegular(size: 12 * screenscale)
        txtviewDes.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        txtviewDes.textContainer.lineFragmentPadding = 0
    }
    
    
    
    
    
    @objc func didTapTextView(sender: UITapGestureRecognizer){
        debugPrint("!!!!!!! didTapTextView !!!!!!!!!!!!")
        clickDetailPost(UIButton())
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        let userIdGet = URL.absoluteString
        debugPrint("@@@@@ ",userIdGet)
        let tel = userIdGet.components(separatedBy: ":").first
        if tel == "tel"{
            if let phone = userIdGet.components(separatedBy: ":").last{
                dialNumber_MS(number: phone)
            }
        }
        else if tel == "mailto" {
            if let emailStr = userIdGet.components(separatedBy: ":").last{
                linkurlOpen_Email(email: emailStr)
            }
        }
        else{
            linkurlOpen_MS(linkurl: userIdGet)
        }
        return false
    }
    
    var _model : allPostModel?

    
    var model : allPostModel? {
        
        get{
            return _model
        }
        set(model)
        {
            _model = model
            
            imgArticle.setImageWithURL(model?.arrImages.first?.image_name, "place_logo")
            imgProfile.setImageWithURL(model?.profile_image_thumb, "Big_dp_placeholder")
            
            lblUserName.text = model?.author
//            lblDate.text = model?.post_date.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_ddMMM)
            lblDate.text = calculateTimeDifference(model!.post_date)
            lblTitle.text = model?.post_title
            lblDes.text = model?.post_description
            lblLikeCount.text = toString(model?.post_like_count)
            lblCommentCount.text = toString(model?.total_answers)
            lblProfession.text = model?.professional_qualification
            lblCategory.text = "Article | " + toString(model?.category_name)
            
            //let imgesCount = ToInt((model?.arrImages.count))

            //let wTemp = imgesCount >= 1 ? sizeArticleImage : 0

            //self.imgArticleWidthConst.constant = imgesCount == 1 ? screenwidth : wTemp
            
            //self.imgArticleHeightConst.constant = imgesCount > 0 ? sizeArticleImage : 0

           btnFlash.isSelected = model?.is_post_flashed ?? false

            
            btnLike.isSelected = model?.is_post_liked ?? false
            btnMultiImage?.addTarget(self, action: #selector(tapMultiImage), for: .touchUpInside)
            
            //Caryover
            btnCaryover.isSelected = model?.is_carryover ?? false
            stckCaryover.isHidden =   model?.is_carryover_expire ?? false
//            btnCaryover.isUserInteractionEnabled = !btnCaryover.isSelected

            
            txtviewDes.textContainer.lineBreakMode = .byTruncatingTail
            txtviewDes.text = lblDes.text
            
            if let allURL = model?.arrImages.map({$0.image_name}).compactMap({URL (string: $0)})
            {
                //UIImage.init(named: "place_logo")
                
                imgArticle.setupImageViewer(urls: allURL, initialIndex: 0, options: [.theme(.dark)], placeholder: nil, from: self.tableView?.parentViewController)
            }


            let imgesCount = ToInt((model?.arrImages.count))
            let wTemp = imgesCount >= 1 ? sizeArticleImage : 0
            let hTemp = imgesCount >= 1 ? sizeArticleImage + 200 : 0

            imgArticle.isHidden = imgesCount == 0
            viewAlpha?.alpha = imgesCount > 1 ? 0.4 : 0.0
            lblImageCount?.text = imgesCount > 1 ? "+ \(imgesCount - 1)" : ""
            
            if self.tableView?.parentViewController is ArticleDetailVC
            {
                self.imgArticleWidthConst.constant = imgesCount == 1 ? screenwidth : wTemp
                
                self.imgArticleHeightConst.constant  = imgesCount == 1 ? sizeArticleSingleImage : hTemp
                
                
                lblTitle.numberOfLines = 0
                //                lblDes.numberOfLines = 0
                //                txtviewDes.textContainer.maximumNumberOfLines = 0
                
                if let previewUrl = model?.dicPostLink?.link, checkValidString(previewUrl)
                {
                    viewPreviewHeightConst.constant = 300
                    self.showPreview(toString(previewUrl))
//                    imgArticle.heightAnchor.constraint(equalToConstant: 200).isActive = true
                }
                else
                {
                    viewPreviewHeightConst.constant = 0
//                    imgArticle.heightAnchor.constraint(equalToConstant: 0).isActive = true
                    self.detailedView?.isHidden = true
                }
                
                
                lblDes.numberOfLines =  btnViewMore.titleLabel?.text?.lowercased() != "view more" ? 0 : 6
                txtviewDes.textContainer.maximumNumberOfLines = btnViewMore.titleLabel?.text?.lowercased() != "view more" ? 0 : 6
                
                btnViewMore.isHidden = txtviewDes.maxNumberOfLinesTextview > 6 ? false : true

                
            } else
            {
                self.imgArticleWidthConst.constant = wTemp
                self.imgArticleHeightConst.constant = hTemp
                btnViewMore.isHidden = true
                lblTitle.numberOfLines = 2
                lblDes.numberOfLines = 3
                txtviewDes.textContainer.maximumNumberOfLines = 3
                viewPreviewHeightConst.constant = 0
                self.detailedView?.isHidden = true
            }
            self.updateConstraintsIfNeeded()
            self.layoutIfNeeded()
//            viewPreviewHeightConst.constant = 0
//
//            if (self.tableView?.parentViewController as? ArticleDetailVC) != nil
//            {
//                txtviewDes.textContainer.maximumNumberOfLines = 0
//                viewPreviewHeightConst.constant = 100
//            }
////            self.layoutIfNeeded()
//            UIView.animate(withDuration: 0.2) {
//                self.layoutIfNeeded()
//            }

            
        }
    }
    
    @IBAction func viewMore(_ sender: UIButton)
    {
        sender.setTitle(sender.titleLabel?.text?.lowercased() == "view more" ? "View Less" : "View More", for: .normal)
        
        if let ArticleDetailVC = self.tableView?.parentViewController as? ArticleDetailVC {
//            ArticleDetailVC.tblview.reloadData()
//            ArticleDetailVC.tblview.ScrollToBottom(animated: false, ScrollPosition: .top)
            UIView.animate(withDuration: 0.3, animations: {
                ArticleDetailVC.tblview.beginUpdates()
                ArticleDetailVC.tblview.setContentOffset(CGPoint.zero, animated: false)
                ArticleDetailVC.tblview.endUpdates()
                
            }) { (Bool) in
                ArticleDetailVC.tblview.reloadData()
            }
        }
    }
    
    //MARK:- Multi image show

    @IBAction func tapMultiImage(_ sender: UIButton) {
        
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        if (self.tableView?.parentViewController as? ArticleDetailVC) != nil
        {
            if let get = model?.arrImages, get.count > 0
            {
                let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "imageGalleryVC") as! imageGalleryVC
                vcInstace.getarrImages = get
                self.tableView?.parentViewController?.navigationController?.pushViewController(vcInstace, animated: true)
            }
        }
        else
        {
            let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "ArticleDetailVC") as! ArticleDetailVC
            vcInstace.dictPost = model
            self.tableView?.parentViewController?.pushTo(vcInstace)
        }
    }
    @IBAction func clickDiscuss(_ sender: Any) {
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        if let get = model?.post_id
        {
            let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "DiscussionVC") as! DiscussionVC
            vcInstace.strPostID = get
            self.tableView?.parentViewController?.present(vcInstace, animated: true, completion: nil)
        }
    }
    @IBAction func clickDetailPost(_ sender: UIButton) {
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        if (self.tableView?.parentViewController as? ArticleDetailVC) != nil
        {
            return
        }
        let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "ArticleDetailVC") as! ArticleDetailVC
        vcInstace.dictPost = model
        self.tableView?.parentViewController?.pushTo(vcInstace)
    }
    
    @IBAction func clickProfile(_ sender: UIButton)
    {
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        if let getObje = model
        {
            if getObje.is_user, getObje.author_id == globalUserId
            {
                let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                self.tableView?.parentViewController?.pushTo(vcInstace)
            }
            else{
                let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "OtherProfileVC") as! OtherProfileVC
                vcInstace.profileId = getObje.author_id
                self.tableView?.parentViewController?.pushTo(vcInstace)
                
            }
        }
    }
    
}

    //MARK:- Like Unlike
extension ArticleDeclineTableCell {
    
    @IBAction func clickLikeUnlike(_ sender: UIButton) {
        
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        
        if let getObj = model
        {
            callLikeUnlikePost(getObj.post_id, sender.isSelected == true ? "0" : "1")
            
            sender.isSelected = !sender.isSelected
            
            if sender.isSelected
            {
                getObj.post_like_count += 1
            }
            else
            {
                getObj.post_like_count -= 1
            }
            getObj.is_post_liked = !getObj.is_post_liked
            lblLikeCount.text = toString(model?.post_like_count)
        }
    }
    
    func callLikeUnlikePost(_ post_id:String, _ like_unlike:String ) {
        WebService.shared.RequesURL(ServerURL.LikeUnlikePost, Perameters: ["user_id" : globalUserId,"post_id":post_id,"like_unlike":like_unlike],showProgress: false,completion: { (dicRes, success) in
            if success == true{
                debugPrint(dicRes)
            }
        }) { (err) in
        }
    }
}

    //MARK:- Decline Post
extension ArticleDeclineTableCell {
    @IBAction func clickDecline(_ sender: UIButton) {
        
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        
        if let getObj = model
        {
            
            if let indexPath = self.tableView?.indexPathForView(self){
                
                if let HomeVC = self.tableView?.parentViewController as? HomeVC {
                    if self.tableView == HomeVC.tableviewAD {
                        HomeVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeclinePost) { (bool) in
                            if bool
                            {
                                self.callDeclinePost(getObj.post_id)
                                
                                HomeVC.arrArticle.remove(at: indexPath.row)
                                self.tableView?.deleteRows(at: [indexPath], with: .left)
                                
                                if let find_index = (HomeVC.arrALLHomePost).firstIndex(where: {$0.post_id == getObj.post_id}) {
                                    HomeVC.arrALLHomePost.remove(at: find_index)
                                }
                                
                            }
                        }
                    }
                        
                        
                    else if self.tableView == HomeVC.tableHome {
                        HomeVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeclinePost) { (bool) in
                            if bool
                            {
                                self.callDeclinePost(getObj.post_id)
                                
                                HomeVC.arrALLHomePost.remove(at: indexPath.row)
                                self.tableView?.deleteRows(at: [indexPath], with: .left)
                                
                                if let find_index = (HomeVC.arrArticle).firstIndex(where: {$0.post_id == getObj.post_id}) {
                                    HomeVC.arrArticle.remove(at: find_index)
                                }
                            }
                        }
                    }
                    
                }
                    
                else if let ProfileVC = self.tableView?.parentViewController as? ProfileVC {
                    ProfileVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeclinePost) { (bool) in
                        if bool
                        {
                            self.callDeclinePost(getObj.post_id)
                            
                            ProfileVC.arrALLHomePost.remove(at: indexPath.row)
                            self.tableView?.deleteRows(at: [indexPath], with: .left)
                            
                            self.removeFindIndexHomePost(getObj.post_id)
                            
                        }
                        
                    }
                }
                else if let OtherProfileVC = self.tableView?.parentViewController as? OtherProfileVC {
                    OtherProfileVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeclinePost) { (bool) in
                        if bool
                        {
                            self.callDeclinePost(getObj.post_id)
                            
                            OtherProfileVC.arrALLHomePost.remove(at: indexPath.row)
                            self.tableView?.deleteRows(at: [indexPath], with: .left)
                            self.removeFindIndexHomePost(getObj.post_id)
                        }
                    }
                }
                    
                else if let ArticleDetailVC = self.tableView?.parentViewController as? ArticleDetailVC {
                    ArticleDetailVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeclinePost) { (bool) in
                        if bool
                        {
                            self.callDeclinePost(getObj.post_id)
                            ArticleDetailVC.clickBack(self)
                            self.removeFindIndexHomePost(getObj.post_id)

//                            NotificationCenter.default.post(name: .DeclineArticle, object: getObj.post_id)
                        }
                    }
                }
                    
                else if let AnswerUnanswerVC = self.tableView?.parentViewController as? AnswerUnanswerVC {
                    AnswerUnanswerVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeclinePost) { (bool) in
                        if bool
                        {
                            self.callDeclinePost(getObj.post_id)
                            
                            AnswerUnanswerVC.arrPost.remove(at: indexPath.row)
                            self.tableView?.deleteRows(at: [indexPath], with: .left)
                            self.removeFindIndexHomePost(getObj.post_id)
                        }
                    }
                }
            }
            
        }
    }
    
    func removeFindIndexHomePost(_ removePostId:String) {
        
//        NotificationCenter.default.post(name: .DeclineArticle, object: removePostId)

        NotificationCenter.default.post(name: .Delete_Post, object: ["id":removePostId,"type":"1"])

       /* if let HomeVC = self.tableView?.parentViewController as? HomeVC {
            
            if let find_index = (HomeVC.arrALLHomePost).firstIndex(where: {$0.post_id == removePostId}) {
                HomeVC.arrALLHomePost.remove(at: find_index)
            }
            
            if let find_index = (HomeVC.arrArticle).firstIndex(where: {$0.post_id == removePostId}) {
                HomeVC.arrArticle.remove(at: find_index)
            }
        }*/
    }
    
    func callDeclinePost(_ post_id:String) {
        WebService.shared.RequesURL(ServerURL.DeclineQestion, Perameters: ["user_id" : globalUserId,"post_id":post_id,"status":"1"],showProgress: false,completion: { (dicRes, success) in
            if success == true{
                debugPrint(dicRes)
            }
        }) { (err) in
        }
    }
}
    
//MARK:- Three Dot

extension ArticleDeclineTableCell {
    
    @IBAction func clickDot(_ sender: UIButton) {
        
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        
        if let getObj = model
        {
            if getObj.is_user, getObj.author_id == globalUserId{
                self.tapDotMenuMy(postDict: getObj)
            }
            else{
                self.tapDotMenuOtherUser(postDict: getObj)
            }
        }
    }
    func tapDotMenuMy(postDict : allPostModel) {
        
        DispatchQueue.main.async {
            guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
            popupVC.arrTitle = [BottomName.Edit,BottomName.Share,BottomName.Delete,BottomName.RequestExpert]
            //            popupVC.HeightHeader = 50 * screenscale
            popupVC.callback =  { str in
                debugPrint(str)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    if str == BottomName.Edit
                    {
                        switch postDict.postType
                        {
                        case .Article :
                            let vc = StoryBoard.AddPost.instantiateViewController(withIdentifier: "CreateArticleVC") as! CreateArticleVC
                            vc.editDict = postDict
                            self.tableView?.parentViewController?.pushTo(vc)
                            
                        case .Question :
                            let vc = StoryBoard.AddPost.instantiateViewController(withIdentifier: "AskQuestionVC") as! AskQuestionVC
                            vc.editQuesDict = postDict
                            self.tableView?.parentViewController?.pushTo(vc)
                            
                        case .ResearchPapers :
                            let vc = StoryBoard.AddPost.instantiateViewController(withIdentifier: "UploadResearchPapersVC") as! UploadResearchPapersVC
                            vc.editDict = postDict
                            self.tableView?.parentViewController?.pushTo(vc)
                        case .VoiceVideo:
                            return
                            
                        case .Advert:
                            return
                        }
                        
                        
                    }
                    else if str == BottomName.Delete
                    {
                        self.clickDeletePost(postDict.post_id)
                    }
                    else if str == BottomName.RequestExpert
                    {
                        let selectExpert = StoryBoard.AddPost.instantiateViewController(withIdentifier: "SelectExpertVC") as! SelectExpertVC
                        selectExpert.postId = postDict.post_id
                        self.tableView?.parentViewController?.pushTo(selectExpert)
                    }
                    
                    else if str == BottomName.Share {
                        self.tableView?.parentViewController?.shareMaulik(shareText: postDict.post_title, shareImage: nil, shreUrl: postDict.arrImages.first?.image_name)
                    }
                })
            }
            self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
        }
    }
    
    func tapDotMenuOtherUser(postDict : allPostModel) {
        
        DispatchQueue.main.async {
            guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "BottomPopUpVC") as? BottomPopUpVC else { return }
            popupVC.arrTitle = [BottomName.BroadcastAll,postDict.is_Mute ? BottomName.UNMute : BottomName.Mute,BottomName.Share,BottomName.ReportAbuse]//BottomName.CompareAnswer
            popupVC.callback =  { str in
                debugPrint(str)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    if str == BottomName.ReportAbuse {
                        let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "ReportabuseDesVC") as! ReportabuseDesVC
                        vcInstace.post_id = postDict.post_id
                        self.tableView?.parentViewController?.pushTo(vcInstace)
                    }
                    else if str == BottomName.RequestExpert
                    {
                        let selectExpert = StoryBoard.AddPost.instantiateViewController(withIdentifier: "SelectExpertVC") as! SelectExpertVC
                        selectExpert.postId = postDict.post_id
                        self.tableView?.parentViewController?.pushTo(selectExpert)
                    }
                    
                    else if str == BottomName.Share {
                        self.tableView?.parentViewController?.shareMaulik(shareText: postDict.post_title, shareImage: nil, shreUrl: postDict.arrImages.first?.image_name)
                    }
                    else if str == BottomName.BroadcastAll {
                        self.callBroadcastAllPost(postDict.post_id)
                    }
                    else if str == BottomName.Mute
                                       {
                                           self.webCall_Mute(postDict.post_id, status: "1") {
                                               postDict.is_Mute = true
                                           }
                                       }
                                       else if str == BottomName.UNMute
                                       {
                                           self.webCall_Mute(postDict.post_id, status: "0") {
                                               postDict.is_Mute = false
                                           }
                                       }
                })
            }
            self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
        }
        
    }
    
    func webCall_Mute(_ post_id:String ,status:String,callback: (()->())?){
        WebService.shared.RequesURL(ServerURL.MuteUnmuteConversation, Perameters: ["user_id":globalUserId,"post_id":post_id,"status":status],showProgress: true, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true
            {
                callback?()
                self.tableView?.parentViewController?.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
            else{
                self.tableView?.parentViewController?.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
}



//MARK:- Delete Post
extension ArticleDeclineTableCell {
    
    func clickDeletePost(_ post_id:String)
    {
        if let indexPath = self.tableView?.indexPathForView(self){
            
            if let HomeVC = self.tableView?.parentViewController as? HomeVC {
                
                HomeVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeletePost) { (bool) in
                    if bool
                    {
                        self.callDeletePost(post_id)
                        
                        if let find_index = (HomeVC.arrArticle).firstIndex(where: {$0.post_id == post_id}) {
                            HomeVC.arrArticle.remove(at: find_index)
                            
                        }
                        
                        if let find_index = (HomeVC.arrALLHomePost).firstIndex(where: {$0.post_id == post_id}) {
                            HomeVC.arrALLHomePost.remove(at: find_index)
                        }
                        
                        self.tableView?.deleteRows(at: [indexPath], with: .left)
                        
                    }
                }
            }
            else if let ProfileVC = self.tableView?.parentViewController as? ProfileVC {
                
                ProfileVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeletePost) { (bool) in
                    if bool
                    {
                        self.callDeletePost(post_id)
                        
                        ProfileVC.arrALLHomePost.remove(at: indexPath.row)
                        self.tableView?.deleteRows(at: [indexPath], with: .left)
                    }
                }
            }
            else if let ArticleDetailVC = self.tableView?.parentViewController as? ArticleDetailVC {
                
                ArticleDetailVC.showOkCancelAlertWithAction(msg: Constant.kAlertDeletePost) { (bool) in
                    if bool
                    {
                        self.callDeletePost(post_id)
                        ArticleDetailVC.clickBack(self)
                        NotificationCenter.default.post(name: .Delete_Post, object: ["id":post_id,"type":"1"])
                    }
                }
            }
        }
    }
    func callDeletePost(_ post_id:String) {
        WebService.shared.RequesURL(ServerURL.DeletePost, Perameters: ["user_id" : globalUserId,"post_id":post_id],showProgress: false,completion: { (dicRes, success) in
            if success == true{
                debugPrint(dicRes)
            }
        }) { (err) in
        }
    }
}


//MARK:- Flash Post
extension ArticleDeclineTableCell
{
    @IBAction func clickFlash(_ sender: UIButton) {
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        if let getObj = model
        {
            let vcInstace = StoryBoard.Flash.instantiateViewController(withIdentifier: "FlashPostVC") as! FlashPostVC
            vcInstace.getPostID = getObj.post_id
            self.tableView?.parentViewController?.pushTo(vcInstace)
        }
    }
}
//MARK:- Preview Show

extension ArticleDeclineTableCell
{
    func showPreview(_ linkURL:String) {
        
        self.detailedView?.isHidden = true
        indiCator?.startAnimating()
        
        func setDataPreview(_ result: Response) {
            print("url: ", result.url ?? "no url")
            print("finalUrl: ", result.finalUrl ?? "no finalUrl")
            print("canonicalUrl: ", result.canonicalUrl ?? "no canonicalUrl")
            print("title: ", result.title ?? "no title")
            print("images: ", result.images ?? "no images")
            print("image: ", result.image ?? "no image")
            print("video: ", result.video ?? "no video")
            print("icon: ", result.icon ?? "no icon")
            print("description: ", result.description ?? "no description")
            
            self.previewTitle?.text = toString(result.title)
            self.previewCanonicalUrl?.text = toString(result.finalUrl)
            self.previewDescription?.text = toString(result.description)
            
            if let icon = result.image
            {
                self.favicon?.setImageWithURL(toString(icon), "logo_small")
            }
            else
            {
                self.favicon?.setImageWithURL(toString(result.icon), "logo_small")
            }
            self.detailedView?.isHidden = false
            self.indiCator?.stopAnimating()
        }
        

        self.slp.preview(
            linkURL,
            onSuccess: { result in
                setDataPreview(result)
                //                self.result = result
        },
            onError: { error in
                print(error)
//                self.showOkAlert(msg: "Invalid URL!")
                self.indiCator?.stopAnimating()
        }
        )
    }
    @IBAction func openWithAction(_ sender: UIButton) {
        if let previewUrl = model?.dicPostLink?.link, checkValidString(previewUrl)
        {
            openURL(URL.init(string: previewUrl)!)
        }
    }
    func openURL(_ url: URL) {
        if ["http", "https"].contains(url.scheme?.lowercased() ?? "") {
            self.tableView?.parentViewController?.present(SFSafariViewController(url: url), animated: true, completion: nil)
        } else {
            linkurlOpen_MS(linkurl: url.absoluteString)
        }
    }
    
}
//MARK:- BroadcastAll POST
extension ArticleDeclineTableCell
{
    func callBroadcastAllPost(_ post_id:String) {
        WebService.shared.RequesURL(ServerURL.BroadCastPost, Perameters: ["user_id" : globalUserId,"post_id":post_id],showProgress: true,completion: { (dicRes, success) in
            self.tableView?.parentViewController?.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
        }) { (err) in
        }
    }
}
//MARK:- Carryover POST
extension ArticleDeclineTableCell
{
    @IBAction func clickCarryover(_ sender: UIButton) {
        
        guard globalUserId != "" else {
                        appDelegate.showAlertGuest()
                        return
                    }
        
        if let getObj = model
        {
            if sender.isSelected
            {
                self.tableView?.parentViewController?.showOkAlert(msg: Constant.kAlertCaryover)
            }
            else
            {
                guard let popupVC = StoryBoard.Home.instantiateViewController(withIdentifier: "CalandarVC") as? CalandarVC else { return }
                popupVC.callback =  { date in
                    
                    sender.isSelected = true
                    
                    WebService.shared.RequesURL(ServerURL.CaryOverPost, Perameters: ["user_id" : globalUserId,"post_id":getObj.post_id,"carryover_date":date],showProgress: true,completion: { (dicRes, success) in
                        
                        //                    self.btnCaryover.isUserInteractionEnabled = false
                        self.tableView?.parentViewController?.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
                    }) { (err) in
                    }
                    
                    
                };            self.tableView?.parentViewController?.present(popupVC, animated: true, completion: nil)
            }
        }
    }
}
