//
//  TrendingVC.swift
//  Proclapp
//
//  Created by Mac-4 on 04/06/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class TrendingVC: UIViewController {

    @IBOutlet weak var clvTrending: UICollectionView!
    
    
    
    var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.size.width - 21
        layout.estimatedItemSize = CGSize(width: width, height: 10)
        return layout
    }()
    
//    var arrRowdata = [String]()
    
    var arrTrendingList = [TrendingListModel]()
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var isAvailavbleMoreDat = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    DispatchQueue.main.async {
               self.indicator.frame = CGRect(x: CGFloat(0), y: CGFloat(screenheight - 30 - BottomPadding), width: screenwidth, height: CGFloat(30))
               }
               

        
//
//        arrRowdata.append("Science & Teachnology")
//        arrRowdata.append("Low & Order")
//        arrRowdata.append("Sport & Athlets")
//        arrRowdata.append("Infomatiom Communication Technology ")
//        arrRowdata.append("Data organizaiton Infomatiom Communication Technology  Data organizaiton Infomatiom ")
//        arrRowdata.append("Science & Teachnology")
//        arrRowdata.append("Science & Teachnology")
//        arrRowdata.append("Low & Order")
//        arrRowdata.append("Sport & Athlets")
//        arrRowdata.append("Infomatiom Communication Technology ")
//        arrRowdata.append("Data organizaiton Infomatiom Communication Technology  Data organizaiton Infomatiom ")
//        arrRowdata.append("Science & Teachnology")
//        arrRowdata.append("Science & Teachnology")
//        arrRowdata.append("Low & Order")
//        arrRowdata.append("Sport & Athlets")
//        arrRowdata.append("Infomatiom Communication Technology ")
//        arrRowdata.append("Data organizaiton Infomatiom Communication Technology  Data organizaiton Infomatiom ")
//        arrRowdata.append("Science & Teachnology")
        
        // Do any additional setup after loading the view.
        
        clvTrending.register(UINib.init(nibName: "TrendingCLVCell", bundle: nil), forCellWithReuseIdentifier: "TrendingCLVCell")
//        clvTrending?.collectionViewLayout = layout
//        clvTrending.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        clvTrending?.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        
        if let layout = clvTrending?.collectionViewLayout as? PinterestLayout {
            layout.delegate = self
        }
        
//        if let flowLayout = clvTrending.collectionViewLayout as? UICollectionViewFlowLayout
//        {
//            flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
//        }

       /* DispatchQueue.main.async {
            self.clvTrending.reloadData()
        }*/
        
         self.clvTrending.refreshControl = UIRefreshControl()
         self.clvTrending.refreshControl?.tintColor = .AppSkyBlue
         self.clvTrending.refreshControl?.addTarget(self, action: #selector(refreshCalled), for: UIControl.Event.valueChanged)
         self.getTrendingList()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.isNavigationBarHidden = false
    }
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func refreshCalled() {
        self.getTrendingList()
    }
    
  /*  @objc func getTrendingList(_ serviceCount : Int = 0) {
        
        if self.clvTrending.accessibilityHint == "service_calling" { return }
        
        self.clvTrending.accessibilityHint = "service_calling"
        var delayTime = DispatchTime.now()
        
        if self.clvTrending.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.clvTrending.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.Trending, Perameters: ["user_id" : globalUserId],showProgress: false,completion: { (dicRes, success) in
                debugPrint(dicRes)
                if success == true{
                    if let arrPrice = dicRes["topics"] as? [[String:Any]] {
                        
                        self.arrTrendingList = arrPrice.map({TrendingListModel.init($0)})
                    }
                }
                DispatchQueue.main.async {
                    UIView.performWithoutAnimation {
                        self.clvTrending.reloadData()
                    }
                    self.clvTrending.accessibilityHint = nil
                    self.clvTrending.refreshControl?.endRefreshing()
                }
                
                
            }) { (err) in
                if serviceCount < 1 {
                    self.clvTrending.accessibilityHint = nil
                    self.getTrendingList(serviceCount + 1)
                } else {
                    debugPrint(err)
                    self.clvTrending.accessibilityHint = nil
                    self.clvTrending.refreshControl?.endRefreshing()
                }
            }
        }
        
    }*/
    
    @objc func getTrendingList(_ offset : Int = 0) {
        
        var delayTime = DispatchTime.now()
        
        if self.clvTrending.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.clvTrending.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.Trending, Perameters: ["user_id" : globalUserId,"offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                    debugPrint(dicRes)
                    debugPrint("Offset= \(offset)")
                    if let arrPrice = dicRes["topics"] as? [[String:Any]] {
                        if offset == 0{
                            self.arrTrendingList = arrPrice.map({TrendingListModel.init($0)})
                        }
                        else
                        {
                            self.arrTrendingList.append(contentsOf: arrPrice.map({TrendingListModel.init($0)}))
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.isAvailavbleMoreDat = self.arrTrendingList.count >= ToInt(dicRes["offset"])
                    UIView.performWithoutAnimation {
                        self.clvTrending.reloadData()
                    }
                    self.clvTrending.accessibilityHint = nil
                    self.clvTrending.refreshControl?.endRefreshing()
                    self.indicator.stopAnimating()
                }
            }) { (err) in
                self.clvTrending.accessibilityHint = nil
                self.clvTrending.refreshControl?.endRefreshing()
                self.indicator.stopAnimating()
            }
        }
    }
    


}

extension TrendingVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrTrendingList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrendingCLVCell", for: indexPath) as! TrendingCLVCell
        
        let obj = arrTrendingList[indexPath.row]
        
        cell.lblTitle.text = obj.category_name
        cell.lblFollower.text = obj.total_followers + " Followers"
        cell.btnFollowUnFollow.addTarget(self, action: #selector(clickFollow), for: .touchUpInside)

        cell.imgview.setImageWithURL(obj.category_img, "")
        cell.btnFollowUnFollow.isSelected = obj.is_selected
        return cell
    }
    func scrollViewDidEndDragging(_ aScrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if self.isAvailavbleMoreDat == true , self.clvTrending.accessibilityHint == nil{
            self.indicator.startAnimating()
            self.clvTrending.accessibilityHint = "service_calling"
            self.getTrendingList(arrTrendingList.count)
        }
    }
    
    @IBAction func clickFollow(_ sender: UIButton) {
        
        if let getIndexPath = self.clvTrending.indexPathForView_Collection(sender)
        {
            arrTrendingList[getIndexPath.row].is_selected = !arrTrendingList[getIndexPath.row].is_selected
            webCall_InterestedCategory_Save()
            self.clvTrending.reloadItems(at: [getIndexPath])
            
        }
        
    }
    func webCall_InterestedCategory_Save()
    {
        WebService.shared.RequesURL(ServerURL.Interested_Category_Save, Perameters: ["user_id" : globalUserId,"category":self.arrTrendingList.filter({$0.is_selected}).map({$0.category_id}).joined(separator: ",")],showProgress: true,completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            if success == true
            {
              
            }
            
        }) { (err) in
            debugPrint("\(ServerURL.Interested_Category):-->",err)
        }
    }
 
}

//MARK: - PINTEREST LAYOUT DELEGATE
extension TrendingVC : PinterestLayoutDelegate {
    
    // 1. Returns the photo height
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
 
        let obj = arrTrendingList[indexPath.row]

        return 190 + obj.category_name.height(withConstrainedWidth: 142 * screenscale, font: UIFont.FontLatoHeavy(size: 12 * screenscale))
    }
    
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}
class TrendingListModel {
    
    var category_id =  ""
    var category_img =  ""
    var category_img_thumb =  ""
    var category_name =  ""
    var category_selected_img =  ""
    var category_selected_img_thumb =  ""
    var date =  ""
    var total_followers = ""
    var is_selected =  false
    
    init(_ dict : [String:Any]) {
        self.category_id = toString(dict["category_id"])
        self.category_img = toString(dict["category_img"])
        self.category_img_thumb = toString(dict["category_img_thumb"])
        self.category_name = toString(dict["category_name"])
        self.category_selected_img = toString(dict["category_selected_img"])
        self.category_selected_img_thumb = toString(dict["category_selected_img_thumb"])
        self.date = toString(dict["date"])
        self.total_followers = toString(dict["total_followers"])

        self.is_selected = toString(dict["is_selected"]) == "1"
    }
}
