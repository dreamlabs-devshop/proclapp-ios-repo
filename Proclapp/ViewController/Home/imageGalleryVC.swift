//
//  imageGalleryVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 09/08/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class imageGalleryVC: UIViewController {

    var getarrImages = [imagePostModel]()
    
    var im = UIImageView()
    @IBOutlet weak var viewImage: UIView!

    @IBOutlet weak var collctionView: UICollectionView!

    @IBOutlet weak var imageScrollView: ImageScrollView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewImage.isHidden = true
        
        
        
//        addSwipe()
        // Do any additional setup after loading the view.
    }
    
    func addSwipe() {
        let directions: [UISwipeGestureRecognizer.Direction] = [.right, .left, .up, .down]
        for direction in directions {
            let gesture = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe))
            gesture.direction = direction
            self.viewImage.addGestureRecognizer(gesture)
        }
    }
    

    
    @objc func handleSwipe(sender: UISwipeGestureRecognizer) {
        print(sender.direction)
        viewImage.isHidden = true
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func clickClose(_ sender: Any) {
        
            self.viewImage.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        self.imageScrollView.superview?.layoutIfNeeded()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.isNavigationBarHidden = false
    }

    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
        if let gesture = navigationController?.interactivePopGestureRecognizer
        {
            view.addGestureRecognizer(gesture)
        }
    }
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
    }
}


extension imageGalleryVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return getarrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "imgGallry", for: indexPath) as! imgGallry
       cell.img.setImageWithURL(getarrImages[indexPath.row].image_name, "place_logo")

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.width/3, height: collectionView.frame.width/3)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let preview = StoryBoard.Other.instantiateViewController(withIdentifier: "ImagePreviewVC") as! ImagePreviewVC
        preview.arrImages = getarrImages.map({$0.image_name})
        preview.indexPath = indexPath
        self.present(preview, animated: true, completion: nil)
        
        /*    self.viewImage.isHidden = false
        
        if let cell = collectionView.cellForItem(at: indexPath) as? imgGallry
        {
            imageScrollView.display(image: cell.img.image ?? UIImage())
        }*/
    }
    
}

class imgGallry : UICollectionViewCell
{
    @IBOutlet var img: UIImageView!
}
