import UIKit

class AnswerUnanswerVC: UIViewController {
    
    
    var arrPost = [allPostModel]()
    
    var isPost = false
    var isFromDiscuss = false
    
    
    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var isMore = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navTitle.text = isFromDiscuss == false ? "Question & Answer" : "Articles & Discussions"
        
        
        tblView.register(UINib.init(nibName: "QuestionAnswerTableViewCell", bundle: nil), forCellReuseIdentifier: "QuestionAnswerTableViewCell")
        
        tblView.register(UINib.init(nibName: "ArticleDeclineTableCell", bundle: nil), forCellReuseIdentifier: "ArticleDeclineTableCell")

        
        
        DispatchQueue.main.async {
        
            self.indicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tblView.bounds.width, height: CGFloat(30))
            self.tblView.tableFooterView = self.indicator
        }
        
        self.tblView.refreshControl = UIRefreshControl()
        self.tblView.refreshControl?.tintColor = .AppSkyBlue
        self.tblView.refreshControl?.addTarget(self, action: #selector(refreshCalled), for: UIControl.Event.valueChanged)
        self.getPost()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func btnBackClicked(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func refreshCalled() {
        
        self.getPost()
    }
    
    @objc func getPost(_ offset : Int = 0) {
        
        let url =  isFromDiscuss == true ?  self.isPost ? ServerURL.Getdiscussed_articles : ServerURL.Getnondiscussed_articles : self.isPost ? ServerURL.GetAnsweredQaPosts : ServerURL.GetUnansweredQaPosts
        
        var delayTime = DispatchTime.now()
        if self.tblView.refreshControl?.isRefreshing == false{
            delayTime = DispatchTime.now() + 0.5
            self.tblView.refreshControl?.beginRefreshingManually()
        }
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(url, Perameters: ["user_id" : globalUserId,"offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                    debugPrint(dicRes)
                    let key = self.isFromDiscuss == false ? "post_list" : "posts"
                    if let arrData = dicRes[key] as? [[String:Any]] {
                        if offset == 0{
                            self.arrPost = arrData.map({allPostModel.init($0)})
                        }
                        else{
                            self.arrPost.append(contentsOf: arrData.map({allPostModel.init($0)}))
                        }
                    }
                    self.isMore = self.arrPost.count >= ToInt(dicRes["offset"])
                }
                else{
                    self.isMore = false
                    self.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
                }
                
                DispatchQueue.main.async {
                    debugPrint("isAvailableMoreData= \(self.isMore)")
//                    debugPrint("arr= \(self.arrPost.count)")
//                    debugPrint("toint= \(ToInt(dicRes["offset"]))")
                    UIView.performWithoutAnimation {
                        self.tblView.reloadData()
                    }
                    self.tblView.accessibilityHint = nil
                    self.tblView.refreshControl?.endRefreshing()
                    self.indicator.stopAnimating()
                }
            }) { (err) in
                self.tblView.accessibilityHint = nil
                self.tblView.refreshControl?.endRefreshing()
                self.indicator.stopAnimating()
            }
        }
    }
  
}


extension AnswerUnanswerVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPost.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isFromDiscuss
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleDeclineTableCell") as! ArticleDeclineTableCell
            cell.model = arrPost[indexPath.row]
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionAnswerTableViewCell") as! QuestionAnswerTableViewCell
            cell.model = arrPost[indexPath.row]
            return cell
        }
    }
    func scrollViewDidEndDragging(_ aScrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        let offset: CGPoint = aScrollView.contentOffset
        let bounds: CGRect = aScrollView.bounds
        let size: CGSize = aScrollView.contentSize
        let inset: UIEdgeInsets = aScrollView.contentInset
        let y = Float(offset.y + bounds.size.height - inset.bottom)
        let h = Float(size.height)
        let reload_distance: Float = 0
        //            print("load more data!!!!!")
        if y > h + reload_distance{
            
            if self.isMore == true , self.tblView.accessibilityHint == nil{
                self.indicator.startAnimating()
                self.tblView.accessibilityHint = "service_calling"
                self.getPost(self.arrPost.count)
            }
        }
    }
}
