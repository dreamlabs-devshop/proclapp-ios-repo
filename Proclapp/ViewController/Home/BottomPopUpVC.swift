//
//  BottomPopUpVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/8/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import BottomPopup

class BottomPopUpVC: BottomPopupViewController {
    
    var callback : ((String) -> Void)?
    
    var height: CGFloat?
    
    var topCornerRadius: CGFloat?
    var presentDuration: Double?
    var dismissDuration: Double?
    var shouldDismissInteractivelty: Bool?
    
    var arrTitle = [String]()

    var HeightHeader = 0 * screenscale
    
    @IBAction func dismissButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    // Bottom popup attribute methods
    // You can override the desired method to change appearance
    
    override func getPopupHeight() -> CGFloat {
        return height ?? CGFloat(arrTitle.count) * (44 * screenscale) + BottomPadding + HeightHeader
    }
    
    override func getPopupTopCornerRadius() -> CGFloat {
        return topCornerRadius ?? CGFloat(10*screenscale)
    }
    
    override func getPopupPresentDuration() -> Double {
        return presentDuration ?? 0.5
    }
    
    override func getPopupDismissDuration() -> Double {
        return dismissDuration ?? 0.5
    }
    
    override func shouldPopupDismissInteractivelty() -> Bool {
        return shouldDismissInteractivelty ?? true
    }
}
//MARK: - Table view data source
extension BottomPopUpVC: UITableViewDataSource,UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "titleTableCell") as! titleTableCell
        cell.title.text = arrTitle[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 44 * screenscale
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            let title = self.arrTitle[indexPath.row]
            self.callback?(title)
            self.dismiss(animated: true) {
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let cell: UITableViewCell
        cell = tableView.dequeueReusableCell(withIdentifier: "tableviewHeader")!
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.white
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return HeightHeader
    }
    
}
class titleTableCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
}

var BottomPadding : CGFloat
{
    var bottomPadding : CGFloat = 0
    if #available(iOS 11.0, *) {
        if let wind = UIApplication.shared.keyWindow
        {
            bottomPadding = wind.safeAreaInsets.bottom
        }
    }
    return bottomPadding
}
