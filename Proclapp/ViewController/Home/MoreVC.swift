//
//  MoreVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 3/27/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class MoreVC: UIViewController {

    @IBOutlet weak var badgeMsg: UIButton!
    @IBOutlet weak var badgeNoti: UIButton!
    @IBOutlet weak var badgeFriend: UIButton!

    
    @IBOutlet weak var tblMore: UITableView!
    
    var arrRow = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        badgeMsg.layer.cornerRadius = badgeMsg.frame.height/2
        badgeMsg.layer.masksToBounds = true
        
        badgeNoti.layer.cornerRadius = badgeNoti.frame.height/2
        badgeNoti.layer.masksToBounds = true
        
        badgeFriend.layer.cornerRadius = badgeFriend.frame.height/2
        badgeFriend.layer.masksToBounds = true
        
        self.arrRow.append("Decline Questions/Article")
        self.arrRow.append("Videos")
        self.arrRow.append("Bookmark")
        self.arrRow.append("Categories/Topics")
        self.arrRow.append("Help & Support")
        self.arrRow.append("Wallet")
        self.arrRow.append("Settings")
        
        
        
        // Do any additional setup after loading the view.
        tblMore.register(UINib.init(nibName: "MoreTblCell", bundle: nil), forCellReuseIdentifier: "MoreTblCell")
        tblMore.estimatedRowHeight = 100
        tblMore.rowHeight = UITableView.automaticDimension
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = UIColor.AppSkyBlue
        self.navigationController?.isNavigationBarHidden = false
     
        self.setBages()
        appDelegate.funGetnoticount{
            self.setBages()
        }
        tblMore.reloadData()
        
    }
    
    func setBages(){
        self.badgeMsg.setTitle(global_count_Msg, for: .normal)
        self.badgeNoti.setTitle(global_count_Noti, for: .normal)
        self.badgeFriend.setTitle(global_count_Friend, for: .normal)
        
        self.badgeMsg.isHidden = global_count_Msg == "0"
        self.badgeNoti.isHidden = global_count_Noti == "0"
        self.badgeFriend.isHidden = global_count_Friend == "0"
    }
    
    @IBAction func btnSearch(_ sender: UIButton) {
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        self.pushTo(vcInstace)
    }
    @IBAction func btnProfile(_ sender: UIButton) {
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigationController?.pushViewController(vcInstace, animated: true)
    }
    @IBAction func btnNotification(_ sender: UIButton) {
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        let vcInstace = StoryBoard.Notification.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        self.pushTo(vcInstace)
        
    }
    @IBAction func btnMessage(_ sender: UIButton) {
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        let vcInstace = StoryBoard.Message.instantiateViewController(withIdentifier: "MessageVC") as! MessageVC
        self.pushTo(vcInstace)
    }
    @IBAction func btnFriends(_ sender: UIButton) {
        guard globalUserId != "" else {
                  appDelegate.showAlertGuest()
                  return
              }
        let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "FriendsVC") as! FriendsVC
        self.pushTo(vcInstace)
    }

}

extension MoreVC:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrRow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoreTblCell") as! MoreTblCell
        cell.lblTitle.text = self.arrRow[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let select  = self.arrRow[indexPath.row]
        
        guard globalUserId != "" || select == "Help & Support"  else {
            appDelegate.showAlertGuest()
            return
        }
        
        
        if select == "Bookmark"{
            
            let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "BookmarkedPostVC") as! BookmarkedPostVC
            self.pushTo(vcInstace)
            
        }
        else if select == "Videos"{
            let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "VideosVC") as! VideosVC
            self.pushTo(vcInstace)
        }
        else if select == "Decline Questions/Article"{
            let vcInstace = StoryBoard.More.instantiateViewController(withIdentifier: "DeclinedQueAndArticleVC") as! DeclinedQueAndArticleVC
            self.navigationController?.pushViewController(vcInstace, animated: true)
        }
        else if select == "Help & Support"{
            self.pushTo(HelpSupportVC.vcInstace)
        }
        else if select == "Settings"{
            self.pushTo(SettingsVC.vcInstace)
        }
        else if select == "Categories/Topics"{
            let vcInstace = StoryBoard.More.instantiateViewController(withIdentifier: "CategoryVC") as! CategoryVC
            self.navigationController?.pushViewController(vcInstace, animated: true)
        }
        else if select == "Wallet"
        {
            let vcInstace = StoryBoard.Wallet.instantiateViewController(withIdentifier: "ExpertWalletVC") as! ExpertWalletVC
            self.navigationController?.pushViewController(vcInstace, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if loginData?.is_expert == false,self.arrRow[indexPath.row].lowercased() == "Wallet".lowercased()
        {
                return 0
        }
        return UITableView.automaticDimension
    }
    
}
