//
//  VoiceVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 4/4/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import AVKit
//import SwiftAudio
class VoiceVC: UIViewController {

    @IBOutlet weak var tableviewVV: UITableView!

    @IBOutlet weak var badgeMsg: UIButton!
    @IBOutlet weak var badgeNoti: UIButton!
    @IBOutlet weak var badgeFriend: UIButton!

    
    var arrVoiceVideo = [allPostModel]()
//    var arrayAudioList = [AudioItem]()
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var isMore = false
    var AudioCell: AudioTableViewCell?
    var indexrow = -1
    var arrayIndex = [IndexPath]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
               badgeMsg.layer.cornerRadius = badgeMsg.frame.height/2
               badgeMsg.layer.masksToBounds = true

               badgeNoti.layer.cornerRadius = badgeNoti.frame.height/2
               badgeNoti.layer.masksToBounds = true
               
               badgeFriend.layer.cornerRadius = badgeFriend.frame.height/2
               badgeFriend.layer.masksToBounds = true
        
        DispatchQueue.main.async {
            self.indicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tableviewVV.bounds.width, height: CGFloat(30))
            self.tableviewVV.tableFooterView = self.indicator
        }
        
        self.tableviewVV.refreshControl = UIRefreshControl()
        self.tableviewVV.refreshControl?.tintColor = .AppSkyBlue
        self.tableviewVV.refreshControl?.addTarget(self, action: #selector(refreshCalled_VoiceVideo), for: UIControl.Event.valueChanged)
        self.getVoiceVideoList()

        tableviewVV.register(UINib.init(nibName: "VideoTableViewCell", bundle: nil), forCellReuseIdentifier: "VideoTableViewCell")
        tableviewVV.register(UINib.init(nibName: "AudioTableViewCell", bundle: nil), forCellReuseIdentifier: "AudioTableViewCell")
//        player.event.stateChange.addListener(self, self.handleAudioPlayerStateChange)
        AudioCell = tableviewVV.dequeueReusableCell(withIdentifier: "AudioTableViewCell") as? AudioTableViewCell
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = UIColor.AppSkyBlue
        self.navigationController?.isNavigationBarHidden = false
        
       self.setBages()
        appDelegate.funGetnoticount{
            self.setBages()
        }

    }
    func setBages(){
        self.badgeMsg.setTitle(global_count_Msg, for: .normal)
        self.badgeNoti.setTitle(global_count_Noti, for: .normal)
        self.badgeFriend.setTitle(global_count_Friend, for: .normal)
        
        self.badgeMsg.isHidden = global_count_Msg == "0"
        self.badgeNoti.isHidden = global_count_Noti == "0"
        self.badgeFriend.isHidden = global_count_Friend == "0"
    }
//     func handleAudioPlayerStateChange(state: AudioPlayerState) {
//           // Handle the event
//
//           DispatchQueue.main.async {
//
//                self.tableviewVV.reloadData()
//            }
//       }
    @IBAction func btnSearch(_ sender: UIButton) {
        let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        self.pushTo(vcInstace)
    }
    @IBAction func btnProfile(_ sender: UIButton) {
        let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigationController?.pushViewController(vcInstace, animated: true)
    }
    @IBAction func btnNotification(_ sender: UIButton) {
        let vcInstace = StoryBoard.Notification.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        self.pushTo(vcInstace)
        
    }
    @IBAction func btnMessage(_ sender: UIButton) {
        let vcInstace = StoryBoard.Message.instantiateViewController(withIdentifier: "MessageVC") as! MessageVC
        self.pushTo(vcInstace)
    }
    @IBAction func btnFriends(_ sender: UIButton) {
        let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "FriendsVC") as! FriendsVC
        self.pushTo(vcInstace)
    }
}
//MARK: - Table view data source
extension VoiceVC: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrVoiceVideo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        switch self.arrVoiceVideo[indexPath.row].audioVideoType{
        case .audio:
             AudioCell = tableviewVV.dequeueReusableCell(withIdentifier: "AudioTableViewCell") as? AudioTableViewCell
//             AudioCell?.audiitem = arrayAudioList[indexPath.row]
             AudioCell?.model = arrVoiceVideo[indexPath.row]
//             AudioCell?.btnPLay.tag = indexPath.row
//             AudioCell?.btnPLay.addTarget(self, action: #selector(tapPlayAudio(sender:)), for: .touchUpInside)
           
             
             /*if indexrow == indexPath.row
             {
//                let state = player.playerState
                if state == .buffering || state == .loading || state == .idle
                {
                    AudioCell?.lblTime.text = "0b"
                    AudioCell?.btnPLay.isHidden = true
                    AudioCell?.activity.startAnimating()
                }
                else if state == .playing
                {
                    AudioCell?.lblTime.text = "\(ToInt(player.duration))"

                    AudioCell?.activity.stopAnimating()
                    AudioCell?.btnPLay.isHidden = false
                    AudioCell?.btnPLay.isSelected = true
                }
                else if state == .paused
                {
                    AudioCell?.lblTime.text = "\(ToInt(player.bufferDuration))"

                    AudioCell?.activity.stopAnimating()
                    AudioCell?.btnPLay.isHidden = false
                    AudioCell?.btnPLay.isSelected = false
                }
                
                
             }
             else{
                AudioCell?.btnPLay.isSelected = false
                AudioCell?.btnPLay.isHidden = false

             }*/
//            cell.handleAudioPlayerStateChange(state: player.playerState)
             return AudioCell!
            
        case .video:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell") as! VideoTableViewCell
            cell.model = arrVoiceVideo[indexPath.row]
 
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableView.automaticDimension
    }
    func scrollViewDidEndDragging(_ aScrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        let offset: CGPoint = aScrollView.contentOffset
        let bounds: CGRect = aScrollView.bounds
        let size: CGSize = aScrollView.contentSize
        let inset: UIEdgeInsets = aScrollView.contentInset
        let y = Float(offset.y + bounds.size.height - inset.bottom)
        let h = Float(size.height)
        let reload_distance: Float = 0
        //            print("load more data!!!!!")
        if y > h + reload_distance{
            
            if self.isMore == true , self.tableviewVV.accessibilityHint == nil{
                self.indicator.startAnimating()
                self.tableviewVV.accessibilityHint = "service_calling"
                self.getVoiceVideoList(self.arrVoiceVideo.count)
            }
        }
    }
    
    @objc func tapPlayAudio(sender: UIButton)
    {
      /*  if let audiitem = arrayAudioList[sender.tag] as? AudioItem {
            
            if player.playerState == .playing && indexrow == sender.tag
            {
                player.pause()
            }
            else if player.playerState == .paused && indexrow == sender.tag
            {
                 player.play()
            }
            else
            {
                indexrow = sender.tag

                try? player.load(item: audiitem, playWhenReady: true) //(item: , playWhenReady: true)
            }
            
        }*/
    }
    
}

//MARK: - Button Event IBAction

extension VoiceVC
{
    @objc func refreshCalled_VoiceVideo()
    {
        self.getVoiceVideoList()
    }
    @objc func getVoiceVideoList(_ offset : Int = 0) {
        var delayTime = DispatchTime.now()
        if self.tableviewVV.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tableviewVV.refreshControl?.beginRefreshingManually()
        }
        //        debugPrint("Offset= \(offset)")
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.vv_general_list, Perameters: ["user_id" : globalUserId,"vv_type":"1","offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                    //                                        debugPrint(dicRes)
                    
                    if let arrData = dicRes["vv_list"] as? [[String:Any]] {
                        if offset == 0
                        {
                            //                            player.stop()
                            self.indexrow = -1
                            self.arrVoiceVideo = arrData.map({allPostModel.init($0)})
                        }
                        else{
                            self.arrVoiceVideo.append(contentsOf: arrData.map({allPostModel.init($0)}))
                        }
                    }
                    
                    self.isMore = self.arrVoiceVideo.count >= ToInt(dicRes["offset"])
                    
                    /* for audio in self.arrVoiceVideo
                     {
                     if let url = audio.dicAudio?.audio_url
                     {
                     let audioItem = DefaultAudioItem(audioUrl: url, sourceType: .stream)
                     self.arrayAudioList.append(audioItem)
                     }
                     }*/
                }
                else
                {
                    self.isMore = false
                    if self.arrVoiceVideo.count == 0
                    {
                        self.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
                    }
                }
                
                DispatchQueue.main.async {
                    
                    //                    debugPrint("isAvailableMoreData= \(self.isMore)")
                    //
                    //                    debugPrint("arr= \(self.arrVoiceVideo.count)")
                    //
                    //                    debugPrint("toint= \(ToInt(dicRes["offset"]))")
                    
                    
                    UIView.performWithoutAnimation {
                        self.tableviewVV.reloadData()
                    }
                    self.tableviewVV.accessibilityHint = nil
                    self.tableviewVV.refreshControl?.endRefreshing()
                    self.indicator.stopAnimating()
                }
            }) { (err) in
                self.tableviewVV.accessibilityHint = nil
                self.tableviewVV.refreshControl?.endRefreshing()
                self.indicator.stopAnimating()
            }
        }
    }
}
