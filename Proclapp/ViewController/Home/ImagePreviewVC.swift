//
//  ImagePreviewVC.swift
//  YeahYag
//
//  Created by Pris Mac on 6/10/19.
//  Copyright © 2019 Ajay. All rights reserved.
//

import UIKit

class ImagePreviewVC: UIViewController {

    var arrImages = [String]()
    @IBOutlet weak var collection : UICollectionView!
    @IBOutlet weak var page_control : UIPageControl!
    var indexPath : IndexPath?

   
    @IBAction func closeBtnTapped(_ sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collection.alpha = 0
        collection.contentInsetAdjustmentBehavior = .never
        page_control.numberOfPages = arrImages.count
        collection.reloadData()
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2, animations: {
                self.collection.alpha = 1
            }, completion: { _ in
                DispatchQueue.main.async {
                    if let getPath = self.indexPath {
                        self.collection.scrollToItem(at: getPath, at: .centeredHorizontally, animated: false)
                        self.page_control.currentPage = getPath.row
                    }
                }
            })
        }

        page_control.isHidden = arrImages.count <= 1
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == collection {
            page_control.currentPage = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        }
    }
}

extension ImagePreviewVC : UICollectionViewDataSource , UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PreviewCell", for: indexPath) as! PreviewCell
        cell.img_url = arrImages[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
}

class PreviewCell : UICollectionViewCell, UIScrollViewDelegate {
    @IBOutlet weak var scroll : UIScrollView!
    @IBOutlet weak var image_view : UIImageView!
    
    var img_url : String? {
        didSet {
            
            image_view.setImageWithURLWithCompletionBlock(img_url, Placeholderimage: nil) { (image, error) in
                self.scrollViewDidZoom(self.scroll)
            }
            self.scrollViewDidZoom(self.scroll)
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return image_view
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        
        if scrollView.zoomScale > 1 {
            
            if let image = self.image_view.image {
                
                let ratioW = self.image_view.frame.width / image.size.width
                let ratioH = self.image_view.frame.height / image.size.height
                
                let ratio = ratioW < ratioH ? ratioW:ratioH
                
                let newWidth = image.size.width*ratio
                let newHeight = image.size.height*ratio
                
                let left = 0.5 * (newWidth * scrollView.zoomScale > self.image_view.frame.width ? (newWidth - self.image_view.frame.width) : (scrollView.frame.width - scrollView.contentSize.width))
                let top = 0.5 * (newHeight * scrollView.zoomScale > self.image_view.frame.height ? (newHeight - self.image_view.frame.height) : (scrollView.frame.height - scrollView.contentSize.height))
                
                scrollView.contentInset = UIEdgeInsets(top: top, left: left, bottom: top, right: left)
            }
        } else {
            scrollView.contentInset = UIEdgeInsets.zero
        }
    }

}
