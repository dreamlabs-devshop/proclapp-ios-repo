



//
//  NotificationListVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/23/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class NotificationListVC: UIViewController {
    
    @IBOutlet weak var tblview: UITableView!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!

    var isAvailavbleMoreDat = false
    var arrNoti = [NotificationListModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblview.refreshControl = UIRefreshControl()
        self.tblview.refreshControl?.tintColor = .AppSkyBlue
        self.tblview.refreshControl?.addTarget(self, action: #selector(refreshCalled), for: UIControl.Event.valueChanged)
        
        self.refreshCalled()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    @objc func refreshCalled() {
        self.getNotificationList()
    }
    
    @objc func getNotificationList(_ offset : Int = 0) {
        
        var delayTime = DispatchTime.now()
        
        if self.tblview.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblview.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.GetNotificationList, Perameters: ["user_id" : globalUserId,"offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                    debugPrint(dicRes)
                    if let arrPrice = dicRes["notification_list"] as? [[String:Any]] {
                        
                        if offset == 0{
                            self.arrNoti = arrPrice.map({NotificationListModel.init($0)})
                        }
                        else
                        {
                            self.arrNoti.append(contentsOf: arrPrice.map({NotificationListModel.init($0)}))
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.isAvailavbleMoreDat = self.arrNoti.count >= ToInt(dicRes["offset"])
                    UIView.performWithoutAnimation {
                        self.tblview.reloadData()
                    }
                    self.tblview.accessibilityHint = nil
                    self.tblview.refreshControl?.endRefreshing()
                    self.indicator.stopAnimating()
                }
            }) { (err) in
                self.tblview.accessibilityHint = nil
                self.tblview.refreshControl?.endRefreshing()
                self.indicator.stopAnimating()
            }
        }
    }
    
    
    @IBAction func btnAcceptClicked(_ sender: UIButton){
        if let indexPath = tblview.indexPathForView(sender){
            self.handleAccepetSuggestedQuetion(notification_id: toString(arrNoti[indexPath.row].notification_id),status:"1", completion: {
                self.arrNoti.remove(at: indexPath.row)
                self.tblview.deleteRows(at: [indexPath], with: .automatic)
            })
        }
    }
    
    @IBAction func btnRejectClicked(_ sender: UIButton){
        if let indexPath = tblview.indexPathForView(sender){
            self.handleAccepetSuggestedQuetion(notification_id: toString(arrNoti[indexPath.row].notification_id),status:"0", completion: {
                self.arrNoti.remove(at: indexPath.row)
                self.tblview.deleteRows(at: [indexPath], with: .automatic)
            })
        }
    }
    
    func handleAccepetSuggestedQuetion(notification_id: String,status:String, completion:(()->())?)
    {
        var dict = [String: Any]()
        dict["user_id"] = globalUserId
        dict["notification_id"] = notification_id
        dict["status"] = status

        
        WebService.shared.RequesURL(ServerURL.update_post_from_suggestion, Perameters: dict, showProgress: true, completion: { (dictResponse, status) in
            debugPrint(dictResponse)
            if status{
                completion?()
            }else{
                self.showOkAlert(msg: toString(dictResponse.object(forKey: "response_msg")))
            }
        }) { (error) in
            self.showOkAlert(msg: error.localizedDescription)
        }
    }
    

    func postDetails(_ post_id: String, completion:((allPostModel) -> Void)?){
        var dict = [String: Any]()
        dict["user_id"] = globalUserId
        dict["post_id"] = post_id
        WebService.shared.RequesURL(ServerURL.get_post_by_id, Perameters: dict, showProgress: true, completion: { (dictResponse, status) in
            debugPrint("->>>>>",dictResponse)
            if status{
                if let dic = dictResponse["detail"] as? [String:Any] {
                    completion?(allPostModel.init(dic))
                }
            }else
            {
                self.showOkAlert(msg: ToString(dictResponse["response_msg"]))
            }
        }) { (error) in
            self.showOkAlert(msg: error.localizedDescription)
        }
    }
    
}

//MARK: - Table view data source
extension NotificationListVC: UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrNoti.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let obj = arrNoti[indexPath.row]
        let notification_type = obj.notification_type
        
        if notification_type == "13"
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellNotificationListAccept") as! cellNotificationList
            let obj = arrNoti[indexPath.row]
            cell.imgProfile.setImageWithURL(obj.image_thumb, "Big_dp_placeholder")
            cell.lblDes.text = obj.message
            cell.lblQues?.text = obj.post_title
            cell.btnAccept?.addTarget(self, action: #selector(btnAcceptClicked), for: .touchUpInside)
            cell.btnDecline?.addTarget(self, action: #selector(btnRejectClicked), for: .touchUpInside)
            cell.lblTime.text = obj.date.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_HHMMMdd)
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellNotificationList") as! cellNotificationList
            let obj = arrNoti[indexPath.row]
            cell.imgProfile.setImageWithURL(obj.image_thumb, "Big_dp_placeholder")
            cell.lblDes.text = obj.message
            cell.lblTime.text = obj.date.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_HHMMMdd)
            //        cell.btnAccept.isHidden = obj.notification_type != "9"
            //        cell.btnDecline.isHidden = cell.btnAccept.isHidden
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat{
        return 58
    }
    @IBAction func clickProfile(_ sender: UIButton)
    {
        
        guard globalUserId != "" else {
            appDelegate.showAlertGuest()
            return
        }
        
        if let indexPath = self.tblview?.indexPathForView(sender)
        {
            
            let getObje = arrNoti[indexPath.row]
            
            if getObje.user_id == globalUserId
            {
                let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                self.pushTo(vcInstace)
            }
            else{
                let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "OtherProfileVC") as! OtherProfileVC
                vcInstace.profileId = getObje.user_id
                self.pushTo(vcInstace)
                
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = arrNoti[indexPath.row]
        let notification_type = obj.notification_type
        let post_type = obj.postType
        let post_answers_id = obj.post_answers_id
        if notification_type == "1"
        {
            let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "FriendsVC") as! FriendsVC
            vcInstace.isFromNoti = true
            self.pushTo(vcInstace)
        }
        else if notification_type == "2"//accept friend
        {
            let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "OtherProfileVC") as! OtherProfileVC
            vcInstace.profileId = obj.user_id
            self.navigationController?.pushViewController(vcInstace, animated: true)
        }
        
        else if notification_type == "4" || notification_type == "5" || notification_type == "9" ||
            notification_type == "13" || notification_type == "21"  || notification_type == "22" || notification_type == "20" ||  notification_type == "18" ||   notification_type == "19"  || notification_type == "3"
        {
            postDetails(obj.post_id) { (model) in
                
                if post_type == .Article
                {
                    let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "ArticleDetailVC") as! ArticleDetailVC
                    vcInstace.dictPost = model
                    self.pushTo(vcInstace)
                }
                else if post_type == .Question ||  post_type == .ResearchPapers
                {
                    let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerListVC") as! AnswerListVC
                    vcInstace.dictPost = model
                    self.pushTo(vcInstace)
                }
                else if post_type == .VoiceVideo
                {
                    let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "VoiceVideoDetailVC") as! VoiceVideoDetailVC
                    vcInstace.objPost = model
                    self.pushTo(vcInstace)
                }
                else if post_type == .Advert
                {
                    let vcInstace = StoryBoard.Advert.instantiateViewController(withIdentifier: "AdvertDetailsVC") as! AdvertDetailsVC
                    vcInstace.dictPost = model
                    self.pushTo(vcInstace)
                }
            }
        }
        else if notification_type == "23" || notification_type == "6"
        {
            if post_type == .Article  || post_type == .Question ||  post_type == .ResearchPapers || post_type == .Advert
            {
                if let nav = StoryBoard.Home.instantiateViewController(withIdentifier: "AnswerDetailNAV") as? UINavigationController, let vcInstace = nav.viewControllers.first as? AnswerDetailVC
                {
                    vcInstace.post_answers_id = post_answers_id
                    self.present(nav, animated: true, completion: nil)
                }
                
            }
            
            //                else if post_type == .VoiceVideo
            //                {
            //                    let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "VoiceVideoDetailVC") as! VoiceVideoDetailVC
            //                    vcInstace.objPost = model
            //                    self.pushTo(vcInstace)
            //                }
            
        }
        
    }
    
    func scrollViewDidEndDragging(_ aScrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if aScrollView == self.tblview{
            let offset: CGPoint = aScrollView.contentOffset
            let bounds: CGRect = aScrollView.bounds
            let size: CGSize = aScrollView.contentSize
            let inset: UIEdgeInsets = aScrollView.contentInset
            let y = Float(offset.y + bounds.size.height - inset.bottom)
            let h = Float(size.height)
            let reload_distance: Float = 0
            //            print("load more data!!!!!")
            if y > h + reload_distance{
                if self.isAvailavbleMoreDat == true , self.tblview.accessibilityHint == nil{
                    
                    self.indicator.startAnimating()
                    self.tblview.accessibilityHint = "service_calling"
                    getNotificationList(self.arrNoti.count)
                }
            }
        }
    }
}


class cellNotificationList : UITableViewCell
{
    @IBOutlet var imgProfile: UIImageView!

    @IBOutlet var lblDes: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var btnAccept: UIButton?
    @IBOutlet var btnDecline: UIButton?
    @IBOutlet var lblQues: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnAccept?.layer.borderWidth = 1
        btnDecline?.layer.borderWidth = 1
        btnAccept?.layer.cornerRadius = 5
        btnDecline?.layer.cornerRadius = 5
        btnAccept?.layer.borderColor = UIColor.Appcolor96.cgColor
        btnDecline?.layer.borderColor = UIColor.Appcolor96.cgColor
        
        DispatchQueue.main.async {
            self.imgProfile.layer.cornerRadius =  self.imgProfile.frame.height / 2
            self.imgProfile.layer.masksToBounds = true
        }
    }
}

/*class cellNotificationListAccept : UITableViewCell
{
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblQues: UILabel!
    @IBOutlet var lblDes: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var btnAccept: UIButton!
    @IBOutlet var btnDecline: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnAccept.layer.borderWidth = 1
        btnDecline.layer.borderWidth = 1
        btnAccept.layer.cornerRadius = 5
        btnDecline.layer.cornerRadius = 5
        btnAccept.layer.borderColor = UIColor.Appcolor96.cgColor
        btnDecline.layer.borderColor = UIColor.Appcolor96.cgColor
        
        DispatchQueue.main.async {
            self.imgProfile.layer.cornerRadius =  self.imgProfile.frame.height / 2
            self.imgProfile.layer.masksToBounds = true
        }
    }
}*/

class NotificationListModel {
    
    var message =  ""
    var notification_count =  ""
    var notification_id =  ""
    var notification_type =  ""
    var post_title =  ""
    var date =  ""
    var to_user_id =  ""
    var user_id =  ""
//    var image =  ""
    var image_thumb =  ""
    
    var post_id =  ""
    var post_type =  ""
    var post_answers_id = ""
    var is_read =  false
    var status =  false
    
    var postType = PostTypeEnum.Article

    
    init(_ dict : [String:Any]) {
        self.notification_id = toString(dict["notification_id"])
        self.post_title = toString(dict["post_title"])
        self.message = toString(dict["message"])
        self.notification_count = toString(dict["notification_count"])
        self.date = toString(dict["date"])
        self.notification_type = toString(dict["notification_type"])
        self.is_read = toString(dict["is_read"]) == "1"
        self.to_user_id = toString(dict["to_user_id"])
        self.user_id = toString(dict["user_id"])
//        self.image = toString(dict["image"])
        self.image_thumb = toString(dict["image_thumb"])
        self.post_id = toString(dict["post_id"])
        self.post_type = toString(dict["post_type"])
        self.post_answers_id = toString(dict["post_answers_id"])

        if let getType = PostTypeEnum.init(rawValue: self.post_type.integerValue){
            self.postType = getType
        }

    }
}

