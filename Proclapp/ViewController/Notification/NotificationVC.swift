//
//  NotificationVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 5/28/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {
    
    
    
//    static let vcInstace = StoryBoard.Notification.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
    
    var arrStatusList = NSArray()
    
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        getNotificationStatusList()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        self.navigationController?.isNavigationBarHidden = false
    }
    
}
//MARK: - Table view data source
extension NotificationVC: UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrStatusList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellNotification") as! cellNotification
        
        if let dict = arrStatusList[indexPath.row] as? NSDictionary{
            cell.lblTitle.text = toString(dict.object(forKey: "notification_type"))
            cell.btnOnOff.isOn = toString(dict.object(forKey: "status")) == "1"
            
            cell.btnOnOff.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
            cell.btnOnOff.accessibilityHint = "\(indexPath.row)"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
        
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 44
        
    }
    
    @objc func switchChanged(mySwitch: UISwitch) {
        let value = mySwitch.isOn
        
        let indexPath = IndexPath.init(row: Int(mySwitch.accessibilityHint!)!, section: 0)
        if let dict = arrStatusList[indexPath.row] as? NSDictionary{
            
            if mySwitch.isOn{
                updateNotificationStatus("1", notiID: toString(dict.object(forKey: "notification_type_id")))
            }else{
                updateNotificationStatus("0", notiID: toString(dict.object(forKey: "notification_type_id")))
            }
        }
        
        
        debugPrint("Value :-> \(value)\n",mySwitch.accessibilityHint ?? "")
    }
    
    
    func getNotificationStatusList(){
        
        WebService.shared.RequesURL(ServerURL.GetNotificationStatus, Perameters: ["user_id": globalUserId], showProgress: true, completion: { (dictResponse, Status) in
            if Status{
                
                self.arrStatusList = ((dictResponse.object(forKey: "notification_type") as? NSDictionary)?.object(forKey: "notifications") as? NSArray)!
                debugPrint(self.arrStatusList)
                
                self.tblView.reloadData()
            }
        }) { (error) in
            debugPrint(error.localizedDescription)
        }
    }
    
    func updateNotificationStatus(_ status: String, notiID: String){
        
        var dict = [String: Any]()
        dict["user_id"] = globalUserId
        dict["notification_type"] = notiID
        dict["status"] = status
        
        WebService.shared.RequesURL(ServerURL.UpdateNotificationStatus, Perameters: dict, showProgress: false, completion: { (dictResponse, status) in
            if status{
                debugPrint(dictResponse)
                self.getNotificationStatusList()

            }else{
                
            }
        }) { (error) in
            self.showOkAlert(msg: error.localizedDescription)
        }
    }
    
    
}
class cellNotification : UITableViewCell
{
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnOnOff: UISwitch!
}

