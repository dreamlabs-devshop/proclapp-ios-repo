//
//  ChangePassVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 24/07/2019.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class ChangePassVC: UIViewController {

    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtCPassword: UITextField!

    var isComeFromHome = false
    
    
    static var vcInstace = StoryBoard.More.instantiateViewController(withIdentifier: "ChangePassVC") as! ChangePassVC

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickBack(_ sender: Any) {
        if isComeFromHome {
            DispatchQueue.main.async {
                
                self.showOkCancelAlertWithAction(msg: "Are you sure you want to exit?", handler: { (isclickYes) in
                    
                    if isclickYes
                    {
                        resetData()
                       appDelegate.LoadLoginView()
                    }
                    
                })
              
            }
            
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func clickChangePassword(_ sender: Any) {
        
        if validationField{
            webCall_Changepassword()
        }
    }

    
}

extension ChangePassVC
{
    var validationField: Bool{
        self.view.endEditing(true)
        var mess = ""
        let newPassLenth = ToString(txtPassword.text).trim
        if ToString(txtPassword.text).isBlank {
            mess = "Please enter a new password"
        }
        else if newPassLenth.count < 6 ||  newPassLenth.count > 15
        {
            mess = "Please enter a password at least 6 to 15 characters"
        }
        else if ToString(txtCPassword.text).isBlank{
            mess = "Please enter a confirm new password "
        }
        else if ToString(txtPassword.text) != ToString(txtCPassword.text){
            mess = "New password and confirm new password does not match"
        }
        if mess != ""{
            self.showOkAlert(msg: mess)
            return false
        }
        return true
    }
    
    func webCall_Changepassword()
    {
        WebService.shared.RequesURL(ServerURL.ChangePswd, Perameters: ["user_id":globalUserId,"password":toString(txtPassword.text)],showProgress: true, completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            if success == true
            {
                if var dicGetLogin = getUserDefault(Key: Constant.userDeafult_LoginDic) as? [String : Any] {
                    dicGetLogin["screen_code"] = ""
                    loginData?.screen_code = ""
                    setUserDefault(dicGetLogin as AnyObject, Key: Constant.userDeafult_LoginDic)
                }
                
            }
            self.showOkAlertWithHandler(msg: toString(dicRes[Constant.responsemsg]), handler: {
                if success {
                    self.navigationController?.popViewController(animated: true)
                }
            })
        }) { (err) in
            debugPrint(err)
        }
    }
}
