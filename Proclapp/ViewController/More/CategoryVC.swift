//
//  CategoryVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 7/24/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class CategoryVC: UIViewController {
    
    var callback : (([String]) -> Void)?

    
    @IBOutlet weak var collctionView: UICollectionView!
    
    
    var arrCategoryList = [InterestedCategoryListModel]()


    
    override func viewDidLoad() {
        super.viewDidLoad()

        collctionView.register(UINib.init(nibName: "categorySelectCLVCell", bundle: nil), forCellWithReuseIdentifier: "categorySelectCLVCell")

        
        collctionView.contentInset = UIEdgeInsets.init(top: 25, left: 0, bottom: 0, right: 0)
        
        webCall_InterestedCategory()
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func clickSave(_ sender: Any) {
        if self.arrCategoryList.filter({$0.is_selected}).map({$0.category_id}).count == 0{return}
        
        webCall_InterestedCategory_Save()
    }

}

extension CategoryVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCategoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "categorySelectCLVCell", for: indexPath) as! categorySelectCLVCell
        
        let obj = arrCategoryList[indexPath.row]
        cell.lblTitle.text = obj.category_name
        
        cell.btnImage.addTarget(self, action: #selector(tapSelectCategory(_:)), for: .touchUpInside)
        
        cell.btnImage.isSelected = obj.is_selected
        
        if let url = URL.init(string: obj.category_img){
//            cell.btnImage.af_setImage(for: .normal, url: url)
            cell.btnImage.af_setImage(for: .normal, url: url, placeholderImage: UIImage.init(named: "place_logo"))
        }
        if let url = URL.init(string: obj.category_selected_img){
//            cell.btnImage.af_setImage(for: .selected, url: url)
            cell.btnImage.af_setImage(for: .selected, url: url, placeholderImage: UIImage.init(named: "place_logo"))
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.width/3, height: 106 * screenscale)
    }
    
    @IBAction func tapSelectCategory(_ sender: UIButton) {
        
        if let getIndexPath = self.collctionView.indexPathForView_Collection(sender)
        {
            arrCategoryList[getIndexPath.row].is_selected = !arrCategoryList[getIndexPath.row].is_selected
            collctionView.reloadItems(at: [getIndexPath])
            
        }
        
    }
    
    func webCall_InterestedCategory()
    {
        WebService.shared.RequesURL(ServerURL.Interested_Category, Perameters: ["user_id" : globalUserId],showProgress: true,completion: { (dicRes, success) in
            
            debugPrint("\(ServerURL.Interested_Category):-->",dicRes)
            
            if success == true
            {
                if let arrPrice = dicRes["interested_category"] as? [[String:Any]] {
                    self.arrCategoryList = arrPrice.map({InterestedCategoryListModel.init($0)})
                }
                self.collctionView.reloadData()
            }
            
        }) { (err) in
            debugPrint("\(ServerURL.Interested_Category):-->",err)
        }
    }
    
    func webCall_InterestedCategory_Save()
    {
        WebService.shared.RequesURL(ServerURL.Interested_Category_Save, Perameters: ["user_id" : globalUserId,"category":self.arrCategoryList.filter({$0.is_selected}).map({$0.category_id}).joined(separator: ",")],showProgress: true,completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            if success == true
            {
            self.callback?(self.arrCategoryList.filter({$0.is_selected}).map({$0.category_name}))
                self.clickBack(self)
                /*if let arrPrice = dicRes["interested_category"] as? [[String:Any]] {
                    self.arrCategoryList = arrPrice.map({InterestedCategoryListModel.init($0)})
                }
                self.collctionView.reloadData()*/
            }
            
        }) { (err) in
            debugPrint("\(ServerURL.Interested_Category):-->",err)
        }
    }
    
}
