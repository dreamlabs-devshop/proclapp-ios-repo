//
//  GeneralWebVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 24/07/2019.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import WebKit

class GeneralWebVC: UIViewController {

//    static let vcInstace = StoryBoard.More.instantiateViewController(withIdentifier: "GeneralWebVC") as! GeneralWebVC

    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var textView: UITextView!

    var key = ""
    var strTitle = ""
 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLbl.text = strTitle

        webCall()
    }
    
    
    func webCall(){
        
        var url = ""
        if key == "1"{
            url = ServerURL.aboutUS
        }
        else if key == "2"{
            url = ServerURL.termsCondition
            
        }
        else if key == "3"{
            url = ServerURL.privacyPolicy
        }
        
        WebService.shared.RequesURL(url, Perameters: nil,showProgress: true, completion: { (dicRes, success) in
            debugPrint(dicRes)
            if success == true{
                
                if self.key == "1"{
                    self.textView.attributedText = toString(dicRes["about_us"]).htmlToAttributedString
                }
                else if  self.key == "2"{
                    self.textView.attributedText = toString(dicRes["term_and_conditions"]).htmlToAttributedString

                }
                else if  self.key == "3"{
                    self.textView.attributedText = toString(dicRes["privacy_policy"]).htmlToAttributedString
                }

            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.isNavigationBarHidden = false
    }
    
     @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
     }
  
}
