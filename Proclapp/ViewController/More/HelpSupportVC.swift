//
//  HelpSupportVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 24/07/2019.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class HelpSupportVC: UIViewController {

    static let vcInstace = StoryBoard.More.instantiateViewController(withIdentifier: "HelpSupportVC") as! HelpSupportVC

    var arrRow = [String]()
    var arrKey = [String]()

    


    @IBOutlet var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.register(UINib.init(nibName: "MoreTblCell", bundle: nil), forCellReuseIdentifier: "MoreTblCell")
        
        arrRow.append("About App")
        arrRow.append("Terms & Conditions")
        arrRow.append("Privacy Policy")
        
        arrKey.append("1")
        arrKey.append("2")
        arrKey.append("3")


        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.isNavigationBarHidden = false
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension HelpSupportVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRow.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoreTblCell") as! MoreTblCell
        cell.lblTitle.text = arrRow[indexPath.row]
        cell.bottomLline.isHidden = true
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         let vc = StoryBoard.More.instantiateViewController(withIdentifier: "GeneralWebVC") as! GeneralWebVC
        vc.strTitle = arrRow[indexPath.row]
        vc.key = arrKey[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
