//
//  DeclinedQueAndArticleVC.swift
//  Proclapp
//
//  Created by iMac-4 on 6/14/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class DeclinedQueAndArticleVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    

    //MARK:- Properties
//    var arrQuestions = [QuestionList]()
    
    var arrDeclineQuestion = [DeclinePostModel]()


    //MARK:- IBOutlets
    @IBOutlet weak var btnUndo: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var shadowUndo: UIView!
    @IBOutlet weak var shadowDelete: UIView!
    @IBOutlet weak var tblQuestionList: UITableView!
    

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var isMore = false

    //MARK:- View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ButtonHideShow()
        
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.Appcolor51,NSAttributedString.Key.font: UIFont.FontLatoRegular(size: 15)]

        tblQuestionList.estimatedRowHeight = 100
        tblQuestionList.rowHeight = UITableView.automaticDimension
        
        DispatchQueue.main.async {
            self.shadowUndo.applyDropShadow(color: UIColor.AppShadowBlack, alpha: 0.5, y: 0, blur: 5)
            self.shadowDelete.applyDropShadow(color: UIColor.AppShadowBlack, alpha: 0.5, y: 0, blur: 5)
            
            self.indicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.tblQuestionList.bounds.width, height: CGFloat(30))
            self.tblQuestionList.tableFooterView = self.indicator
        }
        
       /* let queDetail = QuestionDetail.init("Joseph Robert", "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, lipsum as it is sometimes known lipsum as it is sometimes known lipsum as it is sometimes known lipsum as it is sometimes knowngraphic or web designs.")
        
        let queDetail2 = QuestionDetail.init("test test", "is dummy text used in laying out print, graphic or web designs.")
        
        let que1 = QuestionList.init("Article", date: "june 18 2018", QueDetail: [queDetail])
        let que2 = QuestionList.init("Question", date: "june 20 2018", QueDetail: [queDetail])
        let que3 = QuestionList.init("Question", date: "june 20 2018", QueDetail: [queDetail2])
        let que4 = QuestionList.init("Question", date: "june 20 2018", QueDetail: [queDetail])
        
        
        arrQuestions.append(que1)
        arrQuestions.append(que2)
        arrQuestions.append(que3)
        arrQuestions.append(que4)
        
        tblQuestionList.reloadData()*/
        
        self.tblQuestionList.refreshControl = UIRefreshControl()
        self.tblQuestionList.refreshControl?.tintColor = .AppSkyBlue
        self.tblQuestionList.refreshControl?.addTarget(self, action: #selector(refreshCalled), for: UIControl.Event.valueChanged)
        self.getDeclineQuestionList()

        

    }
    
    func ButtonHideShow() {
        self.shadowUndo.isHidden = self.arrDeclineQuestion.count == 0
        self.shadowDelete.isHidden = self.arrDeclineQuestion.count == 0
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    @objc func refreshCalled() {
        
        self.getDeclineQuestionList()
    }
    
/*@objc func getDeclineQuestionList() {
        
        
        var delayTime = DispatchTime.now()
        
        if self.tblQuestionList.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblQuestionList.refreshControl?.beginRefreshingManually()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.GetDeclinePostList, Perameters: ["user_id" : globalUserId],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                    
                   debugPrint(dicRes)
                    if let arrData = dicRes["post_list"] as? [[String:Any]] {
                        self.arrDeclineQuestion = arrData.map({DeclinePostModel.init($0)})
                    }
                    
                }
                
                self.ButtonHideShow()
                
                DispatchQueue.main.async {
                    UIView.performWithoutAnimation {
                        self.tblQuestionList.reloadData()
                    }
                    self.tblQuestionList.accessibilityHint = nil
                    self.tblQuestionList.refreshControl?.endRefreshing()
                }
                
                
            }) { (err) in
                self.tblQuestionList.accessibilityHint = nil
                self.tblQuestionList.refreshControl?.endRefreshing()
            }
        }
        
    }*/
    
    @objc func getDeclineQuestionList(_ offset : Int = 0) {
        
        var delayTime = DispatchTime.now()
        
        if self.tblQuestionList.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tblQuestionList.refreshControl?.beginRefreshingManually()
        }
        
        debugPrint("Offset= \(offset)")
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.GetDeclinePostList, Perameters: ["user_id" : globalUserId,"offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
//                                        debugPrint(dicRes)
                    
                    if let arrData = dicRes["post_list"] as? [[String:Any]]
                    {
                        if offset == 0{
                            self.arrDeclineQuestion = arrData.map({DeclinePostModel.init($0)})
                        }
                        else
                        {
                            self.arrDeclineQuestion.append(contentsOf: arrData.map({DeclinePostModel.init($0)}))
                        }
                    }
                }
                self.ButtonHideShow()

                DispatchQueue.main.async {
                    

                    
                    self.isMore = self.arrDeclineQuestion.count == 0 ? false : self.arrDeclineQuestion.count >= ToInt(dicRes["offset"])
                    
//                    debugPrint("isAvailableMoreData= \(self.isMore)")
                    
//                    debugPrint("arr= \(self.arrDeclineQuestion.count)")
                    
//                    debugPrint("toint= \(ToInt(dicRes["offset"]))")
                    
                    
                    UIView.performWithoutAnimation {
                        self.tblQuestionList.reloadData()
                    }
                    self.tblQuestionList.accessibilityHint = nil
                    self.tblQuestionList.refreshControl?.endRefreshing()
                    self.indicator.stopAnimating()
                }
            }) { (err) in
                self.tblQuestionList.accessibilityHint = nil
                self.tblQuestionList.refreshControl?.endRefreshing()
                self.indicator.stopAnimating()
            }
        }
    }
    
    
    func webDecline_DeletePost(_ postId:String,_ serviceName:String ,  callback: (()->())?)
    {
        if postId.isEmpty { return }
        
        WebService.shared.RequesURL(serviceName, Perameters: ["user_id" : globalUserId, "post_id":postId],showProgress: true, completion: { (dicRes, success) in
            
            debugPrint(dicRes)
            
            if success == true
            {
                callback?()
            }
            else{
                self.showOkAlert(msg: toString(dicRes[Constant.responsemsg]))
            }
        }) { (err) in
            debugPrint(err)
        }
    }
    
    
    
    //MARK:- IBActions
    @IBAction func btnBackClicked(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnExpandClicked(_ sender: UIButton){
//        print("tapped_section= \(sender.tag)")
        arrDeclineQuestion[sender.tag].isopen = !arrDeclineQuestion[sender.tag].isopen
        tblQuestionList.reloadSections(IndexSet.init(integer: sender.tag), with: .automatic)
    }
    
    @IBAction func tapUndoAll(_ sender: UIButton){
        
        self.showOkCancelAlertWithAction(msg: Constant.kAlertDeclineUndoALL) { (success) in
            if success{
                self.webDecline_DeletePost(self.arrDeclineQuestion.map({$0.post_id}).joined(separator: ","),ServerURL.UndoDeclinePost) {
                    self.arrDeclineQuestion.removeAll()
                    self.tblQuestionList.reloadData()
                    
                    self.ButtonHideShow()
                }
            }
        };
    }
    
    @IBAction func tapUndoPost(_ sender: UIButton) {
        
        self.showOkCancelAlertWithAction(msg: Constant.kAlertDeclineUndo) { (success) in
            if success{
                self.webDecline_DeletePost(sender.accessibilityHint ?? "",ServerURL.UndoDeclinePost) {
                    self.arrDeclineQuestion.remove(at:ToInt(sender.accessibilityValue))
                    self.tblQuestionList.reloadData()
                    
                    self.ButtonHideShow()
                    
                }
            }
        };
    }
    
    
    
    @IBAction func tapDeleteAll(_ sender: UIButton){
        self.showOkCancelAlertWithAction(msg: Constant.kAlertDeclineDeleteALL) { (success) in
            if success{
                self.webDecline_DeletePost(self.arrDeclineQuestion.map({$0.post_id}).joined(separator: ","),ServerURL.DeleteDeclinePost) {
                    self.arrDeclineQuestion.removeAll()
                    self.tblQuestionList.reloadData()
                    self.ButtonHideShow()
                }
            }
        };
    }
    @IBAction func tapDeletePost(_ sender: UIButton) {
        
        self.showOkCancelAlertWithAction(msg: Constant.kAlertDeclineDelete) { (success) in
            if success{
                
                self.webDecline_DeletePost(sender.accessibilityHint ?? "",ServerURL.DeleteDeclinePost) {
                    self.arrDeclineQuestion.remove(at:ToInt(sender.accessibilityValue))
                    self.tblQuestionList.reloadData()
                    
                    self.ButtonHideShow()
                    
                }
            }
        };
    }
    
    
    
     //MARK:- UITableview Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrDeclineQuestion.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return (46 * screenscale)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "SectionCell") as! SectionCell
        
        header.model = arrDeclineQuestion[section]
        header.btnExpand.tag = section
        
        header.downupImage.image = arrDeclineQuestion[header.btnExpand.tag].isopen == true ? UIImage.init(named: "up") : UIImage.init(named: "down")
        
        header.btnExpand.addTarget(self, action: #selector(btnExpandClicked(_:)), for: .touchUpInside)
        
        header.btnUndo.addTarget(self, action: #selector(tapUndoPost(_:)), for: .touchUpInside)
        header.btnUndo.accessibilityHint = arrDeclineQuestion[section].post_id
        header.btnUndo.accessibilityValue = "\(section)"

        
        header.btnDelete.addTarget(self, action: #selector(tapDeletePost(_:)), for: .touchUpInside)
        header.btnDelete.accessibilityHint = arrDeclineQuestion[section].post_id
        header.btnDelete.accessibilityValue = "\(section)"


        return header.contentView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDeclineQuestion[section].isopen == false ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch self.arrDeclineQuestion[indexPath.section].postType {
        case .Article:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeclineArticleCell") as! DeclineArticleCell
            cell.model = arrDeclineQuestion[indexPath.section]
            return cell
        case .Question:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeclineQuestionCell") as! DeclineQuestionCell
            cell.model = arrDeclineQuestion[indexPath.section]
            return cell

        default:
            return UITableViewCell()
        }
//        case .VoiceVideo:
//            <#code#>
//        case .ResearchPapers:
//            <#code#>
        //return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func scrollViewDidEndDragging(_ aScrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        let offset: CGPoint = aScrollView.contentOffset
        let bounds: CGRect = aScrollView.bounds
        let size: CGSize = aScrollView.contentSize
        let inset: UIEdgeInsets = aScrollView.contentInset
        let y = Float(offset.y + bounds.size.height - inset.bottom)
        let h = Float(size.height)
        let reload_distance: Float = 0
        //            print("load more data!!!!!")
        if y > h + reload_distance{
            
            if self.isMore == true , self.tblQuestionList.accessibilityHint == nil{
                
                self.indicator.startAnimating()
                self.tblQuestionList.accessibilityHint = "service_calling"
                self.getDeclineQuestionList(self.arrDeclineQuestion.count)
            }
        }
    }
}

//MARK:- UITableViewCell
class SectionCell: UITableViewCell{
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblType: UILabel!
    @IBOutlet var btnExpand: UIButton!
    @IBOutlet var btnUndo: UIButton!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var downupImage: UIImageView!
    
    
    var model : DeclinePostModel? {
        didSet {
            lblDate.text = model?.post_date.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_MMMddyyyy)
            lblType.text = model?.post_type == "2" ? "Question" : "Article"
            
        }
    }
    
    

}

class DeclineQuestionCell: UITableViewCell{
    
    @IBOutlet weak var imgProfileWidthConst: NSLayoutConstraint!
    @IBOutlet weak var imgProfileHeightConst: NSLayoutConstraint!
    
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var btnDot: UIButton!
    
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblProfession: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
   

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            self.imgProfileWidthConst.constant = 30 * screenscale
            self.imgProfileHeightConst.constant = 30 * screenscale
            self.imgProfile.layer.cornerRadius =  self.imgProfileWidthConst.constant / 2
        }
        // Initialization code
    }
    var model : DeclinePostModel? {
        didSet {
            lblUserName.text = model?.author
            imgProfile.setImageWithURL(model?.profile_image_thumb, "Big_dp_placeholder")
            lblDate.text = model?.post_date.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_ddMMM)
            lblTitle.text = model?.post_title
            lblProfession.text = model?.professional_qualification
            lblProfession.text = model?.professional_qualification

        }
    }
}


class DeclineArticleCell: UITableViewCell {
    
    @IBOutlet weak var imgProfileWidthConst: NSLayoutConstraint!
    @IBOutlet weak var imgProfileHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var imgArticleWidthConst: NSLayoutConstraint!
    @IBOutlet weak var imgArticleHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgArticle: UIImageView!
    
    @IBOutlet weak var btnDot: UIButton!
    @IBOutlet weak var btnMultiImage: UIButton!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblProfession: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDes: UILabel!
    @IBOutlet weak var lblImageCount: UILabel!

    
    @IBOutlet weak var viewAlpha: UIView!
    
    
    let sizeArticleImage = 75 * screenscale
    let sizeProfileImage = 30 * screenscale
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //        DispatchQueue.main.async {
        
        self.imgProfileWidthConst.constant = self.sizeProfileImage
        self.imgProfileHeightConst.constant = self.sizeProfileImage
        
        self.imgArticleWidthConst.constant = self.sizeArticleImage
        self.imgArticleHeightConst.constant = self.sizeArticleImage
        
        
        self.imgProfile.layer.cornerRadius =  self.imgProfileWidthConst.constant / 2
        //        }
        // Initialization code
    }
    
    var model : DeclinePostModel? {
        didSet {
            
            imgArticle.setImageWithURL(model?.arrImages.first?.image_name, "place_logo")
            imgProfile.setImageWithURL(model?.profile_image_thumb, "Big_dp_placeholder")
            
            lblUserName.text = model?.author
            lblDate.text = model?.post_date.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_ddMMM)
            lblTitle.text = model?.post_title
            lblDes.text = model?.post_description
            lblProfession.text = model?.professional_qualification
            
            let imgesCount = ToInt((model?.arrImages.count))
            self.imgArticleWidthConst.constant = imgesCount >= 1 ? sizeArticleImage : 0


            imgArticle.isHidden = imgesCount == 0
            viewAlpha.alpha = imgesCount > 1 ? 0.4 : 0.0
            lblImageCount.text = imgesCount > 1 ? "+ \(imgesCount - 1)" : ""
            
            btnMultiImage.addTarget(self, action: #selector(tapMultiImage), for: .touchUpInside)
            
        }
    }
    
    @IBAction func tapMultiImage(_ sender: UIButton) {
        
        if let get = model?.arrImages, get.count > 0
        {
            let vcInstace = StoryBoard.Home.instantiateViewController(withIdentifier: "imageGalleryVC") as! imageGalleryVC
            vcInstace.getarrImages = get
            self.tableView?.parentViewController?.navigationController?.pushViewController(vcInstace, animated: true)
            
        }
        
    }
}


//MARK:- Model Class
/*class QuestionList{
    
    var date = ""
    var title = ""
    var isopen = false
    var question_detail = [QuestionDetail]()
    
    init(_ Title : String , date : String , QueDetail : [QuestionDetail] = []) {
        self.title = Title
        self.date = date
        self.question_detail = QueDetail
    }
}

class QuestionDetail{
    var name = ""
    var description = ""
    var date = false
    
    init(_ name : String, _ des : String) {
        self.name = name
        self.description = des
    }
}*/


//MARK:- UIView Extension
extension UIView{
    
    func applyDropShadow(
        color: UIColor = .black,
        alpha: Float = 0.5,
        x: CGFloat = 0,
        y: CGFloat = 2,
        blur: CGFloat = 4,
        spread: CGFloat = 0,
        radius: CGFloat = 0,
        borderWidth: CGFloat = 0)
    {
        self.backgroundColor = .white

        self.layer.borderWidth = borderWidth
        self.layer.borderColor = color.cgColor
        self.layer.cornerRadius = radius
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = alpha
        self.layer.shadowOffset = CGSize(width: x, height: y)
        self.layer.shadowRadius = blur / 2.0
        if spread == 0 {
            self.layer.shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            self.layer.shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
}



class DeclinePostModel {
    
    var user_id =  ""
    var profile_image =  ""
    var profile_image_thumb =  ""
    var additional_info =  ""
    var admin_id =  ""
    var author =  ""
    var author_id =  ""
    var categories_id =  ""
    var category_name =  ""
    var category_status =  ""
    var cover_image =  ""
    var expert_id =  ""
    
//    var post_comment_count =  ""
    var post_date =  ""
    var declined_post_date =  ""

    
    var post_description =  ""
//    var post_flashed_count =  ""
    var post_id =  ""
//    var post_like_count =  0
    var post_title =  ""
    
    var post_type =  ""
    var postType = PostTypeEnum.Article
    
    var professional_qualification =  ""
    
    var type =  ""
    var updated_date =  ""
    
//    var audioVideoType = audioVideoEnum.audio
//    var vv_type =  ""
    
    //Bool
//    var is_shared =  false
//    var status =  false
//    var is_following =  false
//    var is_post_flashed =  false
//    var is_post_liked =  false
    
    var arrImages = [imagePostModel]()
//    var dicPDF : PDFPostModel?
//    var dicAudio : audioPostModel?
//    var dicVideo : videoPostModel?
    
    //for only QA
//    var post_share_count =  ""
    
    //For Decline
    var isopen = false
    
    
    
    init(_ dict : [String:Any]) {
        
        self.user_id =  toString(dict["user_id"])
        self.profile_image = toString(dict["profile_image"])
        self.profile_image_thumb = toString(dict["profile_image_thumb"])
        self.additional_info = toString(dict["additional_info"])
        
        self.admin_id = toString(dict["admin_id"])
        self.author = toString(dict["author"])
        self.author_id = toString(dict["author_id"])
        self.expert_id = toString(dict["expert_id"])
        
        self.categories_id = toString(dict["categories_id"])
        self.category_name = toString(dict["category_name"])
        self.category_status = toString(dict["category_status"])
        self.cover_image = toString(dict["cover_image"])
        
        //noti count
        //self.post_comment_count = toString(dict["post_comment_count"])
       // self.post_like_count = ToInt(dict["post_like_count"])
       // self.post_share_count = toString(dict["post_share_count"])
      //  self.post_flashed_count = toString(dict["post_flashed_count"])
        
        self.post_date = toString(dict["post_date"])
        self.declined_post_date = toString(dict["declined_post_date"])
        
        self.post_description = toString(dict["post_description"]).htmlToString
        self.post_id = toString(dict["post_id"])
        self.post_title = toString(dict["post_title"])
        self.post_type = toString(dict["post_type"])
        if let getType = PostTypeEnum.init(rawValue: self.post_type.integerValue){
            self.postType = getType
        }
        self.professional_qualification = toString(dict["professional_qualification"])
        
        self.type = toString(dict["type"])
        self.updated_date = toString(dict["updated_date"])
        
       /*self.vv_type = toString(dict["vv_type"])
        if let getType = audioVideoEnum.init(rawValue: self.vv_type.integerValue){
            self.audioVideoType = getType
        }
        //bool
        self.is_shared = ConvertToBool(toString(dict["is_shared"]))
        self.status = ConvertToBool(toString(dict["status"]))
        self.is_following = ConvertToBool(toString(dict["is_following"]))
        self.is_post_flashed = ConvertToBool(toString(dict["is_post_flashed"]))
        self.is_post_liked = ConvertToBool(toString(dict["is_post_liked"]))*/
        
        self.isopen = false
        
        
        //arr
        //For Interested Category
        if let arr = dict["images"] as? [[String:Any]] {
            self.arrImages = arr.map({imagePostModel.init($0)})
        }
        
       /* //For PDF
        if let dic = dict["files"] as? [String:Any] {
            self.dicPDF = PDFPostModel.init(dic)
        }
        
        //For audio
        if let dic = dict["audios"] as? [String:Any] {
            self.dicAudio = audioPostModel.init(dic)
        }
        
        //For video
        if let dic = dict["videos"] as? [String:Any] {
            self.dicVideo = videoPostModel.init(dic)
        }*/
        
    }
}
