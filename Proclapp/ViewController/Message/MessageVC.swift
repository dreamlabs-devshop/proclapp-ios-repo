//
//  MessageVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 5/28/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit

class MessageVC: UIViewController {


    var arrMsgConverList = [FriendsModel]()

    var isFirstTime = true
    
    @IBOutlet var tblMessage: UITableView!

    @IBOutlet var viewEmptyMessage: UIView!

    var btnRight : UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.viewEmptyMessage.isHidden = true

        self.tblMessage.refreshControl = UIRefreshControl()
        self.tblMessage.refreshControl?.tintColor = .AppSkyBlue
        self.tblMessage.refreshControl?.addTarget(self, action: #selector(refreshCalled), for: UIControl.Event.valueChanged)
       /* DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
        }*/
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.Appcolor51 ?? UIColor.black,NSAttributedString.Key.font: UIFont.FontLatoRegular(size: 15)]
        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        
        globalChatUserId = ""

        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        
        self.getConvertionList()
    }
    
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
        if let gesture = navigationController?.interactivePopGestureRecognizer
        {
            view.addGestureRecognizer(gesture)
        }
    }
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    @IBAction func btnFriends(_ sender: UIButton) {
        let vcInstace = StoryBoard.Other.instantiateViewController(withIdentifier: "FriendsVC") as! FriendsVC
        self.pushTo(vcInstace)
    }

}

//MARK: - Table view data source
extension MessageVC: UITableViewDataSource,UITableViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrMsgConverList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellChatMessageList") as! cellChatMessageList
        
        let obj = arrMsgConverList[indexPath.row]
      cell.imgProfile.setImageWithURL(obj.profile_image,"Big_dp_placeholder")
        
        cell.imgProfile.setupImageViewer(options: [.theme(.dark)])

        cell.lblUserName.text = obj.name
        cell.lblMsg.text = obj.message
        cell.lblMsgCount.text = toString(obj.unread_msg)
        cell.lblMsgCount.isHidden = cell.lblMsgCount.text == "0"
        cell.lblTime.text = obj.sent_at.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_HHMMMddyy)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 65 * screenscale
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vcInstace = StoryBoard.Message.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        vcInstace.touserId = arrMsgConverList[indexPath.row].to_user_id
        vcInstace.profileURL = arrMsgConverList[indexPath.row].profile_image
        vcInstace.name = arrMsgConverList[indexPath.row].name
//        if let nav = appDelegate.window?.rootViewController as? UINavigationController {
//            nav.pushViewController(vcInstace, animated: true)
//        } else {
//            self.pushTo(vcInstace)
//        }
        self.pushTo(vcInstace)
 
    }
   
}
class cellChatMessageList : UITableViewCell
{
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblMsg: UILabel!
    @IBOutlet var lblMsgCount: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var imgProfile: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            
            self.lblMsgCount.layer.cornerRadius = self.lblMsgCount.frame.height/2
            self.lblMsgCount.layer.masksToBounds = true
            
            self.imgProfile.layer.cornerRadius = self.imgProfile.frame.height/2
            self.imgProfile.layer.masksToBounds = true
            
        }
    }
}

/*class objSearchUser {
    
    //All Search user list
    var actual_username:String = ""
    var email:String = ""
    var id:String = ""
    var profile_pic:String = ""
    var userName:String = ""
    
    init(_ dict : [String:Any]) {
        
        actual_username =  toString(dict["actual_username"])
        email =  toString(dict["email"])
        id =  toString(dict["id"])
        profile_pic =  toString(dict["profile_pic"])
        userName =  toString(dict["userName"])
    }
    

}*/

class objSearchUser: NSObject {
    
    //All Search user list
    var actual_username:String = ""
    var email:String = ""
    var id:String = ""
    var profile_pic:String = ""
    var userName:String = ""
    
    
    class func getAllsearchUser(result:NSArray) -> [objSearchUser] {
        var arrList = [objSearchUser]()
        for obj in result {
            if let dic = obj as? NSDictionary {
                let objUser = objSearchUser()
                
                objUser.actual_username =  String(format: "%@", dic.object(forKey: "actual_username") as? CVarArg ?? "")
                objUser.email =  String(format: "%@", dic.object(forKey: "email") as? CVarArg ?? "")
                objUser.profile_pic =  String(format: "%@", dic.object(forKey: "profile_pic") as? CVarArg ?? "")
                objUser.userName =  String(format: "%@", dic.object(forKey: "userName") as? CVarArg ?? "")
                objUser.id =  String(format: "%@", dic.object(forKey: "id") as? CVarArg ?? "")
                arrList.append(objUser)
            }
        }
        return arrList
    }
    //    override init(_ dic : NSDictionary) {
    //        self.actual_username =  String(format: "%@", dic.object(forKey: "actual_username") as? CVarArg ?? "")
    //        self.email =  String(format: "%@", dic.object(forKey: "email") as? CVarArg ?? "")
    //        self.profile_pic =  String(format: "%@", dic.object(forKey: "profile_pic") as? CVarArg ?? "")
    //        self.userName =  String(format: "%@", dic.object(forKey: "userName") as? CVarArg ?? "")
    //        self.id =  String(format: "%@", dic.object(forKey: "id") as? CVarArg ?? "")
    //    }
    
    class func getFromJson(_ dic : NSDictionary) -> objSearchUser
    {
        let objUser = objSearchUser()
        
        objUser.actual_username =  String(format: "%@", dic.object(forKey: "actual_username") as? CVarArg ?? "")
        objUser.email =  String(format: "%@", dic.object(forKey: "email") as? CVarArg ?? "")
        objUser.profile_pic =  String(format: "%@", dic.object(forKey: "profile_pic") as? CVarArg ?? "")
        objUser.userName =  String(format: "%@", dic.object(forKey: "userName") as? CVarArg ?? "")
        objUser.id =  String(format: "%@", dic.object(forKey: "id") as? CVarArg ?? "")
        return objUser
    }
    
}

   /* class func getFromJson(_ dic : NSDictionary) -> objSearchUser
    {
        let objUser = objSearchUser()
        
        objUser.actual_username =  String(format: "%@", dic.object(forKey: "actual_username") as? CVarArg ?? "")
        objUser.email =  String(format: "%@", dic.object(forKey: "email") as? CVarArg ?? "")
        objUser.profile_pic =  String(format: "%@", dic.object(forKey: "profile_pic") as? CVarArg ?? "")
        objUser.userName =  String(format: "%@", dic.object(forKey: "userName") as? CVarArg ?? "")
        objUser.id =  String(format: "%@", dic.object(forKey: "id") as? CVarArg ?? "")
        return objUser
    }*/
    
//MARK: - Api Call

extension MessageVC
{
    @objc func refreshCalled() {
        self.isFirstTime = true
        self.getConvertionList()
    }
    @objc func getConvertionList(_ offset : Int = 0) {
        
        var delayTime = DispatchTime.now()
        
        if isFirstTime
        {
            self.isFirstTime = false
            
            if self.tblMessage.refreshControl?.isRefreshing == false
            {
                delayTime = DispatchTime.now() + 0.5
                self.tblMessage.refreshControl?.beginRefreshingManually()
            }
        }
        
       
        
//        debugPrint("Offset= \(offset)")
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.GetConversationList, Perameters: ["user_id" : globalUserId],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                    debugPrint(dicRes)
                    
                    if let arrData = dicRes["conversation_list"] as? [[String:Any]] {
                         self.arrMsgConverList = arrData.map({FriendsModel.init($0)})
                       /* if offset == 0{
                            self.arrVoiceVideo = arrData.map({allPostModel.init($0)})
                        }
                        else{
                            self.arrVoiceVideo.append(contentsOf: arrData.map({allPostModel.init($0)}))
                        }*/
                    }
                    
//                    self.isMore = self.arrVoiceVideo.count >= ToInt(dicRes["offset"])
                    
                    
                }
                else{
//                    self.isMore = false
                    self.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
                }
                
                self.viewEmptyMessage.isHidden = self.arrMsgConverList.count != 0

                
                DispatchQueue.main.async {
                    
//                    debugPrint("isAvailableMoreData= \(self.isMore)")
                    
//                    debugPrint("arr= \(self.arrVoiceVideo.count)")
                    
//                    debugPrint("toint= \(ToInt(dicRes["offset"]))")
                    
                    
                    UIView.performWithoutAnimation {
                        self.tblMessage.reloadData()
                    }
                    self.tblMessage.accessibilityHint = nil
                    self.tblMessage.refreshControl?.endRefreshing()
//                    self.indicator.stopAnimating()
                }
            }) { (err) in
                self.tblMessage.accessibilityHint = nil
                self.tblMessage.refreshControl?.endRefreshing()
//                self.indicator.stopAnimating()
            }
        }
    }
}


