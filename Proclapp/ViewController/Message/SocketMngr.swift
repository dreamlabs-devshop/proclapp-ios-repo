import Foundation
import SocketIO

let socketConnectionURL =  ServerURL.socketURL

let connected = "connected"
let disconnected = "disconnected"
let userOnline = "online"
let userOffline = "offline"
let getOnlineUsers = "online_users"
let MsgSendIdentifier = "send_msg"
let MsgReceiveIdentifier = "new_msg"
let GetOldMessageIdentifier = "_old_messages"
let requestForOldMessages = "old_messages"

let StartTyping = "typing_start"
let OtherUserStartedTyping = "_typing_start"
let StopTyping = "typing_stop"
let OtherUserStopedTyping = "_typing_stop"
let SendSimpleMessage = "send_data"
let ReceiveSimpleMessage = "_data"
let SendMsgDeliveredStatus = "msg_delivered"
let ReceivedMsgDeliveredStatus = "_msg_delivered"
let SendMsgSeenStatus = "msg_seen"
let ReceivedMsgSeenStatus = "_msg_seen"
let UpdateAppBackgroundForgroundStatus = "update_last_seen"
let GetOtherUserAppBackGroundForgorundStatus = "_last_seen"
let GetOtherUserLastSeen = "get_last_seen"
let CreateGroup = "create_group"
let NewGroupCreated = "_new_group"
let requestForAllGroups = "my_groups"
let getAllGroups = "_my_groups"
let deleteSelectedMsg = "delete_msg"
let MsgDeletedReceived = "_msg_deleted"
let removeUserFromGorup = "remove_user"
let RemovedFromGroup = "_user_removed"

let MsgSeenAll = "msg_seen_all"
let MsgSeenSingle = "msg_seen"


var arrOnlineUsers = [String]()


var manager = SocketManager(socketURL: URL(string: socketConnectionURL)!, config: [.log(true), .connectParams(["user_id": globalUserId])])

var socket = manager.defaultSocket
//var socket : SocketIOClient!

var isSocketConnected = false
var glbChatMsgDelegate : SocketChatDelegate?

enum SocketState : Int
{
    case connected = 0
    case disconnected
    case error
}


//let socketStateChanged = NSNotification.Name.init("socketStateChanged")

func connectSocket()
{
    manager = SocketManager(socketURL: URL(string: socketConnectionURL)!, config: [.log(true), .connectParams(["user_id": globalUserId])])
    socket = manager.defaultSocket

    socket.connect()
    
    socket.on(connected) { (data, ack) in
        isSocketConnected = true
        NotificationCenter.default.post(name: .socketStateChanged, object: SocketState.connected)
        debugPrint("\nconnected To Socket  >>>>> \(data)")
    }
    socket.on(disconnected) { (data, ack) in
        isSocketConnected = false
        NotificationCenter.default.post(name: .socketStateChanged, object: SocketState.disconnected)

//        NotificationCenter.default.post(name: .userOnlineOffline, object: nil, userInfo: nil)
        debugPrint("\ndisconnected From Socket  >>>>> \(data)")
    }
    socket.on(clientEvent: SocketClientEvent.error) { (data, ack) in
        isSocketConnected = false
        NotificationCenter.default.post(name: .socketStateChanged, object: SocketState.error)

        debugPrint("\nSocketClientEvent.error  >>>>> \(data)")
    }
    
    socket.on(clientEvent: SocketClientEvent.reconnect) { (data, ack) in
        isSocketConnected = true
        NotificationCenter.default.post(name: .socketStateChanged, object: SocketState.connected)


//        NotificationCenter.default.post(name: .userOnlineOffline, object: nil, userInfo: nil)
    }
    socket.on(getOnlineUsers) { (data, ack) in
        
        if let dictGetID = (data as? [NSDictionary])?.first , let user_ids =  dictGetID["user_id"] as? [String]
        {
            for online_user_id in user_ids
            {
                if arrOnlineUsers.contains(online_user_id) == false
                {
                    arrOnlineUsers.append(online_user_id)
                }
                glbChatMsgDelegate?.userOnlineOffline(online_user_id, date: "", isOnline: true)
//                NotificationCenter.default.post(name: .userOnlineOffline, object: nil, userInfo: ["user_id" : online_user_id])
            }
        }
    }
    
    socket.on(GetOldMessageIdentifier) { (data, ack) in
            if let dict = data.first as? NSDictionary , let getArray = dict["_old_messages"] as? [NSDictionary]
            {
                glbChatMsgDelegate?.didReceiveAllMessage(getArray)
            }
//        debugPrint("MsgReceiveIdentifier >>>>>> \(data)")
    }

    socket.on(MsgReceiveIdentifier) { (data, ack) in
        if let dicMsg = data.first as? NSDictionary
        {
            glbChatMsgDelegate?.didReceiveMessage(FriendsModel.init(dicMsg as! [String : Any]))
//          glbChatMsgDelegate?.didReceiveMessage(ChatMessage.getFromJSON(dicMsg))
        }
//        debugPrint("ReceivedMsgSeenStatus >>>>>> \(data)")
    }
    
    socket.on(ReceiveSimpleMessage) { (data, ack) in
        if let dictData = data.first as? NSDictionary
        {
            if (dictData["type"] as? String ?? "") == "block_unblock"
            {
                glbChatMsgDelegate?.UserBlockedYou(dictData)
            }
        }
    }

//    socket.on(OtherUserStartedTyping) { (data, ack) in
//        if let dictData = data.first as? NSDictionary, ToString(dictData["user_id"]) != glbUserID
//        {
//            glbChatMsgDelegate?.otherUserTypingMessage(ToString(dictData["user_id"]), get_group_id: ToString(dictData["group_id"]), isTyping: true)
////            NotificationCenter.default.post(name: .userStartedTyping, object: nil, userInfo: dictData)
//        }
//    }

//    socket.on(OtherUserStopedTyping) { (data, ack) in
//        if let dictData = data.first as? NSDictionary, ToString(dictData["user_id"]) != glbUserID
//        {
//            glbChatMsgDelegate?.otherUserTypingMessage(ToString(dictData["user_id"]), get_group_id: ToString(dictData["group_id"]), isTyping: false)
////            NotificationCenter.default.post(name: .userStopedTyping, object: nil, userInfo: dictData)
//        }
//    }
    
        }

func disconnectSocket()
{
    socket.disconnect()
    manager.disconnect()
//    manager.removeSocket(socket)
}

protocol SocketChatDelegate
{
    func sendMessageToOtherUser(_ ChatMsg : FriendsModel)
    func didReceiveMessage(_ ChatMsg : FriendsModel)
//    func senderMessageReceived(_ messageId : Int64 , server_date : Date?)
    func senderMessageReceived(_ dictMsg : NSDictionary)
    func UserBlockedYou(_ dictMsg : NSDictionary)
    func otherUserTypingMessage(_ user_id : String , get_group_id : String , isTyping : Bool)
    func userOnlineOffline(_ user_id : String , date : String , isOnline : Bool)
    func didReceiveAllMessage(_ arrChatMsg : [NSDictionary])



}

extension SocketChatDelegate
{
    func sendMessageToOtherUser(_ ChatMsg : FriendsModel) { }
    func didReceiveMessage(_ ChatMsg : FriendsModel) { }
//    func senderMessageReceived(_ messageId : Int64 , server_date : Date?) { }
    func senderMessageReceived(_ dictMsg : NSDictionary) { }
    func otherUserTypingMessage(_ user_id : String , get_group_id : String , isTyping : Bool) { }
    func userOnlineOffline(_ user_id : String , date : String , isOnline : Bool) { }
    func didReceiveAllMessage(_ arrChatMsg : [NSDictionary]) {}
    
    func UserBlockedYou(_ dictMsg : NSDictionary){}

}


