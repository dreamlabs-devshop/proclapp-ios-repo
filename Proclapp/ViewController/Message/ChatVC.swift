//
//  ChatVC.swift
//  Proclapp
//
//  Created by Ashish Parmar on 5/28/19.
//  Copyright © 2019 Ashish Parmar. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

var isDisplayEmoji = false

class ChatVC: UIViewController,UITextViewDelegate {



    @IBOutlet var tableview: UITableView!

    @IBOutlet var viewBorder: UIView!
    
    @IBOutlet var txtViewMsg: UITextView!
    
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblName: UILabel!

    
    @IBOutlet var viewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var constantTextHeight: NSLayoutConstraint!
    
    
//    var dictData : FriendsModel?

    
//    var obj : objSearchUser?
    
    var i_hve_blocked = false
    var is_blocked_me = false
    
//    var arrChatMsg = [ChatMessage]()
    
    let typeMsg = "Type here..."

    var touserId = ""
    var profileURL = ""
    var name = ""

    var arrMsgConverList = [FriendsModel]()
    var isMore = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        globalChatUserId = touserId

        
//        isFromChat = true
        
        //        if let obj = dictData
        //        {
//        self.touserId = obj.to_user_id
        self.imgProfile.setImageWithURL(profileURL,"Big_dp_placeholder")
        self.lblName.text = name.capitalized
        //        }
        
        txtViewMsg.delegate = self
        txtViewMsg.text = typeMsg
        
        // *** Hide keyboard when tapping outside ***
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler))
        tableview.addGestureRecognizer(tapGesture)
        
        viewBorder.layer.cornerRadius = viewBorder.frame.height/2
        
        DispatchQueue.main.async {
            self.imgProfile.layer.cornerRadius = self.imgProfile.frame.height/2
            self.imgProfile.layer.masksToBounds = true
        }
        
        self.tableview.refreshControl = UIRefreshControl()
        self.tableview.refreshControl?.tintColor = .AppSkyBlue
        self.tableview.refreshControl?.addTarget(self, action: #selector(refreshCalled), for: UIControl.Event.valueChanged)
        self.getOldMsgList()
        
        SeenAllMsg()
        
        let tapImage: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(clickProfile(_:)))
        tapImage.numberOfTapsRequired = 1
        imgProfile.addGestureRecognizer(tapImage)
        imgProfile.isUserInteractionEnabled = true

        
        // Do any additional setup after loading the view.
    }
    
    
    
    @objc func tapGestureHandler() {
        view.endEditing(true)
    }
    //MARK:Textview delegate
    func textViewDidChange(_ textView: UITextView) {
        
        if textView.contentSize.height <= 100{
            constantTextHeight.constant = textView.contentSize.height
        }
        else
        {
            constantTextHeight.constant = 100
        }
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if  textView.text == typeMsg
        {
            textView.text = ""
        }
        debugPrint("1 \(isDisplayEmoji)")
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if  textView.text == ""
        {
            textView.text = typeMsg
        }
        
        debugPrint("2 \(isDisplayEmoji)")
        
        isDisplayEmoji = false
        txtViewMsg.reloadInputViews()

        debugPrint("3 \(isDisplayEmoji)")

    }
    
   
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
       
        glbChatMsgDelegate = self

        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.enable = false
        navigationController?.navigationBar.barTintColor = UIColor.white

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)

        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
       

      
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
        
        glbChatMsgDelegate = nil
        isDisplayEmoji = false


        NotificationCenter.default.removeObserver(self)
        
//        isFromChat = false

    }

    //MARK: KeyBoard Method
    @objc private func keyboardWillChange(_ notification: Notification) {
        guard let userInfo = (notification as Notification).userInfo, let value = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        let newHeight: CGFloat
        if #available(iOS 11.0, *) {
            newHeight = value.cgRectValue.height - BottomPadding
        } else {
            newHeight = value.cgRectValue.height
        }
       self.viewBottomConstraint.constant = newHeight
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
            self.tableview.ScrollToBottom(animated: true, ScrollPosition: .bottom)
        }
     
    }
    
    
    @objc func keyboardWillHide(notification:NSNotification)
    {
        self.viewBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
            //            self.tableview.scrollToBottom(animated: true)
        }
    }
    
    
    @IBAction func clickBack(_ sender: Any) {
        
        globalChatUserId = ""

        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
        if let gesture = navigationController?.interactivePopGestureRecognizer
        {
            view.addGestureRecognizer(gesture)
        }
    }
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    //MARK: - Profile vieww

    @IBAction func clickProfile(_ sender: UIButton)
    {
        let vcInstace = StoryBoard.Profile.instantiateViewController(withIdentifier: "OtherProfileVC") as! OtherProfileVC
        vcInstace.profileId = touserId
        self.pushTo(vcInstace)
        
    }
    @IBAction func clickEmoji(_ sender: UIButton)
    {
        isDisplayEmoji = true
//        txtViewMsg.resignFirstResponder()
        txtViewMsg.reloadInputViews()
        txtViewMsg.becomeFirstResponder()
    }

}
//MARK: - Table view data source
extension ChatVC: UITableViewDataSource,UITableViewDelegate
{
        func numberOfSections(in tableView: UITableView) -> Int
        {
            return 1
        }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrMsgConverList.count
    }
    
    /*func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if indexPath.row % 2 == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sendMessageCell") as! sendMessageCell
            
            
            cell.txtviewMessage.text = arrChatMsg[indexPath.row]
            

          
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "receiveMessageCell") as! receiveMessageCell
            
            cell.txtviewMessage.text = arrChatMsg[indexPath.row]


            cell.selectionStyle = .none
            return cell
        }
    }*/
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let objMsg = arrMsgConverList[indexPath.row]
        
        if objMsg.isSender
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sendMessageCell") as! sendMessageCell
            
            cell.txtviewMessage.text = objMsg.message
            cell.lblTime.text = objMsg.sent_at.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_HHMMMddyy)

            
            /*let date = dateConvert_Global(date: objMsg.sent_at, ToFormate: "yyyy-MM-dd HH:mm:ss", GetFormate: "MM-dd-yy")
            let time = dateConvert_Global(date: objMsg.sent_at, ToFormate: "yyyy-MM-dd HH:mm:ss", GetFormate: "hh:mm a")
            if TodayDate == date{
                cell.lblTime.text = time
            }
            else{
                cell.lblTime.text = "\(date)"
            }*/
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "receiveMessageCell") as! receiveMessageCell
            
            cell.txtviewMessage.text = objMsg.message
            cell.lblTime.text = objMsg.sent_at.dateConvertUTC(serverFormate: Constant.date_ServerFormate, appFormate: Constant.date_HHMMMddyy)

            
           /* let date = dateConvert_Global(date: objMsg.sent_at, ToFormate: "yyyy-MM-dd HH:mm:ss", GetFormate: "MM-dd-yy")
            let time = dateConvert_Global(date: objMsg.sent_at, ToFormate: "yyyy-MM-dd HH:mm:ss", GetFormate: "hh:mm a")
            if TodayDate == date{
                cell.lblTime.text = time
            }
            else{
                cell.lblTime.text = "\(date)"
            }*/
            
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
        
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 31
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableviewHeaderCell") as! tableviewHeaderCell
        
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.white
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0//35
    }
}

class tableviewHeaderCell: UITableViewCell
{
    @IBOutlet var lblTime: UILabel!
}


class receiveMessageCell: UITableViewCell
{
    @IBOutlet var imageCorner: UIImageView!
    
    
    @IBOutlet var lblTime: UILabel!
    
    @IBOutlet var viewShadows: UIView!
    
    @IBOutlet var txtviewMessage: UITextView!
    
    override func awakeFromNib() {
        
        txtviewMessage.font = UIFont.FontLatoMedium(size: 13 * screenscale)

        txtviewMessage.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        txtviewMessage.textContainer.lineFragmentPadding = 0
        
       // viewShadows.addShadowWithRadius(radius:2, cornerRadius: 5)
        
        guard let image = UIImage(named: "bubble_received") else { return }
        imageCorner.image = image
            .resizableImage(withCapInsets:
                UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21),
                            resizingMode: .stretch)
            .withRenderingMode(.alwaysTemplate)
        imageCorner.tintColor = UIColor(named: "chat_bubble_color_received")
    }
}

class sendMessageCell: UITableViewCell
{
    @IBOutlet var imageCorner: UIImageView!
    @IBOutlet var txtviewMessage: UITextView!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var viewShadows: UIView!
    override func awakeFromNib() {
        
        txtviewMessage.font = UIFont.FontLatoMedium(size: 13 * screenscale)

        txtviewMessage.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        txtviewMessage.textContainer.lineFragmentPadding = 0
        
        //viewShadows.addShadowSend(radius:2, cornerRadius: 5)
        
        guard let image = UIImage(named: "bubble_send") else { return }
        imageCorner.image = image
            .resizableImage(withCapInsets:
                UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21),
                            resizingMode: .stretch)
            .withRenderingMode(.alwaysTemplate)
        imageCorner.tintColor = UIColor(named: "chat_bubble_color_sent")
    }
    
}
//MARK: - API CALL

extension ChatVC
{
    @IBAction func clickSend(_ sender: Any)
    {
        if self.CheckIsUserBlocked(){
            // U Blocked this Person or This person had Blocked you
            return
        }
        
        if isSocketConnected == true
        {
            let trimmed = (txtViewMsg.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
            
            if (trimmed.isEmpty == false) && txtViewMsg.text != typeMsg{
                socket.emit(MsgSendIdentifier, with: [["message":trimmed,"msg_type":"text","to_user_id":self.touserId
                    ]])
                txtViewMsg.text = ""
                self.constantTextHeight.constant = 36.5
            }
        }
        else {
            socket.connect()
        }
    }
    
    func CheckIsUserBlocked()-> Bool
    {
        return false
        
        /* var isBlockedThisUser = false
         if self.i_hve_blocked
         {
         isBlockedThisUser = true
         self.showAlertForBlockUser(false, ShowPopUpAtMessageSendTime: true)
         }
         else if self.is_blocked_me
         {
         isBlockedThisUser = true
         DispatchQueue.main.async {
         
         let Alert = UIAlertController(title: Constant.appName, message: "Sorry you can't send message to \(self.lblUserName.text ?? ""), because \(self.lblUserName.text ?? "") had Blocked you." , preferredStyle: .alert)
         
         Alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
         
         self.present(Alert, animated: true, completion: nil)
         }
         }
         
         return isBlockedThisUser*/
    }
    
    
    
    func showAlertForBlockUser(_ isBlock : Bool ,ShowPopUpAtMessageSendTime : Bool = false)
    {
        self.view.endEditing(true)
        
        /*DispatchQueue.main.async {
         
         let strIdenti = isBlock ? "Block" : "Unblock"
         
         let AlertyMessage = ShowPopUpAtMessageSendTime ? "Unblock \(self.lblUserName.text ?? "") to send message!" : "Are you sure you want to \(strIdenti) \(self.lblUserName.text ?? "")?"
         
         let imageOption = UIAlertController(title: Constant.appName, message: AlertyMessage, preferredStyle: .alert)
         
         imageOption.addAction(UIAlertAction(title: strIdenti, style: .destructive, handler:{ _ in
         DispatchQueue.main.async {
         self.apiBlock(self.i_hve_blocked ? "unblock" : "block")
         }
         }))
         
         imageOption.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
         
         self.present(imageOption, animated: true, completion: nil)
         }*/
    }
    
    func oldMsg()
    {
        socket.emit(requestForOldMessages, with:[["offset":"0","limit":"50","to_user_id":self.touserId
            ]])
    }
    @objc func refresh(_ sender: AnyObject)
    {
        /* if isOldMsgAvailable
         {
         offset = arrChatMsg.count
         
         //            let msgCount = arrChatMsg.count
         //
         //            if msgCount == 0
         //            {
         //                offset = 0
         //            }
         //            else
         //            {
         //                offset = offset + MsgLimit
         //            }
         
         self.apigetOldMsgService()
         }
         else
         {
         refreshControl.endRefreshing()
         }*/
    }
    func apigetOldMsgService()
    {
        /*   var chatMSg : ChatMessage?
         let param = [Constant.kUSER_ID : getUserDefault(Constant.kUSER_ID),
         Constant.kMETHOD       : "old_messages",
         "to_user_id":obj?.id ?? "",
         "offset":"\(offset)",
         "limit":"\(MsgLimit)"
         ]
         SGServiceController.instance().hitPostService(param, unReachable: {
         self.isOldMsgAvailable = true
         self.refreshControl.endRefreshing()
         self.showOkAlert("Please check your network connection")
         }) { (response) in
         //            debugPrint(response)
         if response == nil{
         self.refreshControl.endRefreshing()
         return
         }else{
         if response![Constant.kERROR_CODE] as! String == "0"
         {
         if let dicData = response![Constant.kDATA] as? [NSDictionary]
         {
         if self.offset == 0
         {
         self.arrChatMsg.removeAll()
         self.arrChatMsg.append(contentsOf: ChatMessage.getFromJsonResponse(dicData))
         }
         else
         {
         chatMSg = self.arrChatMsg.first
         self.arrChatMsg = ChatMessage.getFromJsonResponse(dicData) + self.arrChatMsg
         }
         }
         if let dicUserDetail = response!["to_user_data"] as? NSDictionary
         {
         self.is_blocked_me =  String(format: "%@", dicUserDetail.object(forKey: "is_blocked_me") as? CVarArg ?? "") == "1"
         self.i_hve_blocked =  String(format: "%@", dicUserDetail.object(forKey: "i_hve_blocked") as? CVarArg ?? "") == "1"
         }
         
         self.isOldMsgAvailable = self.arrChatMsg.count >= (self.MsgLimit + self.offset)
         
         self.tableview.reloadData()
         
         if self.offset == 0
         {
         self.tableview.ScrollToBottom(animated: false, ScrollPosition: .bottom)
         }
         else
         {
         if let find_ind = self.arrChatMsg.firstIndex(where: {$0.chat_id == chatMSg?.chat_id}) , find_ind > 1
         {
         self.tableview.scrollToRow(at: IndexPath.init(row: find_ind - 1, section: 0), at: .top, animated: false)
         }
         }
         self.refreshControl.endRefreshing()
         
         }
         else if response![Constant.kERROR_CODE] as! String == "2"
         {
         self.isOldMsgAvailable = false
         }
         
         SwiftLoader.hide()
         self.refreshControl.endRefreshing()
         }
         }*/
    }
    
    
    /*  @IBAction func tap_Report(_ sender: Any) {
     
     let alert = UIAlertController(title: lblUserName.text ?? "" , message: nil, preferredStyle: .actionSheet)
     
     alert.addAction(UIAlertAction(title: "Report user", style: .destructive, handler: { (alert) in
     DispatchQueue.main.async {
     self.apiReport()
     }
     }))
     alert.addAction(UIAlertAction(title: self.i_hve_blocked ? "Unblock user" : "Block user", style: .destructive, handler: { (alert) in
     DispatchQueue.main.async {
     self.apiBlock(self.i_hve_blocked ? "unblock" : "block")
     }
     }))
     alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
     present(alert, animated: true)
     }
     func apiReport()
     {
     let param = ["user_id" : getUserDefault(Constant.kUSER_ID),
     Constant.kMETHOD       : "report_user",
     "reported_user_id":obj?.id ?? ""
     ]
     SGServiceController.instance().hitPostService(param, unReachable: {
     self.showOkAlert("Please check your network connection")
     }) { (response) in
     if response == nil{
     return
     }else{
     
     //                debugPrint(response)
     
     if response![Constant.kERROR_CODE] as! String == "0"
     {
     
     }
     else if response![Constant.kERROR_CODE] as! String == "2"
     {
     
     }
     self.showOkAlert(response![Constant.kERROR_MSG] as? String ?? "")
     
     }
     }
     }
     func apiBlock(_ is_block_unblock:String)
     {
     let param = ["user_id" : getUserDefault(Constant.kUSER_ID),
     Constant.kMETHOD       : "block_user",
     "blocked_user_id":obj?.id ?? "",
     "is_block_unblock":is_block_unblock
     ]
     SGServiceController.instance().hitPostService(param, unReachable: {
     self.showOkAlert("Please check your network connection")
     }) { (response) in
     if response == nil{
     return
     }else{
     
     debugPrint(response)
     
     if response![Constant.kERROR_CODE] as! String == "0"
     {
     self.i_hve_blocked = is_block_unblock == "block"
     
     socket.emit(SendSimpleMessage, with: [["user_id" : self.obj?.id ?? "", "from_user_id" : getUserDefault(Constant.kUSER_ID) , "type" : "block_unblock" , "status" : self.i_hve_blocked ? "1": "0"]])
     }
     else if response![Constant.kERROR_CODE] as! String == "2"
     {
     
     }
     self.showOkAlert(response![Constant.kERROR_MSG] as? String ?? "")
     
     }
     }
     }*/
    

    @objc func refreshCalled()
    {
        if self.isMore == true , self.tableview.accessibilityHint == nil
        {
            self.tableview.accessibilityHint = "service_calling"
            self.getOldMsgList(self.arrMsgConverList.count)
        }
        else
        {
            self.tableview.refreshControl?.endRefreshing()
        }

    }
    @objc func getOldMsgList(_ offset : Int = 0) {
        
        var chatMSg : FriendsModel?
        
        var delayTime = DispatchTime.now()
        
        if self.tableview.refreshControl?.isRefreshing == false
        {
            delayTime = DispatchTime.now() + 0.5
            self.tableview.refreshControl?.beginRefreshingManually()
        }
        
        //        debugPrint("Offset= \(offset)")
        
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            WebService.shared.RequesURL(ServerURL.GetMsgList, Perameters: ["user_id" : globalUserId,"to_user_id":self.touserId,"offset":offset],showProgress: false,completion: { (dicRes, success) in
                if success == true{
                    debugPrint(dicRes)
                    
                    if let arrData = dicRes["conversation_msgs"] as? [[String:Any]] {
                         if offset == 0{
                            self.arrMsgConverList = arrData.map({FriendsModel.init($0)}).reversed()
                         }
                         else{
                            let arr = arrData.map({FriendsModel.init($0)}).reversed()
                            self.arrMsgConverList = arr + self.arrMsgConverList
//                            self.arrMsgConverList.append(contentsOf:arr)
                        }
                    }
                    
                
                    
                    self.isMore = self.arrMsgConverList.count >= ToInt(dicRes["offset"])
                    
                    
                }
                else{
                    //                    self.isMore = false
                    self.showOkAlert(msg: toString(dicRes.object(forKey: "response_msg")))
                }
                
                DispatchQueue.main.async {
                    
                                        debugPrint("isAvailableMoreData= \(self.isMore)")
                    
                                        debugPrint("arr= \(self.arrMsgConverList.count)")
                    
                    //                    debugPrint("toint= \(ToInt(dicRes["offset"]))")
                    
                    
                    UIView.performWithoutAnimation {
                        self.tableview.reloadData()
                    }
                    self.tableview.accessibilityHint = nil
                    self.tableview.refreshControl?.endRefreshing()
                    //                    self.indicator.stopAnimating()
                }
                
                if offset == 0
                {
                    self.tableview.ScrollToBottom(animated: false, ScrollPosition: .bottom)
                }
                else
                {
                    if let find_ind = self.arrMsgConverList.firstIndex(where: {$0.chat_id == chatMSg?.chat_id}) , find_ind > 1
                    {
                        self.tableview.scrollToRow(at: IndexPath.init(row: find_ind - 1, section: 0), at: .top, animated: false)
                    }
                }
              
                
            }) { (err) in
                self.tableview.accessibilityHint = nil
                self.tableview.refreshControl?.endRefreshing()
                //                self.indicator.stopAnimating()
            }
        }
    }
    
    func SeenAllMsg()
    {
        if self.CheckIsUserBlocked(){
            // U Blocked this Person or This person had Blocked you
            return
        }
        
        if isSocketConnected == true{
        
            socket.emit(MsgSeenAll, with: [["user_id":globalUserId,"to_user_id":self.touserId
                ]])
        }
        else {
            socket.connect()
        }
    }
    func SeenSingleMsg(_ ChatId:String)
    {
            socket.emit(MsgSeenSingle, with: [["chat_id":ChatId
                ]])
    }
    
}
extension ChatVC : SocketChatDelegate
{
    func didReceiveAllMessage(_ arrChatMsg : [NSDictionary])
    {
        /*SwiftLoader.hide()
        
        for dictMsg in arrChatMsg.reversed()
        {
            self.arrChatMsg.append(ChatMessage.getFromJSON(dictMsg))
        }
        self.tableview.reloadData()
        
        self.tableview.ScrollToBottom(animated: false, ScrollPosition: .bottom)*/
        
    }
    func sendMessageToOtherUser(_ ChatMsg : FriendsModel)
    {
        
    }
    func didReceiveMessage(_ ChatMsg : FriendsModel)
    {
        if ChatMsg.to_user_id == touserId || ChatMsg.user_id == touserId
        {
            self.arrMsgConverList.append(ChatMsg)
            self.tableview.reloadData()
            self.tableview.ScrollToBottom(animated: false, ScrollPosition: .bottom)
            
            self.SeenSingleMsg(ChatMsg.chat_id)
        }
        
    }
    func UserBlockedYou(_ dictMsg: NSDictionary) {
        
        /*if dictMsg["from_user_id"] as? String ?? "" == obj?.id
        {
            self.is_blocked_me =  (dictMsg["status"] as? String ?? "") == "1"
        }*/
    }
    
}
class EmojiTextView: UITextView {
    
    // required for iOS 13
    override var textInputContextIdentifier: String? { "" } // return non-nil to show the Emoji keyboard ¯\_(ツ)_/¯
    
    override var textInputMode: UITextInputMode? {
        if isDisplayEmoji{
            debugPrint("emji@@@@@@@@@@@@@@@@@@@@@@@@")
            for mode in UITextInputMode.activeInputModes {
                if mode.primaryLanguage == "emoji" {
                    return mode
                }
            }
        }
        else
        {
                for mode in UITextInputMode.activeInputModes {
                if mode.primaryLanguage == "en-US" {
                    return mode
                }
            }
        }
        debugPrint("no!!!!!!!!!!!!!!!!!!!!!")
        return nil
    }
}
